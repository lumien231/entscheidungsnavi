export default {
  projects: [
    '<rootDir>/apps/decision-tool',
    '<rootDir>/libs/widgets',
    '<rootDir>/libs/decision-data',
    '<rootDir>/libs/tools',
    '<rootDir>/apps/backend',
    '<rootDir>/libs/api-types',
    '<rootDir>/apps/cockpit',
    '<rootDir>/libs/api-client',
    '<rootDir>/apps/klug-tool',
  ],
};
