import { CommonModule } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { MatLegacyButtonModule } from '@angular/material/legacy-button';
import { MatIconModule } from '@angular/material/icon';
import { MatLegacyProgressSpinnerModule } from '@angular/material/legacy-progress-spinner';
import {
  MAT_LEGACY_SNACK_BAR_DATA as MAT_SNACK_BAR_DATA,
  MatLegacySnackBarModule,
  MatLegacySnackBarRef as MatSnackBarRef,
} from '@angular/material/legacy-snack-bar';

export interface SnackbarData {
  message: string;
  icon?: string;
  dismissButton?: boolean;
  loadingSpinner?: boolean;
}

@Component({
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss'],
  standalone: true,
  imports: [CommonModule, MatLegacySnackBarModule, MatLegacyProgressSpinnerModule, MatIconModule, MatLegacyButtonModule],
})
export class SnackbarComponent {
  constructor(public snackBarRef: MatSnackBarRef<SnackbarComponent>, @Inject(MAT_SNACK_BAR_DATA) public data: SnackbarData) {}

  dismissClick() {
    this.snackBarRef.dismiss();
  }
}
