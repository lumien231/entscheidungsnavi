import { Injectable, NgZone, OnDestroy } from '@angular/core';

type KeyPressDirection = null | 'down' | 'up';

class KeyBind {
  constructor(
    public key: string | null,
    public ctrlKey: boolean | null,
    public shiftKey: boolean | null,
    public callback: (event?: KeyboardEvent) => void,
    public callbackOnBlur: boolean,
    public direction: KeyPressDirection,
    public onRepeat: boolean,
    public conditions: Array<(event: KeyboardEvent) => boolean>
  ) {}
}

/**
 * Utility Class to simplify Key Bindings, example in ObjectiveAspectHierarchyComponent
 */
@Injectable({ providedIn: null })
export class KeyBindHandlerService implements OnDestroy {
  private bindings: KeyBind[];

  private keyDownHandler: (event: KeyboardEvent) => void;
  private keyUpHandler: (event: KeyboardEvent) => void;
  private blurHandler: () => void;

  private lastKeyboardEvent: KeyboardEvent | null = null;

  constructor(private zone: NgZone) {
    this.bindings = [];

    zone.runOutsideAngular(() => {
      window.addEventListener('keydown', (this.keyDownHandler = event => this.handleKeyPress(event, 'down')));
      window.addEventListener('keyup', (this.keyUpHandler = event => this.handleKeyPress(event, 'up')));
      window.addEventListener('blur', (this.blurHandler = () => this.handleBlur()));
    });
  }

  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      window.removeEventListener('keydown', this.keyDownHandler);
      window.removeEventListener('keyup', this.keyUpHandler);
      window.removeEventListener('blur', this.blurHandler);
    });
  }

  register({
    callback,
    key = null,
    ctrlKey = null,
    shiftKey = null,
    direction = 'down',
    callbackOnBlur = false,
    onRepeat = true,
    conditions = [],
  }: {
    callback: (event?: KeyboardEvent) => void;
    key?: string;
    ctrlKey?: boolean;
    shiftKey?: boolean;
    direction?: KeyPressDirection;
    callbackOnBlur?: boolean;
    onRepeat?: boolean;
    conditions?: Array<(event: KeyboardEvent) => boolean>;
  }): void {
    this.bindings.push(new KeyBind(key, ctrlKey, shiftKey, callback, callbackOnBlur, direction, onRepeat, conditions));
  }

  // ignore direction
  doesKeyBindMatch(keyBind: KeyBind, event: KeyboardEvent) {
    return (
      (keyBind.key === null || keyBind.key === event.key) &&
      (keyBind.ctrlKey == null || keyBind.ctrlKey === event.ctrlKey) &&
      (keyBind.shiftKey == null || keyBind.shiftKey === event.shiftKey) &&
      keyBind.conditions.every(c => c(event))
    );
  }

  handleKeyPress(event: KeyboardEvent, direction: KeyPressDirection) {
    const matchingBindings = this.bindings.filter((keyBind: KeyBind) => {
      if (event.repeat && !keyBind.onRepeat) return false;

      const matchesLastEvent = () => (this.lastKeyboardEvent ? this.doesKeyBindMatch(keyBind, this.lastKeyboardEvent) : false);
      const matchesCurrentEvent = () => this.doesKeyBindMatch(keyBind, event);

      if (keyBind.direction === null) {
        return matchesLastEvent() || matchesCurrentEvent();
      } else if (direction === 'down' && keyBind.direction === 'down') {
        return matchesCurrentEvent();
      } else if (direction === 'up' && keyBind.direction === 'up') {
        return matchesLastEvent();
      } else {
        return false;
      }
    });

    if (matchingBindings.length > 0) {
      this.zone.run(() => {
        matchingBindings.forEach((triggeredKeyBind: KeyBind) => {
          triggeredKeyBind.callback(event);
        });
      });
    }

    this.lastKeyboardEvent = event;
  }

  handleBlur() {
    this.bindings.filter(binding => binding.callbackOnBlur).forEach(keyUpBinding => this.zone.run(() => keyUpBinding.callback()));
  }
}
