import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';

/*
 Usage:
 <dt-button-tiny (click)="foo()" [icon]="'delete'"></dt-button-tiny>
 */

@Component({
  selector: 'dt-button-tiny',
  styleUrls: ['./button-tiny.component.scss'],
  template: `
    <button [attr.type]="type" [style]="'--hover-color:' + invertColor" [disabled]="disabled" [class.disabled]="disabled">
      <mat-icon>{{ icon }}</mat-icon>
    </button>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [MatIconModule],
})
export class ButtonTinyComponent {
  @Input() icon: string;
  @Input() disabled = false;
  @Input() type: 'submit' | 'reset' | 'button' = 'button';
  @Input() invertColor = '#f8931e'; // orange default
}
