import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatLegacyButtonModule as MatButtonModule } from '@angular/material/legacy-button';
import { MatIconModule } from '@angular/material/icon';
import { ButtonComponent, ButtonTinyComponent } from '.';

@NgModule({
  imports: [CommonModule, MatButtonModule, MatIconModule, ButtonTinyComponent],
  declarations: [ButtonComponent],
  exports: [ButtonComponent, ButtonTinyComponent],
})
export class ButtonModule {}
