import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'dt-collapse-all',
  templateUrl: './collapse-all.component.html',
  styleUrls: ['./collapse-all.component.scss'],
})
export class CollapseAllComponent {
  @Output() expandAll = new EventEmitter<void>();
  @Output() collapseAll = new EventEmitter<void>();
  @Input() allExpanded: boolean;
  @Input() allCollapsed: boolean;

  expandAllClick() {
    this.expandAll.emit();
  }

  collapseAllClick() {
    this.collapseAll.emit();
  }
}
