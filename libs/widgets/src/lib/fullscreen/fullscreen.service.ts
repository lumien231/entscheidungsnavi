import { Overlay } from '@angular/cdk/overlay';
import { DomPortal } from '@angular/cdk/portal';
import { ElementRef, EventEmitter, Injectable } from '@angular/core';

export type FullscreenRef = { onClose: EventEmitter<void>; close: () => void };

@Injectable({ providedIn: 'root' })
export class FullscreenService {
  constructor(private overlay: Overlay) {}

  fullscreenElement(element: ElementRef): FullscreenRef {
    const ref = this.overlay.create({
      height: '100vh',
      width: '100vw',
      panelClass: 'dt-fullscreen-panel',
      backdropClass: 'dummy',
      hasBackdrop: true,
    });

    ref.keydownEvents().subscribe(event => {
      if (event.key === 'Escape') {
        ref.dispose();

        event.preventDefault();
        event.stopPropagation();

        closeEmitter.emit();
      }
    });

    ref.backdropClick().subscribe(event => {
      ref.dispose();

      event.preventDefault();
      event.stopPropagation();

      closeEmitter.emit();
    });

    const domPortal = new DomPortal(element);
    ref.attach(domPortal);

    const closeEmitter = new EventEmitter<void>();

    return {
      onClose: closeEmitter,
      close: () => {
        ref.dispose();
        closeEmitter.emit();
      },
    };
  }
}
