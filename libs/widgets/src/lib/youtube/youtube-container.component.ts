import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'dt-youtube-container',
  templateUrl: './youtube-container.component.html',
  styleUrls: ['./youtube-container.component.scss'],
})
export class YoutubeContainerComponent {
  @Input() videoId: string;
  @Output() closeModal = new EventEmitter<void>();
  @Input() consent: boolean;
}
