import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'dt-breadcrumb-options',
  templateUrl: './breadcrumb-options.component.html',
  styleUrls: ['./breadcrumb-options.component.scss'],
})
export class BreadcrumbOptionsComponent implements OnInit {
  @Input() heading: string;
  @Input() options: string[];
  @Input() optionsDisabled: boolean[];
  @Input() selectedIndex = 0;

  @Output() selectedIndexChange = new EventEmitter<number>();

  ngOnInit() {
    if (this.optionsDisabled == null || this.optionsDisabled.length !== this.options.length) {
      this.optionsDisabled = new Array(this.options.length).fill(false);
    }
  }

  clickOption(idx: number) {
    if (this.optionsDisabled[idx] || this.selectedIndex === idx) {
      // disabled or already selected
      return;
    }
    this.selectedIndex = idx;
    this.selectedIndexChange.emit(this.selectedIndex);
  }
}
