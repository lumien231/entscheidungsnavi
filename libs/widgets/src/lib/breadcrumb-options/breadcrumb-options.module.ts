import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbOptionsComponent } from './breadcrumb-options.component';

@NgModule({
  imports: [CommonModule],
  declarations: [BreadcrumbOptionsComponent],
  exports: [BreadcrumbOptionsComponent],
})
export class BreadcrumbOptionsModule {}
