import { AfterViewInit, ChangeDetectorRef, Directive, ElementRef, Input } from '@angular/core';
import { BooleanInput, coerceBooleanProperty } from '@angular/cdk/coercion';

@Directive({
  selector: '[dtAutoFocus]',
})
export class AutoFocusDirective implements AfterViewInit {
  private _enabled = true;
  @Input('dtAutoFocus') set enabled(value: BooleanInput) {
    this._enabled = coerceBooleanProperty(value);
  }

  constructor(private el: ElementRef, private cdRef: ChangeDetectorRef) {}

  focus(attempt = 0) {
    if (this.el) {
      this.el.nativeElement.focus();
      this.cdRef.detectChanges();
    } else if (attempt > 20) {
      throw new Error("couldn't find element");
    } else {
      setTimeout(() => this.focus(attempt + 1), 50);
    }
  }

  ngAfterViewInit() {
    if (this._enabled) {
      this.focus();
    }
  }
}
