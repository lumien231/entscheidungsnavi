import { AfterViewInit, Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[dtScrollIntoView]',
})
export class ScrollIntoViewDirective implements AfterViewInit {
  private _enabled = false;

  @Input()
  dtScrollIntoViewOnAdd = false;

  @Input() set dtScrollIntoView(value: boolean) {
    if (value && this._enabled) {
      this.elementRef.nativeElement.scrollIntoView({ behavior: 'smooth' });
    }
  }

  constructor(private elementRef: ElementRef<HTMLElement>) {}

  ngAfterViewInit() {
    this._enabled = true;

    if (this.dtScrollIntoViewOnAdd) {
      this.elementRef.nativeElement.scrollIntoView({ behavior: 'smooth' });
    }
  }
}
