import { Directive, ElementRef, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
  selector: '[dtClickOutside]',
  standalone: true,
})
export class ClickOutsideDirective {
  @Output() public dtClickOutside = new EventEmitter<MouseEvent>();

  constructor(private elementRef: ElementRef) {}

  @HostListener('document:click', ['$event'])
  public hostClickHandler(event: MouseEvent) {
    if (event && event.target && !this.elementRef.nativeElement.contains(event.target)) {
      this.dtClickOutside.emit(event);
    }
  }
}
