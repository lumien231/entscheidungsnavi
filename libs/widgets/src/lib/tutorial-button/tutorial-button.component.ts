import { Component, Inject, Input, LOCALE_ID } from '@angular/core';

@Component({
  selector: 'dt-tutorial-button',
  templateUrl: './tutorial-button.component.html',
})
export class TutorialButtonComponent {
  // Youtube ID for videos for the specified languages
  @Input() tutorialIdDE: string;
  @Input() tutorialIdEN: string;

  get tutorialId() {
    return this.isEnglish ? this.tutorialIdEN : this.tutorialIdDE;
  }

  readonly isEnglish: boolean;

  constructor(@Inject(LOCALE_ID) locale: string) {
    this.isEnglish = !locale.startsWith('de');
  }
}
