import { Component, EventEmitter, Input, Output, ViewChild, TemplateRef } from '@angular/core';
import { NoteBtnCaption, NoteBtnComponent } from '../../note-btn';
import { InputboxComponent } from '../inputbox.component';

/*
 Usage:
 <dt-inputbox [deleteButton]="true" [collapsibleButton]="true">
 <div class="caption-wrapper">Headline</div>
 <div class="control-wrapper">
 Option to add certain controls e.g. a delete button
 </div>
 <div class="content-wrapper">
 Test Content
 </div>
 </dt-inputbox>
 */

@Component({
  selector: 'dt-inputbox-simple',
  templateUrl: 'inputbox-simple.component.html',
  styleUrls: ['inputbox-simple.component.scss'],
})
export class InputboxSimpleComponent {
  @Input() showSmallContainer = true;

  @Input() noteCaption?: NoteBtnCaption;
  @Input() notes: string;
  @Input() notesEditable = true;
  @Input() notesPlaceholder = '';

  @ViewChild(NoteBtnComponent, { static: true }) noteBtn: NoteBtnComponent;

  @Input() name: string;
  @Input() namePlaceholder: string;

  @Input() readonly = false;
  @Input() draggable = true;
  @Input() deleteDisabled = false;
  @Input() shortQuill = false;
  @Input() collapsibleButton = true;
  @Input() collapsedByDefault = false;
  @Input() nameInput = true;

  @Input() customData: any;

  @Input() confirmDeleteTemplate: TemplateRef<any>;

  @Output() deleteEvent = new EventEmitter();

  @Output() nameChange = new EventEmitter();
  @Output() notesChange = new EventEmitter();

  @Output() noteModalClick = new EventEmitter();

  @ViewChild(InputboxComponent, { static: true })
  inputBoxBase: InputboxComponent;

  editorModules = {};

  constructor() {
    this.editorModules = {
      toolbar: {
        container: [['bold', 'italic', 'underline', 'strike']],
      },
    };
  }

  public getBase() {
    return this.inputBoxBase;
  }

  openNotes() {
    this.noteBtn.open();
  }

  onDelete() {
    this.deleteEvent.emit();
  }

  onNameChange(event: any) {
    this.nameChange.emit(event);
  }

  onNotesChange(event: any) {
    this.notesChange.emit(event);
  }

  test() {
    console.log('test');
  }
}
