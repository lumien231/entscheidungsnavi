import { Pipe, PipeTransform } from '@angular/core';
import { InfluenceFactor, PredefinedInfluenceFactor, UserdefinedInfluenceFactor } from '@entscheidungsnavi/decision-data/classes';
import { PREDEFINED_INFLUENCE_FACTOR_NAMES } from './predefined-influence-factor-names';

/**
 * Example Usage: {{ value | influenceFactorName }}
 *
 * Casts the given value to a number.
 */

@Pipe({ name: 'influenceFactorName' })
export class InfluenceFactorNamePipe implements PipeTransform {
  transform(influenceFactor: InfluenceFactor): string {
    if (influenceFactor == null) {
      return '';
    } else if (influenceFactor instanceof UserdefinedInfluenceFactor) {
      /* Userdefined influence factor */
      return influenceFactor.name;
    } else if (influenceFactor instanceof PredefinedInfluenceFactor) {
      /* Predefined influence factor */
      return PREDEFINED_INFLUENCE_FACTOR_NAMES[influenceFactor.id].name;
    }
    return '';
  }
}
