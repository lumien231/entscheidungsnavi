import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({
  name: 'toFixed',
})
export class ToFixedPipe implements PipeTransform {
  constructor(private decimalPipe: DecimalPipe) {}

  transform(value: number, sigDigits = 2): string {
    return this.decimalPipe.transform(value, `1.${sigDigits}-${sigDigits}`);
  }
}
