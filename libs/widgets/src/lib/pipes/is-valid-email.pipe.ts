import { Pipe, PipeTransform } from '@angular/core';
import { isEmail } from 'class-validator';

@Pipe({ name: 'isValidEmail' })
export class IsValidEmailPipe implements PipeTransform {
  transform(value: string): boolean {
    return isEmail(value);
  }
}
