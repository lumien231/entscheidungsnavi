/* Usage:
 *   ngFor="let c of oneDimArray | sortBy:'asc'"
 *   ngFor="let c of arrayOfObjects | sortBy:'asc':'propertyName'"
 */
import { Pipe, PipeTransform } from '@angular/core';
import { identity, orderBy } from 'lodash';

@Pipe({ name: 'sortBy' })
export class SortByPipe implements PipeTransform {
  transform<T>(value: T[], order: 'asc' | 'desc' = 'asc', column?: string): T[] {
    if (!value || value.length <= 1) {
      // no array or array with only one item
      return value;
    }
    return orderBy(value, [column ?? identity], [order]);
  }
}
