import { Directive, Input, OnInit } from '@angular/core';

@Directive()
export abstract class AbstractSelectItemDirective implements OnInit {
  @Input()
  value: any;

  ngOnInit() {
    if (this.value === undefined) {
      throw new Error('`value` must be set.');
    }
  }
}
