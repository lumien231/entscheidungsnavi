import { Component, Inject } from '@angular/core';
import { MatLegacySnackBar as MatSnackBar, MAT_LEGACY_SNACK_BAR_DATA as MAT_SNACK_BAR_DATA } from '@angular/material/legacy-snack-bar';
import { finalize, Observable } from 'rxjs';

@Component({
  templateUrl: './loading-snackbar.component.html',
  styleUrls: ['./loading-snackbar.component.scss'],
})
export class LoadingSnackbarComponent {
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: { text: string }) {}
}

export function loadingIndicator<T>(snackBar: MatSnackBar, message: string) {
  return (source: Observable<T>) => {
    return new Observable<T>(subscriber => {
      const snackbarRef = snackBar.openFromComponent(LoadingSnackbarComponent, { data: { text: message } });

      source.pipe(finalize(() => snackbarRef.dismiss())).subscribe(subscriber);
    });
  };
}
