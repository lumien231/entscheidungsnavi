import { OverlayModule } from '@angular/cdk/overlay';
import { CommonModule, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatLegacyButtonModule as MatButtonModule } from '@angular/material/legacy-button';
import { MatStepperModule } from '@angular/material/stepper';
import { MatLegacyCheckboxModule as MatCheckboxModule } from '@angular/material/legacy-checkbox';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MAT_ICON_DEFAULT_OPTIONS, MatIconModule } from '@angular/material/icon';
import { MatLegacyInputModule as MatInputModule } from '@angular/material/legacy-input';
import { MatLegacyMenuModule as MatMenuModule } from '@angular/material/legacy-menu';
import { MatLegacyProgressBarModule as MatProgressBarModule } from '@angular/material/legacy-progress-bar';
import { MatLegacyProgressSpinnerModule as MatProgressSpinnerModule } from '@angular/material/legacy-progress-spinner';
import { MatLegacyRadioModule as MatRadioModule } from '@angular/material/legacy-radio';
import { MatLegacySelectModule as MatSelectModule } from '@angular/material/legacy-select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatLegacySnackBarModule as MatSnackBarModule } from '@angular/material/legacy-snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatLegacyTooltipModule as MatTooltipModule } from '@angular/material/legacy-tooltip';
import { MatLegacyCardModule as MatCardModule } from '@angular/material/legacy-card';
import { MatLegacyTabsModule as MatTabsModule } from '@angular/material/legacy-tabs';
import { MatLegacyTableModule as MatTableModule } from '@angular/material/legacy-table';
import { MatLegacyChipsModule as MatChipsModule } from '@angular/material/legacy-chips';
import { MatLegacyListModule as MatListModule } from '@angular/material/legacy-list';
import { MatLegacyAutocompleteModule as MatAutocompleteModule } from '@angular/material/legacy-autocomplete';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatLegacySliderModule as MatSliderModule } from '@angular/material/legacy-slider';
import { MatLegacySlideToggleModule as MatSlideToggleModule } from '@angular/material/legacy-slide-toggle';
import { RouterModule } from '@angular/router';
import { ColorSketchModule } from 'ngx-color/sketch';
import { QuillModule } from 'ngx-quill';
import { PortalModule } from '@angular/cdk/portal';
import {
  MAT_LEGACY_DIALOG_DEFAULT_OPTIONS as MAT_DIALOG_DEFAULT_OPTIONS,
  MatLegacyDialogModule as MatDialogModule,
} from '@angular/material/legacy-dialog';
import { MatLegacyPaginatorIntl, MatLegacyPaginatorModule as MatPaginatorModule } from '@angular/material/legacy-paginator';
import { Platform } from '@angular/cdk/platform';
import { ClipboardModule } from 'ngx-clipboard';
import { NoteBtnComponent } from './note-btn';
import { BarSliderComponent } from './bar-slider';
import { CollapsibleComponent } from './collapsible';
import { ColorPickerDirective } from './color-picker';
import { DragDropListComponent } from './drag-drop-list';
import { FlexColumnDirective, FlexTableComponent } from './flex-table';
import { InputboxComponent, InputboxGroupedComponent, InputboxSimpleComponent } from './inputbox';
import { RangeInputComponent } from './range-input';
import { TabComponent, TabsComponent } from './tabs';
import { TreeSelectComponent } from './tree-select';
import { YoutubeContainerComponent, YoutubeModalComponent } from './youtube';
import { TriangleComponent } from './triangle';
import {
  AutoFocusDirective,
  DirectEventDirective,
  DynamicChangeDirectiveModule,
  ElementRefDirective,
  InlineSVGDirective,
  OverflowDirective,
  PinchZoomDirective,
  RegexBlockDirective,
  ResizeObserverDirective,
  ScrollbarDirective,
  ScrollIntoViewDirective,
  ViewIntersectionDirective,
  VisibleDirective,
  WidthTriggerDirective,
} from './directives';
import { OverlayProgressBarDirective } from './overlay-progress-bar';
import { VerticalSliderComponent } from './vertical-slider';
import { CopyWhistleDirective, PopOverComponent, WhistleComponent } from './popover';
import { TutorialButtonComponent } from './tutorial-button/tutorial-button.component';
import { ValidateNumberInputDirective } from './validate-number-input/validate-number-input.directive';
import { NumberRoundingPipe } from './pipes/number-rounding.pipe';
import { NumberRoundingAsPower } from './pipes/number-rounding-as-power.pipe';
import { AspectBoxComponent } from './aspect-box';
import { PasswordStrengthComponent } from './password-strength/password-strength.component';
import { PasswordFormComponent } from './form-elements/password-form';
import { EvaluationCheckboxesComponent } from './evaluation-checkboxes';
import { ModalModule } from './modal/modal.module';
import { ButtonModule } from './buttons/button.module';
import { LoadingSnackbarComponent } from './loading-snackbar';
import { SelectComponent, SelectGroupDirective, SelectLabelDirective, SelectLineComponent, SelectOptionDirective } from './select';
import {
  InfluenceFactorNamePipe,
  IsValidEmailPipe,
  LocalizedStringPipe,
  NumberCastPipe,
  SafeArrayIndexPipe,
  SafeNumberPipe,
  SafeUrlPipe,
  SortByPipe,
  StateNamePipe,
  ToFixedPipe,
} from './pipes';
import { TagInputComponent } from './tag-input/tag-input.component';
import { ObjectBoxComponent } from './object-box/object-box.component';
import { InputSpinnerComponent } from './input-spinner/input-spinner.component';
import { CollapseAllComponent } from './collapse-all/collapse-all.component';
import { EmailConfirmationModalComponent } from './email-confirmation-modal';
import { SnackbarComponent } from './snackbar';
import {
  AccountModalComponent,
  ChangeEmailComponent,
  ChangePasswordComponent,
  ChangeUsernameComponent,
  DeleteAccountModalComponent,
} from './account-modal';
import { RequestPasswordResetModalComponent } from './request-password-reset-modal';
import { RangeInputLogComponent } from './range-input-log/range-input-log.component';
import {
  QuestionnaireComponent,
  QuestionnaireQuestionComponent,
  QuestionnaireQuestionNumberComponent,
  QuestionnaireQuestionOptionsComponent,
  QuestionnaireQuestionTableComponent,
  QuestionnaireQuestionTextBlockComponent,
  QuestionnaireQuestionTextComponent,
} from './questionnaire';
import { PaginatorIntlService } from './paginator/paginator-intl.service';

const DECLARE_AND_EXPORT = [
  AutoFocusDirective,
  BarSliderComponent,
  CollapseAllComponent,
  CollapsibleComponent,
  VerticalSliderComponent,
  DragDropListComponent,
  InputboxComponent,
  InputboxGroupedComponent,
  InputboxSimpleComponent,
  RegexBlockDirective,
  VisibleDirective,
  DirectEventDirective,
  RangeInputComponent,
  TabComponent,
  TabsComponent,
  PopOverComponent,
  WhistleComponent,
  CopyWhistleDirective,
  TreeSelectComponent,
  TriangleComponent,
  ViewIntersectionDirective,
  ScrollbarDirective,
  YoutubeContainerComponent,
  YoutubeModalComponent,
  FlexTableComponent,
  FlexColumnDirective,
  ColorPickerDirective,
  SafeNumberPipe,
  SafeArrayIndexPipe,
  SortByPipe,
  NumberCastPipe,
  StateNamePipe,
  InfluenceFactorNamePipe,
  TutorialButtonComponent,
  ValidateNumberInputDirective,
  NumberRoundingPipe,
  NumberRoundingAsPower,
  AspectBoxComponent,
  ToFixedPipe,
  PasswordStrengthComponent,
  PasswordFormComponent,
  ElementRefDirective,
  PinchZoomDirective,
  ScrollIntoViewDirective,
  EvaluationCheckboxesComponent,
  LoadingSnackbarComponent,
  InlineSVGDirective,
  LocalizedStringPipe,
  TagInputComponent,
  ObjectBoxComponent,
  SelectComponent,
  SelectLabelDirective,
  SelectOptionDirective,
  SelectGroupDirective,
  SelectLineComponent,
  InputSpinnerComponent,
  ResizeObserverDirective,
  RangeInputLogComponent,
  EmailConfirmationModalComponent,
  AccountModalComponent,
  RequestPasswordResetModalComponent,
  QuestionnaireComponent,
  QuestionnaireQuestionComponent,
  QuestionnaireQuestionTextBlockComponent,
  QuestionnaireQuestionNumberComponent,
  QuestionnaireQuestionOptionsComponent,
  QuestionnaireQuestionTableComponent,
  QuestionnaireQuestionTextComponent,
  IsValidEmailPipe,
];

const COMMON_RE_EXPORTS = [
  MatMenuModule,
  MatCheckboxModule,
  MatSnackBarModule,
  MatIconModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatButtonModule,
  MatSortModule,
  MatRadioModule,
  MatTooltipModule,
  MatDividerModule,
  MatSidenavModule,
  MatToolbarModule,
  MatSelectModule,
  MatRippleModule,
  MatListModule,
  MatSliderModule,
  PortalModule,
  MatCardModule,
  MatTabsModule,
  MatStepperModule,
  MatExpansionModule,
  FormsModule,
  DragDropModule,
  ReactiveFormsModule,
  MatSlideToggleModule,
  MatDialogModule,
  ModalModule,
  ButtonModule,
  MatPaginatorModule,
  MatTableModule,
  MatChipsModule,
  MatAutocompleteModule,
  DynamicChangeDirectiveModule,
  ClipboardModule,
  SafeUrlPipe,
];

@NgModule({
  imports: [
    ...COMMON_RE_EXPORTS,
    OverlayProgressBarDirective,
    WidthTriggerDirective,
    ColorSketchModule,
    CommonModule,
    NoteBtnComponent,
    OverflowDirective,
    SnackbarComponent,
    OverlayModule,
    RouterModule,
    QuillModule.forRoot(),
  ],
  declarations: [
    ...DECLARE_AND_EXPORT,
    ChangeEmailComponent,
    ChangePasswordComponent,
    ChangeUsernameComponent,
    DeleteAccountModalComponent,
  ],
  exports: [...DECLARE_AND_EXPORT, ...COMMON_RE_EXPORTS, OverlayProgressBarDirective, WidthTriggerDirective, QuillModule],
  providers: [
    DecimalPipe,
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: {
        panelClass: 'dt-mat-dialog-panel',
        hasBackdrop: true,
        backdropClass: 'dt-mat-dialog-backdrop',
        position: {
          top: '10vh',
        },
      },
    },
    {
      provide: MAT_ICON_DEFAULT_OPTIONS,
      useValue: {
        fontSet: 'material-symbols-outlined',
      },
    },
    {
      provide: MatLegacyPaginatorIntl,
      useClass: PaginatorIntlService,
    },
  ],
})
export class WidgetsModule {
  constructor(platform: Platform) {
    if (platform.WEBKIT) {
      document.body.classList.add('dt-webkit');
    }
  }
}
