import { Injectable, TemplateRef, ElementRef } from '@angular/core';
import { Overlay, ConnectedPosition, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal, ComponentType } from '@angular/cdk/portal';
import { ThemePalette } from '@angular/material/core';
import { PopOverComponent } from './popover.component';
import { WhistleComponent } from './whistle/whistle.component';

export type PopOverRef = { close: () => void; overlayRef: OverlayRef };
export type ComponentPopOverRef<T> = PopOverRef & { componentInstance: T };

type PopOverConfig = {
  closeCallback?: () => void;
  dimensions?: { width?: number; height?: number };
  position?: ConnectedPosition[];
  transformOriginOn?: string;
  hasBackdrop?: boolean;
  panelClass?: string;
  disableAnimation?: boolean;
  repositionOnScroll?: boolean;
};

@Injectable({ providedIn: 'root' })
export class PopOverService {
  constructor(private overlay: Overlay) {}

  open(toOpen: TemplateRef<any>, attachedTo: ElementRef<HTMLElement> | HTMLElement, config?: PopOverConfig): PopOverRef;
  open<T>(toOpen: ComponentType<T>, attachedTo: ElementRef<HTMLElement> | HTMLElement, config?: PopOverConfig): ComponentPopOverRef<T>;
  open<T>(
    toOpen: TemplateRef<any> | ComponentType<T>,
    attachedTo: ElementRef<HTMLElement> | HTMLElement,
    config?: PopOverConfig
  ): ComponentPopOverRef<T> | PopOverRef;
  open(
    toOpen: TemplateRef<any> | ComponentType<any>,
    attachedTo: ElementRef<HTMLElement> | HTMLElement,
    {
      closeCallback = null,
      dimensions = null,
      position = [{ originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top' }],
      transformOriginOn = 'dt-popover',
      hasBackdrop = true,
      panelClass = null,
      disableAnimation = false,
      repositionOnScroll = false,
    }: PopOverConfig = {}
  ): PopOverRef | ComponentPopOverRef<any> {
    if (attachedTo instanceof ElementRef) {
      attachedTo = attachedTo.nativeElement;
    }

    let positionStrategy = this.overlay.position().flexibleConnectedTo(attachedTo).withPositions(position);
    const scrollStrategy = this.getScrollStrategy(repositionOnScroll);

    if (transformOriginOn) {
      positionStrategy = positionStrategy.withTransformOriginOn(transformOriginOn);
    }

    const overlayRef = this.overlay.create({
      positionStrategy: positionStrategy,
      scrollStrategy: scrollStrategy,
      hasBackdrop,
      backdropClass: 'dt-popover-backdrop',
      panelClass,
      width: dimensions?.width,
      height: dimensions?.height,
    });

    const close = () => {
      overlayRef.detach();
      overlayRef.detachBackdrop();

      closeCallback?.();
    };

    overlayRef.hostElement.style.pointerEvents = 'none';

    overlayRef.backdropClick().subscribe((event: MouseEvent) => {
      event.stopImmediatePropagation();
      close();
    });

    overlayRef.keydownEvents().subscribe(event => {
      if (event.key === 'Escape') {
        event.stopImmediatePropagation();
        close();
      }
    });

    const portal = new ComponentPortal(PopOverComponent);
    const component = overlayRef.attach(portal).instance;
    component.disableAnimation = disableAnimation;

    component.popoverClose.subscribe(() => close());
    component.finishedLeaveAnimation.subscribe(() => {
      overlayRef.dispose();
    });

    if (toOpen instanceof TemplateRef) {
      component.templateRef = toOpen;
      return { close, overlayRef };
    } else {
      const popComponentInstance = component.portalOutlet.attach(new ComponentPortal(toOpen)).instance;

      return { close, componentInstance: popComponentInstance, overlayRef };
    }
  }

  whistle(attachedTo: ElementRef<HTMLElement> | HTMLElement, text: string, icon?: string, duration = 2000, iconColor?: ThemePalette) {
    const ref = this.showAnimation(WhistleComponent, attachedTo, 2);
    ref.componentInstance.text = text;
    ref.componentInstance.icon = icon;
    ref.componentInstance.duration = duration;
    ref.componentInstance.iconColor = iconColor;
  }

  showAnimation<T>(
    toOpen: ComponentType<T>,
    attachedTo: ElementRef<HTMLElement> | HTMLElement,
    animationsToRun?: number
  ): ComponentPopOverRef<T>;
  showAnimation(toOpen: TemplateRef<any>, attachedTo: ElementRef<HTMLElement> | HTMLElement, animationsToRun?: number): PopOverRef;
  showAnimation<T>(
    toOpen: TemplateRef<any> | ComponentType<T>,
    attachedTo: ElementRef<HTMLElement> | HTMLElement,
    animationsToRun = 1
  ): PopOverRef | ComponentPopOverRef<T> {
    const ref = this.open(toOpen, attachedTo, {
      hasBackdrop: false,
      disableAnimation: true,
      position: [
        { originX: 'center', originY: 'top', overlayX: 'center', overlayY: 'bottom' },
        { originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top' },
        { originX: 'end', originY: 'center', overlayX: 'start', overlayY: 'center' },
        { originX: 'start', originY: 'center', overlayX: 'end', overlayY: 'center' },
      ],
      transformOriginOn: '.dt-transform-origin',
    });

    ref.overlayRef.hostElement.addEventListener('animationend', () => {
      if (--animationsToRun === 0) {
        ref.close();
      }
    });

    return ref;
  }

  getScrollStrategy(repositionOnScroll: boolean) {
    if (repositionOnScroll) {
      return this.overlay.scrollStrategies.reposition();
    } else {
      return this.overlay.scrollStrategies.noop();
    }
  }
}
