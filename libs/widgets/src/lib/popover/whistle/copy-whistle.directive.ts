/* eslint-disable @angular-eslint/directive-selector */
import { Directive, ElementRef, HostListener } from '@angular/core';
import { PopOverService } from '../popover.service';

@Directive({
  selector: '[ngxClipboard]',
})
export class CopyWhistleDirective {
  constructor(private popOverService: PopOverService, private elementRef: ElementRef<HTMLElement>) {}

  @HostListener('cbOnSuccess')
  copySuccess() {
    this.popOverService.whistle(this.elementRef.nativeElement, $localize`In die Zwischenablage kopiert!`, 'done');
  }

  @HostListener('cbOnError')
  copyError() {
    this.popOverService.whistle(this.elementRef.nativeElement, $localize`Text konnte nicht in die Zwischenablage kopiert werden.`, 'error');
  }
}
