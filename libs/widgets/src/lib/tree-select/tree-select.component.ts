import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

interface Tree<T> {
  value?: T;
  children?: Array<Tree<T>>;
}

@Component({
  selector: 'dt-tree-select',
  templateUrl: './tree-select.component.html',
  styleUrls: ['./tree-select.component.scss'],
})
export class TreeSelectComponent implements OnInit {
  @Input()
  data: Tree<any>;

  @Input()
  default: any;

  @Input()
  displayFunction: (value: any) => string;

  @Output()
  treeSelectChange: EventEmitter<any> = new EventEmitter<any>();

  currentLayer: Tree<any>;
  parent: Tree<any>;

  selectedParent: Tree<any>;
  selected: any;

  isOpen: boolean;

  ngOnInit() {
    this.currentLayer = this.data;
  }

  toggle() {
    this.isOpen = !this.isOpen;

    if (this.isOpen && this.selectedParent) {
      this.currentLayer = this.selectedParent;
    }
  }

  select(event: MouseEvent, child: Tree<any>) {
    if (child.children) {
      event.stopImmediatePropagation();
      this.parent = this.currentLayer;
      this.currentLayer = child;
    } else {
      this.selectedParent = this.currentLayer;
      this.selected = child.value;
      this.treeSelectChange.emit(this.selected);
    }
  }
}
