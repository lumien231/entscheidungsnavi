import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TextBlockEntry } from '@entscheidungsnavi/api-types';

@Component({
  selector: 'dt-questionnaire-question-textblock',
  templateUrl: './questionnaire-textblock.component.html',
  styleUrls: ['./questionnaire-textblock.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuestionnaireQuestionTextBlockComponent {
  @Input()
  entry: TextBlockEntry;
}
