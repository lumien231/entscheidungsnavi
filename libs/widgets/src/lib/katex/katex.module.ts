import { NgModule } from '@angular/core';
import { KatexComponent } from './katex.component';

@NgModule({
  declarations: [KatexComponent],
  exports: [KatexComponent],
})
export class KatexModule {}
