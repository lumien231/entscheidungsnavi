import { ExtendedViewDimensions } from '..';

export interface ViewDimensionParameters {
  width: number;
  height: number;
  margins: [number, number, number, number];
  showXAxis: boolean;
  showYAxis: boolean;
  xAxisHeight: number;
  yAxisWidth: number;
  showXLabel: boolean;
  showYLabel: boolean;
}

/**
 * This is the same function as in ngx-charts, with one small change regarding the chart width.
 * Also, the part about the legend was removed.
 */
export function calculateViewDimensions({
  width,
  height,
  margins,
  showXAxis,
  showYAxis,
  xAxisHeight,
  yAxisWidth,
  showXLabel,
  showYLabel,
}: ViewDimensionParameters): ExtendedViewDimensions {
  let xOffset = margins[3];
  let chartWidth = width - margins[1] - margins[3];
  let chartHeight = height - margins[0] - margins[2];

  if (showXAxis) {
    chartHeight -= 5;
    chartHeight -= xAxisHeight;

    if (showXLabel) {
      // text height + spacing between axis label and tick labels
      const offset = 25 + 5;
      chartHeight -= offset;
    }
  }

  if (showYAxis) {
    // HERE is the change from ngx-charts: Previously there was a 5 here which made no sense.
    chartWidth -= 10;
    chartWidth -= yAxisWidth;
    xOffset += yAxisWidth;
    xOffset += 10;

    if (showYLabel) {
      // text height + spacing between axis label and tick labels
      const offset = 25 + 5;
      chartWidth -= offset;
      xOffset += offset;
    }
  }

  chartWidth = Math.max(0, chartWidth);
  chartHeight = Math.max(0, chartHeight);

  return {
    width: Math.floor(chartWidth),
    height: Math.floor(chartHeight),
    xOffset: Math.floor(xOffset),
    margins,
  };
}
