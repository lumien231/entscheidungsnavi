import { ViewDimensions } from '@swimlane/ngx-charts';
import { ScaleLinear, ScalePoint, ScaleTime } from 'd3-scale';

/**
 * One single data point on a line.
 * The 'name' property is the x-value and the 'value'-property the y-value.
 */
export interface Point<T> {
  name: any;
  value: T;
}

/**
 * Base class for a data series.
 */
export abstract class AbstractSeries<T> {
  // Defines the color with which this series is displayed (e.g. #000000 or red)
  color: string;
  series: T[];
}

/**
 * This defines one line.
 * One line/data series consisting of multiple datapoints.
 */
export class LineSeries extends AbstractSeries<Point<number>> {
  // This name is shown for the line in the tooltips
  name: string;
  // A separate color for the circles or null
  circleColor?: string;
}

/**
 * This defines one area.
 * An area consists of two lines that define the boundaries. Those are defined
 * by the series for which each point contains two y-values for both of those lines.
 */
export class AreaSeries extends AbstractSeries<Point<[number, number]>> {
  opacity: number;
}

/**
 * Properties that can be set for each individual circle.
 */
export interface CircleProperties {
  id?: number; // A unique id by which this circle is identified (used for animations)
  color?: string; // The color of the circle is set globally in the series but can be overwritten here

  // Text displayed next to the circle (not in the tooltip)
  text?: string;
  textColor?: string;
  showText?: boolean;

  tag?: any; // A tag that is exposed in the ng-template for the tooltip
}

/**
 * A series of circles with text beside them. First value is the y-coordinate, second
 * value are additional properties for this circle.
 *
 * There is one color set in the series that is the default.
 * However, each circle can override that color.
 */
export class TextCircleSeries extends AbstractSeries<Point<[number, CircleProperties]>> {}

/**
 * In what direction data should be sorted. (short for ascending and descending)
 */
export type SortDirection = 'asc' | 'dsc';

/**
 * Defines one Axis of the chart
 */
export class Axis {
  displayAxis = true;
  showLabel = false;
  labelText = '';
  gridLines = true;
  ticks: any[]; // The x/y-values where ticks should be visible
  format?: (input: any) => string; // Input is the x-/y-value to be formatted

  // When min and max are set, the axis direction is already determined by
  // whether (min < max) or (min > max) holds.
  direction: SortDirection = 'asc';
  min?: any;
  max?: any;

  constructor(init?: Partial<Axis>) {
    Object.assign(this, init);
  }
}

/**
 * The type of data that is displayed on a scale/axis.
 */
export type ScaleType = 'ordinal' | 'linear' | 'time';

/**
 * A x or y axis scale.
 */
export type XScale = ScaleTime<number, number> | ScaleLinear<number, number> | ScalePoint<string>;
export type YScale = ScaleLinear<number, number>;

/**
 * Necessary for the axis, extends the base type from ngx-charts.
 */
export interface ExtendedViewDimensions extends ViewDimensions {
  // top, right, bottom, left
  margins: [number, number, number, number];
}
