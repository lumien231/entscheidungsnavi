import { Component, ContentChild, EventEmitter, HostBinding, Input, Output, TemplateRef } from '@angular/core';
import { MatDividerModule } from '@angular/material/divider';
import { CommonModule } from '@angular/common';
import { ButtonTinyComponent } from '../buttons/button-tiny.component';

@Component({
  selector: 'dt-collapsible-tiny',
  templateUrl: './collapsible-tiny.component.html',
  styleUrls: ['./collapsible-tiny.component.scss'],
  standalone: true,
  imports: [CommonModule, ButtonTinyComponent, MatDividerModule],
})
export class CollapsibleTinyComponent {
  @ContentChild('header') headerTemplateRef: TemplateRef<unknown>;
  @ContentChild('content') contentTemplateRef: TemplateRef<unknown>;

  @HostBinding('class.open')
  @Input()
  open = true;

  @Output()
  openChange = new EventEmitter<boolean>();
}
