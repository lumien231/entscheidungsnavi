import { diff_match_patch } from 'diff-match-patch';

export const DIFF_PATCH = new diff_match_patch();

/**
 * Creates a text-encoded patch from {@link from} to {@link to}.
 *
 * @param from - The source text
 * @param to - The target text
 * @returns The text-encoded patch
 */
export function createPatch(from: string, to: string) {
  const historyEntry = DIFF_PATCH.diff_main(from, to);
  DIFF_PATCH.diff_cleanupEfficiency(historyEntry);
  const patch = DIFF_PATCH.patch_make(historyEntry);
  return DIFF_PATCH.patch_toText(patch);
}

/**
 * Applies the text-encoded {@link patchText} to {@link source}.
 *
 * @param patchText - The text encoded patch
 * @param source - The source to apply the patch to
 */
export function applyPatch(patchText: string, source: string) {
  const patch = DIFF_PATCH.patch_fromText(patchText);
  const [result, success] = DIFF_PATCH.patch_apply(patch, source);

  if (success.indexOf(false) !== -1) {
    throw new Error('failed to apply patch');
  }

  return result;
}
