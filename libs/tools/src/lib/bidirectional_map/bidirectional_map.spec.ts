import BidirectionalMap from './bidirectional_map';

describe('Bidirectional Map', () => {
  let map: BidirectionalMap<number, string>;

  beforeEach(() => {
    map = new BidirectionalMap();
    map.set(2, 'foo');
    map.set(7, 'bar');
    map.set(1, 'baz');
  });

  it('check size', () => {
    expect(map.size).toBe(3);
  });

  it('test has()', () => {
    expect(map.has(2)).toBe(true);
    expect(map.has(7)).toBe(true);
    expect(map.has(1)).toBe(true);
    expect(map.has(0)).toBe(false);
  });

  it('test hasValue()', () => {
    expect(map.hasValue('foo')).toBe(true);
    expect(map.hasValue('bar')).toBe(true);
    expect(map.hasValue('baz')).toBe(true);
    expect(map.hasValue('bla')).toBe(false);
  });

  it('test get()', () => {
    expect(map.get(2)).toBe('foo');
    expect(map.get(7)).toBe('bar');
    expect(map.get(1)).toBe('baz');
    expect(map.get(0)).toBeUndefined();
  });

  it('test getKey()', () => {
    expect(map.getKey('foo')).toBe(2);
    expect(map.getKey('bar')).toBe(7);
    expect(map.getKey('baz')).toBe(1);
    expect(map.getKey('bla')).toBeUndefined();
  });

  it('test keys()', () => {
    const keys = map.keys();
    expect(keys.next().value).toBe(2);
    expect(keys.next().value).toBe(7);
    expect(keys.next().value).toBe(1);
    expect(keys.next().value).toBeUndefined();
  });

  it('test clear()', () => {
    map.clear();
    expect(map.size).toBe(0);
    expect(map.has(2)).toBe(false);
    expect(map.get(2)).toBeUndefined();
    expect(map.hasValue('foo')).toBe(false);
    map.set(3, 'test');
    expect(map.get(3)).toBe('test');
  });

  it('test delete()', () => {
    expect(map.size).toBe(3);
    expect(map.delete(7)).toBe(true);
    expect(map.has(7)).toBe(false);
    expect(map.hasValue('bar')).toBe(false);
  });

  it('test deleteValue()', () => {
    expect(map.deleteValue('foo')).toBe(true);
    expect(map.hasValue('foo')).toBe(false);
    expect(map.has(2)).toBe(false);
    expect(map.size).toBe(2);
  });

  it('test constructor()', () => {
    const entries: Array<[number, string]> = [
      [4, 't1'],
      [10, 't2'],
      [42, 't3'],
    ];
    const m = new BidirectionalMap(entries);
    expect(m.size).toBe(3);
    expect(m.get(10)).toBe('t2');
    expect(m.getKey('t3')).toBe(42);
  });

  it('test entries()', () => {
    const values = map.entries();
    expect(values.next().value[1]).toBe('foo');
    expect(values.next().value[0]).toBe(7);
    expect(values.next().value[1]).toBe('baz');
    expect(values.next().value).toBeUndefined();
    expect(values.next().done).toBe(true);
  });

  it('test forEach()', () => {
    let i = 0;
    map.forEach((val, key) => {
      if (i === 0) {
        expect(val).toBe('foo');
        expect(key).toBe(2);
      } else if (i === 1) {
        expect(val).toBe('bar');
        expect(key).toBe(7);
      } else if (i === 2) {
        expect(val).toBe('baz');
        expect(key).toBe(1);
      }
      i++;
    });
    expect(i).toBe(3);
  });

  it('test values()', () => {
    const values = map.values();
    expect(values.next().value).toBe('foo');
    expect(values.next().value).toBe('bar');
    expect(values.next().value).toBe('baz');
    expect(values.next().value).toBeUndefined();
    expect(values.next().done).toBe(true);
  });
});
