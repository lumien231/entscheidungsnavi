/**
 * ES 6 Map with reverse lookups
 * Use with caution, since entries with the same value will be overwritten, too
 */
export default class BidirectionalMap<TK, TV> implements Map<TK, TV> {
  protected _map = new Map<TK, TV>();
  protected _reverse = new Map<TV, TK>();

  readonly [Symbol.toStringTag]: 'Map'; // TODO: change if possible

  [Symbol.iterator](): IterableIterator<[TK, TV]> {
    return this._map[Symbol.iterator]();
  }

  constructor(iterable?: Iterable<[TK, TV]>) {
    if (iterable) {
      const array = Array.from(iterable); // TODO: change (usage of 'let ... of' with IterableIterator with target 'es5' is tricky)
      for (const [key, val] of array) {
        this.set(key, val);
      }
    }
  }

  clear(): void {
    this._map.clear();
    this._reverse.clear();
  }

  delete(key: TK): boolean {
    const val = this._map.get(key);
    return this._map.delete(key) && this._reverse.delete(val);
  }

  deleteValue(value: TV): boolean {
    const key = this._reverse.get(value);
    return this._reverse.delete(value) && this._map.delete(key);
  }

  entries(): IterableIterator<[TK, TV]> {
    return this._map.entries();
  }

  forEach(callbackfn: (value: TV, key: TK, map: Map<TK, TV>) => void, thisArg?: any): void {
    this._map.forEach(callbackfn, thisArg);
  }

  get(key: TK): TV {
    return this._map.get(key);
  }

  getKey(value: TV): TK {
    return this._reverse.get(value);
  }

  has(key: TK): boolean {
    return this._map.has(key);
  }

  hasValue(value: TV): boolean {
    return this._reverse.has(value);
  }

  keys(): IterableIterator<TK> {
    return this._map.keys();
  }

  set(key: TK, value: TV): this {
    // remove existing reverse entries
    if (this._map.has(key)) {
      this._reverse.delete(this._map.get(key));
    }
    if (this._reverse.has(value)) {
      this._map.delete(this._reverse.get(value));
    }
    this._map.set(key, value);
    this._reverse.set(value, key);
    return this;
  }

  get size(): number {
    return this._map.size;
  }

  values(): IterableIterator<TV> {
    return this._map.values();
  }
}
