/**
 * Takes rich text from Quill or normal text as input and determines whether it is empty.
 * It searches the "insert" property and checks if its value is empty (\n) or not.
 * The "attributes" property is ignored.
 * Returns true if empty and false otherwise.
 * @param text The text to check
 */
export function isRichTextEmpty(text: string) {
  if (text == null) {
    return true;
  }

  try {
    const textJSON = JSON.parse(text);

    if ('ops' in textJSON && textJSON.ops.length === 0) {
      return true;
    }

    for (let i = 0; i < textJSON.ops.length; i++) {
      if (textJSON.ops[i].insert != null) {
        if (typeof textJSON.ops[i].insert === 'object') {
          // image or a video
          return false;
        }
        // remove "\n"
        textJSON.ops[i].insert = textJSON.ops[i].insert.replace(/(\r\n|\n|\r)/gm, '');
        // remove " "
        textJSON.ops[i].insert = textJSON.ops[i].insert.replace(/ /g, '');
        if (textJSON.ops[i].insert.length > 0) {
          // has characters != "\n" or " "
          return false;
        }
      }
    }
  } catch (e: any) {
    return true;
  }

  return true;
}
