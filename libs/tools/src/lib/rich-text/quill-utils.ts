import Delta from 'quill-delta';

export function richTextLength(richOrPlainText: string) {
  const delta = getDelta(richOrPlainText);

  if (!delta) {
    return 0;
  }

  let length = 0;
  for (const op of delta.ops) {
    if (typeof op.insert === 'string' && op.insert) {
      length += op.insert.length;
    }
  }

  return length;
}

export function ensureIsRichText(input: string) {
  if (input == null || input === '') {
    return input;
  }

  if (!input.startsWith(`{"ops"`)) {
    // Plain Text
    const delta = new Delta();
    delta.push({ insert: input });

    return JSON.stringify({ ops: delta.ops });
  }

  return input;
}

export function richTextMinLength(richOrPlainText: string, characters: number) {
  const delta = getDelta(richOrPlainText);

  if (!delta) {
    return false;
  }

  let length = 0;
  for (const op of delta.ops) {
    if (typeof op.insert === 'string' && op.insert) {
      length += op.insert.length;

      if (length >= characters) {
        return true;
      }
    }
  }

  return false;
}

export function appendRichText(richOrPlainText: string, input: string) {
  const delta = getDelta(richOrPlainText);

  if (delta == null) {
    // empty (or invalid) quill, append to a fresh quill instance
    return createNewQuillFromInput(input);
  }

  if ('ops' in delta && delta.ops.length > 0 && 'insert' in delta.ops[delta.ops.length - 1]) {
    // valid JSON, valid structure
    delta.ops[delta.ops.length - 1].insert += '\n' + input;
    return JSON.stringify(delta);
  }

  // valid JSON, invalid structure
  return createNewQuillFromInput(input);
}

function createNewQuillFromInput(input: string) {
  return JSON.stringify({
    ops: [{ insert: input }],
  });
}

function getDelta(richOrPlainText: string) {
  if (!richOrPlainText) {
    return null;
  }

  try {
    const object = JSON.parse(richOrPlainText);

    if (!object) {
      return null;
    }

    return new Delta(object);
  } catch {
    // This is not JSON, we simply treat it as plain text
    return new Delta().insert(richOrPlainText);
  }
}
