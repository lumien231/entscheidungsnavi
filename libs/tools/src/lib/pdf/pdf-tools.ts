/**
 * Margins in pdfMake are in points. This function converts from centimeters to points.
 * https://github.com/bpampuch/pdfmake/issues/334
 *
 * @param cm - length in centimeters
 */
export function cmToP(cm: number): number {
  return (cm / 2.54) * 72;
}
