import { ClassConstructor, plainToInstance } from 'class-transformer';
import { validate, ValidationError } from 'class-validator';
import { mergeMap, from, map, OperatorFunction } from 'rxjs';

export class ValidationFailedError extends Error {
  constructor(target: ClassConstructor<unknown>, errors: ValidationError[]) {
    super(`Error transforming object to ${target.name}

${errors.map(error => '- ' + error.toString()).join('\n')}`);

    this.name = 'ValidationFailedError';
  }
}

type TransformOutputFor<TInput, TOutput> = TInput extends Array<unknown> ? TOutput[] : TOutput;

export function transformAndValidate<TInput, TOutput extends object>(
  to: ClassConstructor<TOutput>
): OperatorFunction<TInput, TransformOutputFor<TInput, TOutput>> {
  return mergeMap((value: TInput) => {
    const result = plainToInstance(to, value) as TransformOutputFor<TInput, TOutput>;
    return from(validate(result)).pipe(
      map(validationErrors => {
        if (validationErrors.length === 0) {
          return result;
        } else {
          throw new ValidationFailedError(to, validationErrors);
        }
      })
    );
  });
}
