// eslint-disable-next-line @typescript-eslint/no-unused-vars
interface Number {
  /**
   * like +(number.toFixed(digits)), but without the range restriction for digits to [0, 20]
   * @param digits - number of decimal points to round to
   * @returns rounded number
   */
  round(digits: number): number;

  /**
   *
   * @returns - magnitude / floored decimal power of the number
   */
  getMagnitude(): number;
}

Number.prototype.round = function (places: number): number {
  // round number to base (10^exp) [(exp) decimal points]
  const res = Math.round(+this * Math.pow(10, places)) * Math.pow(10, -places);
  // round again to avoid floating point inaccuracy
  if (places <= 0) {
    // res has to be a whole number
    return Math.round(res);
  } else {
    return +res.toFixed(places);
  }
};

Number.prototype.getMagnitude = function (this: number): number {
  if (this === 0) {
    return 0;
  } else {
    return Math.floor(Math.log10(Math.abs(this)));
  }
};
