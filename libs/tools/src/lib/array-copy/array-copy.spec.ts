import { ArrayCopy } from './array-copy';

interface TestType {
  id: number;
}

describe('ArrayCopy', () => {
  let original: TestType[];
  let copy: ArrayCopy<TestType>;

  beforeEach(() => {
    original = [{ id: 0 }, { id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }];
    copy = ArrayCopy.createArrayCopy<TestType, TestType>(original, element => ({ id: element.id }));
  });

  it('copies correctly', () => {
    expect(copy.length).toBe(original.length);
    copy.elements.forEach((element, index) => {
      expect(element.id).toBe(original[index].id);
      expect(element).not.toBe(original[index]);
    });
  });

  it('deletes correctly', () => {
    copy.remove(0);
    copy.remove(3);
    copy.remove(1);
    expect(copy.length).toBe(2);
    expect(copy.elements[0].id).toBe(1);
    expect(copy.elements[1].id).toBe(3);
  });

  it('adds correctly', () => {
    copy.add(0, { id: 10 });
    copy.add(6, { id: 11 });
    copy.add(3, { id: 12 });
    const order = [10, 0, 1, 12, 2, 3, 4, 11];
    copy.elements.forEach((element, index) => {
      expect(element.id).toBe(order[index]);
    });
  });

  it('moves correctly', () => {
    copy.move(2, 4);
    copy.move(3, 1);
    expect(copy.length).toBe(5);
    const order = [0, 4, 1, 3, 2];
    copy.elements.forEach((element, index) => {
      expect(element.id).toBe(order[index]);
    });
  });

  describe('mergeBack', () => {
    let add: jest.Mock<void, [number, TestType]>;
    let set: jest.Mock<void, [number, TestType]>;
    let move: jest.Mock<void, [number, number]>;
    let remove: jest.Mock<void, [number]>;

    beforeEach(() => {
      add = jest.fn((position: number, element: TestType) => {
        original.splice(position, 0, element);
      });
      set = jest.fn((position: number, element: TestType) => {
        original[position].id = element.id;
      });
      move = jest.fn((oldPosition: number, newPosition: number) => {
        original.splice(newPosition, 0, ...original.splice(oldPosition, 1));
      });
      remove = jest.fn((position: number) => {
        original.splice(position, 1);
      });
    });

    it('[delete, set]', () => {
      copy.remove(0);
      copy.remove(1);
      copy.remove(2);
      copy.get(0).id = 20;
      copy.mergeBack(original, add, set, remove, move);

      expect(add).not.toBeCalled();
      expect(set.mock.calls.length).toBe(2);
      expect(move).not.toBeCalled();
      expect(remove.mock.calls.length).toBe(3);
      expect(original.map(element => element.id)).toEqual([20, 3]);
    });

    it('[add, set]', () => {
      copy.add(0, { id: 10 });
      copy.add(6, { id: 11 });
      copy.add(3, { id: 12 });
      copy.get(1).id = 20;
      copy.get(3).id = 21;
      copy.mergeBack(original, add, set, remove, move);

      expect(add.mock.calls.length).toBe(3);
      expect(set.mock.calls.length).toBe(5);
      expect(move).not.toBeCalled();
      expect(remove).not.toBeCalled();
      expect(original.map(element => element.id)).toEqual([10, 20, 1, 21, 2, 3, 4, 11]);
    });

    it('[add, set, delete]', () => {
      copy.add(0, { id: 10 });
      copy.add(6, { id: 11 });
      copy.add(3, { id: 12 });
      copy.get(1).id = 20;
      copy.get(3).id = 21;
      copy.remove(0);
      copy.remove(1);
      copy.mergeBack(original, add, set, remove, move);

      expect(add.mock.calls.length).toBe(2);
      expect(set.mock.calls.length).toBe(4);
      expect(move).not.toBeCalled();
      expect(remove.mock.calls.length).toBe(1);
      expect(original.map(element => element.id)).toEqual([20, 21, 2, 3, 4, 11]);
    });

    it('[add, move, set, delete]', () => {
      copy.add(3, { id: 10 });
      copy.add(5, { id: 11 });
      copy.move(3, 4);
      copy.move(6, 0);
      copy.get(0).id = 12;
      copy.get(6).id = 13;
      copy.remove(0);
      copy.remove(1);
      copy.move(2, 1);
      copy.mergeBack(original, add, set, remove, move);

      expect(add).toBeCalled();
      expect(set).toBeCalled();
      expect(move).toBeCalled();
      expect(remove).toBeCalled();
      expect(original.map(element => element.id)).toEqual([0, 3, 2, 10, 13]);
    });
  });

  describe('hasChanges', () => {
    it('handles no changes', () => {
      expect(copy.hasChanges(original, (source, current) => source.id === current.id)).toBe(false);
    });

    it('handles added items', () => {
      copy.add(1, { id: 0 });
      expect(copy.hasChanges(original, (source, current) => source.id === current.id)).toBe(true);
    });

    it('handles removed items', () => {
      copy.remove(2);
      expect(copy.hasChanges(original, (source, current) => source.id === current.id)).toBe(true);
    });

    it('handles changed items', () => {
      original[2].id = -1;
      expect(copy.hasChanges(original, (source, current) => source.id === current.id)).toBe(true);
    });

    it('handles moved items', () => {
      copy.move(2, 3);
      expect(copy.hasChanges(original, (source, current) => source.id === current.id)).toBe(true);
    });
  });
});
