// Taken from https://stackoverflow.com/a/39419171/14878700
export function assertUnreachable(_: never): never {
  throw new Error();
}
