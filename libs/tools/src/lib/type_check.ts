/**
 * Checks that {@link value} is of type {@link T} and if so, returns {@link value} as {@link T}.
 *
 * @see {@link https://www.typescriptlang.org/docs/handbook/release-notes/typescript-4-9.html#the-satisfies-operator `satisifies` operator}
 * @param value - The value from which to check the type.
 * @returns T
 */
export function checkType<T>(value: T): T {
  return value;
}
