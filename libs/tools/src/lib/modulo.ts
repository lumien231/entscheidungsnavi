/**
 * "True" Modulo
 * mod(-1, 5) = 4 | -1 % 5 = -1
 */
export function mod(n: number, m: number) {
  return ((n % m) + m) % m;
}
