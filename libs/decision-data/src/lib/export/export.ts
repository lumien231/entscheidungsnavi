import { pick } from 'lodash';
import { DecisionData } from '../decision-data';
import { propertyNames } from './properties';

/**
 * @param data
 * @param exportVersion: the software version used for this export
 */
export function dataToText(data: DecisionData, exportVersion: string): string {
  const exportData = { exportVersion, ...pick(data, propertyNames) };
  return JSON.stringify(exportData);
}

/** @deprecated use dataToText instead */
// eslint-disable-next-line @typescript-eslint/naming-convention
export const data_to_text = dataToText;
