// the order of the array defines the import order. 'outcomes' has to be behind 'influenceFactors', 'alternatives' and 'objectives'
import {
  Alternative,
  Outcome,
  DecisionQuality,
  DecisionStatement,
  UserdefinedInfluenceFactor,
  Objective,
  Weights,
  ProjectMode,
  SavedValues,
  TimeRecording,
} from '../classes';
import { ObjectiveAspects, ObjectiveElement } from '../steps/hint-aspects';
import { HintAlternatives, NaviStep } from '../steps';

export const propertyNames = [
  'projectMode',
  'authorName',
  'decisionProblem',
  'decisionQuality',
  'decisionStatement',
  'objectiveHierarchyMainElement',
  'objectives',
  'objectiveAspects',
  'weights',
  'alternatives',
  'hintAlternatives',
  'influenceFactors',
  'projectNotes',
  'bonus1',
  'bonus2',
  'bonus3',
  'stepExplanations',
  'outcomes',
  'version',
  'lastUrl',
  'resultSubstepProgress',
  'savedValues',
  'timeRecording',
  'extraData',
];

// arrays for type checks

// properties of the type Array
export const arrayPropertyNames = ['objectives', 'alternatives', 'influenceFactors', 'outcomes'];
// properties of the type Object
export const objPropertyNames = [
  'decisionQuality',
  'decisionStatement',
  'hintAlternatives',
  'stepExplanations',
  'objectiveAspects',
  'weights',
  'objectiveHierarchyMainElement',
  'savedValues',
  'timeRecording',
  'extraData',
];
// optional properties
export const optPropertyNames = [
  'projectMode',
  'authorName',
  'decisionProblem',
  'decisionQuality',
  'decisionStatement',
  'objectiveHierarchyMainElement',
  'outcomes',
  'projectNotes',
  'bonus1',
  'bonus2',
  'bonus3',
  'stepExplanations',
  'objectiveAspects',
  'version',
  'lastUrl',
  'resultSubstepProgress',
  'savedValues',
  'timeRecording',
  'extraData',
];

export interface CommonData {
  projectMode: ProjectMode;
  authorName: string;
  decisionProblem: string;
  decisionQuality: DecisionQuality;
  decisionStatement: DecisionStatement;
  objectiveHierarchyMainElement: ObjectiveElement;
  objectives: Objective[];
  objectiveAspects: ObjectiveAspects;
  weights: Weights;
  alternatives: Alternative[];
  hintAlternatives: HintAlternatives;
  influenceFactors: UserdefinedInfluenceFactor[];
  projectNotes: string;
  bonus1: string;
  bonus2: string;
  bonus3: string;
  stepExplanations: Record<NaviStep, string>;
  outcomes: Outcome[][];
  version: string;
  lastUrl: string;
  resultSubstepProgress: number;
  savedValues: SavedValues;
  timeRecording: TimeRecording;
  extraData: { [key: string]: unknown };
}

export interface ExportData extends CommonData {
  exportVersion: string;
}
