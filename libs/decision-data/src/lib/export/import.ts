import { DecisionData } from '../decision-data';
import { arrayPropertyNames, ExportData, objPropertyNames, optPropertyNames, propertyNames } from './properties';
import {
  loadDecisionQuality,
  loadStatement,
  loadObjectives,
  loadAlternatives,
  loadHintAlternatives,
  loadInfluenceFactors,
  loadOutcomes,
  loadWeights,
  loadObjectiveAspects,
  loadStepExplanations,
  loadSavedValues,
  loadTimeRecording,
} from './import-loaders';
import { migrateDecisionData } from './import-compat';

export function importText(text: string, target: DecisionData): DecisionData {
  return importTextWithVersion(text, target)[0];
}

// safer than using read_text() directly, because the target will not be modified before the conversion is finished
export function importTextWithVersion(text: string, target: DecisionData): [DecisionData, string] {
  const [tempData, exportVersion] = readTextWithVersion(text);
  return [copyProperties(tempData, target), exportVersion];
}

export function readText(text: string, target?: DecisionData): DecisionData {
  return readTextWithVersion(text, target)[0];
}

export function readTextWithVersion(text: string, target?: DecisionData): [DecisionData, string] {
  if (!target) {
    target = new DecisionData();
  }
  let data: ExportData;
  try {
    data = JSON.parse(text);
    if (typeof data === 'string') {
      // TODO: Remove
      // temporary fix for files that have been stringified twice
      data = JSON.parse(data);
    }
  } catch (e) {
    console.error(e);
    throw new Error('The given text is not valid JSON.');
  }

  try {
    // Perform migrations on top-level properties in DecisionData
    migrateDecisionData(data);
  } catch (e) {
    console.error(e);
    throw new Error('Failed to convert the file to the current data format.');
  }

  if (checkData(data)) {
    try {
      return [convertProperties(data, target), data.exportVersion];
    } catch (e) {
      console.error(e);
      throw new Error('Failed to import the given data.');
    }
  } else {
    throw new Error('The given data has the wrong format.');
  }
}

/**
 * @param data - the data to be checked
 * @returns true, if no properties are missing or of the wrong type
 */
export function checkData(data: any): boolean {
  // Iterate over all required properties
  const missing: Array<string> = [];
  const wrongType: Array<string> = [];
  for (const name of propertyNames) {
    if (name in data && data[name] != null) {
      // Check the property types
      if (
        !(
          (!arrayPropertyNames.includes(name) &&
            (typeof data[name] === 'string' ||
              typeof data[name] === 'number' ||
              typeof data[name] === 'boolean' ||
              (objPropertyNames.includes(name) && typeof data[name] === 'object'))) ||
          (arrayPropertyNames.includes(name) && Array.isArray(data[name]))
        )
      ) {
        wrongType.push(name);
        console.log(objPropertyNames.includes(name));
        console.log(objPropertyNames);
        console.log('property ' + name + ' has the wrong type ' + typeof data[name] + '!');
      }
    } else if (!optPropertyNames.includes(name)) {
      missing.push(name);
      console.log('property ' + name + ' missing!');
    }
  }
  return missing.length === 0 && wrongType.length === 0;
}

/**
 * creates new
 * @param data
 * @param target
 */
export function convertProperties(data: ExportData, target?: DecisionData): DecisionData {
  if (!target) {
    target = new DecisionData();
  }
  for (const name of propertyNames) {
    switch (name) {
      case 'decisionQuality':
        loadDecisionQuality(data.decisionQuality, target);
        break;
      case 'decisionStatement':
        loadStatement(data.decisionStatement, target);
        break;
      case 'objectives':
        loadObjectives(data.objectives, target);
        break;
      case 'objectiveAspects':
        loadObjectiveAspects(data.objectiveAspects, target);
        break;
      case 'alternatives':
        loadAlternatives(data.alternatives, target);
        break;
      case 'hintAlternatives':
        loadHintAlternatives(data.hintAlternatives, target);
        break;
      case 'influenceFactors':
        loadInfluenceFactors(data.influenceFactors, target);
        break;
      case 'outcomes':
        loadOutcomes(data.outcomes, target);
        break;
      case 'weights':
        loadWeights(data.weights, target);
        break;
      case 'stepExplanations':
        loadStepExplanations(data.stepExplanations, target);
        break;
      case 'savedValues':
        loadSavedValues(data.savedValues, target);
        break;
      case 'timeRecording':
        loadTimeRecording(data.timeRecording, target);
        break;
      case 'extraData':
        target.extraData = data.extraData;
        break;
      default:
        if (data[name] != null) {
          target[name] = data[name];
        }
    }
  }

  target.weights.restoreArrayLengths(target.objectives.length);

  return target;
}

export function copyProperties(source: DecisionData, target: DecisionData): DecisionData {
  propertyNames.forEach(name => {
    if (source[name] != null) {
      target[name] = source[name];
    }
  });
  target.restoreInfluenceFactorIDs(); // repair broken IDs in old exports

  // Redo Cyclic DecisionData References
  target.hintAlternatives.decisionData = target;
  target.objectiveAspects.decisionData = target;

  return target;
}

/* eslint-disable @typescript-eslint/naming-convention */
/** @deprecated */
export const import_text = importText;
/** @deprecated */
export const read_text = readText;
/** @deprecated */
export const check_data = checkData;
/** @deprecated */
export const convert_properties = convertProperties;
/** @deprecated */
export const copy_properties = copyProperties;
