import {
  Objective,
  NumericalObjectiveData,
  UtilityFunction,
  ObjectiveType,
  IndicatorObjectiveData,
  Indicator,
  Alternative,
  UserdefinedInfluenceFactor,
  UserdefinedState,
  Outcome,
} from '../classes';
import { calculateDominanceRelations } from './dominance';

function setAllProcessed(outcomes: Outcome[][]) {
  outcomes.forEach(ocs => ocs.forEach(oc => (oc.processed = true)));
}

describe('Dominance', () => {
  describe('calculateDominanceRelations', () => {
    describe('TestSuite [Numerical, Indicator]', () => {
      let objs: Objective[];
      let alts: Alternative[];

      beforeEach(() => {
        objs = [
          new Objective(
            'z1',
            new NumericalObjectiveData(6, 1, '', new UtilityFunction(10, 5)),
            undefined,
            undefined,
            ObjectiveType.Numerical
          ),
          new Objective(
            'z2',
            undefined,
            undefined,
            new IndicatorObjectiveData([new Indicator('', 0, 10, '', 10), new Indicator('', -1, 9, '', 1)]),
            ObjectiveType.Indicator
          ),
        ];
        alts = [new Alternative(0, 'a1'), new Alternative(0, 'a2'), new Alternative(0, 'a3')];
      });

      it('recognizes dominance', () => {
        const outcs = [
          [new Outcome([6]), new Outcome([[5, 5]])],
          [new Outcome([3]), new Outcome([[5, 5]])],
          [new Outcome([1]), new Outcome([[4, 5]])],
        ];
        setAllProcessed(outcs);

        const dr = calculateDominanceRelations(objs, alts, outcs);
        expect(dr.length).toBe(1);
        expect(dr[0]).toMatchObject({ dominated: alts[0], dominating: alts[1] });
      });

      it('handles non-processed outcomes', () => {
        const outcs = [
          [new Outcome([6]), new Outcome([[5, 5]])],
          [new Outcome([3]), new Outcome([[5, 5]])],
          [new Outcome([1]), new Outcome([[4, 5]])],
        ];
        setAllProcessed(outcs);
        outcs[1][0].processed = false;

        const dr = calculateDominanceRelations(objs, alts, outcs);
        expect(dr.length).toBe(0);
      });

      it('recognizes non-dominance', () => {
        const outcs = [
          [new Outcome([6]), new Outcome([[10, 1]])],
          [new Outcome([3]), new Outcome([[10, 0]])],
          [new Outcome([1]), new Outcome([[10, -1]])],
        ];
        setAllProcessed(outcs);

        const dr = calculateDominanceRelations(objs, alts, outcs);
        expect(dr.length).toBe(0);
      });
    });

    it('handles dominance with same influence factor (issue #7)', () => {
      const objs = [
        new Objective('Objective 1', new NumericalObjectiveData(1, 7)),
        new Objective('Objective 2', new NumericalObjectiveData(1, 7)),
      ];
      const alts = [new Alternative(0, 'Option 1'), new Alternative(0, 'Option 2')];
      const uf = new UserdefinedInfluenceFactor('Weather', [
        new UserdefinedState('', 10),
        new UserdefinedState('', 40),
        new UserdefinedState('', 50),
      ]);

      const outcs = [
        [new Outcome([3, 3, 5], undefined, uf), new Outcome([3])],
        [new Outcome([3]), new Outcome([3])],
      ];
      setAllProcessed(outcs);

      const dr = calculateDominanceRelations(objs, alts, outcs);
      expect(dr.length).toBe(1);
      expect(dr[0]).toMatchObject({ dominating: alts[0], dominated: alts[1] });
    });

    describe('handles dominance with different influence factors', () => {
      it('correctly finds no stochastic dominance (overlapping step functions) #1', () => {
        const objs_1 = [
          new Objective('Objective 1', new NumericalObjectiveData(5, 1)),
          new Objective('Objective 2', new NumericalObjectiveData(1, 7)),
        ];
        const alts_1 = [new Alternative(0, 'Option 1'), new Alternative(0, 'Option 2')];
        const uf1_1 = new UserdefinedInfluenceFactor('IF1', [
          new UserdefinedState('2', 20),
          new UserdefinedState('3', 30),
          new UserdefinedState('5', 50),
        ]);
        const uf2_1 = new UserdefinedInfluenceFactor('IF2', [
          new UserdefinedState('1', 10),
          new UserdefinedState('4', 40),
          new UserdefinedState('5', 50),
        ]);

        const outcs_1 = [
          [new Outcome([2, 3, 5], undefined, uf1_1), new Outcome([4])],
          [new Outcome([1, 4, 5], undefined, uf2_1), new Outcome([3])],
        ];
        setAllProcessed(outcs_1);

        const dr_1 = calculateDominanceRelations(objs_1, alts_1, outcs_1);
        expect(dr_1.length).toBe(0);
      });

      it('correctly finds no stochastic dominance (overlapping step functions) #2', () => {
        const objs_2 = [
          new Objective('Objective 1', new NumericalObjectiveData(5, 1)),
          new Objective('Objective 2', new NumericalObjectiveData(1, 7)),
        ];
        const alts_2 = [new Alternative(0, 'Option 1'), new Alternative(0, 'Option 2')];
        const uf1_2 = new UserdefinedInfluenceFactor('IF1', [
          new UserdefinedState('1', 10),
          new UserdefinedState('3', 40),
          new UserdefinedState('5', 50),
        ]);
        const uf2_2 = new UserdefinedInfluenceFactor('IF2', [
          new UserdefinedState('2', 20),
          new UserdefinedState('4', 30),
          new UserdefinedState('5', 50),
        ]);

        const outcs_2 = [
          [new Outcome([1, 3, 5], undefined, uf1_2), new Outcome([4])],
          [new Outcome([2, 4, 5], undefined, uf2_2), new Outcome([3])],
        ];
        setAllProcessed(outcs_2);

        const dr_2 = calculateDominanceRelations(objs_2, alts_2, outcs_2);
        expect(dr_2.length).toBe(0);
      });

      it('correctly identifies a dominance (stochastic dominance + dominance in other columns)', () => {
        const objs_3 = [
          new Objective('Objective 1', new NumericalObjectiveData(5, 1)),
          new Objective('Objective 2', new NumericalObjectiveData(1, 7)),
        ];
        const alts_3 = [new Alternative(0, 'Option 1'), new Alternative(0, 'Option 2')];
        const uf1_3 = new UserdefinedInfluenceFactor('IF1', [
          new UserdefinedState('1', 5),
          new UserdefinedState('2', 10),
          new UserdefinedState('3', 25),
          new UserdefinedState('4', 40),
          new UserdefinedState('5', 20),
        ]);
        const uf2_3 = new UserdefinedInfluenceFactor('IF2', [
          new UserdefinedState('1', 1),
          new UserdefinedState('2', 9),
          new UserdefinedState('3', 20),
          new UserdefinedState('4', 20),
          new UserdefinedState('5', 50),
        ]);

        const outcs_3 = [
          [new Outcome([1, 2, 3, 4, 5], undefined, uf1_3), new Outcome([4])],
          [new Outcome([1, 2, 3, 4, 5], undefined, uf2_3), new Outcome([3])],
        ];
        setAllProcessed(outcs_3);

        const dr_3 = calculateDominanceRelations(objs_3, alts_3, outcs_3);
        expect(dr_3.length).toBe(1);
        expect(dr_3[0]).toMatchObject({ dominating: alts_3[0], dominated: alts_3[1] });
      });

      it('correctly identifies a dominance (only stochastic dominance)', () => {
        const objs_4 = [new Objective('Objective 1'), new Objective('Objective 2')];
        const alts_4 = [new Alternative(0, 'Option 1'), new Alternative(0, 'Option 2')];
        const uf1_4 = new UserdefinedInfluenceFactor('IF1', [
          new UserdefinedState('Z1', 10),
          new UserdefinedState('Z2', 30),
          new UserdefinedState('Z3', 60),
        ]);
        const uf2_4 = new UserdefinedInfluenceFactor('IF2', [
          new UserdefinedState('Z1', 60),
          new UserdefinedState('Z2', 30),
          new UserdefinedState('Z3', 10),
        ]);

        const outcs_4 = [[new Outcome([40, 60, 80], undefined, uf1_4)], [new Outcome([40, 60, 80], undefined, uf2_4)]];
        setAllProcessed(outcs_4);

        const dr_4 = calculateDominanceRelations(objs_4, alts_4, outcs_4);
        expect(dr_4.length).toBe(1);
        expect(dr_4[0]).toMatchObject({ dominating: alts_4[0], dominated: alts_4[1] });
      });
    });

    it('handles dominance with same influence factor', () => {
      const objs = [
        new Objective('Objective 1', new NumericalObjectiveData(1, 7)),
        new Objective('Objective 2', new NumericalObjectiveData(1, 7)),
      ];
      const alts = [new Alternative(0, 'Option 1'), new Alternative(0, 'Option 2')];
      const uf = new UserdefinedInfluenceFactor('Weather', [
        new UserdefinedState('', 10),
        new UserdefinedState('', 40),
        new UserdefinedState('', 50),
      ]);

      const outcs = [
        [new Outcome([3, 3, 5], undefined, uf), new Outcome([5])],
        [new Outcome([3, 3, 5], undefined, uf), new Outcome([3])],
      ];
      setAllProcessed(outcs);

      const dr = calculateDominanceRelations(objs, alts, outcs);
      expect(dr.length).toBe(1);
      expect(dr[0]).toMatchObject({ dominating: alts[0], dominated: alts[1] });
    });

    describe('indicatorDominance', () => {
      it('handles absence of indicator objectives', () => {
        const objs = [new Objective('z1', new NumericalObjectiveData(6, 1)), new Objective('z1', new NumericalObjectiveData(1, 6))];
        const alts = [new Alternative(0, 'a1'), new Alternative(0, 'a2')];

        const outcs = [
          [new Outcome([3]), new Outcome([3])],
          [new Outcome([1]), new Outcome([3])],
        ];
        setAllProcessed(outcs);

        const dr = calculateDominanceRelations(objs, alts, outcs);
        expect(dr.length).toBe(1);
        expect(dr[0]).toMatchObject({ dominated: alts[0], dominating: alts[1] });
      });

      it('handles same influence factor', () => {
        const objs = [
          new Objective(
            'Objective 1',
            undefined,
            undefined,
            new IndicatorObjectiveData([new Indicator('Indicator 1', 0, 10, '', 10), new Indicator('Indicator 2', 0, 10, '', 10)]),
            ObjectiveType.Indicator
          ),
        ];
        const alts = [new Alternative(0, 'Option 1'), new Alternative(0, 'Option 2')];
        const uf = new UserdefinedInfluenceFactor('Weather', [new UserdefinedState('', 50), new UserdefinedState('', 50)]);

        const outcs = [
          [
            new Outcome(
              [
                [1, 3],
                [1, 4],
              ],
              undefined,
              uf
            ),
          ],
          [
            new Outcome(
              [
                [1, 2],
                [1, 4],
              ],
              undefined,
              uf
            ),
          ],
        ];
        setAllProcessed(outcs);

        const dr = calculateDominanceRelations(objs, alts, outcs);
        expect(dr.length).toBe(1);
        expect(dr[0]).toMatchObject({ dominating: alts[0], dominated: alts[1] });
      });
    });

    it('handles dominance based on indicator', () => {
      const objs = [
        new Objective('z1', new NumericalObjectiveData(0, 10)),
        new Objective(
          'z2',
          undefined,
          undefined,
          new IndicatorObjectiveData([new Indicator('', 0, 10, '', 10), new Indicator('', 5, -4, '', -1)]),
          ObjectiveType.Indicator
        ),
      ];
      const alts = [new Alternative(0, 'a1'), new Alternative(0, 'a2'), new Alternative(0, 'a3'), new Alternative(0, 'a4')];

      const outcs = [
        [new Outcome([3]), new Outcome([[5, 0]])],
        [new Outcome([5]), new Outcome([[5, 0]])],
        [new Outcome([4]), new Outcome([[6, 1]])],
        [new Outcome([3]), new Outcome([[10, -1]])],
      ];
      setAllProcessed(outcs);

      expect(calculateDominanceRelations(objs, alts, outcs)).toEqual([
        { dominated: alts[0], dominating: alts[1] }, // dominated only by number
        { dominated: alts[0], dominating: alts[2] }, // dominated by both
        { dominated: alts[0], dominating: alts[3] }, // dominated only by indicator
      ]);
    });
  });
});
