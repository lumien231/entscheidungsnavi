import { Indicator, ObjectiveInput } from '../classes';
import { getIndicatorAggregationFunction, getIndicatorCoefficientName } from './indicator-aggregation';

describe('getIndicatorCoefficientName', () => {
  it('handles values below 26', () => {
    expect(getIndicatorCoefficientName(0)).toBe('a');
    expect(getIndicatorCoefficientName(10)).toBe('k');
    expect(getIndicatorCoefficientName(25)).toBe('z');
  });

  it('handles values above 25', () => {
    expect(getIndicatorCoefficientName(26)).toBe('aa');
    expect(getIndicatorCoefficientName(260)).toBe('ja');
    expect(getIndicatorCoefficientName(1000)).toBe('alm');
  });
});

describe('getIndicatorAggregationFunction', () => {
  describe('with default aggregation function', () => {
    let fct: (values: ObjectiveInput) => number;
    let fct2: (values: ObjectiveInput) => number;
    beforeEach(() => {
      fct = getIndicatorAggregationFunction([new Indicator('', 1, 7, '', 2), new Indicator('', 5, 1, '', 1)], { worst: 1, best: 7 });
      fct2 = getIndicatorAggregationFunction([new Indicator('', 1, 7, '', 2), new Indicator('', 5, 1, '', 1)], { worst: 7, best: 1 });
    });

    it('throws error on malformed data', () => {
      expect(() => fct([1])).toThrow();
      expect(() => fct([1, 2, 3])).toThrow();
      expect(() => fct(0)).toThrow();
    });

    it('returns NaN if a value is null', () => {
      expect(fct([1, null])).toBe(NaN);
      expect(fct([null, null])).toBe(NaN);
    });

    it('calculates correct aggregations with min < max', () => {
      expect(fct([1, 5])).toBe(1);
      expect(fct([2, 4])).toBeCloseTo(2.166);
      expect(fct([7, 1])).toBe(7);
    });

    it('calculates correct aggregations with min > max', () => {
      expect(fct2([1, 5])).toBe(7);
      expect(fct2([2, 4])).toBeCloseTo(5.833);
      expect(fct2([7, 1])).toBe(1);
    });
  });

  describe('with custom aggregation function', () => {
    let indicators: Indicator[];
    beforeEach(() => {
      indicators = [new Indicator('', 1, 7, '', 2), new Indicator('', 5, 1, '', 1)];
    });

    it('throws error on malformed data', () => {
      const fct = getIndicatorAggregationFunction(indicators, 'a * Ind1 + b * Ind2');
      expect(() => fct([1])).toThrow();
      expect(() => fct([1, 2, 3])).toThrow();
      expect(() => fct(0)).toThrow();
    });

    it('returns NaN if a value is null', () => {
      const fct = getIndicatorAggregationFunction(indicators, 'a * Ind1 + b * Ind2');
      expect(fct([1, null])).toBe(NaN);
      expect(fct([null, null])).toBe(NaN);
    });

    it('works with an additive formula', () => {
      const fct = getIndicatorAggregationFunction(indicators, 'a * Ind1 + b * Ind2');
      expect(fct([1, 1])).toBe(3);
      expect(fct([5, 2])).toBe(12);
    });

    it('works with simple arithmetic operations', () => {
      const fct = getIndicatorAggregationFunction(indicators, 'a * Ind1 + Ind2 / b - 2');
      expect(fct([1, 1])).toBe(1);
      expect(fct([5, 2])).toBe(10);
    });

    it('works with exp and log', () => {
      const fct = getIndicatorAggregationFunction(indicators, 'exp(a * Ind1) + log(Ind2 * b)');
      expect(fct([1, 1])).toBeCloseTo(7.389);
      expect(fct([5, 2])).toBeCloseTo(22027.1589);
    });

    it('throws on invalid formula', () => {
      expect(() => getIndicatorAggregationFunction(indicators, 'a * Ind1 + b * Ind2 + c * Ind3')([1, 1])).toThrow();
      expect(() => getIndicatorAggregationFunction(indicators, 'a * Ind1 + b * Ind2 + exp(abc)')([1, 1])).toThrow();
    });

    it('handles inverted scale [min=best, max=worst]', () => {
      const fct = getIndicatorAggregationFunction(indicators, '-(a * Ind1 + b * Ind2)');
      expect(fct([1, 1])).toBe(-3);
      expect(fct([5, 2])).toBe(-12);
    });
  });
});
