export const NAVI_STEP_ORDER = ['decisionStatement', 'objectives', 'alternatives', 'impactModel', 'results', 'finishProject'] as const;
export type NaviStep = (typeof NAVI_STEP_ORDER)[number];

export function isNaviStep(value: string): value is NaviStep {
  return (NAVI_STEP_ORDER as readonly string[]).indexOf(value) !== -1;
}

export interface NaviSubStep {
  step: NaviStep;
  // Set to undefined for the main step
  subStepIndex?: number;
}
