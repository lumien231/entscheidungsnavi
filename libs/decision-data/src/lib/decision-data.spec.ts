import { omit } from 'lodash';
import { Alternative, UserdefinedState, UserdefinedInfluenceFactor, Objective } from './classes';
import { DecisionData } from './decision-data';

/* eslint-disable deprecation/deprecation */
describe('Data', () => {
  let data: DecisionData;

  beforeEach(() => {
    data = new DecisionData();
  });

  it('add empty Ziel', () => {
    expect(data.ziele.length).toBe(0);
    data.addZiel();
    expect(data.ziele.length).toBe(1);
    expect(data.getAttachedObjectiveData(0)).toEqual([]);
    data.addZiel();
    const objective0WithoutUuid = omit(data.ziele[0], 'uuid');
    const objective1WithoutUuid = omit(data.ziele[1], 'uuid');
    expect(data.ziele.length).toBe(2);
    expect(objective0WithoutUuid).not.toBe(objective1WithoutUuid);
    expect(objective0WithoutUuid).toEqual(objective1WithoutUuid);
  });
  it('delete empty Ziel', () => {
    data.addZiel();
    data.addZiel();
    expect(data.ziele.length).toBe(2);
    data.removeZiel(0);
    expect(data.ziele.length).toBe(1);
  });
  it('add (insert) Ziel', () => {
    data.addZiel({ ziel: new Objective('1') });
    data.addZiel({ ziel: new Objective('2') });
    data.addZiel({ ziel: new Objective('3') });
    data.addZiel({ ziel: new Objective('new'), position: 1 });
    expect(data.ziele[1].name).toBe('new');
  });

  it('add empty Alternative', () => {
    expect(data.alternativen.length).toBe(0);
    data.addAlternative();
    expect(data.alternativen.length).toBe(1);
    data.addAlternative();
    expect(data.alternativen.length).toBe(2);
  });
  it('delete empty Alternative', () => {
    data.addAlternative();
    data.addAlternative();
    expect(data.alternativen.length).toBe(2);
    data.removeAlternative(0);
    expect(data.alternativen.length).toBe(1);
  });
  it('add (insert) Alternative', () => {
    data.addAlternative({ alternative: new Alternative(1, '1') });
    data.addAlternative({ alternative: new Alternative(1, '2') });
    data.addAlternative({ alternative: new Alternative(1, '3') });
    data.addAlternative({ alternative: new Alternative(1, 'new'), position: 1 });
    expect(data.alternativen[1].name).toBe('new');
  });

  it('add empty UserdefinedInfluenceFactor', () => {
    expect(data.unsicherheitsfaktoren.length).toBe(0);
    data.addUnsicherheitsfaktor();
    expect(data.unsicherheitsfaktoren.length).toBe(1);
    data.addUnsicherheitsfaktor();
    expect(data.unsicherheitsfaktoren.length).toBe(2);
  });
  it('delete empty UserdefinedInfluenceFactor', () => {
    data.addUnsicherheitsfaktor();
    data.addUnsicherheitsfaktor();
    expect(data.unsicherheitsfaktoren.length).toBe(2);
    data.removeUnsicherheitsfaktor(0);
    expect(data.unsicherheitsfaktoren.length).toBe(1);
  });
  it('add (insert) UserdefinedInfluenceFactor', () => {
    data.addUnsicherheitsfaktor({ unsicherheitsfaktor: new UserdefinedInfluenceFactor('1') });
    data.addUnsicherheitsfaktor({ unsicherheitsfaktor: new UserdefinedInfluenceFactor('2') });
    data.addUnsicherheitsfaktor({ unsicherheitsfaktor: new UserdefinedInfluenceFactor('3') });
    data.addUnsicherheitsfaktor({ unsicherheitsfaktor: new UserdefinedInfluenceFactor('new') });
    expect(data.unsicherheitsfaktoren[3].name).toBe('new');
    expect(data.unsicherheitsfaktoren[3].probabilities_sum()).toBe(0);
    expect(data.unsicherheitsfaktoren[3].check_probabilities()).toBeFalsy();
  });

  it('set entscheidungsproblem', () => {
    data.entscheidungsproblem = 'foo';
    expect(data.entscheidungsproblem).toBe('foo');
    data.entscheidungsproblem = 'bar';
    expect(data.entscheidungsproblem).toBe('bar');
  });
});

describe('all data', () => {
  let data: DecisionData;

  beforeEach(() => {
    data = new DecisionData();

    data.addZiel({ ziel: new Objective('1') });
    data.addZiel({ ziel: new Objective('2') });
    data.addZiel({ ziel: new Objective('3') });

    data.addAlternative({ alternative: new Alternative(1, 'a') });
    data.addAlternative({ alternative: new Alternative(1, 'b') });
    data.addAlternative({ alternative: new Alternative(1, 'c') });
    data.addAlternative({ alternative: new Alternative(1, 'd') });

    const zustaende = [new UserdefinedState('u1', 30), new UserdefinedState('u2', 70)];

    data.addUnsicherheitsfaktor({ unsicherheitsfaktor: new UserdefinedInfluenceFactor('uf1', zustaende) });
  });

  it('auspraegungen set', () => {
    expect(data.auspraegungen.length).toBe(4);

    data.auspraegungen.forEach(a => {
      expect(a).toBeDefined();
      expect(a.length).toBe(3);
      a.forEach(auspraegung => {
        expect(auspraegung).toBeDefined();
        expect(auspraegung.processed).toBeFalsy();
        auspraegung.checkProcessed();
        expect(auspraegung.processed).toBeFalsy();
        expect(auspraegung.values).toBeDefined();
        expect(auspraegung.values.length).toBe(1);
        expect(auspraegung.values[0]).not.toBeDefined();
      });
    });
  });

  it('remove ziel', () => {
    // mark and check auspraegungen
    data.auspraegungen.forEach(a => {
      expect(a.length === 3);
      a.forEach((auspraegung, idx) => (auspraegung.values[0] = idx));
      expect(a.map(auspraegung => auspraegung.values[0])).toEqual([0, 1, 2]);
    });

    // remove ziel
    data.removeZiel(1);

    // check auspraegungen
    data.auspraegungen.forEach(a => {
      expect(a.length === 2);
      expect(a.map(auspraegung => auspraegung.values[0])).toEqual([0, 2]);
    });
  });

  it('remove alternative', () => {
    // mark and check auspraegungen
    data.auspraegungen.forEach((a, idx) => {
      expect(a.length === 3);
      a.forEach(auspraegung => (auspraegung.values[0] = idx));
    });
    expect(data.auspraegungen.length).toBe(4);
    expect(data.auspraegungen.map(a => a.map(auspraegung => auspraegung.values[0]))).toEqual([
      [0, 0, 0],
      [1, 1, 1],
      [2, 2, 2],
      [3, 3, 3],
    ]);

    // remove alternative
    data.removeAlternative(1);

    // check auspraegungen
    expect(data.auspraegungen.length).toBe(3);
    expect(data.auspraegungen.map(a => a.map(auspraegung => auspraegung.values[0]))).toEqual([
      [0, 0, 0],
      [2, 2, 2],
      [3, 3, 3],
    ]);
  });

  it('unsicherheitsfaktor propabilities', () => {
    expect(data.unsicherheitsfaktoren[0].probabilities_sum()).toBe(100);
    expect(data.unsicherheitsfaktoren[0].check_probabilities).toBeTruthy();
  });

  describe('set unsicherheitsfaktor', () => {
    beforeEach(() => {
      // set unsicherheitsfaktor for all auspraegungen
      data.auspraegungen.forEach(a =>
        a.forEach(auspraegung => {
          auspraegung.setInfluenceFactor(data.unsicherheitsfaktoren[0]);
        })
      );
    });
    it('check length', () => {
      // check values lengths
      data.auspraegungen.forEach(a =>
        a.forEach(auspraegung => {
          expect(auspraegung.values.length).toBe(data.unsicherheitsfaktoren[0].zustaende.length);
        })
      );
    });

    it('check default values', () => {
      data.auspraegungen.forEach(a =>
        a.forEach(auspraegung => {
          expect(auspraegung.values).toEqual([undefined, undefined]);
        })
      );
    });

    it('add zustand default position', () => {
      // set all values to their index
      data.auspraegungen.forEach(a =>
        a.forEach(auspraegung => {
          data.unsicherheitsfaktoren[0].zustaende.forEach((v, idx) => {
            auspraegung.values[idx] = idx;
          });
        })
      );
      // check values [0, 1]
      data.auspraegungen.forEach(a =>
        a.forEach(auspraegung => {
          expect(auspraegung.values).toEqual([0, 1]);
        })
      );
      // add zustand
      data.addZustand({ uf: data.unsicherheitsfaktoren[0], zustand: new UserdefinedState('test name', 20) });
      // check values [0, 1, undefined]
      data.auspraegungen.forEach(a =>
        a.forEach(auspraegung => {
          expect(auspraegung.values).toEqual([0, 1, undefined]);
        })
      );
      expect(data.unsicherheitsfaktoren[0].probabilities_sum()).toBe(120);
      expect(data.unsicherheitsfaktoren[0].check_probabilities()).toBeFalsy();
    });

    it('add zustand at index 1', () => {
      // set all values to their index
      data.auspraegungen.forEach(a =>
        a.forEach(auspraegung => {
          data.unsicherheitsfaktoren[0].zustaende.forEach((v, idx) => {
            auspraegung.values[idx] = idx;
          });
        })
      );
      // check values [0, 1]
      data.auspraegungen.forEach(a =>
        a.forEach(auspraegung => {
          expect(auspraegung.values).toEqual([0, 1]);
        })
      );
      // add zustand
      data.addZustand({ uf: data.unsicherheitsfaktoren[0], zustand: new UserdefinedState('test name', 30), position: 1 });
      // check values [0, undefined, 1]
      data.auspraegungen.forEach(a =>
        a.forEach(auspraegung => {
          expect(auspraegung.values).toEqual([0, undefined, 1]);
        })
      );
      expect(data.unsicherheitsfaktoren[0].probabilities_sum()).toBe(130);
    });

    it('remove zustand 0', () => {
      // set all values to their index
      data.auspraegungen.forEach(a =>
        a.forEach(auspraegung => {
          data.unsicherheitsfaktoren[0].zustaende.forEach((v, idx) => {
            auspraegung.values[idx] = idx;
          });
        })
      );
      // remove zustand 0
      data.removeZustand(data.unsicherheitsfaktoren[0], 0);
      // check values [1]
      data.auspraegungen.forEach(a =>
        a.forEach(auspraegung => {
          expect(auspraegung.values).toEqual([1]);
        })
      );
      expect(data.unsicherheitsfaktoren[0].probabilities_sum()).toBe(70);
    });

    it('remove zustand 1', () => {
      // set all values to their index
      data.auspraegungen.forEach(a =>
        a.forEach(auspraegung => {
          data.unsicherheitsfaktoren[0].zustaende.forEach((v, idx) => {
            auspraegung.values[idx] = idx;
          });
        })
      );
      // remove zustand 1
      data.removeZustand(data.unsicherheitsfaktoren[0], 1);
      // check values [0]
      data.auspraegungen.forEach(a =>
        a.forEach(auspraegung => {
          expect(auspraegung.values).toEqual([0]);
        })
      );
      expect(data.unsicherheitsfaktoren[0].probabilities_sum()).toBe(30);
    });
  });
});
