import { UtilityFunction } from './utility-function';

export class NumericalObjectiveData {
  constructor(
    public from = 1,
    public to = 7,
    public unit = '',
    public utilityfunction: UtilityFunction = new UtilityFunction(),
    public commentFrom?: string,
    public commentTo?: string,
    public explanationFrom?: string,
    public explanationTo?: string
  ) {}

  /* eslint-disable @typescript-eslint/naming-convention */

  /** @deprecated */
  get von() {
    return this.from;
  }
  /** @deprecated */
  set von(val: number) {
    this.from = val;
  }
  /** @deprecated */
  get bis() {
    return this.to;
  }
  /** @deprecated */
  set bis(val: number) {
    this.to = val;
  }
  /** @deprecated */
  get einheit() {
    return this.unit;
  }
  /** @deprecated */
  set einheit(val: string) {
    this.unit = val;
  }
  /** @deprecated */
  get nutzenfunktion() {
    return this.utilityfunction;
  }
  /** @deprecated */
  set nutzenfunktion(val: UtilityFunction) {
    this.utilityfunction = val;
  }
  /** @deprecated */
  get comment_von() {
    return this.commentFrom;
  }
  /** @deprecated */
  set comment_von(val: string) {
    this.commentFrom = val;
  }
  /** @deprecated */
  get comment_bis() {
    return this.commentTo;
  }
  /** @deprecated */
  set comment_bis(val: string) {
    this.commentTo = val;
  }
  /** @deprecated */
  get explanation_von() {
    return this.explanationFrom;
  }
  /** @deprecated */
  set explanation_von(val: string) {
    this.explanationFrom = val;
  }
  /** @deprecated */
  get explanation_bis() {
    return this.explanationTo;
  }
  /** @deprecated */
  set explanation_bis(val: string) {
    this.explanationTo = val;
  }

  /* eslint-enable @typescript-eslint/naming-convention */
}
