export class VerbalObjectiveDataInitParams {
  constructor(public stepNumber?: number, public from?: string, public to?: string) {
    if (stepNumber === undefined) {
      this.stepNumber = 5;
    }
  }

  /** @deprecated */
  get stufen() {
    return this.stepNumber;
  }
  /** @deprecated */
  set stufen(val: number) {
    this.stepNumber = val;
  }
  /** @deprecated */
  get von() {
    return this.from;
  }
  /** @deprecated */
  set von(val: string) {
    this.from = val;
  }
  /** @deprecated */
  get bis() {
    return this.to;
  }
  /** @deprecated */
  set bis(val: string) {
    this.to = val;
  }

  get stepNumberIsValid() {
    return this.stepNumber >= 2 && this.stepNumber <= 7;
  }
  /** @deprecated */
  get stufenIsValid(): boolean {
    return this.stepNumberIsValid;
  }

  // valid if truthy (not empty or undefined)
  get fromIsValid() {
    return !!this.from;
  }
  /** @deprecated */
  get vonIsValid(): boolean {
    return this.fromIsValid;
  }

  // valid if truthy (not empty or undefined)
  get toIsValid() {
    return !!this.to;
  }
  /** @deprecated */
  get bisIsValid(): boolean {
    return this.toIsValid;
  }

  get isValid(): boolean {
    return this.stepNumberIsValid && this.fromIsValid && this.toIsValid;
  }
}
