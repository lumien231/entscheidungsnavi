import { invert } from 'lodash';
import { NaviStep } from '../steps';

export class TeamUUIDs {
  static readonly steps = {
    decisionStatement: 'step-decisionStatement',
    objectives: 'step-objectives',
    alternatives: 'step-alternatives',
    impactModel: 'step-impactModel',
    results: 'step-results',
    finishProject: 'step-finishProject',
  } as const satisfies Record<NaviStep, string>;

  static readonly stepsInverted = invert(TeamUUIDs.steps) as Record<(typeof TeamUUIDs.steps)[NaviStep], NaviStep>;
}
