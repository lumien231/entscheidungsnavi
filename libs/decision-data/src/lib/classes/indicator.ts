export class Indicator {
  constructor(
    public name: string = '',
    // min > max is allowed here (e.g. school grades). min is the worst value
    // from a utility perspective if the coefficient is positive. Else it
    // is the best value.
    public min = 1,
    public max = 7,
    public unit = '',
    // We enforce coefficient >= 0
    public coefficient = 1,
    public comment?: string
  ) {}

  clone() {
    return new Indicator(this.name, this.min, this.max, this.unit, this.coefficient, this.comment);
  }
}
