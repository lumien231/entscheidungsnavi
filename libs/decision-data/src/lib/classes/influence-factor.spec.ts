import { normalizeProbabilities, UserdefinedState } from './userdefined-influence-factor';
import { generatePredefinedIndicatorScenarios } from './predefined-influence-factor';

describe('InfluenceFactor', () => {
  describe('normalizeProbabilities', () => {
    it('normalizes too large numbers', () => {
      const states = [
        new UserdefinedState('', 50),
        new UserdefinedState('', 150),
        new UserdefinedState('', 250),
        new UserdefinedState('', 350),
      ];
      normalizeProbabilities(states);
      expect(states.map(state => state.probability)).toEqual([6, 19, 31, 44]);
    });

    it('normalizes too small numbers', () => {
      const states = [
        new UserdefinedState('', 5),
        new UserdefinedState('', 15),
        new UserdefinedState('', 25),
        new UserdefinedState('', 35),
      ];
      normalizeProbabilities(states);
      expect(states.map(state => state.probability)).toEqual([6, 19, 31, 44]);
    });

    it('handles zeros gracefully', () => {
      const states = [new UserdefinedState('', 0), new UserdefinedState('', 0)];
      normalizeProbabilities(states);
      expect(states.map(state => state.probability)).toEqual([50, 50]);
    });

    it('converts outcome values into combinations with indexes', () => {
      const outcomeValues = [
        [1, 3, 5],
        [0.1, 0.3, 0.5],
        [10, 30, 50],
      ];
      const stateProbs = [0.1, 0.3, 0.6];

      const combinations = generatePredefinedIndicatorScenarios(outcomeValues, stateProbs);
      expect(combinations).toEqual([
        expect.objectContaining({ value: [1, 3, 5], stateIndices: [0, 0, 0] }),
        expect.objectContaining({ value: [1, 3, 0.5], stateIndices: [0, 0, 1] }),
        expect.objectContaining({ value: [1, 3, 50], stateIndices: [0, 0, 2] }),
        expect.objectContaining({ value: [1, 0.3, 5], stateIndices: [0, 1, 0] }),
        expect.objectContaining({ value: [1, 0.3, 0.5], stateIndices: [0, 1, 1] }),
        expect.objectContaining({ value: [1, 0.3, 50], stateIndices: [0, 1, 2] }),
        expect.objectContaining({ value: [1, 30, 5], stateIndices: [0, 2, 0] }),
        expect.objectContaining({ value: [1, 30, 0.5], stateIndices: [0, 2, 1] }),
        expect.objectContaining({ value: [1, 30, 50], stateIndices: [0, 2, 2] }),
        expect.objectContaining({ value: [0.1, 3, 5], stateIndices: [1, 0, 0] }),
        expect.objectContaining({ value: [0.1, 3, 0.5], stateIndices: [1, 0, 1] }),
        expect.objectContaining({ value: [0.1, 3, 50], stateIndices: [1, 0, 2] }),
        expect.objectContaining({ value: [0.1, 0.3, 5], stateIndices: [1, 1, 0] }),
        expect.objectContaining({ value: [0.1, 0.3, 0.5], stateIndices: [1, 1, 1] }),
        expect.objectContaining({ value: [0.1, 0.3, 50], stateIndices: [1, 1, 2] }),
        expect.objectContaining({ value: [0.1, 30, 5], stateIndices: [1, 2, 0] }),
        expect.objectContaining({ value: [0.1, 30, 0.5], stateIndices: [1, 2, 1] }),
        expect.objectContaining({ value: [0.1, 30, 50], stateIndices: [1, 2, 2] }),
        expect.objectContaining({ value: [10, 3, 5], stateIndices: [2, 0, 0] }),
        expect.objectContaining({ value: [10, 3, 0.5], stateIndices: [2, 0, 1] }),
        expect.objectContaining({ value: [10, 3, 50], stateIndices: [2, 0, 2] }),
        expect.objectContaining({ value: [10, 0.3, 5], stateIndices: [2, 1, 0] }),
        expect.objectContaining({ value: [10, 0.3, 0.5], stateIndices: [2, 1, 1] }),
        expect.objectContaining({ value: [10, 0.3, 50], stateIndices: [2, 1, 2] }),
        expect.objectContaining({ value: [10, 30, 5], stateIndices: [2, 2, 0] }),
        expect.objectContaining({ value: [10, 30, 0.5], stateIndices: [2, 2, 1] }),
        expect.objectContaining({ value: [10, 30, 50], stateIndices: [2, 2, 2] }),
      ]);
    });
  });
});
