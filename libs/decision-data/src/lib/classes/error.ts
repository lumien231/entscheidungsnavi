/* ***** Decision Data ***** */
// name
type NameErrorCode = 100;

// decision statement
type DecisionStatementErrorCodes = 105;

// objectives
type ZielErrorCodes = 110 | 111 | 112 | 113 | 114 | 115 | 116 | 117 | 118 | 119;

// influence factors
type UfErrorCodes = 120 | 121 | 122 | 123;

// alternatives
type AlErrorCodes = 130 | 131;

// utility functions
type NfErrorCodes0 = 144 | 145 | 146;
type NfErrorCodes1num = 140 | 141;
type NfErrorCodes1str = 142 | 143;

// tradeoff objective
type Ziel1ErrorCodes = 150 | 151;

// weights
type GewErrorCodes = 160 | 161 | 163 | 164;

type GewPostErrorCodes = 162;

// outcome value
type ValueErrorCodes0 = 170 | 175 | 176 | 177 | 178 | 186;
type ValueErrorCodes1num = 171 | 172 | 173 | 174;

// outcomes
type AusErrorCodes0 = 180;
type AusErrorCodes1str = 181;
type AusErrorCodes2str = 182 | 183 | 185;
type AusErrorCodes3str = 184;

/* ***** other error codes ***** */
type OtherErrorCodes = 200 | 201;

/* ***** separate interface for different argument types ***** */
// error codes without arguments
export interface EC0 {
  code:
    | NameErrorCode
    | DecisionStatementErrorCodes
    | ZielErrorCodes
    | UfErrorCodes
    | AlErrorCodes
    | NfErrorCodes0
    | Ziel1ErrorCodes
    | GewErrorCodes
    | ValueErrorCodes0
    | AusErrorCodes0
    | OtherErrorCodes;
}

// error codes with 1 number argument
export interface EC1num {
  code: NfErrorCodes1num | ValueErrorCodes1num;
  args: [number];
}

// error codes with 1 string argument
export interface EC1str {
  code: NfErrorCodes1str | AusErrorCodes1str | GewPostErrorCodes;
  args: [string];
}

// error codes with 2 string arguments
export interface EC2str {
  code: AusErrorCodes2str;
  args: [string, string];
}

// error codes with 3 string arguments
export interface EC3str {
  code: AusErrorCodes3str;
  args: [string, string, string];
}

// combination of interfaces
export type ErrorMsg = EC0 | EC1num | EC1str | EC2str | EC3str;
