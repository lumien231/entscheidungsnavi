export class UtilityFunction {
  constructor(public c = 0, public precision = 0, public width?: number, public level?: number, public explanation = '') {}

  /** @deprecated */
  get praezisiongrad() {
    return this.precision;
  }
  /** @deprecated */
  set praezisiongrad(val: number) {
    this.precision = val;
  }

  /** @deprecated */
  get breite() {
    return this.width;
  }
  /** @deprecated */
  set breite(val: number) {
    this.width = val;
  }
}
