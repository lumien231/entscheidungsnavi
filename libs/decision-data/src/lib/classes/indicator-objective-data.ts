import { inRange } from 'lodash';
import { AggregationSetting, getIndicatorAggregationFunction } from '../calculation/indicator-aggregation';
import { UtilityFunction } from './utility-function';
import { Indicator } from './indicator';

export class IndicatorObjectiveData {
  constructor(
    public indicators: Indicator[] = [new Indicator(), new Indicator()],
    public utilityfunction = new UtilityFunction(),
    // Set to true to use the custom aggregation formula
    public useCustomAggregation = false,
    public customAggregationFormula = '',
    // worst > best and worst < best are allowed.
    // This is always used if the default aggregation formula is used and otherwise if automaticCustomAggregationLimits is set to false
    public defaultAggregationWorst = 1,
    public defaultAggregationBest = 7,
    public aggregatedUnit = '',
    public stages: { value: number; description: string }[] = [],
    public automaticCustomAggregationLimits = true
  ) {
    if (this.stages.length === 0) {
      this.stages = [
        { value: this.defaultAggregationWorst, description: '' },
        { value: this.defaultAggregationWorst + (this.defaultAggregationBest - this.defaultAggregationWorst) / 2, description: '' },
        { value: this.defaultAggregationBest, description: '' },
      ];
    }
  }

  addIndicator(position: number = this.indicators.length, indicator: Indicator) {
    if (position >= 0 && position <= this.indicators.length) {
      this.indicators.splice(position, 0, indicator);
    }
  }

  removeIndicator(position: number) {
    if (position >= 0 && position < this.indicators.length) {
      this.indicators.splice(position, 1);
    }
  }

  moveIndicator(fromPosition: number, toPosition: number) {
    if (inRange(fromPosition, this.indicators.length) && inRange(toPosition, this.indicators.length)) {
      this.indicators.splice(toPosition, 0, ...this.indicators.splice(fromPosition, 1));
    }
  }

  get aggregationSetting(): AggregationSetting {
    return this.useCustomAggregation
      ? this.customAggregationFormula
      : { worst: this.defaultAggregationWorst, best: this.defaultAggregationBest };
  }

  /**
   * Returns the aggregation function for this indicator objective. Might throw an error
   * if the aggregation function is invalid.
   */
  get aggregationFunction() {
    return getIndicatorAggregationFunction(this.indicators, this.aggregationSetting);
  }

  /**
   * Returns the "best" outcome value for this objective or NaN if an invalid custom aggregation is used.
   */
  get worstValue() {
    return this.calculateWorstBest('worst');
  }

  /**
   * Returns the "worst" outcome value for this objective or NaN if an invalid custom aggregation is used.
   */
  get bestValue() {
    return this.calculateWorstBest('best');
  }

  private calculateWorstBest(mode: 'worst' | 'best'): number {
    if (this.useCustomAggregation && this.automaticCustomAggregationLimits) {
      try {
        const af = this.aggregationFunction;
        return af(this.indicators.map(ind => (mode === 'worst' ? ind.min : ind.max)));
      } catch {
        return NaN;
      }
    } else {
      return mode === 'worst' ? this.defaultAggregationWorst : this.defaultAggregationBest;
    }
  }
}
