import { NoteGroup } from './note-group';
import { Note } from './note';

export class NotePage {
  private static nextNoteId = 0;
  private static nextGroupId = 0;

  /** @deprecated */
  // eslint-disable-next-line @typescript-eslint/naming-convention
  public static reset_counters = NotePage.resetCounters;

  private static getNextNoteId(): number {
    return NotePage.nextNoteId++;
  }

  private static getNextGroupId(): number {
    return NotePage.nextGroupId++;
  }

  public static resetCounters() {
    NotePage.nextNoteId = 0;
    NotePage.nextGroupId = 0;
  }

  constructor(public noteGroups: NoteGroup[] = []) {
    if (noteGroups && noteGroups.length > 0) {
      this.adjustGroupCounter();
      this.adjustNoteCounter();
    }
  }

  /** @deprecated */
  get notizGroups() {
    return this.noteGroups;
  }

  // increase nextGroupId, if there is a NoteGroup with a higher id than the current nextGroupId
  adjustGroupCounter() {
    let newId: number;
    if (this.noteGroups.length > 0) {
      newId = this.noteGroups.map(group => group.id).sort((a: number, b: number) => b - a)[0] + 1;
    } else {
      newId = 0;
    }
    NotePage.nextGroupId = Math.max(NotePage.nextGroupId, newId);
  }

  // increase nextNoteId, if there is a Note with a higher id than the current nextNoteId
  adjustNoteCounter() {
    let newId: number;
    if (this.noteGroups.length > 0) {
      newId = this.noteGroups
        .map(group => {
          const noteIds = group.notes.filter(note => note.id != null).map(note => note.id);
          if (noteIds.length > 0) {
            return noteIds.sort((a: number, b: number) => b - a)[0] + 1;
          } else {
            return 0;
          }
        })
        .reduce((prev: number, curr: number) => Math.max(prev, curr));
    } else {
      newId = 0;
    }
    NotePage.nextNoteId = Math.max(NotePage.nextNoteId, newId);
  }

  /** @deprecated */
  adjustNotizCounter() {
    this.adjustNoteCounter();
  }

  /** create a deep copy of the NotePage */
  clone(groupStartIdx?: number): NotePage {
    // return a deep copy of the NotePage
    return new NotePage(this.noteGroups.slice(groupStartIdx).map((group: NoteGroup) => group.clone()));
  }

  /** create a flat list of Note objects */
  cloneToArray(secondGroupOnly = false): Note[] {
    if (secondGroupOnly) {
      return this.noteGroups[1].cloneToArray();
    } else {
      return this.noteGroups.reduce((list: Note[], group: NoteGroup) => list.concat(group.cloneToArray()), []);
    }
  }

  /** convert the second group to single note groups and return a new NotePage */
  flattenSecondGroup(clone = false): NotePage {
    this.noteGroups[1].cloneToArray();
    const groupArray: NoteGroup[] = [];
    this.noteGroups.forEach((group, idx) => {
      if (idx === 1) {
        groupArray.push(...this.cloneToArray(true).map((note: Note) => new NoteGroup(NotePage.getNextGroupId(), [note], note.name)));
      } else {
        groupArray.push(clone ? group.clone() : group);
      }
    });
    return new NotePage(groupArray);
  }

  // remove empty groups and split up groups with more than one Note
  flattenGroups(): NotePage {
    const list: NoteGroup[] = [];
    this.noteGroups.forEach((group: NoteGroup) => {
      if (group.notes.length === 1) {
        list.push(group);
      } else if (group.notes.length > 1) {
        // Add a new NoteGroup for each Note
        group.notes.forEach((note: Note) => list.push(new NoteGroup(NotePage.getNextGroupId(), [note])));
      }
    });
    return new NotePage(list);
  }

  addNoteGroup(notes?: Note[], groupName?: string, position?: number) {
    const group = new NoteGroup(NotePage.getNextGroupId(), notes, groupName);
    if (position == null || position === this.noteGroups.length) {
      this.noteGroups.push(group);
    } else if (position >= 0) {
      this.noteGroups.splice(position, 0, group);
    } else {
      throw new Error(`invalid position ${position}`);
    }
    return group;
  }

  /** @deprecated */
  addNotizGroup(notes?: Note[], groupName?: string, position?: number) {
    return this.addNoteGroup(notes, groupName, position);
  }

  removeNoteGroup(idx: number) {
    this.noteGroups.splice(idx, 1);
  }

  /** @deprecated */
  removeNotizGroup(idx: number) {
    this.removeNoteGroup(idx);
  }

  // remove all trailing empty NoteGroups
  removeTrailingNoteGroups() {
    while (this.noteGroups.length > 1 && this.noteGroups[this.noteGroups.length - 1].notes.length === 0) {
      this.removeNoteGroup(this.noteGroups.length - 1);
    }
  }

  /** @deprecated */
  removeTrailingNotizGroups() {
    this.removeTrailingNoteGroups();
  }

  // move a Note to another NoteGroups
  swapNoteGroup(oldGroupIdx: number, oldNoteIdx: number, newGroupIdx: number, newNoteIdx: number, noGroupName = false) {
    const note = this.noteGroups[oldGroupIdx].popNote(oldNoteIdx);
    if (newGroupIdx >= this.noteGroups.length) {
      if (noGroupName) {
        this.addNoteGroup([note]);
      } else {
        // add a new group with the name of the note
        this.addNoteGroup([note], note.name);
      }
    } else {
      this.noteGroups[newGroupIdx].addNote(note, newNoteIdx);
    }
  }

  /** @deprecated */
  swapNotizGroup(oldGroupIdx: number, oldNoteIdx: number, newGroupIdx: number, newNoteIdx: number, noGroupName = false) {
    this.swapNoteGroup(oldGroupIdx, oldNoteIdx, newGroupIdx, newNoteIdx, noGroupName);
  }

  // move a NoteGroup
  moveGroup(oldGroupIdx: number, newGroupIdx: number) {
    if (oldGroupIdx !== newGroupIdx) {
      this.noteGroups.splice(newGroupIdx, 0, this.noteGroups.splice(oldGroupIdx, 1)[0]);
    }
  }

  // add a new Note in an additional NoteGroup
  addNote(name: string, groupIdx?: number) {
    const note = new Note(NotePage.getNextNoteId(), name);
    if (groupIdx === -1) {
      // if groupIdx is -1, use the last group
      groupIdx = this.noteGroups.length - 1;
    }
    if (groupIdx != null && this.noteGroups.length > groupIdx && this.noteGroups[groupIdx]) {
      // if groupIdx is defined, add to the specified group
      this.noteGroups[groupIdx].addNote(note);
    } else if (this.noteGroups.length > 0 && this.noteGroups[this.noteGroups.length - 1].notes.length === 0) {
      // if the last noteGroup is empty, add the note to this group
      this.noteGroups[this.noteGroups.length - 1].addNote(note);
    } else {
      // add the note to a new noteGroup
      this.addNoteGroup([note]);
    }
  }

  /** @deprecated */
  addNotiz(name: string, groupIdx?: number) {
    this.addNote(name, groupIdx);
  }
}
