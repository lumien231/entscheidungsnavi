import { v4 as uuidv4 } from 'uuid';
import { cloneDeep, pick } from 'lodash';
import { getOutcomeUtilityFunction } from '../calculation';
import { InfluenceFactor } from './influence-factor';
import { ObjectiveInput, Objective } from './objective';

export class Outcome {
  public processed = false; // the first change of values[] will set this to true

  /**
   * This array contains only one element if no InfluenceFactor is selected or the InfluenceFactor has exactly
   * one condition. Otherwise there will be a value for each condition of the InfluenceFactor.
   * There is always supposed to be at least one value.
   * Null/undefined entries are allowed and indicate not set/processed entries.
   */
  public values: ObjectiveInput[];
  public influenceFactor: InfluenceFactor;

  /** @deprecated */
  get unsicherheitsfaktor() {
    return this.influenceFactor;
  }
  /** @deprecated */
  set unsicherheitsfaktor(val: InfluenceFactor) {
    this.influenceFactor = val;
  }

  /**
   * It is necessary to either input values or an objective in order
   * to initialize the values.
   * @param values
   * @param objective
   * @param uf
   * @param processed
   * @param comment
   */
  constructor(
    values?: ObjectiveInput[],
    objective?: Objective,
    uf?: InfluenceFactor,
    processed = false,
    public comment?: string,
    public uuid = uuidv4()
  ) {
    if (values != null && Array.isArray(values)) {
      this.setValues(values);
    } else if (objective != null) {
      this.initializeValues(objective);
    } else {
      throw new Error('Outcome constructor called with neither values nor an objective');
    }
    this.setInfluenceFactor(uf);
    this.processed = processed;
  }

  private setValues(values: ObjectiveInput[]) {
    if (values.length === 0) {
      throw new Error('Values must never have length zero');
    }
    // Null/undefined values are explicitly permitted here, as they only indicate not processed outcomes.
    this.values = cloneDeep(values);
  }

  private get emptyValue(): ObjectiveInput {
    const reference = this.values[0];
    return Array.isArray(reference) ? reference.map(() => undefined) : undefined;
  }

  get isEmpty() {
    const reference = this.values[0];
    if (Array.isArray(reference)) {
      return this.values.every((indicatorValues: number[]) => indicatorValues.every(value => value == null));
    } else {
      return this.values.every(value => value == null);
    }
  }

  /**
   * Set the influence factor and update the values accordingly
   * @param uf
   */
  setInfluenceFactor(uf: InfluenceFactor) {
    let newLength = uf && uf.states && uf.states.length;
    if (newLength == null || newLength < 1) {
      newLength = 1;
    }
    this.influenceFactor = uf;

    // We change the size of the array according to the influence factor.
    // (we expect values never has length zero and is never null here)
    const diff = this.values.length - newLength;
    if (diff > 0) {
      // too long by diff elements
      this.values.splice(-diff, diff);
    } else if (diff < 0) {
      for (let i = 0; i < -diff; i++) {
        this.values.push(this.emptyValue);
      }
    }
  }

  /**
   * The influence factor has a new state which has to be reflected in the values.
   * @param position the position of the new state
   */
  addUfState(position: number) {
    // We have to infer whether our values are number[] or number.
    this.values.splice(position, 0, this.emptyValue);
    this.processed = false;
  }

  /**
   * The influence factor had one state removed.
   * @param position the position of the removed state
   */
  removeUfState(position: number) {
    this.values.splice(position, 1);
    this.checkProcessed();
  }

  /**
   * Needs to be called whenever the type of the objective changes.
   * @param objective The objective to initialize the values for
   */
  initializeValues(objective: Objective) {
    // We use a function instead of a fixed value, because for indicators the value
    // is an array which is assigned by reference. We want to assign a new array
    // for each outcome value instead of a reference to the same array.
    const getInitValue: () => ObjectiveInput = () => {
      // We always return undefined as content
      if (objective.isIndicator) {
        return objective.indicatorData.indicators.map(() => undefined);
      } else {
        return undefined;
      }
    };
    this.processed = false;
    this.values = this.values == null ? [getInitValue()] : this.values.map(() => getInitValue());
  }

  /**
   * Called whenever there is a new indicator for the associated objective.
   * @param position Where in the indicators-array the new indicator is
   */
  addObjectiveIndicator(position: number) {
    this.values.forEach(value => {
      if (!Array.isArray(value)) {
        throw new Error(`Expected array, got ${typeof value}`);
      }

      value.splice(position, 0, undefined);
    });
    this.processed = false;
  }

  /**
   * Called whenever an indicator is removed from the associated objective.
   * @param position Where in the indicators-array the indicator was
   */
  removeObjectiveIndicator(position: number) {
    this.values.forEach(value => {
      if (!Array.isArray(value)) {
        throw new Error(`Expected array, got ${typeof value}`);
      }

      value.splice(position, 1);
    });
    // The removed indicator may have been the reason processed was false.
    // It may now be true.
    this.checkProcessed();
  }

  /**
   * Called whenever an indicator changes position in the associated objective.
   * @param fromPosition
   * @param toPosition
   */
  moveObjectiveIndicator(fromPosition: number, toPosition: number) {
    this.values.forEach(value => {
      if (!Array.isArray(value)) {
        throw new Error(`Expected array, got ${typeof value}`);
      }

      value.splice(toPosition, 0, ...value.splice(fromPosition, 1));
    });
  }

  /**
   * Check if the values are set according to the influence factor.
   * This includes checking whether the length of the values-array matches
   * the number of states of the uf and whether all values are non-null.
   */
  checkProcessed() {
    if (this.values == null || this.values.length === 0) {
      this.processed = false;
    } else if (this.influenceFactor != null) {
      // Also account for arrays with null-entries
      this.processed =
        this.influenceFactor.states.length ===
        this.values.filter(el => el != null && (typeof el === 'number' || (Array.isArray(el) && el.every(e => e != null)))).length;
    } else {
      const value = this.values[0];
      this.processed = typeof value === 'number' || (Array.isArray(value) && value.every(e => e != null));
    }
  }

  /**
   * Create a clone of this outcome.
   */
  clone(): Outcome {
    return new Outcome(cloneDeep(this.values), null, this.influenceFactor, this.processed, this.comment, this.uuid);
  }

  /**
   * Copy the values of the given outcome into this outcome.
   * @param outcome
   */
  copyBack(outcome: Outcome) {
    this.values = cloneDeep(outcome.values);
    this.influenceFactor = outcome.influenceFactor;
    this.processed = outcome.processed;
    this.comment = outcome.comment;
    this.uuid = outcome.uuid;
  }

  /**
   * Compute the utility for this outcome.
   *
   * @param objective - The objective corresponding to this outcome
   * @returns The utility value
   */
  getUtility(objective: Objective) {
    return getOutcomeUtilityFunction(
      objective.getUtilityFunction(),
      this.influenceFactor?.generateScenarios.bind(this.influenceFactor)
    )(this.values);
  }

  toJSON() {
    return {
      ...pick(this, ['processed', 'values', 'comment', 'uuid']),
      // Convert the influence factor to its ID
      ...(this.influenceFactor && { influenceFactorId: this.influenceFactor.id }),
    };
  }
}
