export class ObjectiveWeight {
  constructor(public value?: number, public precision = 0, public comparisonPointX?: number, public activeReferencePointIndex?: number) {}

  /** @deprecated */
  get praezision() {
    return this.precision;
  }
  /** @deprecated */
  set praezision(val: number) {
    this.precision = val;
  }
}
