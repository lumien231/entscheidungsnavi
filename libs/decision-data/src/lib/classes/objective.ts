import { cloneDeep, flatMapDeep } from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert_unreachable';
import { ListItem } from '../interfaces/listItem';
import { ObjectiveElement, Tree } from '../steps/hint-aspects';
import { getIndicatorUtilityFunction, getNumericalUtilityFunction, getVerbalUtilityFunction } from '../calculation';
import { Interval } from './interval';
import { NumericalObjectiveData } from './numerical-objective-data';
import { VerbalObjectiveData } from './verbal-objective-data';
import { IndicatorObjectiveData } from './indicator-objective-data';

export enum ObjectiveType {
  Numerical,
  Verbal,
  Indicator,
}

/**
 * Denotes the input from which the utility can be calculated.
 * For numerical and verbal objectives, this is a number,
 * for indicator objectives this is an array of numbers.
 */
export type ObjectiveInput = number | number[];

export class Objective implements ListItem {
  constructor(
    public name = '',
    public numericalData: NumericalObjectiveData = new NumericalObjectiveData(),
    public verbalData: VerbalObjectiveData = new VerbalObjectiveData(),
    public indicatorData: IndicatorObjectiveData = new IndicatorObjectiveData(),
    public objectiveType = ObjectiveType.Numerical,
    public displayed = true,
    public comment?: string,
    public aspects?: Tree<ObjectiveElement>,
    public placeholder?: string,
    public uuid = uuidv4()
  ) {
    if (this.aspects == null) {
      this.resetAspects();
    }

    if (numericalData == null) {
      this.numericalData = new NumericalObjectiveData();
    }
    if (verbalData == null) {
      this.verbalData = new VerbalObjectiveData();
    }
    if (indicatorData == null) {
      this.indicatorData = new IndicatorObjectiveData();
    }
  }

  /**
   * Create a clone of this outcome.
   */
  clone(): Objective {
    return cloneDeep(this);
  }

  /** @deprecated */
  get zielNumerisch() {
    return this.numericalData;
  }
  /** @deprecated */
  get zielVerbal() {
    return this.verbalData;
  }
  /** @deprecated */
  get zielIndicator() {
    return this.indicatorData;
  }

  getUtilityFunction() {
    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return getNumericalUtilityFunction(this.numericalData.utilityfunction.c, this.numericalData.from, this.numericalData.to);
      case ObjectiveType.Verbal:
        return getVerbalUtilityFunction(this.verbalData.utilities);
      case ObjectiveType.Indicator:
        return getIndicatorUtilityFunction(
          this.indicatorData.utilityfunction.c,
          this.indicatorData.aggregationFunction,
          this.indicatorData.worstValue,
          this.indicatorData.bestValue
        );
    }
  }

  getAspectsAsArray(): ReadonlyArray<string> {
    const f = (ct: Tree<ObjectiveElement>): Array<Tree<ObjectiveElement>> => {
      return [ct].concat(ct.children !== undefined ? flatMapDeep(ct.children, f) : []);
    };

    return flatMapDeep(this.aspects.children, f)
      .filter(t => t.value.name)
      .map(t => t.value.name);
  }

  resetAspects() {
    this.aspects = new Tree<ObjectiveElement>(new ObjectiveElement(this.name));
  }

  get isNumerical() {
    return this.objectiveType === ObjectiveType.Numerical;
  }
  get isVerbal() {
    return this.objectiveType === ObjectiveType.Verbal;
  }
  get isIndicator() {
    return this.objectiveType === ObjectiveType.Indicator;
  }

  get isScaleReverse() {
    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return this.numericalData.from > this.numericalData.to;
      case ObjectiveType.Verbal:
        return false;
      case ObjectiveType.Indicator:
        return this.indicatorData.worstValue > this.indicatorData.bestValue;
      default:
        return false;
    }
  }

  getRangeInterval(): Interval {
    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return new Interval(+this.numericalData.from, +this.numericalData.to);
      case ObjectiveType.Indicator:
        return new Interval(this.indicatorData.worstValue, this.indicatorData.bestValue);
      case ObjectiveType.Verbal:
        return new Interval(1, this.verbalData.optionCount());
      default:
        throw new Error(`Unsupported objective type: ${this.objectiveType}`);
    }
  }

  unit(): string {
    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return this.numericalData.unit;
      case ObjectiveType.Indicator:
        return this.indicatorData.aggregatedUnit;
      default:
        throw new Error(`Unsupported objective type: ${this.objectiveType}`);
    }
  }

  hasSameScaleAs(otherObjective: Objective): boolean {
    if (this.objectiveType !== otherObjective.objectiveType) {
      return false;
    }

    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return this.numericalData.from === otherObjective.numericalData.from && this.numericalData.to === otherObjective.numericalData.to;
      case ObjectiveType.Indicator:
        return (
          this.indicatorData.indicators.length === otherObjective.indicatorData.indicators.length &&
          this.indicatorData.indicators.every(
            (indicator, index) =>
              indicator.name === otherObjective.indicatorData.indicators[index].name &&
              indicator.min === otherObjective.indicatorData.indicators[index].min &&
              indicator.max === otherObjective.indicatorData.indicators[index].max
          )
        );
      case ObjectiveType.Verbal:
        return (
          this.verbalData.options.length === otherObjective.verbalData.options.length &&
          this.verbalData.options.every((option, index) => option === otherObjective.verbalData.options[index])
        );
      default:
        assertUnreachable(this.objectiveType);
    }
  }
}
