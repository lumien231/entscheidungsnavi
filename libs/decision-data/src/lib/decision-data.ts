import { BehaviorSubject, Subject } from 'rxjs';
import { isRichTextEmpty } from '@entscheidungsnavi/tools';
import { pick } from 'lodash';
import {
  Alternative,
  DecisionQuality,
  DecisionStatement,
  ErrorMsg,
  Indicator,
  NotePage,
  NumericalObjectiveData,
  Objective,
  ObjectiveType,
  ObjectiveWeight,
  Outcome,
  ProjectMode,
  SavedValues,
  TimeRecording,
  UserdefinedInfluenceFactor,
  UserdefinedState,
  UtilityFunction,
  PredefinedInfluenceFactor,
} from './classes';
import * as validations from './validations';
import { TradeoffPointPair, Weights } from './classes/weights';
import { HintAlternatives, Screw } from './steps/hint-alternatives';
import { CommonData } from './export/properties';
import { NaviStep } from './steps/navi-step';
import { ObjectiveAspects, ObjectiveElement } from './steps/hint-aspects';
import { getAlternativeUtilitiesMeta, getUtilityMatrixMeta, getWeightedUtilityMatrixMeta } from './calculation';

export type AttachedObjectiveData = 'ideas' | 'outcomes' | 'weights' | 'utility' | 'scale' | 'comment';

export class DecisionData implements CommonData {
  public objectiveAdded$ = new Subject<number>();
  public objectiveRemoved$ = new Subject<number>();

  decisionProblem = '';
  authorName = '';

  projectMode$ = new BehaviorSubject<ProjectMode>('educational');
  get projectMode() {
    return this.projectMode$.value;
  }
  set projectMode(newMode: ProjectMode) {
    if (newMode !== this.projectMode) {
      this.projectMode$.next(newMode);
    }
  }

  decisionQuality: DecisionQuality = new DecisionQuality();
  decisionStatement: DecisionStatement = new DecisionStatement();
  objectiveHierarchyMainElement: ObjectiveElement = new ObjectiveElement();
  objectives: Objective[] = [];
  weights: Weights = new Weights();

  alternatives: Alternative[] = [];
  influenceFactors: UserdefinedInfluenceFactor[] = [];

  projectNotes = '';
  bonus1 = '';
  bonus2 = '';
  bonus3 = '';

  /**
   * We save one explanation for each step of the entscheidungsnavi.
   */
  stepExplanations: Record<NaviStep, string> = {
    decisionStatement: '',
    objectives: '',
    alternatives: '',
    impactModel: '',
    results: '',
    finishProject: '',
  };

  savedValues: SavedValues = new SavedValues();

  outcomes: Outcome[][] = []; // [#alternativen][#ziele]

  hintAlternatives = new HintAlternatives(this);

  objectiveAspects = new ObjectiveAspects(this);

  timeRecording = new TimeRecording();

  // Indicates which results substep is currently active (or null if none)
  resultSubstepProgress: number = null;

  lastUrl: string;

  // Field that can be used to add additional data to the project that will be saved / loaded.
  extraData: { [key: string]: unknown } = {};

  /* eslint-disable @typescript-eslint/naming-convention */

  /** @deprecated */
  get entscheidungsproblem(): string {
    return this.decisionProblem;
  }

  /** @deprecated */
  set entscheidungsproblem(value: string) {
    this.decisionProblem = value;
  }

  /** @deprecated */
  get ziele(): Objective[] {
    return this.objectives;
  }

  /** @deprecated */
  set ziele(value: Objective[]) {
    this.objectives = value;
  }

  /** @deprecated */
  get alternativen(): Alternative[] {
    return this.alternatives;
  }

  /** @deprecated */
  set alternativen(value: Alternative[]) {
    this.alternatives = value;
  }

  /** @deprecated */
  get unsicherheitsfaktoren(): UserdefinedInfluenceFactor[] {
    return this.influenceFactors;
  }

  /** @deprecated */
  set unsicherheitsfaktoren(value: UserdefinedInfluenceFactor[]) {
    this.influenceFactors = value;
  }

  /** @deprecated */
  get uncertaintyFactors() {
    return this.influenceFactors;
  }

  /** @deprecated */
  get auspraegungen(): Outcome[][] {
    return this.outcomes;
  }

  /** @deprecated */
  set auspraegungen(value: Outcome[][]) {
    this.outcomes = value;
  }

  /** @deprecated */
  get notiz_text(): string {
    return this.projectNotes;
  }

  /** @deprecated */
  set notiz_text(value: string) {
    this.projectNotes = value;
  }

  /** @deprecated */
  get hint_alternative_ideas(): NotePage {
    return this.hintAlternatives.ideas;
  }

  /** @deprecated */
  set hint_alternative_ideas(ideas: NotePage) {
    this.hintAlternatives.ideas = ideas;
  }

  /** @deprecated */
  get hint_alternative_screws(): Screw[] {
    return this.hintAlternatives.screws;
  }

  /** @deprecated */
  set hint_alternative_screws(screws: Screw[]) {
    this.hintAlternatives.screws = screws;
  }

  /** @deprecated */
  get ziel1_idx(): number {
    return this.weights.tradeoffObjectiveIdx;
  }

  /** @deprecated */
  set ziel1_idx(idx: number) {
    this.weights.tradeoffObjectiveIdx = idx;
  }

  /** @deprecated */
  get gewichtung(): ObjectiveWeight[] {
    return this.weights.preliminaryWeights;
  }

  /** @deprecated */
  set gewichtung(weights: ObjectiveWeight[]) {
    this.weights.preliminaryWeights = weights;
  }

  /** @deprecated */
  get gewichtung_post(): ObjectiveWeight[] {
    return this.weights.tradeoffWeights;
  }

  /** @deprecated */
  set gewichtung_post(weights: ObjectiveWeight[]) {
    this.weights.tradeoffWeights = weights;
  }

  /** @deprecated */
  get unverified_gewichtung_post(): boolean[] {
    return this.weights.unverifiedWeights;
  }

  /** @deprecated */
  set unverified_gewichtung_post(values: boolean[]) {
    this.weights.unverifiedWeights = values;
  }

  /** @deprecated */
  get manual_tradeoffs(): TradeoffPointPair[] {
    return this.weights.manualTradeoffs;
  }

  /** @deprecated */
  set manual_tradeoffs(value: TradeoffPointPair[]) {
    this.weights.manualTradeoffs = value;
  }

  /** @deprecated */
  get tradeoff_explanations(): string[] {
    return this.weights.explanations;
  }

  /** @deprecated */
  set tradeoff_explanations(value: string[]) {
    this.weights.explanations = value;
  }

  /* eslint-enable @typescript-eslint/naming-convention */

  constructor(public version = '0.0.0') {}

  reset(version = '0.0.0') {
    this.decisionQuality = new DecisionQuality();
    this.decisionStatement = new DecisionStatement();
    this.decisionProblem = '';
    this.objectiveAspects = new ObjectiveAspects(this);
    this.objectiveHierarchyMainElement = new ObjectiveElement();
    this.objectives = [];
    this.weights.reset();
    this.alternatives = [];
    this.influenceFactors = [];
    this.projectNotes = '';
    this.bonus1 = '';
    this.bonus2 = '';
    this.bonus3 = '';
    this.outcomes = [];
    Object.keys(this.stepExplanations).forEach(key => (this.stepExplanations[key] = ''));
    this.hintAlternatives.reset();
    this.version = version;
    this.lastUrl = null;
    this.resultSubstepProgress = null;
    this.savedValues.projectDescriptionSelectedIndex = 3;
    this.timeRecording = new TimeRecording();
    NotePage.resetCounters();
    this.extraData = {};
  }

  isEducational() {
    return this.projectMode === 'educational';
  }

  isProfessional() {
    return this.projectMode === 'professional';
  }

  isStarter() {
    return this.projectMode === 'starter';
  }

  addObjective(objective?: Objective, position = this.objectives.length) {
    if (objective === undefined) {
      objective = new Objective();
    }

    this.objectives.splice(position, 0, objective); // insert objective at the given position
    this.weights.addObjective(position);
    this.hintAlternatives.addObjective(position);

    // add the new outcome objects for the new objective
    this.outcomes.forEach(row => row.splice(position, 0, new Outcome(null, objective)));

    this.objectiveAdded$.next(position);
  }

  /** @deprecated */
  addZiel({ ziel, position = this.objectives.length }: { ziel?: Objective; position?: number } = {}) {
    this.addObjective(ziel, position);
  }

  moveObjective(from: number, to: number) {
    this.objectives.splice(to, 0, ...this.objectives.splice(from, 1));
    this.weights.moveObjective(from, to);
    this.outcomes.forEach(row => row.splice(to, 0, ...row.splice(from, 1)));
  }

  removeObjective(position: number) {
    this.objectives.splice(position, 1);
    this.weights.removeObjective(position);
    this.hintAlternatives.removeObjective(position);
    this.outcomes.forEach(e => e.splice(position, 1));

    this.objectiveRemoved$.next(position);
  }

  /** @deprecated */
  removeZiel(position: number) {
    this.removeObjective(position);
  }

  changeObjectiveType(position: number, newType: ObjectiveType) {
    // Only change when the newType is in fact different
    if (position >= 0 && position < this.objectives.length && this.objectives[position].objectiveType !== newType) {
      // We change the type of the objective...
      const objective = this.objectives[position];
      objective.objectiveType = newType;

      // ...but also have to update the value entries of the outcomes
      this.outcomes.forEach(ocForAlternative => {
        ocForAlternative[position].initializeValues(objective);
      });
    }
  }

  addAlternative({
    alternative = new Alternative(1),
    position = this.alternatives.length,
  }: { alternative?: Alternative; position?: number } = {}): Alternative {
    if (this.hintAlternatives.screws && alternative.screwConfiguration.length !== this.hintAlternatives.screws.length)
      alternative.screwConfiguration = new Array(this.hintAlternatives.screws.length).fill(null);

    this.alternatives.splice(position, 0, alternative);
    this.outcomes.splice(
      position,
      0,
      this.objectives.map(obj => new Outcome(null, obj))
    );
    return alternative;
  }

  moveAlternative(from: number, to: number) {
    this.alternatives.splice(to, 0, ...this.alternatives.splice(from, 1));
    this.outcomes.splice(to, 0, ...this.outcomes.splice(from, 1));
  }

  removeAlternative(position: number) {
    this.alternatives.splice(position, 1);
    this.outcomes.splice(position, 1);
  }

  /** @deprecated */
  addUnsicherheitsfaktor({
    unsicherheitsfaktor = new UserdefinedInfluenceFactor(),
    position = this.influenceFactors.length,
  }: { unsicherheitsfaktor?: UserdefinedInfluenceFactor; position?: number } = {}) {
    this.addInfluenceFactor(unsicherheitsfaktor, position);
  }

  /** @deprecated */
  addUncertaintyFactor(uf = new UserdefinedInfluenceFactor(), position = this.influenceFactors.length) {
    this.addInfluenceFactor(uf, position);
  }

  addInfluenceFactor(inf = new UserdefinedInfluenceFactor(), position = this.influenceFactors.length) {
    this.influenceFactors.splice(position, 0, inf);
    this.restoreInfluenceFactorIDs();
    return inf;
  }

  /** @deprecated */
  removeUnsicherheitsfaktor(position: number) {
    this.removeInfluenceFactor(position);
  }

  /** @deprecated */
  removeUncertaintyFactor(position: number) {
    this.removeInfluenceFactor(position);
  }

  removeInfluenceFactor(position: number) {
    // reset all outcomes that depend on that uncertainty factor
    this.outcomes.forEach(row => {
      if (row != null) {
        row.forEach(outcome => {
          if (outcome != null && outcome.influenceFactor === this.influenceFactors[position]) {
            outcome.setInfluenceFactor(undefined);
          }
        });
      }
    });
    this.influenceFactors.splice(position, 1);
    this.restoreInfluenceFactorIDs();
  }

  /** @deprecated */
  moveUncertaintyFactor(from: number, to: number) {
    this.moveInfluenceFactor(from, to);
  }

  moveInfluenceFactor(from: number, to: number) {
    this.influenceFactors.splice(to, 0, ...this.influenceFactors.splice(from, 1));
    this.restoreInfluenceFactorIDs();
  }

  // reset uncertainty factor IDs to their index
  restoreInfluenceFactorIDs() {
    this.influenceFactors.forEach((uf: UserdefinedInfluenceFactor, idx: number) => (uf.id = idx));
  }

  /** @deprecated */
  addZustand({
    uf,
    zustand = new UserdefinedState(null, 10),
    position,
  }: {
    uf: UserdefinedInfluenceFactor;
    zustand?: UserdefinedState;
    position?: number;
  }) {
    this.addState(uf, zustand, position);
  }

  addState(uf: UserdefinedInfluenceFactor, state?: UserdefinedState, position?: number) {
    if (uf != null) {
      if (position == null) {
        position = uf.states.length;
      }
      uf.states.splice(position, 0, state);
      this.outcomes.forEach(e =>
        e.forEach(state => {
          if (state.influenceFactor === uf) {
            state.addUfState(position);
          }
        })
      );
    }
  }

  /** @deprecated */
  removeZustand(unsicherheitsfaktor: UserdefinedInfluenceFactor, position: number) {
    this.removeState(unsicherheitsfaktor, position);
  }

  removeState(uf: UserdefinedInfluenceFactor, position: number) {
    if (uf != null && position != null) {
      uf.states.splice(position, 1);
      this.outcomes.forEach(e =>
        e.forEach(state => {
          if (state.influenceFactor === uf) {
            state.removeUfState(position);
          }
        })
      );
    }
  }

  removeAllStates(uf: UserdefinedInfluenceFactor) {
    const length = uf.states.length;
    for (let i = 0; i < length; i++) {
      this.removeState(uf, 0);
    }
  }

  addObjectiveOption(objectivePosition: number, optionPosition?: number, optionName = '') {
    if (objectivePosition >= 0 && objectivePosition < this.objectives.length) {
      this.objectives[objectivePosition].verbalData.addOption(optionPosition, optionName);
    }
  }

  removeObjectiveOption(objectivePosition: number, optionPosition: number) {
    if (objectivePosition >= 0 && objectivePosition < this.objectives.length) {
      this.objectives[objectivePosition].verbalData.removeOption(optionPosition);
      this.outcomes.forEach(e => (e[objectivePosition].processed = false));
    }
  }

  addObjectiveIndicator(objectivePosition: number, indicatorPosition?: number) {
    if (objectivePosition >= 0 && objectivePosition < this.objectives.length) {
      const objInd = this.objectives[objectivePosition].indicatorData;
      // Default to adding it to the end of the list
      if (indicatorPosition == null) {
        indicatorPosition = objInd.indicators.length;
      }
      objInd.addIndicator(indicatorPosition, new Indicator());
      this.outcomes.forEach(ocForAlternative => {
        ocForAlternative[objectivePosition].addObjectiveIndicator(indicatorPosition);
      });
    }
  }

  removeObjectiveIndicator(objectivePosition: number, indicatorPosition: number) {
    if (objectivePosition >= 0 && objectivePosition < this.objectives.length) {
      this.objectives[objectivePosition].indicatorData.removeIndicator(indicatorPosition);
      this.outcomes.forEach(ocForAlternative => {
        ocForAlternative[objectivePosition].removeObjectiveIndicator(indicatorPosition);
      });
    }
  }

  moveObjectiveIndicator(objectivePosition: number, indicatorFromPosition: number, indicatorToPosition: number) {
    if (objectivePosition >= 0 && objectivePosition < this.objectives.length) {
      this.objectives[objectivePosition].indicatorData.moveIndicator(indicatorFromPosition, indicatorToPosition);
      this.outcomes.forEach(ocForAlternative => {
        ocForAlternative[objectivePosition].moveObjectiveIndicator(indicatorFromPosition, indicatorToPosition);
      });
    }
  }

  validateName(): [boolean, ErrorMsg[]] {
    return validations.validateName(this.decisionProblem);
  }

  validateDecisionStatement(): [boolean, ErrorMsg[]] {
    return validations.validateDecisionStatement(this.decisionStatement);
  }

  validateObjectives(): [boolean, ErrorMsg[]] {
    return validations.validateObjectives(this.objectives);
  }

  validateObjectiveScales(): [boolean, ErrorMsg[]] {
    return validations.validateObjectiveScales(this.objectives);
  }

  validateAlternatives(): [boolean, ErrorMsg[]] {
    return validations.validateAlternatives(this.alternatives);
  }

  validateUtilityFunctions(): [boolean, ErrorMsg[], number] {
    return validations.validateUtilityFunctions(this.objectives);
  }

  validateZiel1(): [boolean, ErrorMsg[]] {
    return validations.validateTradeoffObjective(this);
  }

  validateWeights(): [boolean, ErrorMsg[]] {
    return validations.validateWeights(this.weights.preliminaryWeights);
  }

  validateZielgewichtungenPost(ignoreMissing = true): [boolean, ErrorMsg[], number] {
    return validations.validateWeightsPost(this, ignoreMissing);
  }

  /** @deprecated */
  validateValue(value: number, ziel: Objective): [boolean, ErrorMsg[]] {
    return validations.validateValue(value, ziel);
  }

  validateOutcomes(): [boolean, ErrorMsg[]] {
    return validations.validateOutcomes(this);
  }

  validateInfluenceFactors(): [boolean, ErrorMsg[], number] {
    return validations.validateInfluenceFactors(this.influenceFactors);
  }

  getUtilityMatrix() {
    return getUtilityMatrixMeta(this.outcomes, this.objectives);
  }

  getWeightedUtilityMatrix() {
    return getWeightedUtilityMatrixMeta(this.outcomes, this.objectives, this.weights);
  }

  /**
   * Compute utilities for every alternative.
   *
   * @returns utility for every alternative
   */
  getAlternativeUtilities() {
    return getAlternativeUtilitiesMeta(this.outcomes, this.objectives, this.weights);
  }

  // fix for using ngFor with arrays of primitive types (see https://github.com/angular/angular/issues/10423)
  trackByIndex(index: number) {
    return index;
  }

  getAttachedObjectiveData(objectiveIndex: number) {
    const data: AttachedObjectiveData[] = [];

    const objective = this.objectives[objectiveIndex];

    // Alternative Ideas
    const group = this.hintAlternatives.ideas ? this.hintAlternatives.ideas.notizGroups[objectiveIndex] : undefined;
    const hasIdeas = (group ? group.notizen.length : 0) > 0;

    if (hasIdeas) {
      data.push('ideas');
    }

    // Impact Matrix
    const defaultObjective = new NumericalObjectiveData();
    const hasOutcomes = this.outcomes.some(row => row[objectiveIndex].processed);
    const hasCustomScale =
      !objective.isNumerical ||
      objective.numericalData.von !== defaultObjective.von ||
      objective.numericalData.bis !== objective.numericalData.bis;

    if (hasOutcomes) {
      data.push('outcomes');
    }

    if (hasCustomScale) {
      data.push('scale');
    }

    // Objective Weighting
    const defaultWeight = Weights.getDefault();
    const objectiveWeight = this.weights.getWeights()[objectiveIndex];
    const hasCustomWeight =
      objectiveWeight && (objectiveWeight.value !== defaultWeight.value || objectiveWeight.praezision !== defaultWeight.praezision);

    if (hasCustomWeight) {
      data.push('weights');
    }

    // Utility Function
    const defaultUtilityFunction = new UtilityFunction();

    let hasCustomUtilityFunction = false;
    if (objective.isVerbal) {
      hasCustomUtilityFunction =
        objective.verbalData.hasCustomUtilityValues ||
        objective.verbalData.c !== defaultUtilityFunction.c ||
        objective.verbalData.praezision !== defaultUtilityFunction.praezisiongrad;
    } else {
      const utilityFunction = objective.isNumerical
        ? objective.numericalData.nutzenfunktion
        : objective.isIndicator
        ? objective.indicatorData.utilityfunction
        : undefined;
      hasCustomUtilityFunction =
        utilityFunction &&
        (utilityFunction.c !== defaultUtilityFunction.c || utilityFunction.praezisiongrad !== defaultUtilityFunction.praezisiongrad);
    }

    if (hasCustomUtilityFunction) {
      data.push('utility');
    }

    // Comment
    if (!isRichTextEmpty(objective.comment)) {
      data.push('comment');
    }

    // No Data found
    return data;
  }

  getInaccuracies() {
    const inaccuracies = {
      utilityNumericalIndicator: false,
      utilityVerbal: false,
      weights: false,
      probabilities: false,
      userdefinedInfluenceFactors: false,
      predefinedInfluenceFactors: false,
    };

    for (let i = 0; i < this.objectives.length; i++) {
      const objective = this.objectives[i];

      // Utility Functions
      if (objective.isNumerical) {
        if (objective.numericalData.utilityfunction.precision > 0) {
          inaccuracies.utilityNumericalIndicator = true;
        }
      } else if (objective.isVerbal) {
        if (objective.verbalData.precision > 0) {
          inaccuracies.utilityVerbal = true;
        }
      } else if (objective.isIndicator) {
        if (objective.indicatorData.utilityfunction.precision > 0) {
          inaccuracies.utilityNumericalIndicator = true;
        }
      }

      // Weights
      const weight = this.weights.getWeight(i);

      if (weight.precision > 0) {
        inaccuracies.weights = true;
      }
    }

    for (const ocsForAlternative of this.outcomes) {
      for (const outcome of ocsForAlternative) {
        if (outcome.influenceFactor instanceof UserdefinedInfluenceFactor) {
          inaccuracies.userdefinedInfluenceFactors = true;
          inaccuracies.probabilities ||= outcome.influenceFactor.precision > 0;
        } else if (outcome.influenceFactor instanceof PredefinedInfluenceFactor) {
          inaccuracies.predefinedInfluenceFactors = true;
        }
      }
    }

    return inaccuracies;
  }

  findObject(objectId: string) {
    const findInCollection = <T extends { uuid: string }>(collection: T[]) => {
      const index = collection.findIndex(object => object.uuid === objectId);

      if (index < 0) {
        return null;
      }

      return { object: collection[index], index };
    };

    return (
      findInCollection(this.objectives) ??
      findInCollection(this.alternatives) ??
      findInCollection(this.influenceFactors) ??
      findInCollection(this.outcomes.flat())
    );
  }

  /**
   * Returns a subset of properties of this DecisionData object that is relevant for detecting changes made by the user.   *
   * Among others, this excludes the Observables found in this object, and DecisionData references within children.
   */
  getPropertiesForChangeDetection() {
    const data = pick(this, [
      'projectMode',
      'decisionProblem',
      'decisionQuality',
      'decisionStatement',
      'objectiveHierarchyMainElement',
      'objectives',
      'objectiveAspects',
      'weights',
      'alternatives',
      'hintAlternatives',
      'influenceFactors',
      'projectNotes',
      'stepExplanations',
      'outcomes',
      'resultSubstepProgress',
      'savedValues',
    ]);
    for (const key of Object.keys(data)) {
      if (data[key] && typeof data[key] === 'object' && 'toJSON' in data[key] && typeof data[key]['toJSON'] === 'function') {
        data[key] = data[key].toJSON();
      }
    }
    return data;
  }
}
