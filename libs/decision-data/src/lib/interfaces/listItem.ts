export interface ListItem {
  name: string;
  comment?: string;
}

export interface NestedListItem<T> extends ListItem {
  children: T[];
  isGroup(): boolean;
  addChild(child: T): T;
  removeChild(idx: number): T;
}

export type ListItemClass = new () => ListItem;

export type NestedListItemClass<T> = new () => NestedListItem<T>;
