export interface ObjectiveValue {
  value: string;
  isOption: boolean;
}
