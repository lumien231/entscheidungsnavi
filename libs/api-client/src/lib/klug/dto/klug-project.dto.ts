import { KlugProject } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsBoolean, IsDate, IsMongoId, IsOptional, IsString } from 'class-validator';

export class KlugProjectDto implements KlugProject {
  @IsMongoId()
  id: string;

  @IsString()
  token: string;

  @IsOptional()
  @IsString()
  data: string;

  @IsBoolean()
  isOfficial: boolean;

  @IsBoolean()
  finished: boolean;

  @IsBoolean()
  readonly: boolean;

  @IsOptional()
  @IsDate()
  @Type(() => Date)
  expiresAt?: Date;

  @IsDate()
  @Type(() => Date)
  createdAt: Date;

  @IsDate()
  @Type(() => Date)
  updatedAt: Date;
}
