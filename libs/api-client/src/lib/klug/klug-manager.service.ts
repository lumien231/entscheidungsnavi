import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { logError, transformAndValidate } from '@entscheidungsnavi/tools';
import { KlugProjectFilter, KlugProjectSort, PaginationParams } from '@entscheidungsnavi/api-types';
import { KlugProjectDto } from './dto/klug-project.dto';
import { KlugProjectListDto } from './dto/klug-project-list.dto';

@Injectable({
  providedIn: 'root',
})
export class KlugManagerService {
  constructor(private http: HttpClient) {}

  createKlugProject(token: string, isOfficial: boolean, readonly: boolean) {
    return this.http
      .post<KlugProjectDto>(`/api/klug-manager/projects`, { token, isOfficial, readonly })
      .pipe(transformAndValidate(KlugProjectDto), logError());
  }

  updateKlugProject(token: string, update: { token: string; isOfficial: boolean; readonly: boolean }) {
    return this.http.patch<unknown>(`/api/klug-manager/projects/${token}`, update).pipe(transformAndValidate(KlugProjectDto), logError());
  }

  deleteKlugProject(token: string) {
    return this.http.delete<void>(`/api/klug-manager/projects/${token}`).pipe(logError());
  }

  getKlugProjects(filter: KlugProjectFilter, sort: KlugProjectSort, pagination: PaginationParams) {
    return this.http
      .get<unknown>('/api/klug-manager/projects', { params: { ...this.transformFilterForRequest(filter), ...sort, ...pagination } })
      .pipe(transformAndValidate(KlugProjectListDto), logError());
  }

  generateExcelExport(filter: KlugProjectFilter) {
    return this.http
      .post('/api/klug-manager/excel-export', {}, { params: this.transformFilterForRequest(filter), responseType: 'blob' })
      .pipe(logError());
  }

  private transformFilterForRequest(filter: KlugProjectFilter) {
    const transformedFilter: Omit<KlugProjectFilter, 'startDate' | 'endDate'> & { startDate?: string; endDate?: string } = {};

    if (filter.isOfficial != null) {
      transformedFilter.isOfficial = filter.isOfficial;
    }

    if (filter.finished != null) {
      transformedFilter.finished = filter.finished;
    }

    if (filter.startDate != null) {
      transformedFilter.startDate = filter.startDate.toISOString();
    }

    if (filter.endDate != null) {
      transformedFilter.endDate = filter.endDate.toISOString();
    }

    return transformedFilter;
  }
}
