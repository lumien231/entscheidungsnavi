import { QuickstartTag } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsDate, IsDefined, IsInt, IsOptional, IsString, ValidateNested } from 'class-validator';
import { LocalizedStringDto } from '../../common/localized-string.dto';

export class QuickstartTagDto implements QuickstartTag {
  @IsString()
  id: string;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  name: LocalizedStringDto;

  @IsOptional()
  @IsInt()
  weight?: number;

  @IsDate()
  @Type(() => Date)
  createdAt: Date;

  @IsDate()
  @Type(() => Date)
  updatedAt: Date;
}
