import { Type } from 'class-transformer';
import { IsDefined, ValidateNested } from 'class-validator';
import { AbstractSearchableList } from '../../common/abstract-searchable-list';
import { QuickstartProjectDto } from './quickstart-project.dto';

export class QuickstartProjectListDto extends AbstractSearchableList<QuickstartProjectDto> {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => QuickstartProjectDto)
  list: QuickstartProjectDto[];
}
