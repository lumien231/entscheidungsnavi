import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalizedString, QuickstartValue, QuickstartValueTracking } from '@entscheidungsnavi/api-types';
import { logError, transformAndValidate } from '@entscheidungsnavi/tools';
import { map } from 'rxjs/operators';
import { QuickstartProjectListDto } from './dto/quickstart-project-list.dto';
import { QuickstartProjectDto, QuickstartProjectWithDataDto } from './dto/quickstart-project.dto';
import { QuickstartTagListDto } from './dto/quickstart-tag-list.dto';
import { QuickstartTagDto } from './dto/quickstart-tag.dto';
import { QuickstartValueDto, QuickstartValueListDto, QuickstartValueWithStatsListDto } from './dto/quickstart-value.dto';

@Injectable({
  providedIn: 'root',
})
export class QuickstartService {
  constructor(private http: HttpClient) {}

  getProjects(showInvisible = false) {
    return this.http.get<unknown>('/api/quickstart/projects', { params: { showInvisible } }).pipe(
      map(list => ({ list })),
      transformAndValidate(QuickstartProjectListDto),
      logError()
    );
  }

  getProject(handle: string) {
    return this.http
      .get<unknown>(`/api/quickstart/projects/${handle}`)
      .pipe(transformAndValidate(QuickstartProjectWithDataDto), logError());
  }

  updateProject(id: string, update: Partial<{ data: string; name: string; visible: boolean; tags: string[]; shareToken: string }>) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .patch<unknown>(`/api/quickstart/projects/${id}`, update, { headers })
      .pipe(transformAndValidate(QuickstartProjectDto), logError());
  }

  addProject(data: { name: string; data: string; visible: boolean; tags?: string[]; shareToken?: string }) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .post<unknown>('/api/quickstart/projects', data, { headers })
      .pipe(transformAndValidate(QuickstartProjectDto), logError());
  }

  deleteProject(id: string) {
    return this.http.delete(`/api/quickstart/projects/${id}`, { observe: 'response' }).pipe(logError());
  }

  getTags() {
    return this.http.get<unknown>('/api/quickstart/tags').pipe(
      map(list => ({ list })),
      transformAndValidate(QuickstartTagListDto),
      map(quickStartListDto => quickStartListDto.list),
      logError()
    );
  }

  updateTag(id: string, update: { name?: LocalizedString; weight?: number }) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .patch<unknown>(`/api/quickstart/tags/${id}`, update, { headers })
      .pipe(transformAndValidate(QuickstartTagDto), logError());
  }

  addTag(name: LocalizedString, weight?: number) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .post<unknown>('/api/quickstart/tags', { name, weight }, { headers })
      .pipe(transformAndValidate(QuickstartTagDto), logError());
  }

  deleteTag(id: string) {
    return this.http.delete(`/api/quickstart/tags/${id}`, { observe: 'response' }).pipe(logError());
  }

  getValues() {
    return this.http.get<unknown>('/api/quickstart/values').pipe(
      map(list => ({ list })),
      transformAndValidate(QuickstartValueListDto),
      map(list => list.list),
      logError()
    );
  }

  getValuesWithStats() {
    return this.http.get<unknown>('/api/quickstart/values', { params: { includeStats: true } }).pipe(
      map(list => ({ list })),
      transformAndValidate(QuickstartValueWithStatsListDto),
      map(list => list.list),
      logError()
    );
  }

  updateValue(id: string, update: Pick<QuickstartValue, 'name'>) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .patch<unknown>(`/api/quickstart/values/${id}`, update, { headers })
      .pipe(transformAndValidate(QuickstartValueDto), logError());
  }

  addValue(value: Pick<QuickstartValue, 'name'>) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<unknown>('/api/quickstart/values', value, { headers }).pipe(transformAndValidate(QuickstartValueDto), logError());
  }

  deleteValue(id: string) {
    return this.http.delete(`/api/quickstart/values/${id}`, { observe: 'response' }).pipe(logError());
  }

  trackValueUsage(values: QuickstartValueTracking) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<void>('/api/quickstart/values/metrics', values, { headers }).pipe(logError());
  }
}
