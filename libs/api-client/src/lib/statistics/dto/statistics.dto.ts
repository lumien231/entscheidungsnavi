import { Statistics } from '@entscheidungsnavi/api-types';
import { IsNumber } from 'class-validator';

export class StatisticsDto implements Statistics {
  @IsNumber()
  userCount: number;

  @IsNumber()
  projectCount: number;
}
