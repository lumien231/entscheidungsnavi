import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Event, EventAltAndObjExportRequest, EventDefaultExportRequest, EventProjectsExportRequest } from '@entscheidungsnavi/api-types';
import { logError, cachedSwitchAll, transformAndValidate } from '@entscheidungsnavi/tools';
import { combineLatest, EMPTY, map, retry, startWith, Subject } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { EventListDto } from './dto/event-list.dto';
import { EventDto } from './dto/event.dto';

export type CreateEventType = Pick<Event, 'name' | 'code' | 'startDate' | 'endDate'> &
  Partial<Pick<Event, 'projectRequirements' | 'freeTextConfig' | 'questionnaire' | 'questionnaireReleased'>>;

type UpdateEventType = Partial<CreateEventType> & Partial<Pick<Event, 'owner' | 'editors'>>;

@Injectable({
  providedIn: 'root',
})
export class EventManagementService {
  private refreshEvents$ = new Subject<void>();

  readonly events$ = combineLatest([this.authService.user$, this.refreshEvents$.pipe(startWith(null))]).pipe(
    map(([user]) => (user != null ? this.fetchEvents() : EMPTY)),
    cachedSwitchAll()
  );

  constructor(private http: HttpClient, private authService: AuthService) {}

  refreshEvents() {
    this.refreshEvents$.next();
    return this.events$;
  }

  private fetchEvents() {
    return this.http.get<unknown>('/api/events').pipe(
      retry(2),
      map(list => ({ list })),
      transformAndValidate(EventListDto),
      logError()
    );
  }

  createEvent(event: CreateEventType) {
    return this.http.post<unknown>('/api/events', event).pipe(transformAndValidate(EventDto), logError());
  }

  updateEvent(eventId: string, update: UpdateEventType) {
    return this.http.patch<unknown>(`/api/events/${eventId}`, update).pipe(transformAndValidate(EventDto), logError());
  }

  deleteEvent(eventId: string) {
    return this.http.delete<void>(`/api/events/${eventId}`).pipe(logError());
  }

  generateDefaultExport(request: EventDefaultExportRequest) {
    return this.http.post('/api/events/default-export', request, { responseType: 'blob' }).pipe(logError());
  }

  generateAltAndObjExport(request: EventAltAndObjExportRequest) {
    return this.http.post('/api/events/alternatives-and-objectives-export', request, { responseType: 'blob' }).pipe(logError());
  }

  generateProjectsExport(request: EventProjectsExportRequest) {
    return this.http.post('/api/events/projects-export', request, { responseType: 'blob' }).pipe(logError());
  }

  getSubmittedProject(registrationId: string) {
    return this.http.get(`/api/events/submissions/${registrationId}/project`, { responseType: 'text' }).pipe(logError());
  }
}
