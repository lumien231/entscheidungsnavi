import { CsrfToken } from '@entscheidungsnavi/api-types';
import { IsString } from 'class-validator';

export class CsrfTokenDto implements CsrfToken {
  @IsString()
  token: string;
}
