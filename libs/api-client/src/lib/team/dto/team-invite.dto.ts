import { IsDate, IsString } from 'class-validator';
import { TeamInvite as ApiTeamInvite } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';

export class TeamInviteDto implements ApiTeamInvite {
  @IsString()
  email: string;

  @IsString()
  token: string;

  @IsDate()
  @Type(() => Date)
  createdAt: Date;
}
