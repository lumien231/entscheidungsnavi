export const ROLES = ['admin', 'privileged-user', 'event-manager', 'klug-manager'] as const;

export type Role = (typeof ROLES)[number];
