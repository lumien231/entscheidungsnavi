import { Component } from '@angular/core';
import { inRange } from 'lodash';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HelpMenu } from './help';
import { HelpService } from './help.service';

export interface HelpComponentConfig {
  currentMenu$: Observable<HelpMenu>;
  currentPage$: Observable<number>;
}

@Component({
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss'],
  selector: 'dt-help',
})
export class HelpComponent {
  config: HelpComponentConfig;

  @OnDestroyObservable()
  private onDestroy$: Observable<any>;

  currentMenu: HelpMenu;
  currentPageIndex: number;

  openedTab = 0;

  get currentPage() {
    if (this.currentMenu != null && this.currentPageIndex != null && inRange(this.currentPageIndex, this.currentMenu.length)) {
      return this.currentMenu[this.currentPageIndex];
    }
    return null;
  }

  constructor(private helpService: HelpService) {
    this.setComponentConfig();
  }

  setComponentConfig() {
    this.config = this.helpService.getConfig();
    this.config.currentMenu$.pipe(takeUntil(this.onDestroy$)).subscribe(menu => {
      this.currentMenu = menu;
      this.openedTab = 0;
    });
    this.config.currentPage$.pipe(takeUntil(this.onDestroy$)).subscribe(pageIndex => {
      this.currentPageIndex = pageIndex;
    });
  }
}
