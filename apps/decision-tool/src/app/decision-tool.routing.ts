import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { inject, NgModule } from '@angular/core';
import {
  EnforceGradualProgressionGuard,
  projectLoadingGuard,
  ProfessionalModeGuard,
  projectLoadedGuard,
  startPageGuard,
  ValidateDecisionDataGuard,
} from './guards';
import { FinishProjectComponent } from './main/finish-project/finish-project.component';
import { QuickstartComponent } from './main/quickstart';
import { StartComponent } from './main/start';
import { ResetPasswordGuard, ResetPasswordModalComponent } from './main/userarea';
import { NonProfessionalModeGuard } from './guards/professional/non-professional-mode.guard';
import { NavigationLayoutComponent } from './navigation-layout/navigation-layout.component';
import { ProjectLoadingPageComponent } from './project-loading/project-loading-page.component';

const appRoutes: Routes = [
  {
    path: 'start',
    canActivate: [startPageGuard],
    runGuardsAndResolvers: 'always',
    component: StartComponent,
  },
  {
    path: 'loading',
    canActivate: [projectLoadingGuard],
    runGuardsAndResolvers: 'always',
    component: ProjectLoadingPageComponent,
  },
  {
    path: '',
    canActivate: [projectLoadedGuard, () => inject(NonProfessionalModeGuard).canActivate()],
    canActivateChild: [ValidateDecisionDataGuard, EnforceGradualProgressionGuard],
    component: NavigationLayoutComponent,
    children: [
      {
        path: 'entscheidungsfrage',
        redirectTo: 'decisionstatement',
      },
      {
        path: 'decisionstatement',
        loadChildren: () => import('../modules/decision-statement/decision-statement.module').then(m => m.DecisionStatementModule),
      },
      {
        path: 'zielformulierung',
        redirectTo: 'objectives',
      },
      {
        path: 'objectives',
        loadChildren: () => import('../modules/objectives/objectives.module').then(m => m.ObjectivesModule),
      },
      {
        path: 'alternativen',
        redirectTo: 'alternatives',
      },
      {
        path: 'alternatives',
        loadChildren: () => import('../modules/alternatives/alternatives.module').then(m => m.AlternativesModule),
      },
      // -------------- LEGACY: paths to support routes from the old wirkungsprognosen/unsicherheitsfaktoren combination
      {
        path: 'einflussfaktoren',
        redirectTo: 'unsicherheitsfaktoren',
      },
      {
        path: 'unsicherheitsfaktoren',
        children: [
          {
            path: ':id',
            redirectTo: '/impactmodel/uncertaintyfactors/:id',
          },
          {
            path: '**',
            redirectTo: '/impactmodel/uncertaintyfactors',
          },
        ],
      },
      {
        path: 'unsicherheitsfaktor/:id',
        redirectTo: 'impactmodel/uncertaintyfactors/:id',
      },
      {
        path: 'prognosen',
        children: [
          {
            path: '**',
            redirectTo: '/impactmodel',
          },
        ],
      },
      // ----------------------------------------------------------------------------------------------------------------
      {
        path: 'impactmodel',
        loadChildren: () => import('../modules/impact-model/impact-model.module').then(m => m.ImpactModelModule),
      },
      {
        path: 'bewertung',
        children: [
          {
            path: 'nutzenfunktion',
            redirectTo: '/results/steps/1',
          },
          {
            path: 'zielgewichtung',
            redirectTo: '/results/steps/2',
          },
        ],
      },
      {
        path: 'ergebnis',
        redirectTo: 'results',
      },
      { path: 'results', loadChildren: () => import('../modules/results/results.module').then(m => m.ResultsModule) },
      {
        path: 'finishproject',
        component: FinishProjectComponent,
      },
    ],
  },
  {
    path: 'quickstart/:name',
    component: QuickstartComponent,
  },
  {
    path: 'unguided',
    redirectTo: 'professional',
  },
  {
    path: 'professional',
    canMatch: [projectLoadedGuard],
    canActivate: [() => inject(ProfessionalModeGuard).canActivate()],
    component: NavigationLayoutComponent,
    loadChildren: () => import('../modules/professional/professional.module').then(m => m.ProfessionalModule),
  },
  {
    path: 'reset/:token',
    canActivate: [ResetPasswordGuard],
    component: ResetPasswordModalComponent,
  },
  {
    path: '**',
    redirectTo: '/start',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      useHash: false,
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  exports: [RouterModule],
})
export class DecisionToolRoutingModule {}
