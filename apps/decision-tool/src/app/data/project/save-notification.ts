import { HttpErrorResponse } from '@angular/common/http';
import { PROJECT_SIZE_LIMIT } from '@entscheidungsnavi/api-types';
import { textSize } from '@entscheidungsnavi/tools';
import { timeout, throwError, MonoTypeOperatorFunction, mergeMap, of } from 'rxjs';

/**
 * An RxJS operator that throws a timeout error after 30s
 */
export function saveTimeout<T>(): MonoTypeOperatorFunction<T> {
  return timeout({
    each: 30_000,
    with: () => throwError(() => new Error('timeout')),
  });
}

/**
 * Converts an error returned from an online project save function to a message.
 *
 * @param error - The error to be converted
 * @returns The corresponding message
 */
export function saveErrorToMessage(error: HttpErrorResponse | Error) {
  if ((error instanceof HttpErrorResponse && error.status === 413) || error.message === 'size limit exceeded') {
    return $localize`Dein Projekt ist zu groß, um online gespeichert zu werden.
      Falls Du Bilder in Erläuterungsfelder eingefügt hast, versuche diese zu verkleinern oder zu entfernen.
      Alternativ kannst Du das Projekt als Datei exportieren.`;
  } else {
    return $localize`Speichern fehlgeschlagen. Möglicherweise ist Deine Internetverbindung instabil.`;
  }
}

/**
 * An RxJS operator that throws an error when the project string exceeds the size limit.
 */
export function checkOnlineProjectSize() {
  return mergeMap((data: string) => {
    // hard coded 10MB limit
    if (textSize(data) > PROJECT_SIZE_LIMIT) {
      return throwError(() => new Error('size limit exceeded'));
    }
    return of(data);
  });
}
