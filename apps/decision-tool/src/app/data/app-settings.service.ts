import { Injectable, isDevMode } from '@angular/core';
import { isCypressRun } from '@entscheidungsnavi/widgets';
import { PersistentSetting, PersistentSettingParent } from './persistent-setting';

@Injectable({
  providedIn: 'root',
})
@PersistentSettingParent('AppSettings')
export class AppSettingsService {
  /**
   * Show a warning message when the user attempts to skip a substep.
   */
  @PersistentSetting()
  skipSubstepWarning = true;

  /**
   * Whether the help menu is opened on launch.
   */
  @PersistentSetting()
  helpLaunchMode: 'open' | 'closed' | 'restore-last' = isDevMode() ? 'closed' : 'restore-last';

  /**
   * Only used in nightly mode. If true, the warning modal is shown
   * on each app start.
   */
  @PersistentSetting()
  showNightlyWarning = !isCypressRun();

  /**
   * Whether to show buttons to send objectives & alternatives to other navi instances
   */
  @PersistentSetting()
  showSendButtons = false;

  /**
   * Whether to measure time spent per substep
   */
  @PersistentSetting()
  measureTimeSpent = true;
}
