import { ErrorHandler, Injectable, Injector } from '@angular/core';
import * as Sentry from '@sentry/angular';
import { AuthService, UserDto } from '@entscheidungsnavi/api-client';
import { firstValueFrom } from 'rxjs';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ENVIRONMENT } from '../../environments/environment';
import { GlobalErrorService } from './global-error.service';
import { ProjectService } from './project';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  sentryHandler: ErrorHandler;

  constructor(private injector: Injector) {
    this.sentryHandler = Sentry.createErrorHandler({
      showDialog: false,
      logErrors: false,
    });
  }

  async handleError(error: any) {
    if (ENVIRONMENT.sentryDsn) {
      let projectVersion: string = null;
      try {
        const projectService = this.injector.get(ProjectService);
        if (projectService.isProjectLoaded()) {
          const decisionData = this.injector.get(DecisionData);
          projectVersion = decisionData.version;
        }
      } catch (_) {}

      let user: UserDto = null;
      try {
        const authService = this.injector.get(AuthService);
        user = await firstValueFrom(authService.user$);
      } catch (_) {}

      Sentry.withScope(scope => {
        if (projectVersion) {
          scope.setContext('project', {
            version: projectVersion,
          });
        }
        if (user) {
          scope.setUser({
            id: user.id,
          });
        }
        this.sentryHandler.handleError(error);
      });
    }

    // inject manually, since this provider is loaded before services
    const globalErrorService = this.injector.get(GlobalErrorService);
    globalErrorService.handleError(error);
    // pass the error to the console
    console.error(error);
  }
}
