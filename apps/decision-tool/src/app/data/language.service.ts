import { Injectable, Inject, LOCALE_ID } from '@angular/core';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class LanguageService {
  public isEnglish: boolean;

  private _isChangingLanguage = false;
  get isChangingLanguage() {
    return this._isChangingLanguage;
  }

  constructor(@Inject(LOCALE_ID) locale: string, private location: Location) {
    this.isEnglish = !locale.startsWith('de');
  }

  changeLanguage() {
    this._isChangingLanguage = true;
    let url: string;
    if (this.isEnglish) {
      url = '../de';
    } else {
      url = '../en';
    }
    document.location.href = this.location.prepareExternalUrl(url + this.location.path());
  }
}
