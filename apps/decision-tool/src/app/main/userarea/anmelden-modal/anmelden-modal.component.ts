import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, isDevMode } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { MatLegacyDialog as MatDialog, MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { AuthService } from '@entscheidungsnavi/api-client';
import { RequestPasswordResetModalComponent } from '@entscheidungsnavi/widgets';
import { TrackingService } from '../../../data/tracking.service';
import { RegistrierenModalComponent } from '../registrieren-modal/registrieren-modal.component';
import { ENVIRONMENT } from '../../../../environments/environment';

type SeedUser = {
  email: string;
  password: string;
  name: string;
};

@Component({
  templateUrl: 'anmelden-modal.component.html',
  styleUrls: ['anmelden-modal.component.scss'],
})
export class AnmeldenModalComponent implements OnInit {
  loginForm: UntypedFormGroup;

  protected readonly seedUsers: SeedUser[] = [
    {
      email: 'benutzer@entscheidungsnavi.de',
      password: '12345678',
      name: 'Benutzer',
    },
    {
      email: 'events@entscheidungsnavi.de',
      password: '12345678',
      name: 'Events',
    },
    {
      email: 'admin@entscheidungsnavi.de',
      password: '12345678',
      name: 'Admin',
    },
  ];

  constructor(
    private authService: AuthService,
    private matDialog: MatDialog,
    private dialogRef: MatDialogRef<AnmeldenModalComponent>,
    private snackBarService: MatSnackBar,
    private trackingService: TrackingService
  ) {}

  get shouldShowSeedUsers() {
    return isDevMode() || ENVIRONMENT.type === 'preview';
  }

  openRegisterModal() {
    this.matDialog
      .open(RegistrierenModalComponent)
      .afterClosed()
      .subscribe(result => {
        if (result === 1) {
          this.dialogRef.close();
        }
      });
  }

  openPasswordResetModal() {
    this.matDialog.open(RequestPasswordResetModalComponent);
  }

  ngOnInit() {
    this.loginForm = new UntypedFormGroup({
      email: new UntypedFormControl('', [Validators.required]),
      password: new UntypedFormControl('', [Validators.required]),
    });
  }

  close() {
    this.dialogRef.close();
  }

  loginWithSeedUser(user: SeedUser) {
    this.loginForm.controls.email.setValue(user.email);
    this.loginForm.controls.password.setValue(user.password);
    this.onSubmit();
  }

  onSubmit() {
    this.loginForm.updateValueAndValidity();

    if (this.loginForm.valid) {
      this.loginForm.disable({ emitEvent: false });

      const email = this.loginForm.controls.email.value;
      const password = this.loginForm.controls.password.value;

      this.authService.login(email, password).subscribe(
        () => {
          this.close();
          this.snackBarService.open($localize`Du hast Dich erfolgreich eingeloggt!`, undefined, { duration: 4000 });
          this.trackingService.trackEvent('login', { category: 'user' });
        },
        (error: HttpErrorResponse) => {
          this.loginForm.enable();
          console.error(error);
          if (error.status === 401) {
            this.loginForm.setErrors({ 'server-error': 'credentials' });
          } else if (error.status === 429) {
            this.loginForm.setErrors({ 'server-error': 'rate-limit' });
          } else {
            this.loginForm.setErrors({ 'server-error': 'something' });
          }
        }
      );
    }
  }
}
