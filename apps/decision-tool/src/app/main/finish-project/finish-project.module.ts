import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetsModule } from '@entscheidungsnavi/widgets';
import { SharedModule } from '../../../modules/shared/shared.module';
import { FinishProjectComponent } from './finish-project.component';

// We are putting the component in a module so that we can use it in professional.
// In the future we may include other components from ../../main to be reuse somewhere.
// Todo:
@NgModule({
  declarations: [FinishProjectComponent],
  imports: [CommonModule, WidgetsModule, SharedModule],
  exports: [FinishProjectComponent],
})
export class FinishProjectModule {}
