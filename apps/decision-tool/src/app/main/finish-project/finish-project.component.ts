import { Component, TemplateRef, ViewChild } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { NaviStep } from '@entscheidungsnavi/decision-data/steps';
import { DecisionQuality } from '@entscheidungsnavi/decision-data/classes';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { PdfExportModalComponent } from '../../pdf-export-modal/pdf-export-modal.component';
import { ExcelExportService } from '../../excel-export';
import { ProjectService } from '../../data/project';
import { HelpMenuProvider, helpPage } from '../../help/help';
import { NavigationStep, STEPS } from '../../../modules/shared/navigation/navigation-step';
import { NavLine, navLineElement, Navigation } from '../../../modules/shared/navline';

@Component({
  selector: 'dt-finish-project',
  templateUrl: './finish-project.component.html',
  styleUrls: ['./finish-project.component.scss'],
})
export class FinishProjectComponent implements HelpMenuProvider, Navigation {
  navLine = new NavLine({
    middle: [
      navLineElement()
        .label($localize`Projekt exportieren`)
        .icon('file_download', 'right')
        .onClick(() => this.jsonExport())
        .build(),
      navLineElement()
        .label($localize`PDF-Report des gesamten Projektes`)
        .onClick(() => this.pdfExport())
        .build(),
      navLineElement()
        .label($localize`Excel-Export der Ergebnismatrix`)
        .onClick(() => this.excelExport())
        .build(),
      navLineElement()
        .label($localize`Feedback`)
        .onClick(() => this.feedback())
        .build(),
    ],
  });

  helpMenu = {
    educational: [
      helpPage()
        .name($localize`So funktioniert's`)
        .template(() => this.helpContent1Educational)
        .build(),
      helpPage()
        .name($localize`Weitere Hinweise`)
        .template(() => this.helpContent2Educational)
        .build(),
    ],
    starter: [
      helpPage()
        .name($localize`Projekt abschließen`)
        .template(() => this.helpContentStarter)
        .build(),
    ],
  };
  steps: { [key in NaviStep]: NavigationStep };
  decisionQuality: DecisionQuality;

  @ViewChild('helpContentStarter') helpContentStarter: TemplateRef<any>;
  @ViewChild('helpContent1Educational') helpContent1Educational: TemplateRef<any>;
  @ViewChild('helpContent2Educational') helpContent2Educational: TemplateRef<any>;

  constructor(
    private excelExportService: ExcelExportService,
    private projectService: ProjectService,
    private dialog: MatDialog,
    private decisionData: DecisionData
  ) {
    this.steps = STEPS;
    this.decisionQuality = this.decisionData.decisionQuality;
  }

  excelExport() {
    this.excelExportService.performExcelExport(false);
  }

  pdfExport() {
    this.dialog.open(PdfExportModalComponent);
  }

  jsonExport() {
    this.projectService.getProject().exportFile();
  }

  feedback() {
    window.open('https://entscheidungsnavi.de/contact', '_blank');
  }
}
