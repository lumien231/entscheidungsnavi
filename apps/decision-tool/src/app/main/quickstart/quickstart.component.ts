import { Component } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { first } from 'rxjs/operators';

/**
 * This is just leftover code to still support older quickstart project links
 */
@Component({
  template: ` <h4 i18n>Lade Themenprojekt</h4> `,
})
export class QuickstartComponent {
  constructor(route: ActivatedRoute, router: Router) {
    route.params.pipe(first()).subscribe(async (params: Params) => {
      await router.navigate(['start'], {
        queryParams: {
          quickstart: params['name'],
        },
      });
    });
  }
}
