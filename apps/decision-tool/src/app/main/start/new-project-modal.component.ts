import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatLegacyDialogRef as MatDialogRef, MatLegacyDialog } from '@angular/material/legacy-dialog';
import { ProjectMode } from '@entscheidungsnavi/decision-data/classes';
import { noop } from 'lodash';
import { AuthService, OnlineProjectsService } from '@entscheidungsnavi/api-client';
import { ConfirmModalComponent, ConfirmModalData, createControlDependencyWith } from '@entscheidungsnavi/widgets';
import { Observable, OperatorFunction, catchError, filter, map, of, switchMap } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { ProjectService } from '../../data/project';
import { TemplatesService, TemplateType } from '../../data/templates.service';
import { OnlineProjectManagementService } from '../../navigation/projects/project-management.service';

@Component({
  templateUrl: './new-project-modal.component.html',
  styleUrls: ['./new-project-modal.component.scss'],
})
export class NewProjectModalComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  projectForm = this.fb.group({
    name: [''],
    mode: this.fb.control<ProjectMode>(null, Validators.required),
    template: this.fb.control<TemplateType | 'blank'>(null, Validators.required),
  });

  constructor(
    private dialog: MatLegacyDialog,
    private modal: MatDialogRef<NewProjectModalComponent>,
    private projectService: ProjectService,
    private authService: AuthService,
    private onlineProjectsService: OnlineProjectsService,
    private onlineProjectManagementService: OnlineProjectManagementService,
    templatesService: TemplatesService,
    private fb: FormBuilder
  ) {
    // This starts the request for simple templates so they are immediately available
    // without an loading indicator on fast connections.
    templatesService.availableTemplates.subscribe({ error: noop });
  }

  ngOnInit() {
    createControlDependencyWith(
      this.projectForm.controls.mode,
      this.projectForm.controls.template,
      mode => mode === 'starter',
      this.onDestroy$
    );
  }

  close(createdProject: boolean) {
    this.modal.close(createdProject);
  }

  createProject() {
    if (this.projectForm.invalid || this.projectForm.disabled) return;

    const projectName = this.projectForm.value.name || $localize`Unbenanntes Projekt`;

    const observable =
      this.projectForm.value.mode === 'starter' && this.projectForm.value.template !== 'blank'
        ? this.projectService.loadTemplate(this.projectForm.value.template, projectName)
        : this.projectService.newProject(projectName, this.projectForm.value.mode);

    this.projectForm.disable({ emitEvent: false });
    observable.pipe(filter(Boolean), this.trySaveOnline(projectName)).subscribe(isSavedOnline => {
      if (!isSavedOnline) {
        this.dialog.open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
          data: {
            prompt: $localize`Du hast ein lokales Projekt erstellt.
            Es wird NICHT AUTOMATISCH GESPEICHERT.
            Du musst Deinen Fortschritt selbst regelmäßig Speichern.
            Alternativ kannst Du Dich anmelden und Dein Projekt online abspeichern.`,
            title: $localize`Lokales Projekt`,
            buttonDeny: $localize`Verstanden`,
          },
        });
      }

      this.close(true);
    });
  }

  private trySaveOnline(projectName: string): OperatorFunction<unknown, boolean> {
    return switchMap(() => {
      if (this.authService.loggedIn) {
        return this.onlineProjectsService.getProjectList().pipe(
          switchMap(projectList => {
            const lowerCaseNames = projectList.list.map(project => project.name.toLowerCase());
            let newProjectName = projectName;
            for (let i = 1; lowerCaseNames.includes(newProjectName.toLowerCase()); i++) {
              newProjectName = projectName + ` (${i})`;
            }
            return this.onlineProjectManagementService.saveAs(newProjectName);
          }),
          map(() => true),
          catchError(() => of(false))
        );
      } else {
        return of(false);
      }
    });
  }
}
