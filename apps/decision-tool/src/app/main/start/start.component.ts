import { Component, TemplateRef, ViewChild } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { ActivatedRoute } from '@angular/router';
import { Observable, first, of, switchMap, takeUntil } from 'rxjs';
import {
  AuthService,
  OnlineProjectsService,
  ProjectListDto,
  QuickstartProjectListDto,
  QuickstartService,
} from '@entscheidungsnavi/api-client';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { supportedBrowsersRegexp } from '../../../supported-browsers';
import { StartupService } from '../../data/startup.service';
import { ENVIRONMENT } from '../../../environments/environment';
import { ProjectListModalComponent } from '../../navigation';
import { NewProjectModalComponent } from './new-project-modal.component';

@Component({
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss'],
})
export class StartComponent {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  unsupportedBrowser: boolean;

  isOffline = false;
  quickstartProjects: QuickstartProjectListDto;
  onlineProjects: ProjectListDto;

  @ViewChild('helpContent') helpContent: TemplateRef<any>;

  constructor(
    private dialog: MatDialog,
    activatedRoute: ActivatedRoute,
    startupService: StartupService,
    private quickstartProjectsService: QuickstartService,
    private onlineProjectsService: OnlineProjectsService,
    private authService: AuthService
  ) {
    this.unsupportedBrowser = !supportedBrowsersRegexp.test(navigator.userAgent);

    activatedRoute.queryParams.pipe(first()).subscribe(params => {
      startupService.autorun(params);
    });

    this.quickstartProjectsService.getProjects().subscribe({
      next: list => (this.quickstartProjects = list),
      error: () => (this.isOffline = true),
    });

    this.authService.loggedIn$
      .pipe(
        switchMap(loggedIn => (loggedIn ? this.onlineProjectsService.getProjectList() : of(null))),
        takeUntil(this.onDestroy$)
      )
      .subscribe({
        next: list => (this.onlineProjects = list),
        error: () => (this.isOffline = true),
      });
  }

  newProjectClick() {
    this.dialog.open(NewProjectModalComponent);
  }

  get isNightlyVersion() {
    return ENVIRONMENT.nightly;
  }

  openTemplatesModal(): void {
    if (this.isOffline || this.quickstartProjects == null) {
      return;
    }

    this.dialog.open(ProjectListModalComponent, {
      data: {
        quickstart: true,
        projectList: this.quickstartProjects,
      },
    });
  }

  openProjectList() {
    if (this.isOffline || this.onlineProjects == null) return;

    this.dialog.open(ProjectListModalComponent, { data: { quickstart: false, projectList: this.onlineProjects } });
  }
}
