import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { range } from 'lodash';
import { TCreatedPdf } from 'pdfmake/build/pdfmake';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { validateTradeoffObjective, validateWeights } from '@entscheidungsnavi/decision-data/validations';
import { MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { AuthService } from '@entscheidungsnavi/api-client';
import { Observable, first, takeUntil } from 'rxjs';
import { RobustnessWorkerResult, RobustnessWorkerService } from '../../worker/robustness-worker.service';
import { PdfExportService, PdfMode } from './pdf-export/pdf-export.service';

@Component({
  templateUrl: './pdf-export-modal.component.html',
  styleUrls: ['./pdf-export-modal.component.scss'],
  providers: [RobustnessWorkerService],
})
export class PdfExportModalComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  projectTitle: string;
  date: Date;
  author: string;
  reportType: PdfMode;

  state: 'start' | 'performingRc' | 'processing' | 'done';
  pdfDoc: TCreatedPdf; // The pdf document after it has been created
  rcProgress = 0; // Progress of the robustness check

  // Static settings
  private rcSteps = 100_000;

  get canClose() {
    return this.state === 'start' || this.state === 'done';
  }

  get noWorker() {
    return this.robustnessWorker.noWorker;
  }

  constructor(
    private decisionData: DecisionData,
    private authService: AuthService,
    private pdfExportService: PdfExportService,
    private dialogRef: MatDialogRef<PdfExportModalComponent>,
    private cdRef: ChangeDetectorRef,
    private robustnessWorker: RobustnessWorkerService
  ) {}

  ngOnInit() {
    // Initialize the properties
    this.projectTitle = this.decisionData.decisionProblem;
    this.date = new Date();
    this.reportType = PdfMode.COMPLETE;
    this.state = 'start';

    this.authService.user$.pipe(first()).subscribe(user => (this.author = user?.name || user?.email || ''));

    this.robustnessWorker.onProgressUpdate$.pipe(takeUntil(this.onDestroy$)).subscribe(progress => this.onWorkerProgressUpdate(progress));
    this.robustnessWorker.onFinished$.pipe(takeUntil(this.onDestroy$)).subscribe(result => this.onWorkerResult(result));
  }

  tryClose() {
    if (this.canClose) {
      this.dialogRef.close();
    }
  }

  submit() {
    // Skip the robustness check if there are no valid objective weights
    if (
      this.reportType === PdfMode.REDUCED ||
      !validateTradeoffObjective(this.decisionData)[0] ||
      !validateWeights(this.decisionData.weights.getWeights())[0]
    ) {
      this.generatePdf();
    } else if (
      this.robustnessWorker.startWorker({
        iterationCount: this.rcSteps,
        selectedAlternatives: range(this.decisionData.alternatives.length),
        parameters: {
          influenceFactorScenarios: {
            predefined: true,
            userdefinedIds: this.decisionData.influenceFactors.map(influenceFactor => influenceFactor.id),
          },
          probabilities: true,
          utilityFunctions: true,
          objectiveWeights: true,
        },
      })
    ) {
      this.state = 'performingRc';
      this.rcProgress = 0;
    }
  }

  private onWorkerProgressUpdate(progress: number) {
    this.rcProgress = progress;
    this.cdRef.detectChanges();
  }

  private onWorkerResult(result: RobustnessWorkerResult) {
    // Calculate score (average position)
    const scores: number[] = result.alternatives.map(row => row.positionDistribution.reduce((sum, v, i) => sum + v * (i + 1)));
    // Package for the pdf-report
    const rcResult = result.alternatives.map((alternative, index) => ({
      minUtility: alternative.minUtility,
      maxUtility: alternative.maxUtility,
      score: scores[index],
    }));
    this.generatePdf(rcResult);
  }

  private async generatePdf(rcResult?: Array<{ minUtility: number; maxUtility: number; score: number }>) {
    this.state = 'processing';
    this.pdfDoc = await this.pdfExportService.generatePdfReport(
      this.reportType,
      this.projectTitle,
      this.author.length > 0 ? this.author : undefined,
      this.date,
      rcResult,
      this.rcSteps
    );
    this.state = 'done';
  }
}
