import { Injectable } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { PersistentSetting, PersistentSettingParent } from '../data/persistent-setting';
import { AppSettingsService } from '../data/app-settings.service';
import { OnlineProject, ProjectService } from '../data/project';

@Injectable({ providedIn: 'root' })
@PersistentSettingParent('SideBySideService')
export class SideBySideService {
  @PersistentSetting()
  private isHelpOpen = false;
  private isHistoryOpen = false;

  get state(): 'help' | 'history' | 'closed' {
    if (this.isHistoryOpen) return 'history';
    else if (this.isHelpOpen) return 'help';
    else return 'closed';
  }

  get isOpen() {
    return this.state !== 'closed';
  }

  constructor(appSettings: AppSettingsService, projectService: ProjectService, decisionData: DecisionData) {
    if (appSettings.helpLaunchMode === 'open') {
      this.isHelpOpen = true;
    } else if (appSettings.helpLaunchMode !== 'restore-last') {
      this.isHelpOpen = false;
    }

    projectService.project$.subscribe(newProject => {
      if (newProject && !(newProject instanceof OnlineProject)) {
        this.isHistoryOpen = false;
      }
    });

    decisionData.projectMode$.subscribe(newProjectMode => {
      if (newProjectMode === 'professional') {
        this.isHelpOpen = false;
      }
    });
  }

  openHelp() {
    this.isHelpOpen = true;
  }

  openHistory() {
    this.isHistoryOpen = true;
  }

  close() {
    // Close the top most window
    switch (this.state) {
      case 'history':
        this.isHistoryOpen = false;
        break;
      case 'help':
        this.isHelpOpen = false;
        break;
    }
  }
}
