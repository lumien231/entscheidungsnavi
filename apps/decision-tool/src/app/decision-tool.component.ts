import { Component, HostListener, TemplateRef, isDevMode } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { QuillConfigurationService } from '@entscheidungsnavi/widgets';
import { DebugService } from './debug/debug.service';
import { ProjectService } from './data/project';
import { LanguageService } from './data/language.service';

@Component({
  selector: 'dt-app',
  templateUrl: './decision-tool.component.html',
  styleUrls: ['./decision-tool.component.scss'],
})
export class DecisionToolComponent {
  componentHasDebugTemplate = false;
  debugTemplate: () => TemplateRef<any>;

  constructor(
    public debugService: DebugService,
    cfgQuillService: QuillConfigurationService,
    meta: Meta,
    private projectService: ProjectService,
    private languageService: LanguageService
  ) {
    cfgQuillService.configureQuill();
    meta.addTag({
      name: 'description',
      // eslint-disable-next-line max-len
      content: $localize`Entscheidungsprobleme werden häufig nicht gut strukturiert und aufgrund psychologisch bedingter Faktoren durch irrationale Verzerrungen beeinflusst. Trainiere deshalb Deine Entscheidungskompetenz und lasse Dich vom Entscheidungsnavi zu einer guten Entscheidung führen!`,
    });
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeUnload(event: BeforeUnloadEvent) {
    const project = this.projectService.getProject();

    if (project && !project.isProjectSaved() && !this.languageService.isChangingLanguage && !isDevMode()) {
      event.returnValue = 'Deine eingegebenen Daten gehen beim Verlassen der Seite verloren.';
    }
  }
}
