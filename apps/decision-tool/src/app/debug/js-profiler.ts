export function isJSProfilerAvailable() {
  return console.profile != null;
}

export function startJSProfile(name: string) {
  if (isJSProfilerAvailable()) {
    console.profile(name);
  }
}

export function stopJSProfile(name: string) {
  if (isJSProfilerAvailable()) {
    console.profileEnd(name);
  }
}
