import { ApplicationRef, ChangeDetectorRef, Component, DoCheck, NgZone, OnInit } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { NAVI_STEP_ORDER } from '@entscheidungsnavi/decision-data/steps';
import { Router } from '@angular/router';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { MatLegacyRadioChange as MatRadioChange } from '@angular/material/legacy-radio';
import { map } from 'rxjs';
import { CurrentProgressService } from '../data/current-progress.service';
import { TimeTrackingService } from '../data/time-tracking.service';
import { ENVIRONMENT } from '../../environments/environment';
import 'zone.js';
import { ServiceWorkerService, UpdateState } from '../../services/service-worker.service';
import { ProjectService } from '../data/project';
import { NightlyWarningModalComponent } from '../nightly/nightly-warning-modal/nightly-warning-modal.component';
import { ModeTransitionService } from '../../modules/shared/mode-transition/mode-transition.service';
import { EnvironmentType } from '../../environments/environment-types';
import { AutoSaveService } from '../data/project/auto-save.service';
import { DebugService } from './debug.service';
import { startJSProfile, stopJSProfile } from './js-profiler';

/**
 * Component Debug Templates:
 * - If the Component is the target of a route just implement the Debug Interface and set hasDebugTemplate to true.
 *   | Example: RobustheitstestComponent
 * - If the Component is not the target of a route call DebugService.registerFleetingTemplate in ngOnInit
 *   and DebugService.removeFleetingTemplate in ngOnDestroy with the debug template and the component instance.
 *   | Example:  ObjectiveAspectHierarchyComponent
 */
@Component({
  selector: 'dt-debug',
  templateUrl: './debug.component.html',
  styleUrls: ['./debug.component.scss'],
})
export class DebugComponent implements DoCheck, OnInit {
  indicatorOpacity = 0;
  indicatorCounter = 0;

  logCDDetails = false;

  profiling = false;
  cdRuns = -1;
  avgCdTime = -1;

  open = false;

  modeRadioGroup: UntypedFormGroup;

  get environmentType() {
    return ENVIRONMENT.type;
  }
  set environmentType(value: EnvironmentType) {
    ENVIRONMENT.type = value;
  }

  get timeTrackingActive() {
    return this.timeTrackingService.timeTrackingEnabled;
  }

  lastDecisionDataChangeEventTime$ = this.timeTrackingService.decisionDataChanged().pipe(map(() => new Date()));

  constructor(
    private zone: NgZone,
    private modeTransitionService: ModeTransitionService,
    private appRef: ApplicationRef,
    private cdRef: ChangeDetectorRef,
    protected debugService: DebugService,
    private decisionData: DecisionData,
    private timeTrackingService: TimeTrackingService,
    protected currentProgressService: CurrentProgressService,
    protected projectService: ProjectService,
    private router: Router,
    private dialog: MatDialog,
    protected serviceWorkerService: ServiceWorkerService,
    protected autoSaveService: AutoSaveService
  ) {}

  get fleetingDebugTemplates() {
    return this.debugService.fleetingComponentDebugTemplates;
  }

  ngOnInit() {
    {
      const modeControl = new UntypedFormControl(this.decisionData.projectMode);
      this.modeRadioGroup = new UntypedFormGroup({
        currentMode: modeControl,
      });

      this.decisionData.projectMode$.subscribe(mode => {
        modeControl.setValue(mode);
      });
    }
  }

  valueChange(change: MatRadioChange) {
    // Reset the default change to the target mode since we may not navigate there.
    this.modeRadioGroup.get('currentMode').reset(this.decisionData.projectMode);
    // TODO: Is there a better solution?
    change.source._inputElement.nativeElement.checked = false;

    const nextMode = change.value;

    if (nextMode === 'starter') {
      // Unsupported transition!
      console.warn('DebugComponent performed an unsupported transition.');
      this.decisionData.projectMode = 'starter';
    } else {
      let transition;

      if (nextMode === 'educational') {
        transition = this.modeTransitionService.transitionIntoEducational();
      } else {
        transition = this.modeTransitionService.transitionIntoProfessional();
      }

      transition.then(transitioned => {
        if (transitioned) this.modeRadioGroup.get('currentMode').setValue(this.decisionData.projectMode);
      });
    }
  }

  resetCounter() {
    this.indicatorCounter = -1;
  }

  ngDoCheck() {
    if (this.logCDDetails && !this.profiling && Zone.currentTask) {
      console.log('Source: ' + Zone.currentTask.source);
      this.renderLongStackTrace();
    }

    if (this.open && this.indicatorOpacity < 1 && !this.profiling) {
      this.indicatorOpacity += 0.05;
      this.indicatorCounter++;

      this.zone.runOutsideAngular(() => {
        setTimeout(() => {
          this.indicatorOpacity -= 0.05;
          this.cdRef.detectChanges();
        }, 1000);
      });
      this.cdRef.detectChanges();
    }
  }

  async profileActiveRoute() {
    startJSProfile('Routing');

    const url = this.router.url;

    const runCount = 5;

    let sum = 0;
    for (let i = 0; i < runCount; i++) {
      await this.router.navigateByUrl('/dummy');
      this.appRef.tick();
      const begin = performance.now();
      await this.router.navigateByUrl(url);
      this.appRef.tick();

      sum += performance.now() - begin;
    }
    stopJSProfile('Routing');

    console.log('Average: ' + sum / runCount + 'ms');
  }

  timeChangeDetection(doProfile: boolean) {
    this.profiling = true;

    const record = doProfile;
    const profileName = 'Change Detection';

    this.cdRuns = this.avgCdTime = -1;
    this.cdRef.detectChanges();

    if (record) {
      startJSProfile(profileName);
    }
    const start = performance.now();
    let numTicks = 0;
    while (numTicks < 5 || performance.now() - start < 500) {
      this.appRef.tick();
      numTicks++;
    }
    const end = performance.now();
    if (record) {
      stopJSProfile(profileName);
    }
    const msPerTick = (end - start) / numTicks;

    this.cdRuns = numTicks;
    this.avgCdTime = msPerTick;

    this.profiling = false;
  }

  renderLongStackTrace() {
    const frames: { error: { stack: string } }[] = (Zone.currentTask.data as any).__creationTrace__;

    if (!frames) {
      console.groupCollapsed('Stacks');
      console.log('no frames');
      console.groupEnd();
      return;
    }

    const NEWLINE = '\n';

    // edit this array if you want to ignore or unignore something
    const FILTER_REGEXP: RegExp[] = [
      /checkAndUpdateView/,
      /callViewAction/,
      /execEmbeddedViewsAction/,
      /execComponentViewsAction/,
      /callWithDebugContext/,
      /debugCheckDirectivesFn/,
      /Zone/,
      /checkAndUpdateNode/,
      /debugCheckAndUpdateNode/,
      /onScheduleTask/,
      /onInvoke/,
      /updateDirectives/,
      /@angular/,
      /Observable\._trySubscribe/,
      /Observable.subscribe/,
      /SafeSubscriber/,
      /Subscriber.js.Subscriber/,
      /checkAndUpdateDirectiveInline/,
      /drainMicroTaskQueue/,
      /getStacktraceWithUncaughtError/,
      /LongStackTrace/,
      /Observable._zoneSubscribe/,
    ];

    const filterFrames = (stack: string) => {
      return stack
        .split(NEWLINE)
        .filter(frame => !FILTER_REGEXP.some(reg => reg.test(frame)))
        .join(NEWLINE);
    };

    console.groupCollapsed('Stacks');
    frames
      .filter(frame => frame.error.stack)
      .map(frame => filterFrames(frame.error.stack))
      .forEach(frame => {
        console.log(frame);
      });
    console.groupEnd();
  }

  crash() {
    (null as any).crash();
  }

  dummyUpdateState(state: UpdateState) {
    this.serviceWorkerService.updateState$.next(state);
  }

  manuallyLogTimeRecordings() {
    const timers = this.decisionData.timeRecording.timers;
    for (const naviStep of NAVI_STEP_ORDER) {
      for (const naviSubStepIdx in timers[naviStep]) {
        console.log(
          naviStep +
            naviSubStepIdx +
            ': (A)' +
            timers[naviStep][naviSubStepIdx].activeTime.toFixed(2) +
            ' (I)' +
            timers[naviStep][naviSubStepIdx].totalTime.toFixed(2)
        );
      }
    }
    console.log('-----');
  }

  showNightlyWarning() {
    this.dialog.open(NightlyWarningModalComponent);
  }
}
