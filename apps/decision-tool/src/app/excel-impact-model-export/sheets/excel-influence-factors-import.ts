import { InfluenceFactorNamePipe, StateNamePipe } from '@entscheidungsnavi/widgets';
import { UserdefinedInfluenceFactor } from '@entscheidungsnavi/decision-data/classes';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { cloneDeep } from 'lodash';
import { Worksheet } from 'exceljs';
import { getCellValue } from '../../excel-export/excel-helpers';
import { ErrorPair, ErrorType } from '../excel-impact-model-export.service';

type ImportInfluenceFactorsReturnType = {
  influenceFactors: UserdefinedInfluenceFactor[];
  errorLog: ErrorPair[];
};

export function performInfluenceFactorImport(decisionData: DecisionData, ws: Worksheet): ImportInfluenceFactorsReturnType {
  let currentRowImport = 2;
  const errorLog: ErrorPair[] = [];
  const influenceFactors = cloneDeep(decisionData.influenceFactors);

  // collect data from table and check validity
  decisionData.influenceFactors.forEach((influenceFactor, influenceFactorIdx) => {
    if (ws.getCell(currentRowImport, 1).text !== InfluenceFactorNamePipe.prototype.transform(influenceFactor)) {
      errorLog.push({
        targetName: ws.getCell(currentRowImport, 1).text,
        coordinates: 'A' + currentRowImport,
        errorType: ErrorType.InfluenceFactorUnknownInfluenceFactor,
      });
    } else {
      const precision = parseInt(getCellValue(ws.getCell(currentRowImport, 4)).toString());
      if (isNaN(precision)) {
        errorLog.push({
          targetName: InfluenceFactorNamePipe.prototype.transform(influenceFactor),
          coordinates: 'D' + currentRowImport,
          errorType: ErrorType.InfluenceFactorInvalidValue,
        });
      } else {
        influenceFactors[influenceFactorIdx].precision = precision;
        let probabilitySum = 0,
          errorInStates = false;
        influenceFactor.states.forEach((state, stateIdx) => {
          if (ws.getCell(currentRowImport + stateIdx + 1, 2).text !== StateNamePipe.prototype.transform(influenceFactor, stateIdx)) {
            errorLog.push({
              targetName: InfluenceFactorNamePipe.prototype.transform(influenceFactor),
              coordinates: 'B' + (currentRowImport + stateIdx + 1),
              errorType: ErrorType.InfluenceFactorUnknownState,
            });
            errorInStates = true;
          } else {
            const probability = parseInt(getCellValue(ws.getCell(currentRowImport + stateIdx + 1, 3)).toString());
            if (isNaN(probability)) {
              errorLog.push({
                targetName: InfluenceFactorNamePipe.prototype.transform(influenceFactor),
                coordinates: 'C' + (currentRowImport + stateIdx + 1),
                errorType: ErrorType.InfluenceFactorInvalidValue,
              });
              errorInStates = true;
            } else {
              influenceFactors[influenceFactorIdx].states[stateIdx].probability = probability;
              probabilitySum += probability;
            }
          }
        });
        if (!errorInStates && probabilitySum !== 100) {
          errorLog.push({
            targetName: InfluenceFactorNamePipe.prototype.transform(influenceFactor),
            errorType: ErrorType.InfluenceFactorPrecisionsDontSumUp,
          });
        }
      }
    }
    currentRowImport += influenceFactor.states.length + 1;
  });

  if (errorLog.length > 0) {
    return { influenceFactors: null, errorLog };
  } else {
    return { influenceFactors, errorLog: [] };
  }
}
