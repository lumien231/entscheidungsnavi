import { Worksheet } from 'exceljs';
import { centerAndWrapText } from '../../excel-export/excel-helpers';
import { multiLine } from '../../excel-export/excel-helpers';

export function performExportReadme(ws: Worksheet) {
  const cell = ws.getCell('A1');
  cell.value = multiLine([
    $localize`Empfehlungen zum Umgang: `,
    '',
    $localize`
      Wenn Du Änderungen im Entscheidungsnavi (bspw. Ziele, Skalen, neue Einflussfaktoren) vornehmen möchtest, solltest Du zuerst Deine
      Änderungen aus dieser Datei importieren und später eine neue Datei exportieren. Anderenfalls kann ein korrekter Import dieser Datei
      nicht garantiert werden!`,
    '',
    $localize`Um Probleme mit einer falschen Struktur zu vermeiden, kann es hilfreich sein, Alternativen zu kopieren und anzupassen,
      anstatt sie neu zu erstellen. Dies gilt analog für das Hinzufügen von Einflussfaktoren in einzelnen Matrixfeldern.`,
    '',
    $localize`Einschränkungen:`,
    '',
    $localize`Das Bearbeiten der Ziele und Skalen wird nicht unterstützt! Du darfst weder die Überschriften noch die Spalten verändern.
      Die Zellen sind deshalb geschützt. Du kannst den Schutz jedoch über den Reiter Überprüfen mit Blattschutz aufheben deaktivieren.`,
    '',
    $localize`Es können keine neuen Einflussfaktoren definiert werden. Änderungen im Blatt Einflussfaktoren dürfen lediglich in den
      hellgrün markierten Zellen vorgenommen werden.`,
    '',
    $localize`Die Notizen/Kommentare dienen nur als Hilfestellung.  Änderungen hier werden beim Import nicht übernommen.`,
    $localize`Alternativen, die in dieser Datei gelöscht werden, bleiben beim Importieren unverändert.`,
  ]);
  centerAndWrapText(cell);
  ws.getRow(1).height = 400;
  ws.getColumn(1).width = 160;
}
