import { Component } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';

@Component({
  templateUrl: './about-us-modal.component.html',
  styleUrls: ['./about-us-modal.component.scss'],
})
export class AboutUsModalComponent {
  constructor(public dialogRef: MatDialogRef<AboutUsModalComponent>) {}
}
