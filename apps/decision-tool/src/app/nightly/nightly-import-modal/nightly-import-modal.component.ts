import { Component } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';

@Component({
  templateUrl: './nightly-import-modal.component.html',
  styleUrls: ['./nightly-import-modal.component.scss'],
})
export class NightlyImportModalComponent {
  constructor(public dialogRef: MatDialogRef<NightlyImportModalComponent>) {}
}
