import { Component } from '@angular/core';
import { MatLegacyDialogRef } from '@angular/material/legacy-dialog';
import { Observable, filter, takeUntil } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { AutoSaveService } from '../data/project/auto-save.service';

@Component({
  templateUrl: './unload-online-project-modal.component.html',
  styleUrls: ['./unload-online-project-modal.component.scss'],
})
export class UnloadOnlineProjectModalComponent {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  constructor(protected autoSaveService: AutoSaveService, private dialogRef: MatLegacyDialogRef<UnloadOnlineProjectModalComponent>) {
    this.autoSaveService.saveState$
      .pipe(
        filter(state => state === 'idle'),
        takeUntil(this.onDestroy$)
      )
      .subscribe(() => this.close(true));
    this.autoSaveService.triggerSave();
  }

  close(accept = false) {
    this.dialogRef.close(accept);
  }
}
