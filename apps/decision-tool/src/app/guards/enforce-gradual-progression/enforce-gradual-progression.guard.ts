import { Injectable } from '@angular/core';
import { MatLegacyDialog as MatDialog, MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { ActivatedRouteSnapshot, CanActivateChild, NavigationCancel, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { subStepToNumber } from '@entscheidungsnavi/decision-data/steps';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { SnackbarComponent, SnackbarData } from '@entscheidungsnavi/widgets';
import { naviSubStepToUrl, urlToNaviSubStep } from '../../../modules/shared/navigation/navigation-step';
import { AppSettingsService } from '../../data/app-settings.service';
import { CurrentProgressService } from '../../data/current-progress.service';
import {
  GradualProgressionWarningModalComponent,
  ModalResult,
} from './gradual-progression-warning-modal/gradual-progression-warning-modal.component';

const IMPACT_MODEL_STEP_NUMBER = subStepToNumber({ step: 'impactModel' });

@Injectable({
  providedIn: 'root',
})
export class EnforceGradualProgressionGuard implements CanActivateChild {
  private dialogRef: MatDialogRef<GradualProgressionWarningModalComponent>;

  constructor(
    private currentProgressService: CurrentProgressService,
    private decisionData: DecisionData,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router: Router,
    private appSettings: AppSettingsService
  ) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationCancel && this.dialogRef != null) {
        this.dialogRef.componentInstance.close();
      }
    });
  }

  async canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean | UrlTree> {
    // Only check for the "leaf"-routes of the url tree, that actually show components (not redirects, not intermediates)
    if (childRoute.routeConfig.component == null) {
      return true;
    }

    // Ignore for starter and professional mode
    if (['starter', 'professional'].includes(this.decisionData.projectMode)) {
      return true;
    }

    const nextStep = urlToNaviSubStep(state.url);
    if (nextStep == null) {
      return true;
    }

    this.currentProgressService.update();
    const currentStepNumber = subStepToNumber(this.currentProgressService.currentProgress);

    // We only track steps up to the impact model. After that has been opened, all steps are accessible.
    if (currentStepNumber < IMPACT_MODEL_STEP_NUMBER && currentStepNumber + 1 < subStepToNumber(nextStep)) {
      if (this.appSettings.skipSubstepWarning) {
        this.dialogRef = this.dialog.open(GradualProgressionWarningModalComponent);
        const result: ModalResult = await this.dialogRef.afterClosed().toPromise();
        this.dialogRef = null;
        switch (result) {
          case 'abort':
            return false;
          case 'confirm':
            this.currentProgressService.confirmSkip(nextStep);
            return true;
          case 'navigate-to-latest':
            return this.router.parseUrl(naviSubStepToUrl(this.currentProgressService.currentProgress));
        }
      } else {
        this.currentProgressService.confirmSkip(nextStep);
        this.snackBar.openFromComponent(SnackbarComponent, {
          data: { message: $localize`Teilschritt(e) übersprungen`, icon: 'fast_forward' } as SnackbarData,
          duration: 5000,
        });
      }
    }

    return true;
  }
}
