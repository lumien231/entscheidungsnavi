import { Pipe, PipeTransform } from '@angular/core';
import { ProjectMode } from '@entscheidungsnavi/decision-data/classes';
import { NaviStep } from '@entscheidungsnavi/decision-data/steps';
import { NavigationStep } from '../../modules/shared/navigation/navigation-step';

@Pipe({ name: 'filteredSubSteps' })
export class FilteredSubStepsPipe implements PipeTransform {
  transform(step: NavigationStep & { id: NaviStep }, projectMode: ProjectMode): { name: string; index: number }[] {
    if (projectMode === 'educational') {
      return step.subSteps.map((name, index) => ({ name, index }));
    } else if (step.id === 'results') {
      return step.subSteps.map((name, index) => ({ name, index })).slice(1);
    } else {
      return [];
    }
  }
}
