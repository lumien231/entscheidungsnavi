import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { MatLegacySnackBar } from '@angular/material/legacy-snack-bar';
import { ProjectListDto } from '@entscheidungsnavi/api-client';
import { Project } from '@entscheidungsnavi/api-types';
import { SnackbarComponent, SnackbarData } from '@entscheidungsnavi/widgets';
import { ProjectService } from '../../../data/project';

@Component({
  selector: 'dt-recently-edited-projects',
  templateUrl: './recently-edited-projects.component.html',
  styleUrls: ['./recently-edited-projects.component.scss'],
})
export class RecentlyEditedProjectsComponent implements OnChanges {
  @Input() projects: ProjectListDto;
  @Output() projectLoaded = new EventEmitter<void>();

  recentlyEdited: Project[] = [];

  constructor(private snackBar: MatLegacySnackBar, private projectService: ProjectService) {}

  ngOnChanges(changes: SimpleChanges) {
    if ('projects' in changes) {
      // The 7 most recently updated projects make up the history
      const recentlyOpened = this.projects.list.slice();
      recentlyOpened.sort((a, b) => b.updatedAt.getTime() - a.updatedAt.getTime());
      this.recentlyEdited = recentlyOpened.slice(0, 8);
    }
  }

  loadHistoryProject(id: string) {
    this.projectService.loadOnlineProject(id).subscribe({
      next: () => this.projectLoaded.emit(),
      error: () =>
        this.snackBar.openFromComponent<SnackbarComponent, SnackbarData>(SnackbarComponent, {
          duration: 8000,
          data: {
            message: $localize`Projekt konnte nicht geladen werden. Bist Du offline?`,
            icon: 'error',
          },
        }),
    });
  }
}
