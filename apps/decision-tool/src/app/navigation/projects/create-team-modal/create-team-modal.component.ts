import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { OnlineProjectManagementService } from '../project-management.service';

@Component({
  templateUrl: './create-team-modal.component.html',
  styleUrls: ['./create-team-modal.component.scss'],
})
export class CreateTeamModalComponent {
  protected formGroup = new FormGroup({
    teamName: new FormControl('', Validators.required),
  });
  constructor(
    private dialogRef: MatDialogRef<CreateTeamModalComponent>,
    @Inject(MAT_DIALOG_DATA) private data: { projectId: string },
    private managementService: OnlineProjectManagementService,
    private snackBar: MatSnackBar
  ) {}

  discard() {
    this.dialogRef.close();
  }

  createTeam() {
    if (!this.formGroup.valid) {
      return;
    }

    this.managementService.createTeam(this.data.projectId, this.formGroup.value.teamName).subscribe({
      next: team => this.dialogRef.close(team),
      error: () => this.snackBar.open($localize`Beim Erstellen des Teams ist ein Fehler aufgetreten.`, $localize`Ok`),
    });
  }
}
