import { Component } from '@angular/core';
import { MatLegacyDialog } from '@angular/material/legacy-dialog';
import { MonoTypeOperatorFunction, finalize, map, of, switchMap } from 'rxjs';

@Component({
  templateUrl: './project-loading-modal.component.html',
  styleUrls: ['./project-loading-modal.component.scss'],
})
export class ProjectLoadingModalComponent {}

export function projectLoadingModal<T>(dialog: MatLegacyDialog): MonoTypeOperatorFunction<T> {
  return observable =>
    of(null).pipe(
      map(() => dialog.open(ProjectLoadingModalComponent, { disableClose: true })),
      switchMap(modal => observable.pipe(finalize(() => modal.close())))
    );
}
