/// <reference lib="webworker" />

import { randomizedUtilityGenerator, RandomizationParameters, UtilityGeneratorResult } from '@entscheidungsnavi/decision-data/calculation';
import { PREDEFINED_INFLUENCE_FACTORS } from '@entscheidungsnavi/decision-data/classes';
import { readText } from '@entscheidungsnavi/decision-data/export';
import { InfluenceFactorStateMap } from '@entscheidungsnavi/decision-data/tools';
import { checkType } from '@entscheidungsnavi/tools';

export interface StartMessage {
  decisionData: string;
  selectedAlternatives: number[];
  iterationCount: number;
  parameters: RandomizationParameters;
}

export interface ProgressMessage {
  progress: number;
}

export type DoneMessage = {
  generationDuration: number;
  totalDuration: number;
  result: {
    average: number[];
    min: number[];
    max: number[];
    positions: number[][];
    stateCounts: ReturnType<InfluenceFactorStateMap<number[]>['innerMap']>[][]; // A state count for each position
  };
};

addEventListener('message', (e: MessageEvent<StartMessage>) => {
  const decisionData = readText(e.data.decisionData);
  const selectedOutcomes = decisionData.outcomes.filter((_val, idx) => e.data.selectedAlternatives.includes(idx));

  performance.clearMarks();
  performance.clearMeasures();
  performance.mark('beforeGeneration');

  const generator = randomizedUtilityGenerator(
    decisionData.objectives,
    selectedOutcomes,
    decisionData.weights.getWeights(),
    e.data.parameters
  );

  // Result arrays
  const avg: number[] = new Array(selectedOutcomes.length).fill(0);
  const min: number[] = new Array(selectedOutcomes.length).fill(Number.MAX_SAFE_INTEGER); // minimal new for each alternative
  const max: number[] = new Array(selectedOutcomes.length).fill(Number.MIN_SAFE_INTEGER); // maximal new for each alternative
  // matrix with count of positions for each alternative [alternative][positionCount]
  const positions = avg.map(() => avg.map(() => 0));
  // we count how often each influence factor state occurred for each position
  const stateCounts = avg.map(() => avg.map(() => new InfluenceFactorStateMap<number[]>()));

  performance.mark('afterGeneration');
  performance.measure('gen_duration', 'beforeGeneration', 'afterGeneration');

  for (let i = 0; i < e.data.iterationCount; i++) {
    if (i % 500 === 0) {
      // send the progress in percent (in [0,1]) after every 500 iterations
      postMessage(checkType<ProgressMessage>({ progress: i / e.data.iterationCount }));
    }

    // We know that this generator never terminates
    const currResult = generator.next().value as UtilityGeneratorResult;
    const currUtilities = currResult.alternativeUtilities;

    for (let alternativeID = 0; alternativeID < avg.length; alternativeID++) {
      avg[alternativeID] += currUtilities[alternativeID] / e.data.iterationCount;
      min[alternativeID] = Math.min(min[alternativeID], currUtilities[alternativeID]);
      max[alternativeID] = Math.max(max[alternativeID], currUtilities[alternativeID]);
    }

    // Alternative IDs sorted by their utility
    const sortedNews = currUtilities.map((val: number, idx: number) => ({ val, idx })).sort((a, b) => b.val - a.val);

    sortedNews.forEach((alternative, rankIdx) => {
      positions[alternative.idx][rankIdx] += 1;

      // Increment the respective influence factor states
      const stateCount = stateCounts[alternative.idx][rankIdx];
      for (const [key, state] of currResult.influenceFactorStates.entries()) {
        let countArray = stateCount.get(key);

        if (countArray == null) {
          // Initialize the array if we have not done so yet
          countArray = new Array(
            typeof key === 'number' ? decisionData.influenceFactors[key].states.length : PREDEFINED_INFLUENCE_FACTORS[key.id].states.length
          ).fill(0);
          stateCount.set(key, countArray);
        }

        countArray[state]++;
      }
    });
  }

  postMessage(checkType<ProgressMessage>({ progress: 1 }));
  performance.mark('end');
  performance.measure('total_duration', 'beforeGeneration', 'end');

  const doneMessage: DoneMessage = {
    generationDuration: performance.getEntriesByName('gen_duration')[0].duration,
    totalDuration: performance.getEntriesByName('total_duration')[0].duration,
    result: {
      average: avg,
      min,
      max,
      positions,
      stateCounts: stateCounts.map(outer => outer.map(inner => inner.innerMap())),
    },
  };
  postMessage(doneMessage);
});
