import { Injectable, NgZone, OnDestroy } from '@angular/core';
import { dataToText } from '@entscheidungsnavi/decision-data/export';
import { constant, sum, times } from 'lodash';
import { checkType } from '@entscheidungsnavi/tools';
import { Subject, throttleTime } from 'rxjs';
import { InfluenceFactorStateMap } from '@entscheidungsnavi/decision-data/tools';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { DoneMessage, ProgressMessage, StartMessage } from './robustness-check.worker';

export type AlternativeResult = {
  positionDistribution: number[];
  minUtility: number;
  maxUtility: number;
};

export interface RobustnessWorkerResult {
  // Results for every alternative
  alternatives: AlternativeResult[];
  // The frequencies of every influence factor state for every position [alternativeIdx][position]
  influenceFactors: InfluenceFactorStateMap<number[]>[][];
}

export type RobustnessWorkerSettings = Omit<StartMessage, 'decisionData'>;

@Injectable()
export class RobustnessWorkerService implements OnDestroy {
  private workerPool: Worker[] = [];

  private workerResults: DoneMessage[] = [];
  private workerProgress: number[] = [];
  private iterationCountsPerWorker: number[] = [];
  private settings: RobustnessWorkerSettings;

  private _noWorker = false;
  /**
   * True iff the browser does not support web workers.
   */
  get noWorker() {
    return this._noWorker;
  }

  private onProgressUpdateSubject$ = new Subject<number>();
  /**
   * Emits the progress between 0-1. Emits outside the Angular Zone.
   */
  readonly onProgressUpdate$ = this.onProgressUpdateSubject$.asObservable().pipe(throttleTime(500));

  private onFinishedSubject$ = new Subject<RobustnessWorkerResult>();
  readonly onFinished$ = this.onFinishedSubject$.asObservable();

  constructor(private zone: NgZone, private decisionData: DecisionData) {
    this.initWorkers();
  }

  ngOnDestroy() {
    this.workerPool.forEach(worker => worker.terminate());
    this.workerPool = [];
  }

  restartWorker() {
    this.workerPool.forEach(worker => worker.terminate());
    this.workerPool = [];

    this.initWorkers();
  }

  private initWorkers() {
    if (!Worker) {
      this._noWorker = true;
      return;
    }

    const workerCount = Math.min(5, navigator.hardwareConcurrency || 1);
    for (let c = 0; c < workerCount; c++) {
      const newWorker = new Worker(new URL('./robustness-check.worker', import.meta.url), {
        type: 'module',
        name: this.constructor.name,
      });

      this.zone.runOutsideAngular(() => {
        newWorker.addEventListener('message', e => this.onWorkerMessage(c, e));
        newWorker.addEventListener('error', e => console.error(e));
      });

      this.workerPool.push(newWorker);
    }
  }

  /**
   * Start the worker calculation using the given data.
   *
   * @param settings - The settings for the calculation
   * @returns true if the calculation could be started, false otherwise
   */
  startWorker(settings: RobustnessWorkerSettings) {
    if (this.workerPool.length === 0) {
      return false;
    }
    this.settings = settings;

    // We need to serialize the decisionData object to send it to the worker.
    const serializedDecisionData = dataToText(this.decisionData, '0.0.0');

    // How many iterations should each worker do?
    const partForEach = Math.floor(settings.iterationCount / this.workerPool.length);

    // How many are left?
    const remainder = settings.iterationCount - partForEach * this.workerPool.length;

    // Give each worker their share
    this.iterationCountsPerWorker = this.workerPool.map(_ => partForEach);

    // Give Remainder to last worker
    this.iterationCountsPerWorker[this.workerPool.length - 1] += remainder;

    this.workerProgress = this.workerPool.map(_ => 0);
    this.workerResults = this.workerPool.map(_ => null);

    for (let i = 0; i < this.workerPool.length; i++) {
      const worker = this.workerPool[i];

      if (this.iterationCountsPerWorker[i] > 0) {
        worker.postMessage(
          checkType<StartMessage>({
            ...settings,
            decisionData: serializedDecisionData,
            iterationCount: this.iterationCountsPerWorker[i],
          })
        );
      }
    }

    return true;
  }

  /**
   * This function runs outside the angular zone.
   */
  private onWorkerMessage(workerIndex: number, e: MessageEvent<ProgressMessage | DoneMessage>) {
    if ('progress' in e.data) {
      const progress = +e.data.progress;
      this.workerProgress[workerIndex] = progress;

      const newTotalProgress = sum(this.workerProgress.map((p, i) => p * this.iterationCountsPerWorker[i])) / this.settings.iterationCount;
      this.onProgressUpdateSubject$.next(newTotalProgress);
    } else {
      this.workerResults[workerIndex] = e.data;

      if (this.workerPool.every((_, i) => this.workerResults[i] || this.iterationCountsPerWorker[i] === 0)) {
        // Every Worker is done
        this.onAllWorkersDone();
      }
    }
  }

  private onAllWorkersDone() {
    const alternativeCount = this.settings.selectedAlternatives.length;

    // Aggregate worker results
    const positionCounts = times(alternativeCount, () => times(alternativeCount, constant(0)));
    const stateCounts = times(alternativeCount, () => times(alternativeCount, () => new InfluenceFactorStateMap<number[]>()));

    for (let workerIndex = 0; workerIndex < this.workerResults.length; workerIndex++) {
      if (!this.workerResults[workerIndex]) {
        continue;
      }
      const res = this.workerResults[workerIndex].result;

      for (let alternativeIndex = 0; alternativeIndex < alternativeCount; alternativeIndex++) {
        for (let position = 0; position < alternativeCount; position++) {
          positionCounts[alternativeIndex][position] += res.positions[alternativeIndex][position];

          // Update influence factor state distribution
          for (const [key, counts] of InfluenceFactorStateMap.fromInnerMap(res.stateCounts[alternativeIndex][position]).entries()) {
            let frequencies = stateCounts[alternativeIndex][position].get(key);

            if (frequencies == null) {
              frequencies = new Array(counts.length).fill(0);
              stateCounts[alternativeIndex][position].set(key, frequencies);
            }

            counts.forEach((count, stateIndex) => {
              frequencies[stateIndex] += count;
            });
          }
        }
      }
    }

    // Normalize state distribution by position counts, and normalize positions
    for (let alternativeIndex = 0; alternativeIndex < alternativeCount; alternativeIndex++) {
      for (let position = 0; position < alternativeCount; position++) {
        if (positionCounts[alternativeIndex][position] === 0) continue;

        for (const [, value] of stateCounts[alternativeIndex][position].entries()) {
          for (let stateIndex = 0; stateIndex < value.length; stateIndex++) {
            value[stateIndex] /= positionCounts[alternativeIndex][position];
          }
        }

        positionCounts[alternativeIndex][position] /= this.settings.iterationCount;
      }
    }

    // Callback method
    this.zone.run(() => {
      this.onFinishedSubject$.next({
        alternatives: positionCounts.map((distribution, index) => ({
          positionDistribution: distribution,
          minUtility: Math.min(...this.workerResults.filter(dm => dm).map(dm => dm.result.min[index])),
          maxUtility: Math.max(...this.workerResults.filter(dm => dm).map(dm => dm.result.max[index])),
        })),
        influenceFactors: stateCounts,
      });
    });
  }
}
