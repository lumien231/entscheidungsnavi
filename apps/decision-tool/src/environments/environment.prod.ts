import { createEnvironment, ENVIRONMENT_TYPES, EnvironmentType } from './environment-types';

function getType(): EnvironmentType {
  if (ENVIRONMENT_TYPES.includes(window.dtEnv?.environmentType as EnvironmentType)) {
    return window.dtEnv.environmentType as EnvironmentType;
  }

  return 'production';
}

function getTimestamp() {
  if (!window.dtEnv?.timestamp) return null;

  const timestamp = new Date(window.dtEnv.timestamp);
  if (isNaN(timestamp.valueOf())) {
    // The date string is invalid, we have an in valid date
    return null;
  } else {
    return timestamp;
  }
}

export const ENVIRONMENT = createEnvironment({
  type: getType(),
  sentryDsn: window.dtEnv?.sentryDsn || null,
  cockpitOrigin: window.dtEnv?.cockpitOrigin || null,
  buildTimestamp: getTimestamp(),
});
