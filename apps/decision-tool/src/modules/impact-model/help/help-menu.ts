import { NAVI_STEP_ORDER } from '@entscheidungsnavi/decision-data/steps';
import { helpPage } from '../../../app/help/help';
import { STEPS } from '../../shared/navigation/navigation-step';
import { ImpactModelExplanationComponent } from './explanation.component';
import { ImpactModelHelpMainComponent } from './help-main.component';
import { Help1Component } from './help1/help1.component';
import { Help2Component } from './help2/help2.component';
import { HelpBackgroundComponent } from './help-background/help-background.component';

type ImpactModelHelpContext = 'uncertaintyfactors';

export function getHelpMenu(context?: ImpactModelHelpContext) {
  return {
    educational: [
      helpPage()
        .name($localize`So funktioniert's`)
        .component(Help1Component)
        .context(context)
        .build(),
      helpPage()
        .name($localize`Weitere Hinweise`)
        .component(Help2Component)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 4`)
        .component(HelpBackgroundComponent)
        .build(),
    ],
    starter: [
      helpPage()
        .name($localize`Schritt ${NAVI_STEP_ORDER.indexOf('impactModel') + 1}\: ${STEPS.impactModel.name}`)
        .component(ImpactModelHelpMainComponent)
        .build(),
      helpPage()
        .explanation(ImpactModelExplanationComponent)
        .name($localize`Mehr zur Aufstellung des Wirkungsmodells`)
        .build(),
    ],
  };
}
