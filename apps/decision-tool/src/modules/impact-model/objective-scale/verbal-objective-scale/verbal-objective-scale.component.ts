import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormArray, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { VerbalObjectiveData } from '@entscheidungsnavi/decision-data/classes';
import { PopOverRef, PopOverService } from '@entscheidungsnavi/widgets';
import { zip } from 'lodash';
import { merge } from 'rxjs';
import { first } from 'rxjs/operators';

@Component({
  selector: 'dt-verbal-objective-scale',
  templateUrl: './verbal-objective-scale.component.html',
  styleUrls: ['./verbal-objective-scale.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VerbalObjectiveScaleComponent implements OnInit {
  @Input() verbalData: VerbalObjectiveData;
  @Output() isValid = new EventEmitter<boolean>();
  @Output() isDirty = new EventEmitter<boolean>();

  initParamsForm: UntypedFormGroup;
  statesForm: UntypedFormGroup;
  statesFormArray: UntypedFormArray;

  @ViewChild('initParamsPopover') initParamsPopover: TemplateRef<any>;
  @ViewChild('initParamsOpener', { read: ElementRef }) initParamsOpener: ElementRef;
  openPopover: PopOverRef;

  constructor(private fb: UntypedFormBuilder, private popover: PopOverService) {}

  ngOnInit() {
    this.initParamsForm = this.fb.group({
      from: [this.verbalData.initParams.from ?? $localize`gering`, Validators.required],
      to: [this.verbalData.initParams.to ?? $localize`hoch`, Validators.required],
      stepNumber: [this.verbalData.initParams.stepNumber, [Validators.required, Validators.min(2), Validators.max(7)]],
    });
    this.statesFormArray = this.fb.array(
      zip(this.verbalData.options, this.verbalData.explanations, this.verbalData.comments).map(state => this.getStateForm(...state))
    );
    this.statesForm = this.fb.group({ states: this.statesFormArray });

    if (this.statesFormArray.length === 0) {
      this.applyInitParams();
    }

    // We do not care about the validity of the init params
    this.statesForm.statusChanges.subscribe(status => this.isValid.emit(status === 'VALID'));
    this.isValid.emit(this.statesForm.valid);

    this.isDirty.emit(false);
    merge(this.initParamsForm.valueChanges, this.statesForm.valueChanges)
      .pipe(first())
      .subscribe(() => this.isDirty.emit(true));
  }

  save() {
    this.verbalData.initParams.from = this.initParamsForm.get('from').value;
    this.verbalData.initParams.to = this.initParamsForm.get('to').value;
    this.verbalData.initParams.stepNumber = this.initParamsForm.get('stepNumber').value;

    while (this.verbalData.options.length > this.statesFormArray.length) {
      this.verbalData.removeOption(this.verbalData.options.length - 1);
    }
    this.statesFormArray.controls.forEach((stateGroup, index) => {
      if (this.verbalData.options.length > index) {
        this.verbalData.options[index] = stateGroup.get('name').value;
      } else {
        this.verbalData.addOption(index, stateGroup.get('name').value);
      }
      this.verbalData.explanations[index] = stateGroup.get('explanation').value;
      this.verbalData.comments[index] = stateGroup.get('comment').value;
    });
  }

  openInitParams() {
    this.openPopover = this.popover.open(this.initParamsPopover, this.initParamsOpener, {
      position: [{ originX: 'center', originY: 'top', overlayX: 'center', overlayY: 'bottom' }],
    });
  }

  applyInitParams() {
    if (!this.initParamsForm.valid) {
      return;
    }

    const from: string = this.initParamsForm.get('from').value,
      to: string = this.initParamsForm.get('to').value;
    const optionNameTemplates = [
      $localize`sehr ${from}`,
      from,
      $localize`eher ${from}`,
      $localize`mittel`,
      $localize`eher ${to}`,
      to,
      $localize`sehr ${to}`,
    ];

    let optionIndices: number[] = [];
    switch (this.initParamsForm.get('stepNumber').value) {
      case 2:
        optionIndices = [1, 5];
        break;
      case 3:
        optionIndices = [1, 3, 5];
        break;
      case 4:
        optionIndices = [1, 2, 4, 5];
        break;
      case 5:
        optionIndices = [1, 2, 3, 4, 5];
        break;
      case 6:
        optionIndices = [0, 1, 2, 4, 5, 6];
        break;
      case 7:
        optionIndices = [0, 1, 2, 3, 4, 5, 6];
        break;
    }
    const newOptionNames = optionIndices.map(index => optionNameTemplates[index]);

    this.statesFormArray.clear();
    newOptionNames.forEach(option => this.statesFormArray.push(this.getStateForm(option, '', '')));
  }

  addState() {
    this.statesFormArray.push(this.getStateForm('', '', ''));
  }

  deleteState(index: number) {
    this.statesFormArray.removeAt(index);
  }

  private getStateForm(name: string, explanation: string, comment: string) {
    return this.fb.group({
      name: [name, Validators.required],
      explanation: [explanation],
      comment: [comment],
    });
  }

  drop(event: CdkDragDrop<unknown>) {
    moveItemInArray(this.statesFormArray.controls, event.previousIndex, event.currentIndex);
  }

  getColorForRank(index: number) {
    return `rgb(${200 * (1 - index / this.statesFormArray.length)}, ${200 * (index / this.statesFormArray.length)}, 0)`;
  }
}
