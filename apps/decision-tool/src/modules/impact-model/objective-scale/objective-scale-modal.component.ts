import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { ConfirmModalComponent, ConfirmModalData } from '@entscheidungsnavi/widgets';
import { ObjectiveType } from '@entscheidungsnavi/decision-data/classes';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import {
  MatLegacyDialog as MatDialog,
  MatLegacyDialogRef as MatDialogRef,
  MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA,
} from '@angular/material/legacy-dialog';
import { IndicatorObjectiveScaleComponent, NumericalObjectiveScaleComponent, VerbalObjectiveScaleComponent } from '.';

@Component({
  templateUrl: './objective-scale-modal.component.html',
  styleUrls: ['./objective-scale-modal.component.scss'],
})
export class ObjectiveScaleModalComponent implements OnInit {
  get objective() {
    return this.decisionData.objectives[this.objectiveIdx];
  }

  @ViewChild(NumericalObjectiveScaleComponent) numericalScale: NumericalObjectiveScaleComponent;
  @ViewChild(VerbalObjectiveScaleComponent) verbalScale: VerbalObjectiveScaleComponent;
  @ViewChild(IndicatorObjectiveScaleComponent) indicatorScale: IndicatorObjectiveScaleComponent;

  get activeScaleComponent() {
    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return this.numericalScale;
      case ObjectiveType.Verbal:
        return this.verbalScale;
      case ObjectiveType.Indicator:
        return this.indicatorScale;
    }
  }

  objectiveType: ObjectiveType;
  comment: string;

  // eslint-disable-next-line @typescript-eslint/naming-convention
  ObjectiveType = ObjectiveType;

  numericalIsValid: boolean;
  verbalIsValid: boolean;
  indicatorIsValid: boolean;

  get isValid() {
    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return this.numericalIsValid;
      case ObjectiveType.Verbal:
        return this.verbalIsValid;
      case ObjectiveType.Indicator:
        return this.indicatorIsValid;
    }
  }

  numericalIsDirty: boolean;
  verbalIsDirty: boolean;
  indicatorIsDirty: boolean;

  get scaleIsDirty() {
    switch (this.objectiveType) {
      case ObjectiveType.Numerical:
        return this.numericalIsDirty;
      case ObjectiveType.Verbal:
        return this.verbalIsDirty;
      case ObjectiveType.Indicator:
        return this.indicatorIsDirty;
    }
  }

  get hasObjectiveTypeChanged() {
    return this.objectiveType !== this.objective.objectiveType;
  }

  get isDirty() {
    return this.hasObjectiveTypeChanged || this.comment !== this.objective.comment || this.scaleIsDirty;
  }

  allOutcomesEmpty: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public objectiveIdx: number,
    private dialogRef: MatDialogRef<ObjectiveScaleModalComponent>,
    private decisionData: DecisionData,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.allOutcomesEmpty = this.decisionData.outcomes.every(ocForAlternative => ocForAlternative[this.objectiveIdx].isEmpty);
    this.load();
  }

  close(save: boolean) {
    if (save) {
      this.save();
    }
    this.dialogRef.close(save);
  }

  async closeClick() {
    if (
      !this.isDirty ||
      (await this.dialog
        .open(ConfirmModalComponent, {
          data: {
            title: $localize`Änderungen verwerfen`,
            prompt: $localize`Bist Du sicher, dass Du die ungespeicherten Änderungen an der Zielskala verwerfen willst?`,
            template: 'discard',
          } as ConfirmModalData,
        })
        .afterClosed()
        .toPromise())
    ) {
      this.close(false);
    }
  }

  private load() {
    this.objectiveType = this.objective.objectiveType;
    this.comment = this.objective.comment;
  }

  private save() {
    if (this.objective.objectiveType !== this.objectiveType) {
      this.decisionData.changeObjectiveType(this.objectiveIdx, this.objectiveType);
    }
    this.objective.comment = this.comment;
    this.activeScaleComponent.save();
  }
}
