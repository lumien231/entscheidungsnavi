import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { UntypedFormArray, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, ValidatorFn, Validators } from '@angular/forms';
import {
  getIndicatorAggregationFunction,
  getIndicatorCoefficientName,
  getIndicatorValueName,
} from '@entscheidungsnavi/decision-data/calculation';
import { Indicator, IndicatorObjectiveData } from '@entscheidungsnavi/decision-data/classes';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { ArrayCopy } from '@entscheidungsnavi/tools';
import { ObjectiveElement, Tree } from '@entscheidungsnavi/decision-data/steps';
import { PopOverService } from '@entscheidungsnavi/widgets';
import { cloneDeep, range } from 'lodash';
import { first } from 'rxjs/operators';
import { merge } from 'rxjs';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { IndicatorDescriptionModalComponent } from '../indicator-description-modal/indicator-description-modal.component';

@Component({
  selector: 'dt-indicator-objective-scale',
  templateUrl: './indicator-objective-scale.component.html',
  styleUrls: ['./indicator-objective-scale.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IndicatorObjectiveScaleComponent implements OnInit {
  @Input() objectiveIdx: number;
  @Input() indicatorData: IndicatorObjectiveData;
  @Output() isValid = new EventEmitter<boolean>();
  @Output() isDirty = new EventEmitter<boolean>();

  @ViewChild('hierarchyContainer', { static: true })
  hierarchyContainer: ElementRef<HTMLElement>;

  // This is used to write the indicators back later. Simply changing the indicators array is not enough, we
  // also need to update the outcomes.
  indicators: ArrayCopy<Indicator>;

  form: UntypedFormGroup;
  indicatorsForm: UntypedFormArray;

  aggregationFormulaMin: number;
  aggregationFormulaMax: number;

  stages: { value: number; description: string }[];
  stagesChanged = false;

  // keeps track of worst/best after each convertStages()
  lastWorst: number;
  lastBest: number;

  objectiveTree: Tree<ObjectiveElement>;

  readonly roundingErrorPrecision = 0.0000000001;

  get isFormulaComplete() {
    if (!this.form.get('useCustomAggregation').value) {
      // additive formula is always complete
      return true;
    }
    for (let i = 0; i < this.indicators.length; i++) {
      if (!this.form.get('customAggregationFormula').value.includes(getIndicatorValueName(i))) {
        // missing indicator
        return false;
      }
    }
    // custom formula, complete
    return true;
  }

  get isScaleChanged() {
    return !this.isRangeUnchanged && !this.hasAggregationTypeChanged && (!this.areStagesNew || (this.areStagesNew && this.stagesChanged));
  }

  get areStagesNew() {
    return this.stages.every(stage => {
      return stage.description === '';
    });
  }

  get isFormulaChanged() {
    return this.hasAggregationTypeChanged && (!this.areStagesNew || (this.areStagesNew && this.stagesChanged));
  }

  get isRangeUnchanged() {
    // either additive formula with different worst/best
    // or custom formula with different worst/best
    return (
      ((!this.form.get('useCustomAggregation').value || !this.form.get('automaticCustomAggregationLimits')) &&
        this.indicatorData.defaultAggregationWorst === this.form.get('range.worst').value &&
        this.indicatorData.defaultAggregationBest === this.form.get('range.best').value) ||
      (this.form.get('useCustomAggregation').value &&
        this.calculateAggregationFunctionBounds('min', this.indicatorData.customAggregationFormula) ===
          this.calculateAggregationFunctionBounds('min', this.form.get('customAggregationFormula').value) &&
        this.calculateAggregationFunctionBounds('max', this.indicatorData.customAggregationFormula) ===
          this.calculateAggregationFunctionBounds('max', this.form.get('customAggregationFormula').value))
    );
  }

  get hasAggregationTypeChanged() {
    return this.indicatorData.useCustomAggregation !== this.form.get('useCustomAggregation').value;
  }

  constructor(
    private fb: UntypedFormBuilder,
    private decisionData: DecisionData,
    private dialog: MatDialog,
    private cdRef: ChangeDetectorRef,
    private popOverService: PopOverService
  ) {}

  ngOnInit() {
    this.indicators = ArrayCopy.createArrayCopy(this.indicatorData.indicators, indicator => indicator.clone());
    this.indicatorsForm = this.fb.array(this.indicatorData.indicators.map(this.getIndicatorForm.bind(this)));
    this.stages = cloneDeep(this.indicatorData.stages);

    if (this.indicatorData.useCustomAggregation && this.indicatorData.automaticCustomAggregationLimits) {
      this.lastWorst = this.calculateAggregationFunctionBounds('min', this.indicatorData.customAggregationFormula);
      this.lastBest = this.calculateAggregationFunctionBounds('max', this.indicatorData.customAggregationFormula);
    } else {
      this.lastWorst = this.indicatorData.defaultAggregationWorst;
      this.lastBest = this.indicatorData.defaultAggregationBest;
    }

    this.form = this.fb.group({
      indicators: this.indicatorsForm,
      aggregatedUnit: [this.indicatorData.aggregatedUnit, Validators.maxLength(10)],
      useCustomAggregation: [this.indicatorData.useCustomAggregation],
      automaticCustomAggregationLimits: [this.indicatorData.automaticCustomAggregationLimits],
      customAggregationFormula: [
        this.indicatorData.customAggregationFormula,
        { validators: [Validators.required, this.validateCustomFormula.bind(this)] },
      ],
      range: this.fb.group(
        {
          worst: [this.indicatorData.defaultAggregationWorst, Validators.required],
          best: [this.indicatorData.defaultAggregationBest, Validators.required],
        },
        { validators: this.validateRange.bind(this) as ValidatorFn }
      ),
    });

    // Changes in the indicator scales might make the aggregation formula invalid
    merge(this.indicatorsForm.valueChanges, this.form.get('automaticCustomAggregationLimits').valueChanges).subscribe(() => {
      this.form.get('customAggregationFormula').markAsTouched();
      this.form.get('customAggregationFormula').updateValueAndValidity();

      this.form.get('range').markAsTouched();
      this.form.get('range').updateValueAndValidity();
    });

    this.form.get('useCustomAggregation').valueChanges.subscribe(() => this.onCustomAggregationChange());
    this.onCustomAggregationChange();

    this.form.statusChanges.subscribe(status => this.isValid.emit(status === 'VALID'));
    this.isValid.emit(this.form.valid);

    this.isDirty.emit(false);
    this.form.valueChanges.pipe(first()).subscribe(() => this.isDirty.emit(true));

    const aspectTree = cloneDeep(this.decisionData.objectives[this.objectiveIdx].aspects);
    // Change default colors for the hierarchy
    aspectTree.value.backgroundColor = aspectTree.value.backgroundColor ?? '#5d666f';
    aspectTree.value.textColor = aspectTree.value.textColor ?? '#ffffff';
    aspectTree.children?.forEach(child => {
      child.value.backgroundColor = child.value.backgroundColor ?? '#ffffff';
      child.value.textColor = child.value.textColor ?? '#000000';
    });
    this.objectiveTree = aspectTree;
  }

  save() {
    const hasAggregationTypeChanged = this.hasAggregationTypeChanged;

    this.indicatorData.aggregatedUnit = this.form.get('aggregatedUnit').value;
    this.indicatorData.useCustomAggregation = this.form.get('useCustomAggregation').value;
    this.indicatorData.automaticCustomAggregationLimits = this.form.get('automaticCustomAggregationLimits').value;
    this.indicatorData.customAggregationFormula = this.form.get('customAggregationFormula').value;
    this.indicatorData.defaultAggregationWorst = this.form.get('range.worst').value;
    this.indicatorData.defaultAggregationBest = this.form.get('range.best').value;

    let newWorst = this.indicatorData.defaultAggregationWorst;
    let newBest = this.indicatorData.defaultAggregationBest;
    if (this.indicatorData.useCustomAggregation && this.indicatorData.automaticCustomAggregationLimits) {
      newWorst = this.calculateAggregationFunctionBounds('min', this.form.get('customAggregationFormula').value);
      newBest = this.calculateAggregationFunctionBounds('max', this.form.get('customAggregationFormula').value);
    }

    this.convertStages(hasAggregationTypeChanged, newWorst, newBest);

    this.indicatorData.stages = cloneDeep(this.stages);

    // Write values back to the ArrayCopy...
    this.indicatorsForm.controls.forEach((indicatorGroup, index) => {
      const indicator = this.indicators.get(index);
      indicator.name = indicatorGroup.get('name').value;
      indicator.comment = indicatorGroup.get('comment').value;
      indicator.min = indicatorGroup.get('min').value;
      indicator.max = indicatorGroup.get('max').value;
      indicator.unit = indicatorGroup.get('unit').value;
      indicator.coefficient = indicatorGroup.get('weight').value;
    });

    // ...and then merge that back into the objective
    const dd = this.decisionData;
    this.indicators.mergeBack(
      this.indicatorData.indicators,
      (position: number, element: Indicator) => {
        // add
        dd.addObjectiveIndicator(this.objectiveIdx, position);
        Object.assign(this.indicatorData.indicators[position], element);
      },
      (position: number, element: Indicator) => {
        // set
        Object.assign(this.indicatorData.indicators[position], element);
      },
      (position: number) => {
        // remove
        dd.removeObjectiveIndicator(this.objectiveIdx, position);
      },
      (fromPosition: number, toPosition: number) => {
        // move
        dd.moveObjectiveIndicator(this.objectiveIdx, fromPosition, toPosition);
      }
    );
  }

  private validateCustomFormula(control: UntypedFormControl) {
    this.aggregationFormulaMin = this.calculateAggregationFunctionBounds('min', control.value);
    this.aggregationFormulaMax = this.calculateAggregationFunctionBounds('max', control.value);

    if (isNaN(this.aggregationFormulaMin) || isNaN(this.aggregationFormulaMax)) {
      return { invalidFormula: true };
    }

    const useAutomaticLimits =
      this.form?.get('automaticCustomAggregationLimits').value ?? this.indicatorData.automaticCustomAggregationLimits;

    if (useAutomaticLimits && this.aggregationFormulaMin === this.aggregationFormulaMax) {
      return { collapsedInterval: true };
    } else {
      return null;
    }
  }

  private validateRange(group: UntypedFormGroup) {
    if (this.form?.get('automaticCustomAggregationLimits').value ?? this.indicatorData.automaticCustomAggregationLimits) {
      return null;
    }

    const worst = group.get('worst').value,
      best = group.get('best').value;
    if (worst === best) {
      return { collapsedInterval: true };
    }
    return null;
  }

  private onCustomAggregationChange() {
    // Update validators
    if (this.form.get('useCustomAggregation').value) {
      this.form.get('customAggregationFormula').enable();
    } else {
      this.form.get('customAggregationFormula').disable();
    }
  }

  private getIndicatorForm(indicator: Indicator) {
    return this.fb.group({
      name: [indicator.name, Validators.required],
      comment: [indicator.comment],
      min: [indicator.min, Validators.required],
      max: [indicator.max, Validators.required],
      unit: [indicator.unit, Validators.maxLength(10)],
      weight: [indicator.coefficient, [Validators.required, Validators.min(0)]],
    });
  }

  addIndicator(name?: string) {
    const indicator = new Indicator(name);

    this.indicators.add(this.indicators.length, indicator);
    this.indicatorsForm.push(this.getIndicatorForm(indicator));
  }

  addIndicatorFromHierarchy([{ name }, treeLocationSequence, htmlElement]: [ObjectiveElement, number[], HTMLElement]) {
    if (treeLocationSequence.length === 1) {
      return;
    }

    const emptyIndicator = this.indicatorsForm.controls.find(indicatorControl => indicatorControl.get('name').value === '');

    if (emptyIndicator) {
      emptyIndicator.get('name').setValue(name);
    } else {
      this.addIndicator(name);
      this.cdRef.detectChanges();
      this.hierarchyContainer.nativeElement.scrollIntoView();
    }

    this.popOverService.whistle(htmlElement, $localize`Hinzugefügt!`, 'add');
  }

  convertStages(hasAggregationTypeChanged: boolean, newWorst: number, newBest: number) {
    if (hasAggregationTypeChanged && !this.stagesChanged) {
      // generate new stages
      this.stages = [];
      this.stages.push({ value: newWorst, description: '' });
      this.stages.push({ value: Math.min(newWorst, newBest) + Math.abs(newBest - newWorst) / 2, description: '' });
      this.stages.push({ value: newBest, description: '' });
    } else {
      // convert old stages
      for (let i = 0; i < this.stages.length; i++) {
        const valuePerc = (this.stages[i].value - this.lastWorst) / (this.lastBest - this.lastWorst);
        const value = newWorst + valuePerc * (newBest - newWorst);
        const description = this.stages[i].description;
        this.stages[i] = {
          value: value,
          description: description,
        };
      }
    }
    this.stages.map(s => {
      s.value = Math.round(s.value * (1 / this.roundingErrorPrecision)) / (1 / this.roundingErrorPrecision);
      return s;
    });
    this.lastWorst = newWorst;
    this.lastBest = newBest;
  }

  openDescription() {
    if (!this.form.valid) {
      return;
    }
    const indicators: Indicator[] = [];
    this.indicatorsForm.controls.forEach((indicatorGroup, index) => {
      const indicator = this.indicators.get(index);
      indicator.name = indicatorGroup.get('name').value;
      indicator.comment = indicatorGroup.get('comment').value;
      indicator.min = indicatorGroup.get('min').value;
      indicator.max = indicatorGroup.get('max').value;
      indicator.unit = indicatorGroup.get('unit').value;
      indicator.coefficient = indicatorGroup.get('weight').value;
      indicators.push(indicator);
    });
    let newWorst = this.form.get('range.worst').value;
    let newBest = this.form.get('range.best').value;
    if (this.form.get('useCustomAggregation').value && this.form.get('automaticCustomAggregationLimits').value) {
      newWorst = this.calculateAggregationFunctionBounds('min', this.form.get('customAggregationFormula').value);
      newBest = this.calculateAggregationFunctionBounds('max', this.form.get('customAggregationFormula').value);
    }
    this.convertStages(this.hasAggregationTypeChanged, newWorst, newBest);
    this.dialog
      .open(IndicatorDescriptionModalComponent, {
        data: {
          indicators: indicators,
          aggregate: {
            useCustomAggregation: this.form.get('useCustomAggregation').value,
            automaticCustomAggregationLimits: this.form.get('automaticCustomAggregationLimits').value,
            defaultAggregationWorst: this.form.get('range.worst').value,
            defaultAggregationBest: this.form.get('range.best').value,
            aggregationFormulaMin: this.calculateAggregationFunctionBounds('min', this.form.get('customAggregationFormula').value),
            aggregationFormulaMax: this.calculateAggregationFunctionBounds('max', this.form.get('customAggregationFormula').value),
            customAggregationFormula: this.form.get('customAggregationFormula').value,
            aggregatedUnit: this.form.get('aggregatedUnit').value,
            stages: this.stages,
          },
        },
      })
      .afterClosed()
      .subscribe((stages: { value: number; description: string }[]) => {
        if (stages) {
          this.stages = stages;
          this.stagesChanged = true;
        }
      });
  }

  deleteIndicator(index: number) {
    this.indicators.remove(index);
    this.indicatorsForm.removeAt(index);
  }

  indicatorDrop(event: CdkDragDrop<unknown>) {
    moveItemInArray(this.indicatorsForm.controls, event.previousIndex, event.currentIndex);
    this.indicators.move(event.previousIndex, event.currentIndex);
  }

  get defaultAggregationTex() {
    const addBracketOnNegative = (input: number) => (input < 0 ? `(${input})` : `${input}`);

    return (
      'f(' +
      range(this.indicatorsForm.length)
        .map(index => getIndicatorValueName(index))
        .join(',') +
      ') = ' +
      this.form.get('range.worst').value +
      ' + (' +
      this.form.get('range.best').value +
      ' - ' +
      this.form.get('range.worst').value +
      ') * \\cfrac{' +
      this.indicatorsForm.controls
        .map((indicatorControl, index) => {
          const min = addBracketOnNegative(indicatorControl.get('min').value);
          const max = addBracketOnNegative(indicatorControl.get('max').value);
          return `${getIndicatorCoefficientName(index)} * \\cfrac{${getIndicatorValueName(index)} - ${min}}{${max} - ${min}}`;
        })
        .join(' + ') +
      '}{' +
      range(this.indicatorsForm.length)
        .map(index => getIndicatorCoefficientName(index))
        .join(' + ') +
      '}'
    );
  }

  private calculateAggregationFunctionBounds(mode: 'min' | 'max', formula: string) {
    try {
      const tempIndicators = this.indicatorsForm.controls.map(
        control => new Indicator('', control.get('min').value, control.get('max').value, '', control.get('weight').value)
      );
      const af = getIndicatorAggregationFunction(tempIndicators, formula);
      return af(tempIndicators.map(ind => (mode === 'min' ? ind.min : ind.max)));
    } catch {
      return NaN;
    }
  }
}
