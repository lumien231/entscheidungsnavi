import { Component, ElementRef, Inject, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Indicator } from '@entscheidungsnavi/decision-data/classes';
import { PopOverService } from '@entscheidungsnavi/widgets';
import { cloneDeep } from 'lodash';

@Component({
  templateUrl: './indicator-description-modal.component.html',
  styleUrls: ['./indicator-description-modal.component.scss'],
})
export class IndicatorDescriptionModalComponent implements OnInit {
  @ViewChild('initParamsPopover') initParamsPopover: TemplateRef<any>;
  @ViewChild('initParamsOpener', { read: ElementRef }) initParamsOpener: ElementRef;
  @ViewChild('mainContainer') mainContainer: ElementRef;

  // data
  indicators: Indicator[];
  useCustomAggregation: boolean;
  automaticCustomAggregationLimits: boolean;
  defaultAggregationWorst: number;
  defaultAggregationBest: number;
  aggregationFormulaMin: number;
  aggregationFormulaMax: number;
  customAggregationFormula: string;
  aggregatedUnit: string;
  scaleMin: number;
  scaleMax: number;

  // stages
  numberOfStages: number;
  stagesPrecision: number;
  copyStages: { value: number; description: string }[]; // saves temporary stage data

  // popover
  initParamsForm: UntypedFormGroup;

  // UI
  addingStageMode = false;
  openedStage = -1;
  showStages = true;
  displayVariant: 'simple' | 'extended' = 'simple';

  // constants
  readonly minStages = 2;
  readonly maxStages = 7;
  readonly maxDecimalPoints = 14;
  readonly minNumberOfPrettyNumbers = 100;

  validStages() {
    let currentMinValue = Math.min(this.scaleMin, this.scaleMax);
    if (this.copyStages[0].value <= this.copyStages[this.copyStages.length - 1].value) {
      if (this.scaleMin > this.scaleMax) {
        return false;
      }
      for (let i = 0; i < this.copyStages.length; i++) {
        if (this.copyStages[i].value < currentMinValue || this.copyStages[i].value > Math.max(this.scaleMin, this.scaleMax)) {
          return false;
        }
        currentMinValue = this.copyStages[i].value;
      }
    } else {
      if (this.scaleMin < this.scaleMax) {
        return false;
      }
      for (let i = this.copyStages.length - 1; i >= 0; i--) {
        if (this.copyStages[i].value < currentMinValue || this.copyStages[i].value > Math.max(this.scaleMin, this.scaleMax)) {
          return false;
        }
        currentMinValue = this.copyStages[i].value;
      }
    }
    return true;
  }

  hasDuplicates() {
    const stageValues = this.copyStages.map(s => s.value);
    return new Set(stageValues).size !== stageValues.length;
  }

  validStagesInput() {
    const stepNumber = this.initParamsForm.get('stepNumber').value;
    if (stepNumber < this.minStages || stepNumber > this.maxStages) {
      return false;
    }
    return true;
  }

  constructor(
    public dialogRef: MatDialogRef<IndicatorDescriptionModalComponent>,
    private fb: UntypedFormBuilder,
    private popover: PopOverService,
    @Inject(MAT_DIALOG_DATA)
    data: {
      indicators: Indicator[];
      aggregate: {
        useCustomAggregation: boolean;
        automaticCustomAggregationLimits: boolean;
        defaultAggregationWorst: number;
        defaultAggregationBest: number;
        aggregationFormulaMin: number;
        aggregationFormulaMax: number;
        customAggregationFormula: string;
        aggregatedUnit: string;
        stages: { value: number; description: string }[];
      };
    }
  ) {
    this.useCustomAggregation = data.aggregate.useCustomAggregation;
    this.automaticCustomAggregationLimits = data.aggregate.automaticCustomAggregationLimits;
    this.defaultAggregationWorst = data.aggregate.defaultAggregationWorst;
    this.defaultAggregationBest = data.aggregate.defaultAggregationBest;
    this.aggregationFormulaMin = data.aggregate.aggregationFormulaMin;
    this.aggregationFormulaMax = data.aggregate.aggregationFormulaMax;
    this.customAggregationFormula = data.aggregate.customAggregationFormula;
    this.aggregatedUnit = data.aggregate.aggregatedUnit;
    this.copyStages = cloneDeep(data.aggregate.stages);
    this.numberOfStages = data.aggregate.stages.length;
    this.indicators = cloneDeep(data.indicators);
    if (!this.useCustomAggregation || !this.automaticCustomAggregationLimits) {
      // additive min/max
      this.scaleMin = data.aggregate.defaultAggregationWorst;
      this.scaleMax = data.aggregate.defaultAggregationBest;
    } else {
      // custom min/max
      this.scaleMin = data.aggregate.aggregationFormulaMin;
      this.scaleMax = data.aggregate.aggregationFormulaMax;
    }
  }

  ngOnInit() {
    // popover
    this.initParamsForm = this.fb.group({
      stepNumber: [this.numberOfStages, [Validators.required, Validators.min(2), Validators.max(7)]],
    });
    // stages precision
    if (!this.useCustomAggregation || !this.automaticCustomAggregationLimits) {
      this.stagesPrecision = this.findScalePrecision(
        this.defaultAggregationWorst,
        this.defaultAggregationBest,
        this.minNumberOfPrettyNumbers
      );
    } else {
      this.stagesPrecision = this.findScalePrecision(this.aggregationFormulaMin, this.aggregationFormulaMax, this.minNumberOfPrettyNumbers);
    }
  }

  findScalePrecision(lowerBound: number, upperBound: number, numberOfPrettyNumbers: number) {
    let precision = 1;
    for (let i = 0; i < this.maxDecimalPoints; i++) {
      if (Math.min(lowerBound, upperBound) + precision * numberOfPrettyNumbers <= Math.max(lowerBound, upperBound)) {
        return precision;
      } else {
        precision /= 10;
      }
    }
    return 1;
  }

  openInitParams() {
    this.popover.open(this.initParamsPopover, this.initParamsOpener, {
      position: [{ originX: 'center', originY: 'top', overlayX: 'center', overlayY: 'bottom' }],
    });
  }

  addStage(stageIdx: number) {
    if (this.numberOfStages >= this.maxStages) {
      // already at max number of stages (7)
      return;
    }
    let avgValue;
    if (stageIdx === 0) {
      avgValue = this.scaleMin;
    } else if (stageIdx === this.numberOfStages) {
      avgValue = this.scaleMax;
    } else {
      avgValue = (this.copyStages[stageIdx - 1].value + this.copyStages[stageIdx].value) / 2;
    }
    avgValue = Math.round(avgValue * (1 / this.stagesPrecision)) / (1 / this.stagesPrecision);
    // insert new row
    this.copyStages.splice(stageIdx, 0, { value: avgValue, description: '' });
    this.numberOfStages++;
    this.focusField(stageIdx);
  }

  deleteStage(stageIdx: number) {
    if (this.numberOfStages <= this.minStages || stageIdx === 0 || stageIdx === this.copyStages.length - 1) {
      return;
    }
    this.copyStages.splice(stageIdx, 1);
    this.numberOfStages--;
  }

  focusField(idx: number) {
    setTimeout(() => {
      const inputElements = document.querySelectorAll('.stage-value');
      (inputElements[idx] as HTMLElement)?.focus();
    }, 50);
  }

  saveData() {
    if (this.validStages() && !this.hasDuplicates()) {
      this.dialogRef.close(this.copyStages);
    }
  }

  changeStages(val: number) {
    if (this.numberOfStages + val < this.minStages || this.numberOfStages + val > this.maxStages) {
      return;
    }
    this.numberOfStages += val;
  }

  openStage(expandRowIdx: number) {
    if (!this.validStages() || this.hasDuplicates()) {
      return;
    }

    this.openedStage = expandRowIdx;
    this.showStages = false;
  }

  closeStage() {
    this.openedStage = -1;
    this.showStages = true;
  }

  toggleAddingStageMode() {
    if (!this.addingStageMode && this.numberOfStages >= this.maxStages) {
      return;
    }
    this.addingStageMode = !this.addingStageMode;
  }

  generateStages() {
    if (!this.validStagesInput()) {
      return;
    }
    const stepNumber = this.initParamsForm.get('stepNumber').value;
    this.copyStages = [];
    for (let i = 0; i < stepNumber; i++) {
      const value = this.scaleMin + (i * (this.scaleMax - this.scaleMin)) / (stepNumber - 1);
      const valueRounded = Math.round(value * (1 / this.stagesPrecision)) / (1 / this.stagesPrecision);
      this.copyStages.push({ value: valueRounded, description: '' });
    }
    this.numberOfStages = stepNumber;
    this.addingStageMode = false;
  }
}
