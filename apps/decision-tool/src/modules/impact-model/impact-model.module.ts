import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BreadcrumbOptionsModule } from '@entscheidungsnavi/widgets/breadcrumb-options';
import { KatexModule } from '@entscheidungsnavi/widgets/katex';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {
  HoverPopOverDirective,
  IsIntegerPipe,
  NoteBtnComponent,
  NoteBtnPresetPipe,
  OverflowDirective,
  WidthTriggerDirective,
} from '@entscheidungsnavi/widgets';
import { DiagramLegendComponent } from '@entscheidungsnavi/widgets/diagram-legend/diagram-legend.component';
import { SharedModule } from '../shared/shared.module';
import { ObjectiveAspectHierarchyModule } from '../objectives/aspect-hierarchy';
import { NoteHoverComponent } from '../shared/note-hover/note-hover.component';
import {
  CopyScaleModalComponent,
  ForecastOfOutcomesModalComponent,
  ImpactMatrixComponent,
  ImpactMatrixFieldComponent,
} from './impact-matrix';
import { ImpactModelRoutingModule } from './impact-model-routing.module';
import { IndicatorVariableNamePipe } from './indicator-variable-name.pipe';
import { ImpactModelComponent } from './main';
import { InfluenceFactorBoxComponent, InfluenceFactorModalComponent, InfluenceFactorsComponent } from './influence-factors';
import {
  ImpactModelExplanationComponent,
  ImpactModelHelpMainComponent,
  ImpactModelHint1Component,
  ImpactModelHint2Component,
  ImpactModelHint3Component,
  ImpactModelHint4Component,
  ImpactModelHint5Component,
  ImpactModelHint6Component,
} from './help';
import {
  IndicatorDescriptionModalComponent,
  IndicatorObjectiveScaleComponent,
  NumericalObjectiveScaleComponent,
  ObjectiveScaleModalComponent,
  VerbalObjectiveScaleComponent,
} from './objective-scale';
import { CombinationComponent } from './objective-scale/indicator-description-modal/combination-component/combination-component.component';
import { Help1Component } from './help/help1/help1.component';
import { Help2Component } from './help/help2/help2.component';
import { HelpBackgroundComponent } from './help/help-background/help-background.component';
import { TornadoDiagramComponent } from './impact-matrix/forecast-of-outcomes-modal/tornado-diagram/tornado-diagram.component';
import { RiskProfileComponent } from './impact-matrix/forecast-of-outcomes-modal/risk-profile/risk-profile.component';
import { NameFieldComponent } from './impact-matrix/name-field/name-field.component';
import { IndependentObjectiveValueComponent, ObjectiveValueComponent } from './impact-matrix/objective-value';

@NgModule({
  declarations: [
    CombinationComponent,
    CopyScaleModalComponent,
    ForecastOfOutcomesModalComponent,
    ImpactMatrixComponent,
    ImpactMatrixFieldComponent,
    ImpactModelComponent,
    ImpactModelExplanationComponent,
    ImpactModelHelpMainComponent,
    ImpactModelHint1Component,
    ImpactModelHint2Component,
    ImpactModelHint3Component,
    ImpactModelHint4Component,
    ImpactModelHint5Component,
    ImpactModelHint6Component,
    IndependentObjectiveValueComponent,
    IndicatorDescriptionModalComponent,
    IndicatorObjectiveScaleComponent,
    IndicatorVariableNamePipe,
    NameFieldComponent,
    NumericalObjectiveScaleComponent,
    ObjectiveScaleModalComponent,
    ObjectiveValueComponent,
    VerbalObjectiveScaleComponent,
    Help1Component,
    Help2Component,
    HelpBackgroundComponent,
    TornadoDiagramComponent,
    RiskProfileComponent,
    InfluenceFactorBoxComponent,
    InfluenceFactorModalComponent,
    InfluenceFactorsComponent,
  ],
  imports: [
    BreadcrumbOptionsModule,
    CommonModule,
    DragDropModule,
    DiagramLegendComponent,
    ImpactModelRoutingModule,
    KatexModule,
    ObjectiveAspectHierarchyModule,
    SharedModule,
    OverflowDirective,
    NoteBtnComponent,
    NoteBtnPresetPipe,
    HoverPopOverDirective,
    NoteHoverComponent,
    IsIntegerPipe,
    WidthTriggerDirective,
  ],
  exports: [ImpactMatrixComponent, InfluenceFactorsComponent],
})
export class ImpactModelModule {}
