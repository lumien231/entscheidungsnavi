import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Objective, ObjectiveInput, Outcome } from '@entscheidungsnavi/decision-data/classes';

/**
 * Displays a value field to input the outcome value of an objective.
 *
 * @example
 * ```html
 *  <dt-objective-value [objective]="ziel" [(outcomeValues)]="auspraegung.values[0]" (change)="onChange()"></dt-objective-value>
 * ```
 */

@Component({
  selector: 'dt-independent-objective-value',
  templateUrl: './independent-objective-value.component.html',
  styleUrls: ['./independent-objective-value.component.scss'],
})
export class IndependentObjectiveValueComponent implements OnInit {
  @Input() objective: Objective;
  indicatorAggregationFunction: (values: ObjectiveInput) => number;

  /**
   * This can be bound from decisionData -\> outcomes[i] -\> values.
   * It can be used with a two-way binding.
   */
  @Output() inputIsValid = new EventEmitter<boolean>();

  /* Inputs for independent outcome values. */
  @Input() areValuesIndependent = false;
  @Input() indicatorIdx: number;
  @Input() outcome: Outcome;
  @Input() stateIdx: number;
  @Input() independentOutcomeValues: ObjectiveInput[];
  @Output() independentOutcomeValuesChange = new EventEmitter<ObjectiveInput[]>();

  formControl: UntypedFormControl;
  formArray: UntypedFormArray;
  formGroup: UntypedFormGroup;

  get independentRowMin() {
    return this.objective.indicatorData.indicators[this.indicatorIdx].min;
  }

  get independentRowMax() {
    return this.objective.indicatorData.indicators[this.indicatorIdx].max;
  }

  ngOnInit() {
    // Initialize formControls. We assume the objective does not change while the component is displayed.
    if (this.objective.isIndicator) {
      /* Indicator scale */
      const formControls = this.independentOutcomeValues.map(
        (outcomeRow, rowIndex) =>
          new UntypedFormControl(this.independentOutcomeValues[rowIndex][this.indicatorIdx], [
            Validators.required,
            Validators.min(Math.min(this.independentRowMin, this.independentRowMax)),
            Validators.max(Math.max(this.independentRowMin, this.independentRowMax)),
          ])
      );

      this.formArray = new UntypedFormArray(formControls);
      this.formArray.valueChanges.subscribe(() => {
        this.independentOutcomeValues.forEach((value, index) => {
          value[this.indicatorIdx] = this.formArray.controls[index].value;
        });
        this.independentOutcomeValuesChange.emit(this.independentOutcomeValues);
      });
      this.formGroup = new UntypedFormGroup({ rowArray: this.formArray });
    } else {
      /* Numerical/Verbal scale */
      const min = this.objective.isNumerical ? Math.min(this.objective.numericalData.from, this.objective.numericalData.to) : 1;
      const max = this.objective.isNumerical
        ? Math.max(this.objective.numericalData.from, this.objective.numericalData.to)
        : this.objective.verbalData.optionCount();

      this.formControl = new UntypedFormControl(this.independentOutcomeValues[this.stateIdx], [
        Validators.required,
        Validators.min(min),
        Validators.max(max),
      ]);
      this.formGroup = new UntypedFormGroup({ value: this.formControl });
      this.formControl.valueChanges.subscribe(newValue => {
        this.independentOutcomeValues[this.stateIdx] = newValue;
        this.independentOutcomeValuesChange.emit(this.independentOutcomeValues);
      });
    }

    this.formGroup.statusChanges.subscribe(status => this.inputIsValid.emit(status === 'VALID'));
    this.inputIsValid.emit(this.formGroup.valid);
  }
}
