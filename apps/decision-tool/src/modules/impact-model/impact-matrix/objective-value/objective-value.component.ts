import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Objective, ObjectiveInput } from '@entscheidungsnavi/decision-data/classes';
import { range } from 'lodash';

/**
 * Displays a value field to input the outcome value of an objective.
 *
 * @example
 * ```html
 *  <dt-objective-value [objective]="ziel" [(outcomeValues)]="auspraegung.values[0]" (change)="onChange()"></dt-objective-value>
 * ```
 */

@Component({
  selector: 'dt-objective-value',
  templateUrl: './objective-value.component.html',
  styleUrls: ['./objective-value.component.scss'],
})
export class ObjectiveValueComponent implements OnInit, OnChanges {
  @Input() objective: Objective;
  indicatorAggregationFunction: (values: ObjectiveInput) => number;

  /**
   * This can be bound from decisionData -\> outcomes[i] -\> values.
   * It can be used with a two-way binding.
   */
  @Input() outcomeValues: ObjectiveInput;
  @Output() outcomeValuesChange = new EventEmitter<ObjectiveInput>();

  @Output() inputIsValid = new EventEmitter<boolean>();

  formControl: UntypedFormControl;
  formArray: UntypedFormArray;
  formGroup: UntypedFormGroup;

  ngOnInit() {
    // Initialize formControls. We assume the objective does not change while the component is displayed.
    if (this.objective.isIndicator) {
      /* Indicator scale */
      const formControls = this.objective.indicatorData.indicators.map(
        (indicator, index) =>
          new UntypedFormControl(this.outcomeValues[index], [
            Validators.required,
            Validators.min(Math.min(indicator.min, indicator.max)),
            Validators.max(Math.max(indicator.min, indicator.max)),
          ])
      );

      const aggregationFunction = this.objective.indicatorData.aggregationFunction;
      // Return null for the aggregated value if not all fields are valid
      this.indicatorAggregationFunction = (values: ObjectiveInput) => {
        if (this.formArray?.controls.every((control: UntypedFormControl) => control.errors == null)) {
          return aggregationFunction(values);
        } else {
          return null;
        }
      };

      const indicatorAggregateValidator: ValidatorFn = (control: UntypedFormArray) => {
        const value = this.indicatorAggregationFunction(control.controls.map((control: UntypedFormControl) => control.value));
        return value != null &&
          (Number.isNaN(value) || !isIn(value, this.objective.indicatorData.worstValue, this.objective.indicatorData.bestValue))
          ? { indicatorAggregate: true }
          : null;
      };

      this.formArray = new UntypedFormArray(formControls, indicatorAggregateValidator);
      this.formArray.valueChanges.subscribe(() => {
        this.outcomeValuesChange.emit(
          range(this.objective.indicatorData.indicators.length).map(index => this.formArray.controls[index].value)
        );
      });
      this.formGroup = new UntypedFormGroup({ rowArray: this.formArray });
    } else {
      /* Numerical/Verbal scale */
      const min = this.objective.isNumerical ? Math.min(this.objective.numericalData.from, this.objective.numericalData.to) : 1;
      const max = this.objective.isNumerical
        ? Math.max(this.objective.numericalData.from, this.objective.numericalData.to)
        : this.objective.verbalData.optionCount();

      this.formControl = new UntypedFormControl(this.outcomeValues, [Validators.required, Validators.min(min), Validators.max(max)]);
      this.formGroup = new UntypedFormGroup({ value: this.formControl });
      this.formControl.valueChanges.subscribe(newValue => {
        this.outcomeValues = newValue;
        this.outcomeValuesChange.emit(newValue);
      });
    }

    this.formGroup.statusChanges.subscribe(status => this.inputIsValid.emit(status === 'VALID'));
    this.inputIsValid.emit(this.formGroup.valid);
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('outcomeValues' in changes) {
      this.formControl?.setValue(this.outcomeValues);
    }
  }
}

export function isIn(number: number, i1: number, i2: number) {
  if (isNaN(number)) {
    return true;
  }

  if (i2 > i1) {
    return i1 <= number && number <= i2;
  } else if (i2 < i1) {
    return i1 >= number && number >= i2;
  } else {
    return i1 === number;
  }
}
