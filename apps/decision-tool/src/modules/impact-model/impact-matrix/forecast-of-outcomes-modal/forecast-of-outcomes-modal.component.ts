import { Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ConfirmModalComponent, ConfirmModalData, ModalComponent } from '@entscheidungsnavi/widgets';
import {
  Alternative,
  InfluenceFactor,
  Objective,
  Outcome,
  PREDEFINED_INFLUENCE_FACTORS,
  PredefinedInfluenceFactor,
  UserdefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data/classes';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { isEqual } from 'lodash';
import { validateValue } from '@entscheidungsnavi/decision-data/validations';
import { lastValueFrom } from 'rxjs';
import {
  MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA,
  MatLegacyDialog as MatDialog,
  MatLegacyDialogRef as MatDialogRef,
} from '@angular/material/legacy-dialog';
import { checkType } from '@entscheidungsnavi/tools';
import {
  InfluenceFactorHelperService,
  InfluenceFactorModalComponent,
  InfluenceFactorModalData,
  InfluenceFactorModalResult,
} from '../../influence-factors';
import { TornadoDiagramComponent } from './tornado-diagram/tornado-diagram.component';

@Component({
  selector: 'dt-forecast-of-outcomes-modal',
  templateUrl: './forecast-of-outcomes-modal.component.html',
  styleUrls: ['./forecast-of-outcomes-modal.component.scss'],
})
export class ForecastOfOutcomesModalComponent implements OnInit {
  @Input() objective: Objective;
  @Input() alternative: Alternative;
  @Input() outcome: Outcome;

  @Output() modalSave = new EventEmitter<void>();
  @Output() influenceFactorModified = new EventEmitter<number>(); // Emits the ID of the modified influence factor

  @ViewChild('mainModal') modal: ModalComponent;
  @ViewChild('confirmationModal', { static: true }) confirmationModal: ModalComponent;
  @ViewChild('tornadoDiagram') tornadoDiagram: TornadoDiagramComponent;

  mobileMode: boolean;

  copy: Outcome;
  Object = Object;

  selectedTabIndex = 0;
  predefinedInfluenceFactors = Object.values(PREDEFINED_INFLUENCE_FACTORS);

  readonly wrongOrderMessage = $localize`Unzulässige Reihenfolge der angegebenen Werte.`;
  readonly noTornadoAvailableMessage = $localize`Die Darstellung eines Tornadodiagramms setzt voraus,
  dass eine Indikatorskala mit systemseitigen Einflussfaktoren verknüpft wurde.`;

  get hasChanges() {
    return !(
      isEqual(this.outcome.values, this.copy.values) &&
      this.outcome.influenceFactor === this.copy.influenceFactor &&
      this.outcome.comment === this.copy.comment &&
      this.outcome.processed === this.copy.processed
    );
  }

  get isValid() {
    return this.copy.values.every(value => validateValue(value, this.objective)[0]) && this.isCorrectSystemSidedIF();
  }

  get isTornadoAvailable() {
    return this.objective.isIndicator && this.isInfluenceFactorPredefined(this.copy.unsicherheitsfaktor) && this.isCorrectSystemSidedIF();
  }

  constructor(
    protected decisionData: DecisionData,
    @Inject(MAT_DIALOG_DATA) private data: { selectedTabIndex: number; alternative: Alternative; objective: Objective; outcome: Outcome },
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<ForecastOfOutcomesModalComponent>,
    private influenceFactorHelper: InfluenceFactorHelperService
  ) {
    this.selectedTabIndex = data.selectedTabIndex;
    this.alternative = data.alternative;
    this.objective = data.objective;
    this.outcome = data.outcome;
  }

  ngOnInit() {
    this.load();
    if (this.selectedTabIndex === 2 && !this.isTornadoAvailable) {
      this.selectedTabIndex = 1;
    }
    if (!this.isValid) {
      this.selectedTabIndex = 0;
    }
  }

  close() {
    this.dialogRef.close(true);
  }

  async tryClose() {
    // if (this.isClosing) {
    //   return false;
    // }

    if (!this.hasChanges || (await this.confirmDiscard())) {
      this.close();
      return true;
    }

    return false;
  }

  private async confirmDiscard() {
    const result = await lastValueFrom(
      this.dialog
        .open(ConfirmModalComponent, {
          data: checkType<ConfirmModalData>({
            title: $localize`Ungespeicherte Änderungen`,
            prompt: $localize`Bist Du sicher, dass Du die ungespeicherten Änderungen an der Wirkungsprognose verwerfen willst?`,
            template: 'discard',
          }),
        })
        .afterClosed()
    );
    return !!result;
  }

  private load() {
    this.copy = this.outcome.clone();
  }

  apply() {
    if (this.isValid) {
      this.save();
      this.close();
    }
  }

  save() {
    this.outcome.copyBack(this.copy);
  }

  changeUncertaintyFactor(newUf: UserdefinedInfluenceFactor) {
    this.copy.setInfluenceFactor(newUf);
  }

  /* Checks if row of values follows the order the corresponding (indicator/total) scale. */
  isCorrectSystemSidedIF() {
    if (!this.isInfluenceFactorPredefined(this.copy.unsicherheitsfaktor) || this.copy.unsicherheitsfaktor == null) {
      return true;
    }
    if (this.objective.isIndicator) {
      for (let i = 0; i < (this.copy.values[0] as number[]).length; i++) {
        const direction = this.objective.indicatorData.indicators[i].max - this.objective.indicatorData.indicators[i].min;
        if (
          (Math.sign(this.copy.values[2][i] - this.copy.values[1][i]) !== Math.sign(direction) &&
            this.copy.values[2][i] - this.copy.values[1][i] !== 0) ||
          (Math.sign(this.copy.values[1][i] - this.copy.values[0][i]) !== Math.sign(direction) &&
            this.copy.values[1][i] - this.copy.values[0][i] !== 0)
        ) {
          return false;
        }
      }
    } else {
      let direction = 1; // always positive for verbal scale
      if (this.objective.isNumerical) {
        direction = this.objective.numericalData.to - this.objective.numericalData.from;
      }
      const copyValues = this.copy.values as number[];
      if (
        (Math.sign(copyValues[2] - copyValues[1]) !== Math.sign(direction) && copyValues[2] - copyValues[1] !== 0) ||
        (Math.sign(copyValues[1] - copyValues[0]) !== Math.sign(direction) && copyValues[1] - copyValues[0] !== 0)
      ) {
        return false;
      }
    }
    return true;
  }

  /**
   * The precision in decision-data is relative to the probability. This is calculated here.
   */
  relativeDeviation(probability: number, praezision: number) {
    return probability > 0 ? praezision * Math.min(1 - probability / 100, probability / 100) : 0;
  }

  isInfluenceFactorPredefined(influenceFactor: InfluenceFactor): influenceFactor is PredefinedInfluenceFactor {
    return influenceFactor instanceof PredefinedInfluenceFactor;
  }

  editInfluenceFactor() {
    if (this.copy.influenceFactor != null) {
      const influenceFactor = this.copy.influenceFactor as UserdefinedInfluenceFactor;
      this.dialog
        .open(InfluenceFactorModalComponent, {
          data: checkType<InfluenceFactorModalData>({
            influenceFactor: influenceFactor,
            onSaveCallback: influenceFactorId => {
              this.changeUncertaintyFactor(this.decisionData.influenceFactors[influenceFactorId]);
              this.influenceFactorModified.emit(influenceFactorId);
            },
          }),
        })
        .afterClosed()
        .subscribe(async (result: InfluenceFactorModalResult) => {
          if (result.showConnections && (await this.tryClose())) {
            this.influenceFactorHelper.showInfluenceFactorConnections(influenceFactor);
          }
        });
    }
  }

  addInfluenceFactor() {
    this.dialog.open(InfluenceFactorModalComponent, {
      data: checkType<InfluenceFactorModalData>({
        onSaveCallback: influenceFactorId => this.changeUncertaintyFactor(this.decisionData.influenceFactors[influenceFactorId]),
      }),
    });
  }
}
