import { Component, Input, OnInit } from '@angular/core';
import {
  Alternative,
  generatePredefinedIndicatorScenarios,
  Objective,
  ObjectiveInput,
  Outcome,
  PredefinedInfluenceFactor,
} from '@entscheidungsnavi/decision-data/classes';
import { getIndicatorAggregationFunction } from '@entscheidungsnavi/decision-data/calculation';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { clone, flatten, round } from 'lodash';

@Component({
  selector: 'dt-risk-profile',
  templateUrl: './risk-profile.component.html',
  styleUrls: ['./risk-profile.component.scss'],
})
export class RiskProfileComponent implements OnInit {
  @Input()
  alternative: Alternative;

  @Input()
  objective: Objective;

  @Input()
  outcome: Outcome;

  /* Options in "Vergleichen mit" dropdown. Excludes current outcome. */
  filterOutcomes: { alternativeIndex: number; outcome: Outcome }[];
  /* Selected indexes */
  filterIndexes: number[] = [];

  /* Used for numerical/indicator scales. */
  scaleMin: number;
  scaleMax: number;

  /* Used for indicator scales. */
  indicatorFunction: (values: ObjectiveInput) => number;

  /* Used for verbal scales. */
  verbalOptions: string[] = [];

  scaleTotalMin: number;
  scaleTotalMax: number;

  scaleUnit = '';

  /* "Bandbreite" dropdown. */
  useIndicatorScale = true;

  objectiveScaleType: 'numerical' | 'verbal' | 'indicator';

  /* Contains the names of possible alternatives for comparison. */
  legendList: string[] = [];

  /* Used for numerical/indicator scales. */
  readonly numberOfVerticalLines = 9;
  /* Used for all scales. */
  readonly numberofHorizontalLines = 11; // 0.0, 0.1, .. 0.9, 1.0
  /* Spacing between y axis values. */
  readonly lineSpacing = 40; // px
  /* Array of function colors. 7th function thakes lineColours[0], 8th - [1], etc. */
  readonly lineColours = [
    'hsl(208deg 35% 40%)',
    'hsl(0deg 50% 40%)',
    'hsl(28deg 75% 45%)',
    'hsl(448deg 35% 40%)',
    'hsl(268deg 35% 40%)',
    'hsl(174 50% 40%)',
  ];
  /* Thickness of vertical and horizontal lines. */
  readonly lineThickness = 3;
  /* Width of the chart as percentage of the chart canvas. */
  readonly chartWidthPercentage = 95;
  /* Height of y axis labels. */
  readonly yLabelHeight = 30;
  /* Number of significant digits when rounding numbers. */
  readonly significantDigits = 5;
  readonly machinePrecison = 15;

  riskProfileValues: {
    /* Objective Scale value for numerical/indicator scale, index of characteristic for verbal scale; (x-value) */
    combination: number;
    /* Porbability of combination */
    probability: number;
    /* 1 - Cumulative probability (y-value) */
    riskProbability: number;
    /* Changes on hover. */
    isVisible: boolean;
  }[][] = [];

  hoveredIndex = -1;

  constructor(protected decisionData: DecisionData) {}

  ngOnInit() {
    const objectiveIndex = this.decisionData.objectives.findIndex(objective => {
      return this.objective === objective;
    });
    if (objectiveIndex === -1) {
      this.filterOutcomes = [];
    } else {
      /* Select all outcomes of the current objective that are different from the current outcome and have defined values. */
      this.filterOutcomes = this.decisionData.outcomes
        .map((outcomes, index) => {
          return { alternativeIndex: index, outcome: outcomes[objectiveIndex] };
        })
        .filter((filterOutcome, rowIndex) => {
          // outcome.values can include nulls if any of the values is unset, or they can include undefined if the alternative is fresh
          return (
            !flatten(filterOutcome.outcome.values).includes(undefined) &&
            !flatten(filterOutcome.outcome.values).includes(null) &&
            this.decisionData.alternatives[rowIndex] !== this.alternative
          );
        });
      this.constructLegendList();
    }

    if (this.objective.isNumerical) {
      this.objectiveScaleType = 'numerical';
      this.scaleTotalMin = this.objective.numericalData.from;
      this.scaleTotalMax = this.objective.numericalData.to;
      this.scaleUnit = this.objective.numericalData.unit;
    }
    if (this.objective.isVerbal) {
      this.objectiveScaleType = 'verbal';
      this.verbalOptions = clone(this.objective.verbalData.options);
    }
    if (this.objective.isIndicator) {
      this.objectiveScaleType = 'indicator';
      this.scaleUnit = this.objective.indicatorData.aggregatedUnit;
      /* Get indicator scale min/max. */
      if (!this.objective.indicatorData.useCustomAggregation) {
        /* Additive min/max. */
        this.scaleTotalMin = this.objective.indicatorData.defaultAggregationWorst;
        this.scaleTotalMax = this.objective.indicatorData.defaultAggregationBest;
        this.indicatorFunction = getIndicatorAggregationFunction(this.objective.indicatorData.indicators, {
          worst: this.objective.indicatorData.defaultAggregationWorst,
          best: this.objective.indicatorData.defaultAggregationBest,
        });
      } else {
        /* Custom min/max */
        this.indicatorFunction = getIndicatorAggregationFunction(
          this.objective.indicatorData.indicators,
          this.objective.indicatorData.customAggregationFormula
        );
        this.scaleTotalMin = this.indicatorFunction(this.objective.indicatorData.indicators.map(ind => ind.min));
        this.scaleTotalMax = this.indicatorFunction(this.objective.indicatorData.indicators.map(ind => ind.max));
      }
    }
    /* Use indicator scale min/max by default. */
    this.scaleMin = this.scaleTotalMin;
    this.scaleMax = this.scaleTotalMax;

    this.riskProfileValues = new Array(this.filterOutcomes.length + 1).fill(null).map(() => []);

    this.plotOutcome(this.outcome);
  }

  private plotOutcome(outcome: Outcome, functionIndex = 0) {
    switch (this.objectiveScaleType) {
      case 'numerical':
        if (outcome.values.length > 1) {
          for (let i = 0; i < outcome.values.length; i++) {
            this.riskProfileValues[functionIndex].push({
              combination: outcome.values[i] as number,
              probability: outcome.influenceFactor?.states[i].probability / 100 ?? 0,
              riskProbability: 0,
              isVisible: false,
            });
          }
        } else {
          this.riskProfileValues[functionIndex].push({
            combination: outcome.values[0] as number,
            probability: 1,
            riskProbability: 0,
            isVisible: false,
          });
        }
        this.riskProfileValues[functionIndex].push({
          combination: this.scaleTotalMax,
          probability: 0,
          riskProbability: 0,
          isVisible: false,
        });
        break;
      case 'verbal':
        if (outcome.values.length > 1) {
          for (let i = 0; i < outcome.values.length; i++) {
            this.riskProfileValues[functionIndex].push({
              combination: (outcome.values[i] as number) - 1,
              probability: outcome.influenceFactor?.states[i].probability / 100 ?? 0,
              riskProbability: 0,
              isVisible: false,
            });
          }
        } else {
          this.riskProfileValues[functionIndex].push({
            combination: (outcome.values[0] as number) - 1,
            probability: 1,
            riskProbability: 0,
            isVisible: false,
          });
        }
        this.riskProfileValues[functionIndex].push({
          combination: this.verbalOptions.length - 1,
          probability: 0,
          riskProbability: 0,
          isVisible: false,
        });
        break;
      case 'indicator':
        /* Generate all indicator value combinations */
        if (outcome.influenceFactor) {
          if (outcome.influenceFactor instanceof PredefinedInfluenceFactor) {
            const cartesian = generatePredefinedIndicatorScenarios(
              outcome.values as number[][],
              outcome.influenceFactor.states.map(state => state.probability / 100)
            );
            cartesian.forEach(combination => {
              this.riskProfileValues[functionIndex].push({
                combination: round(this.indicatorFunction(combination.value), this.machinePrecison),
                probability: round(combination.probability, this.machinePrecison),
                riskProbability: 0,
                isVisible: false,
              });
            });
          } else {
            for (let i = 0; i < outcome.values.length; i++) {
              this.riskProfileValues[functionIndex].push({
                combination: round(this.indicatorFunction(outcome.values[i]), this.machinePrecison),
                probability: round(outcome.influenceFactor.states[i].probability / 100 ?? 0, this.machinePrecison),
                riskProbability: 0,
                isVisible: false,
              });
            }
          }
        } else {
          this.riskProfileValues[functionIndex].push({
            combination: round(this.indicatorFunction(outcome.values[0]), this.machinePrecison),
            probability: 1,
            riskProbability: 0,
            isVisible: false,
          });
        }
        this.riskProfileValues[functionIndex].push({
          combination: this.scaleTotalMax,
          probability: 0,
          riskProbability: 0,
          isVisible: false,
        });
        break;
    }

    /* Order by X value. */
    const ascOrDesc = this.scaleTotalMin > this.scaleTotalMax ? 1 : -1;

    this.riskProfileValues[functionIndex].sort((a, b) => {
      if (a.combination < b.combination) {
        return ascOrDesc;
      }
      if (a.combination > b.combination) {
        return -ascOrDesc;
      }
      if (a.combination === b.combination) {
        return a.probability > b.probability ? ascOrDesc : -ascOrDesc;
      }
    });

    /* Merge points with same x. Take the y of the latest equal point.
    E.g. (1, 5), (1, 3), (1, 2) -> (1, 2) */
    const duplicateRiskValueIndexes = [];
    for (let i = 0; i < this.riskProfileValues[functionIndex].length - 2; i++) {
      if (
        i < this.riskProfileValues[functionIndex].length - 1 &&
        this.riskProfileValues[functionIndex][i + 1].combination === this.riskProfileValues[functionIndex][i].combination
      ) {
        duplicateRiskValueIndexes.push(i);
        /* Add probabilities of merged values. */
        this.riskProfileValues[functionIndex][i + 1].probability += this.riskProfileValues[functionIndex][i].probability;
      }
    }
    for (let i = duplicateRiskValueIndexes.length - 1; i >= 0; i--) {
      this.riskProfileValues[functionIndex].splice(duplicateRiskValueIndexes[i], 1);
    }

    /* Calculate riskProbability (1 - probability). */
    for (let i = this.riskProfileValues[functionIndex].length - 2; i >= 0; i--) {
      this.riskProfileValues[functionIndex][i].riskProbability = round(
        this.riskProfileValues[functionIndex][i + 1].riskProbability + this.riskProfileValues[functionIndex][i].probability,
        this.machinePrecison
      );
    }
  }

  constructLegendList() {
    this.legendList = [this.alternative.name];
    this.filterOutcomes.forEach(filterOutcome => {
      if (this.filterIndexes.includes(filterOutcome.alternativeIndex)) {
        this.legendList.push(this.decisionData.alternatives[filterOutcome.alternativeIndex].name);
      }
    });
  }

  isLineHovered(lineIndex: number) {
    return this.hoveredIndex === lineIndex;
  }

  /* Adjust X axis depending on scale option (drop-down). */
  formatXAxis() {
    if (this.useIndicatorScale) {
      this.scaleMin = this.scaleTotalMin;
      this.scaleMax = this.scaleTotalMax;
    } else {
      /* Get min of minimums and max of maximums over all displayed functions. */
      const minValues = this.riskProfileValues.map(riskArray => riskArray[0]?.combination).filter(x => x);
      const maxValues = this.riskProfileValues.map(riskArray => riskArray[riskArray.length - 2]?.combination).filter(x => x);
      if (this.scaleTotalMin < this.scaleTotalMax) {
        this.scaleMin = Math.min(...minValues);
        this.scaleMax = Math.max(...maxValues);
      } else {
        this.scaleMin = Math.max(...minValues);
        this.scaleMax = Math.min(...maxValues);
      }
      if (this.scaleMin === this.scaleMax) {
        this.scaleMax = this.scaleTotalMax;
      }
    }
  }

  changeComparisonFilter() {
    for (const [riskProfileIndex, filterOutcome] of this.filterOutcomes.entries()) {
      /*
        riskProfileValues includes default Outcome ([0]) + remaining valid Outcomes from the current objective ([1,2,..])
        filterOutcomes includes all valid Outcomes from the current objective ([0,1,..])
        filterIndexes includes the alternative indexes of all filterOutcomes that are selected in the dt-select
      */
      if (this.filterIndexes.includes(filterOutcome.alternativeIndex)) {
        if (this.riskProfileValues[riskProfileIndex + 1].length === 0) {
          this.plotOutcome(filterOutcome.outcome, riskProfileIndex + 1);
        }
      } else {
        this.riskProfileValues[riskProfileIndex + 1] = [];
      }
    }
    this.formatXAxis();
    this.constructLegendList();
  }

  calculateLeftPercentage(
    index: number,
    riskArray: { combination: number; probability: number; riskProbability: number; isVisible: boolean }[]
  ) {
    if (this.objectiveScaleType === 'verbal') {
      return (riskArray[index].combination * this.chartWidthPercentage) / (this.verbalOptions.length - 1);
    }
    if (index === riskArray.length - 1) {
      return this.chartWidthPercentage;
    }
    const numerator =
      this.scaleMax > this.scaleMin ? riskArray[index].combination - this.scaleMin : this.scaleMin - riskArray[index].combination;
    return (numerator / Math.abs(this.scaleMin - this.scaleMax)) * this.chartWidthPercentage;
  }

  calculateLeftPercentageStepLine(
    index: number,
    riskArray: { combination: number; probability: number; riskProbability: number; isVisible: boolean }[]
  ) {
    return index === 0 ? 0 : this.calculateLeftPercentage(index - 1, riskArray);
  }

  calculateWidthPercentageStepLine(
    index: number,
    riskArray: { combination: number; probability: number; riskProbability: number; isVisible: boolean }[]
  ) {
    return this.calculateLeftPercentage(index, riskArray) - this.calculateLeftPercentageStepLine(index, riskArray);
  }

  createXTooltipValue(value: number) {
    return this.objectiveScaleType === 'verbal' ? this.verbalOptions[value] : value;
  }

  createTooltip(
    riskArray: { combination: number; probability: number; riskProbability: number; isVisible: boolean }[],
    valueIndex: number
  ) {
    return `
      (
      x: ${
        this.objectiveScaleType === 'verbal' ? this.verbalOptions[riskArray[valueIndex].combination] : riskArray[valueIndex].combination
      } ${this.scaleUnit};
      y: ${round(riskArray[valueIndex + 1].riskProbability * 100, 2)}
      %
      )`;
  }

  isNumberRounded(value: number) {
    return value > 999999 || value < -999999 || String(value).split('.')[1]?.length > this.significantDigits;
  }
}
