import { ProjectMode } from '@entscheidungsnavi/decision-data/classes';
import { NAVI_STEP_ORDER } from '@entscheidungsnavi/decision-data/steps';
import { helpPage } from '../../../app/help/help';
import { AlternativenExplanationComponent } from './explanation.component';

export function getTutorialPage() {
  return helpPage()
    .youtube('7SjVzZruQgM')
    .name($localize`YouTube-Tutorial zu Schritt ${NAVI_STEP_ORDER.indexOf('alternatives') + 1}`)
    .build();
}

export function getExplanationPage(mode: ProjectMode) {
  return helpPage()
    .explanation(AlternativenExplanationComponent)
    .name(
      mode === 'educational'
        ? $localize`Wozu dient Schritt ${NAVI_STEP_ORDER.indexOf('alternatives') + 1}?`
        : $localize`Mehr zur Beschreibung der Alternativen`
    )
    .build();
}
