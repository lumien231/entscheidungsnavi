import { Component, Injector } from '@angular/core';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { AbstractAlternativeHintComponent } from '../alternative-hint.component';
import { Help1Component } from './help1/help1.component';
import { Help2Component } from './help2/help2.component';

@Component({
  templateUrl: './hint6.component.html',
})
export class AlternativeHint6Component extends AbstractAlternativeHintComponent {
  get helpMenu() {
    return {
      educational: [
        helpPage()
          .name($localize`So funktioniert's`)
          .component(Help1Component)
          .build(),
        helpPage()
          .name($localize`Weitere Hinweise`)
          .component(Help2Component)
          .build(),
        helpPage()
          .name($localize`Hintergrundwissen zum Schritt 3`)
          .component(HelpBackgroundComponent)
          .build(),
      ],
    };
  }

  constructor(injector: Injector) {
    super(injector);
    this.pageKey = 6;
  }
}
