import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Alternative, Objective } from '@entscheidungsnavi/decision-data/classes';

@Component({
  selector: 'dt-alternativen-hint2-element',
  templateUrl: './hint2-element.component.html',
  styleUrls: ['./hint2-element.component.scss'],
})
export class AlternativeHint2ListElementComponent {
  @Input() alternative: Alternative;
  @Input() objectives: Objective[];
  @Input() readonly = false;

  @Output() add = new EventEmitter<Alternative>();

  @ViewChild('alternativeName', { static: true }) alternativeInput: ElementRef;

  comment = '';
  name = '';
  selectedObjective: Objective;

  selectObjective(objective: Objective) {
    this.selectedObjective = objective;
    setTimeout(() => this.alternativeInput.nativeElement.focus(), 0);
    // eslint-disable-next-line max-len
    this.comment = $localize`Entstanden aus der Alternative "${this.alternative.name}" durch Verbesserung im Ziel "${this.selectedObjective.name}".`;
  }

  addAlternative() {
    if (!this.readonly && this.name && this.selectedObjective) {
      this.add.emit(new Alternative(2, this.name, this.comment));
      this.name = '';
      this.comment = '';
      this.selectedObjective = undefined;
    }
  }
}
