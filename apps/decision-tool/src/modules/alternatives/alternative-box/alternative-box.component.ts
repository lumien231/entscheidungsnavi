import { Component, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { Alternative } from '@entscheidungsnavi/decision-data/classes';
import { ConfirmModalComponent, ConfirmModalData, PopOverService } from '@entscheidungsnavi/widgets';
import { loadingIndicator } from '@entscheidungsnavi/widgets/loading-snackbar/loading-snackbar.component';
import { AppSettingsService } from '../../../app/data/app-settings.service';
import { TransferService } from '../../../app/transfer';
import { ProjectService } from '../../../app/data/project';

@Component({
  selector: 'dt-alternative-box',
  templateUrl: './alternative-box.component.html',
  styleUrls: ['./alternative-box.component.scss'],
})
export class AlternativeBoxComponent {
  @Input() alternative: Alternative;
  @Input() lockedChildren: boolean[];
  @Input() namePlaceholder: string;
  @Input() notePlaceholder: string;

  @Output() deleteClick = new EventEmitter<void>();
  @Output() childrenChange = new EventEmitter<void>();

  @Input() expanded: boolean;
  @Output() expandedChange = new EventEmitter<boolean>();

  @Input() isLocked = false;
  @Input() readonly = false;

  get sendEnabled() {
    return this.appSettings.showSendButtons;
  }

  get teamTrait() {
    return this.projectService.getTeamTrait();
  }

  constructor(
    private transferService: TransferService,
    private popOverService: PopOverService,
    private dialog: MatDialog,
    private appSettings: AppSettingsService,
    private projectService: ProjectService,
    private snackBar: MatSnackBar
  ) {}

  toggleExpansion() {
    this.expanded = !this.expanded;
  }

  sendAlternative(element: ElementRef<HTMLElement>) {
    this.transferService.broadcastAlternative(this.alternative).then(() => {
      this.popOverService.whistle(element, $localize`Alternative gesendet!`);
    });
  }

  deleteChild(index: number) {
    this.dialog
      .open(ConfirmModalComponent, {
        data: {
          title: $localize`Eingruppierte Alternative löschen`,
          prompt: $localize`Bist Du sicher, dass Du die eingruppierte Alternative
          „${this.alternative.children[index].name}“ löschen möchtest?`,
          template: 'delete',
        } as ConfirmModalData,
      })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.alternative.children.splice(index, 1);
          this.childrenChange.emit();
        }
      });
  }
  onExpandedChange(expanded: boolean) {
    this.expanded = expanded;
    this.expandedChange.emit(expanded);
  }

  transferAlternativeToMe(buttonElement: ElementRef<HTMLElement>) {
    this.teamTrait
      .transferObject(this.alternative.uuid)
      .pipe(loadingIndicator(this.snackBar, $localize`Alternative wird übernommen...`))
      .subscribe({
        next: () => this.popOverService.whistle(buttonElement, $localize`Alternative übernommen!`, 'check'),
        error: () =>
          this.popOverService.whistle(buttonElement, $localize`Beim Übernehmen der Alternative ist ein Fehler aufgetreten.`, 'error'),
      });
  }
}
