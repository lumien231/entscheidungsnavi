import { moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, EventEmitter, Input, Output, QueryList, ViewChildren } from '@angular/core';
import { NotePage } from '@entscheidungsnavi/decision-data/classes';
import { AspectBoxComponent } from '@entscheidungsnavi/widgets';

@Component({
  selector: 'dt-objective-group',
  styleUrls: ['./objective-group.component.scss'],
  templateUrl: './objective-group.component.html',
})
export class ObjectiveGroupComponent {
  static dragDropData: {
    noteGroupIndex: number;
    noteIndex: number;
  };

  @Input() notePage: NotePage;
  @Input() noteGroupIdx: number;

  @Input() readonly = false;
  @Input() showAddButton = true;

  @Output() dirty = new EventEmitter(); // Emit an event when something besides inputs changes

  @ViewChildren(AspectBoxComponent) items: QueryList<AspectBoxComponent>;

  get noteGroup() {
    return this.notePage.notizGroups[this.noteGroupIdx];
  }

  removeNoteGroup() {
    this.notePage.removeNotizGroup(this.noteGroupIdx);
    this.dirty.emit();
  }

  removeNote(index: number) {
    this.noteGroup.removeNotiz(index);
    this.dirty.emit();
  }

  addItem() {
    this.notePage.addNotiz('', this.noteGroupIdx);
    setTimeout(() => this.items.last.edit(), 0);
    this.dirty.emit();
  }

  dragOverGroup(event: DragEvent) {
    const source = ObjectiveGroupComponent.dragDropData;
    if (this.noteGroupIdx !== source.noteGroupIndex) {
      const toIndex = this.noteGroup.notizen.length;
      this.notePage.swapNotizGroup(source.noteGroupIndex, source.noteIndex, this.noteGroupIdx, toIndex);
      source.noteGroupIndex = this.noteGroupIdx;
      source.noteIndex = toIndex;
      this.dirty.emit();
    }

    event.dataTransfer.dropEffect = 'move';
    event.preventDefault();
  }

  dragOver(event: DragEvent, index: number) {
    const source = ObjectiveGroupComponent.dragDropData;
    if (this.noteGroupIdx === source.noteGroupIndex) {
      moveItemInArray(this.noteGroup.notizen, source.noteIndex, index);
    } else {
      this.notePage.swapNotizGroup(source.noteGroupIndex, source.noteIndex, this.noteGroupIdx, index);
    }

    source.noteGroupIndex = this.noteGroupIdx;
    source.noteIndex = index;
    event.dataTransfer.dropEffect = 'move';
    event.preventDefault();
    this.dirty.emit();
  }

  dragStart(event: DragEvent, index: number) {
    event.dataTransfer.setData('text', 'dummy');
    event.dataTransfer.effectAllowed = 'move';
    ObjectiveGroupComponent.dragDropData = {
      noteGroupIndex: this.noteGroupIdx,
      noteIndex: index,
    };
  }

  drop(event: DragEvent) {
    event.preventDefault();
  }
}
