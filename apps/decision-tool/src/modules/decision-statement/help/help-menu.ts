import { ProjectMode } from '@entscheidungsnavi/decision-data/classes';
import { NAVI_STEP_ORDER } from '@entscheidungsnavi/decision-data/steps';
import { helpPage } from '../../../app/help/help';
import { DecisionStatementExplanationComponent } from './explanation.component';

export function getTutorialPage() {
  return helpPage()
    .youtube('fY4xCiDKd78')
    .name($localize`YouTube-Tutorial zu Schritt ${NAVI_STEP_ORDER.indexOf('decisionStatement') + 1}`)
    .build();
}

export function getExplanationPage(mode: ProjectMode) {
  return helpPage()
    .explanation(DecisionStatementExplanationComponent)
    .name(
      mode === 'educational'
        ? $localize`Wozu dient Schritt ${NAVI_STEP_ORDER.indexOf('decisionStatement') + 1}?`
        : $localize`Mehr zur Formulierung der Entscheidungsfrage`
    )
    .build();
}
