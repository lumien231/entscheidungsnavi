import { Component } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { DisplayAtMaxWidth } from '../../../../app/interfaces/display-at-max-width';

@Component({
  templateUrl: './pf-structure-and-estimate-main.component.html',
  styleUrls: ['./pf-structure-and-estimate-main.component.scss'],
})
@DisplayAtMaxWidth
export class PfStructureAndEstimateMainComponent {
  constructor(protected decisionData: DecisionData) {}
}
