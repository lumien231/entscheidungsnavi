import { Component, ViewChild } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { ModalComponent } from '@entscheidungsnavi/widgets';
import { ObjectiveElement, Tree } from '@entscheidungsnavi/decision-data/steps';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';
import { HierarchyElement } from '../../objectives/aspect-hierarchy/hierarchy-element';

@Component({
  template: `
    <dt-step-description [mainDescriptionOnly]="true">
      <ng-template #description>
        <ng-container i18n
          >Hier kannst Du Deine Ziele in weitere Unterziele oder Teilaspekte aufschlüsseln und auf dieser Basis eine transparente
          Gesamtstruktur Deines Zielsystems entwickeln. In die Ergebnismatrix werden nur die Ziele aus der ersten Hierarchieebene (die
          Fundamentalziele) übernommen. Zur einfacheren Bedienung kannst Du auch mit Shortcuts arbeiten.
        </ng-container>
      </ng-template>
    </dt-step-description>
    <dt-objective-aspect-hierarchy
      [listOfAspects]="listOfAspects"
      [listOfDeletedAspects]="listOfDeletedAspects"
      [tree]="tree"
      [brainstormingOpen]="false"
      [trashBinOpen]="false"
      data-cy="structure-and-estimate-tool-hierarchy-hierarchy"
    ></dt-objective-aspect-hierarchy>
  `,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        height: 100%;
      }

      dt-objective-aspect-hierarchy {
        display: flex;
        flex-direction: column;
        flex-basis: 0;
        flex-grow: 1;
        border: 1px solid lightgray;
      }
    `,
  ],
})
@DisplayAtMaxWidth
export class PfObjectiveAspectHierarchyComponent {
  @ViewChild('shortcutExplanationModal')
  private shortcutExplanationModal: ModalComponent;

  listOfAspects: ObjectiveElement[];
  listOfDeletedAspects: ObjectiveElement[];
  tree: Tree<HierarchyElement>;

  constructor(private dialog: MatDialog, decisionData: DecisionData) {
    this.listOfAspects = decisionData.objectiveAspects.listOfAspects;
    this.listOfDeletedAspects = decisionData.objectiveAspects.listOfDeletedAspects;
    this.tree = decisionData.objectiveAspects.getAspectTree($localize`Zielhierarchie`);
  }
}
