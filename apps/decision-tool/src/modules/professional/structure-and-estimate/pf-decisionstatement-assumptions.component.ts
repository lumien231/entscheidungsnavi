import { Component } from '@angular/core';

@Component({
  template: `<dt-decision-statement
    [showTitle]="false"
    data-cy="structure-and-estimate-tool-assumptions-decision-statement"
  ></dt-decision-statement>`,
})
export class PfDecisionstatementAssumptionsComponent {}
