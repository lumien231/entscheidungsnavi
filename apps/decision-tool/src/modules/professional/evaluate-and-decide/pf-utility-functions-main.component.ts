import { Component } from '@angular/core';

@Component({
  template: `
    <dt-step-description [mainDescriptionOnly]="true">
      <ng-template #description [ngSwitch]="selectedObjective">
        <ng-container *ngSwitchCase="-1" i18n>
          Auf dieser Übersichtsseite kannst Du Deine Nutzenfunktionen durch Klicken in die Diagramme verändern. In eine Detailansicht
          gelangst Du, indem Du auf die Zielnamen klickst.
        </ng-container>
        <ng-container *ngSwitchDefault i18n>
          Du kannst die Nutzenfunktion durch Klicken in die Abbildung und über die Buttons unterhalb der Abbildung anpassen. Die Bedeutung
          der Kurvenform wird Dir rechts in den vier Darstellungsoptionen erläutert.
        </ng-container>
      </ng-template>
    </dt-step-description>

    <dt-utility-function-navigation (selectedObjectiveChange)="selectedObjective = $event"></dt-utility-function-navigation>

    <router-outlet></router-outlet>
  `,
  styles: [
    `
      :host {
        height: 100%;
        display: flex;
        flex-direction: column;
      }

      dt-select-line {
        display: block;
        margin-bottom: 15px;
      }
    `,
  ],
})
export class PfUtilityFunctionsMainComponent {
  selectedObjective = -1;
}
