import { Component } from '@angular/core';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';

@Component({
  template: '<dt-pro-and-contra [showTitle]="false"></dt-pro-and-contra>',
  styles: [
    `
      dt-pro-and-contra {
        padding-bottom: 25px;
      }
    `,
  ],
})
@DisplayAtMaxWidth
export class PfProAndContraComponent {}
