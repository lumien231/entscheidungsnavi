import { Component, Inject } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';

@Component({
  selector: 'dt-outcome-selection-modal',
  templateUrl: './outcome-selection-modal.component.html',
  styleUrls: ['./outcome-selection-modal.component.scss'],
})
export class OutcomeSelectionModalComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) protected selectionArray: boolean[][],
    public dialogRef: MatDialogRef<OutcomeSelectionModalComponent>,
    protected decisionData: DecisionData
  ) {}
}
