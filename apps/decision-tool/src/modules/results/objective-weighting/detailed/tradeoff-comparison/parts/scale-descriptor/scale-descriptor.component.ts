import { Component, HostBinding, Input } from '@angular/core';

export type Descriptor = { position: number; height?: number; rich: boolean; description: string };

@Component({
  selector: 'dt-scale-descriptor',
  templateUrl: './scale-descriptor.component.html',
  styleUrls: ['./scale-descriptor.component.scss'],
})
export class ScaleDescriptorComponent {
  @Input()
  points: Descriptor[];

  @Input()
  align: 'left' | 'right' = 'left';

  @Input()
  currentValues: [number, number];

  @HostBinding('style.align-items')
  get alignItems() {
    return this.align === 'left' ? 'flex-start' : 'flex-end';
  }

  isActive(position: number) {
    return this.currentValues.some(value => Math.abs(value - position) < 0.01);
  }
}
