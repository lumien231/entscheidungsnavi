import { Component, Input } from '@angular/core';
import { Objective } from '@entscheidungsnavi/decision-data/classes';
import { ObjectiveValue } from '@entscheidungsnavi/decision-data/interfaces';

@Component({
  selector: 'dt-objective-value-comb',
  templateUrl: './objective-value-comb.component.html',
})
export class ObjectiveValueCombComponent {
  @Input() ziele: [Objective, Objective];
  @Input() values: [ObjectiveValue, ObjectiveValue];
}
