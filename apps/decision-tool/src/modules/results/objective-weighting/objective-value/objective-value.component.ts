import { Component, Input } from '@angular/core';
import { Objective } from '@entscheidungsnavi/decision-data/classes';
import { ObjectiveValue } from '@entscheidungsnavi/decision-data/interfaces';

@Component({
  selector: 'dt-objective-value',
  templateUrl: './objective-value.component.html',
})
export class ObjectiveValueComponent {
  @Input() ziel: Objective;
  @Input() zielValue: ObjectiveValue;
  @Input() showName = false;
}
