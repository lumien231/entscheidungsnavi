import { Component, TemplateRef, ViewChild } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { DisplayAtMaxWidth } from '../../../../app/interfaces/display-at-max-width';
import { HelpMenuProvider, helpPage } from '../../../../app/help/help';
import { STEPS } from '../../../shared/navigation/navigation-step';
import { getExplanationPage } from '../../help';
import { Help1Component } from '../../help/objective-weighting/help1/help1.component';
import { Help2Component } from '../../help/objective-weighting/help2/help2.component';
import { HelpBackgroundComponent } from '../../help/help-background/help-background.component';
import { NavLine, navLineElement, Navigation } from '../../../shared/navline';

@Component({
  selector: 'dt-objective-weighting',
  templateUrl: 'objective-weighting-main.component.html',
  styleUrls: ['objective-weighting-main.component.scss', '../../../hints.scss'],
})
@DisplayAtMaxWidth
export class ObjectiveWeightingMainComponent implements Navigation, HelpMenuProvider {
  navLine = new NavLine({
    left: [
      ...navLineElement()
        .back('/results/steps/1')
        .condition(() => this.decisionData.projectMode === 'educational')
        .orElse(builder => builder.back('/impactmodel')),
    ],
    right: [navLineElement().continue('/results').build()],
  });

  helpMenu = {
    educational: [
      helpPage()
        .name($localize`So funktioniert's`)
        .component(Help1Component)
        .build(),
      helpPage()
        .name($localize`Weitere Hinweise`)
        .component(Help2Component)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 5`)
        .component(HelpBackgroundComponent)
        .build(),
    ],
    starter: [
      helpPage()
        .name(STEPS.results.name + ': ' + $localize`Zielgewichte`)
        .template(() => this.starterHelp)
        .build(),
      getExplanationPage('starter'),
    ],
  };

  @ViewChild('starterHelp') starterHelp: TemplateRef<unknown>;

  selectedObjective = -1;

  get objective() {
    return this.decisionData.objectives[this.selectedObjective];
  }

  get referenceObjective() {
    return this.decisionData.objectives[this.decisionData.weights.tradeoffObjectiveIdx];
  }

  constructor(protected decisionData: DecisionData) {}
}
