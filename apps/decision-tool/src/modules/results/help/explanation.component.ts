import { Component } from '@angular/core';

@Component({
  template: ` <ng-container *dtProjectMode="'educational'">
      <p i18n
        >In diesem Schritt bewertest Du die von Dir angegebenen Alternativen. Hierzu nimmst Du zunächst eine Bewertung der möglichen
        Ergebnisse in einem Ziel unabhängig von den anderen Zielen vor. Dies geschieht über die Ermittlung von Nutzenfunktionen für jedes
        einzelne Ziel. Anschließend gewichtest Du alle Ziele relativ untereinander. Mit den Nutzenfunktionen und Zielgewichten wird dann für
        alle Handlungsalternativen ein Nutzenwert zwischen 0 und 100 berechnet und hierüber die beste Entscheidung abgeleitet.</p
      >

      <p i18n
        >Zweck dieses Schrittes ist auch, anhand der Ergebnisse noch einmal zu überprüfen, ob die so hergeleitete Empfehlung mit Deinem
        Bauchgefühl übereinstimmt. Wenn dies nicht der Fall ist, solltest Du mit verschiedenen Auswertungsmöglichkeiten nach den Ursachen
        suchen und versuchen, diese Diskrepanzen aufzulösen.</p
      >
    </ng-container>

    <ng-container *dtProjectMode="'starter'">
      <div class="dt-help-padding">
        <p i18n>
          In diesem Schritt gibst Du zunächst an, wie wichtig Dir die genannten Ziele im Vergleich sind. Anschließend werden Nutzenwerte wie
          folgt berechnet: Zunächst werden in jedem Ziel die Ergebnisse in Nutzenwerte zwischen 0 (für das schlechteste Ergebnis der Skala)
          und 1 (für das beste Ergebnis der Skala) überführt. Jeder Nutzenwert wird anschließend mit dem Zielgewicht multipliziert.
          Abschließend werden alle gewichteten Nutzenwerte aufaddiert und aus Darstellungsgründen noch mit 100 multipliziert.
        </p>
        <p i18n>
          Bei einer pauschalen Angabe der Zielgewichte wie in dieser Simple-Version handelt es sich allerdings nur um ein sehr pragmatisches
          Vorgehen mit hohen Ungenauigkeiten. Im <a [dtModeTrigger]="'educational'">Entscheidungsnavi Educational</a> hast Du deshalb die
          Möglichkeit, bei der Ermittlung der Zielgewichte eine wissenschaftlich fundierte und erheblich anspruchsvollere Methodik
          anzuwenden. Zugleich stehen auch noch zusätzliche Auswertungsmethoden zur Verfügung.
        </p>
      </div>
    </ng-container>`,
})
export class ResultsExplanationComponent {}
