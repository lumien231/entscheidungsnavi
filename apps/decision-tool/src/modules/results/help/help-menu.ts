import { ProjectMode } from '@entscheidungsnavi/decision-data/classes';
import { NAVI_STEP_ORDER } from '@entscheidungsnavi/decision-data/steps';
import { helpPage } from '../../../app/help/help';
import { STEPS } from '../../shared/navigation/navigation-step';
import { ResultsExplanationComponent } from './explanation.component';
import { HelpBackgroundComponent } from './help-background/help-background.component';
import { HelpMainEducationalComponent } from './help-main-educational/help-main-educational.component';
import { HelpMainStarterComponent } from './help-main-starter/help-main-starter.component';
import { RobustnessCheckHintsComponent } from './robustness-check/robustness-check-hints.component';

type ResultsHelpContext = 'sensitivity-analysis' | 'pro-contra' | 'robustness-check';

export function getExplanationPage(mode: ProjectMode) {
  return helpPage()
    .explanation(ResultsExplanationComponent)
    .name(
      mode === 'educational' ? $localize`Wozu dient Schritt ${NAVI_STEP_ORDER.indexOf('results') + 1}?` : $localize`Mehr zur Evaluation`
    )
    .build();
}

export function getEvaluationTutorialPage() {
  return helpPage()
    .youtube('gtgLD19nhOo')
    .name($localize`YouTube-Tutorial zum Evaluationsteil`)
    .build();
}

export function getHelpMenu(context?: ResultsHelpContext) {
  return {
    educational: [
      helpPage()
        .component(HelpMainEducationalComponent)
        .name($localize`So funktioniert's`)
        .context(context)
        .build(),
      ...(context === 'robustness-check'
        ? [
            helpPage()
              .component(RobustnessCheckHintsComponent)
              .name($localize`Weitere Hinweise`)
              .build(),
          ]
        : []),
      helpPage()
        .name($localize`Hintergrundwissen`)
        .component(HelpBackgroundComponent)
        .build(),
    ],
    starter: [
      helpPage()
        .component(HelpMainStarterComponent)
        .name($localize`Schritt ${NAVI_STEP_ORDER.indexOf('results') + 1}\: ${STEPS.results.name}`)
        .build(),
      getExplanationPage('starter'),
    ],
  };
}
