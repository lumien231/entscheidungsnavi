import { Component } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';

@Component({
  templateUrl: './help2.component.html',
  styleUrls: [],
})
export class Help2Component {
  constructor(protected decisionData: DecisionData) {}
}
