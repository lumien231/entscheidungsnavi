import { Component } from '@angular/core';

@Component({
  templateUrl: './help2.component.html',
  styleUrls: ['../../../../hints.scss'],
})
export class Help2Component {
  ufFormula = String.raw`
  u(x)=\begin{cases}\begin{aligned}
  &\frac{1-e^{-c\frac{x-x^{-}}{x^{+}-x^{-}}}}{1-e^{-c}} & \textit{ ${$localize`:latex:f\\"ur`} } c\neq0 \\[3ex]
  &\frac{x-x^{-}}{x^{+}-x^{-}} & \textit{ ${$localize`:latex:f\\"ur`} } c=0
  \end{aligned}
  \end{cases}
  `;
}
