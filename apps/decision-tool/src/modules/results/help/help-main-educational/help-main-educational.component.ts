import { Component, Inject } from '@angular/core';
import { HELP_PAGE_CONTEXT } from '../../../shared/help-page-context.token';

@Component({
  selector: 'dt-help-main-educational',
  templateUrl: './help-main-educational.component.html',
  styleUrls: ['../../../hints.scss'],
})
export class HelpMainEducationalComponent {
  constructor(@Inject(HELP_PAGE_CONTEXT) protected context: string) {}
}
