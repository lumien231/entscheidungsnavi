import { ChangeDetectorRef, Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { atLeastOneValidator } from '@entscheidungsnavi/widgets';
import { isEqual, range } from 'lodash';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable, takeUntil } from 'rxjs';
import { NonNullableFormBuilder } from '@angular/forms';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { RobustnessWorkerResult, RobustnessWorkerService } from '../../../worker/robustness-worker.service';
import { Debug } from '../../../app/debug/debug-template';
import { HelpMenuProvider } from '../../../app/help/help';
import { getHelpMenu } from '../help';
import { NavLine, Navigation, navLineElement } from '../../shared/navline';

@Component({
  selector: 'dt-robustness-check',
  templateUrl: './robustness-check.component.html',
  styleUrls: ['./robustness-check.component.scss'],
  providers: [RobustnessWorkerService],
})
export class RobustnessCheckComponent implements OnInit, Navigation, Debug, HelpMenuProvider {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @Input()
  showTitle = true;

  readonly navLine = new NavLine({
    middle: [
      navLineElement()
        .label(() =>
          this.areParamsUnchanged ? $localize`Mit identischen Parametern neu berechnen` : $localize`Mit geänderten Parametern neu berechnen`
        )
        .disabled(() => this.paramForm.invalid)
        .icon('calculate', 'left')
        .onClick(() => this.start())
        .cypressId('recompute-button')
        .build(),
      navLineElement()
        .label($localize`Parameter zurücksetzen`)
        .disabled(() => this.areParamsUnchanged)
        .icon('device_reset', 'left')
        .style('raised-less')
        .onClick(() => this.resetParams())
        .build(),
    ],
  }).addWhen(() => this.decisionData.projectMode === 'educational', {
    left: [navLineElement().back('/results').build()],
  });

  expectedUtilityValues: number[];
  sortedAlternativeIndices: number[]; // Sorted by their expected utility

  readonly paramForm = this.fb.group({
    alternatives: this.fb.array(
      this.decisionData.alternatives.map(() => false),
      atLeastOneValidator
    ),
    calculationSteps: [1_000],
    utilities: [true],
    objectiveWeights: [true],
    probabilities: [true],
    predefinedScenarios: [true],
    userdefinedScenarioIDs: this.fb.array(this.decisionData.influenceFactors.map(() => true)),
  });

  progress = 0;
  state: 'no-worker' | 'calculating' | 'done';
  result: RobustnessWorkerResult;
  private activeParams: typeof this.paramForm.value;

  get areParamsUnchanged() {
    return isEqual(this.activeParams, this.paramForm.value);
  }

  get activeAlternativeIndices() {
    return this.activeParams.alternatives.map((active, index) => (active ? index : -1)).filter(index => index !== -1);
  }

  @ViewChild('debug', { static: true })
  debugTemplate: TemplateRef<any> = null;
  hasDebugTemplate = true;

  dbgStartTime: number;
  dbgRunsRemaining = 0;
  dbgRunCount = 10;
  dbgLastStartTime = 0;
  dbgAvgTimePerRun = -1;

  helpMenu = getHelpMenu('robustness-check');

  projectInaccuracies: ReturnType<typeof this.decisionData.getInaccuracies>;

  constructor(
    protected decisionData: DecisionData,
    private cdRef: ChangeDetectorRef,
    private robustnessWorker: RobustnessWorkerService,
    private fb: NonNullableFormBuilder
  ) {}

  ngOnInit() {
    this.expectedUtilityValues = this.decisionData.getAlternativeUtilities();
    this.sortedAlternativeIndices = range(this.decisionData.alternatives.length).sort(
      (idx1, idx2) => this.expectedUtilityValues[idx2] - this.expectedUtilityValues[idx1]
    );

    this.sortedAlternativeIndices
      .slice(0, 3)
      .forEach(alternativeIndex => this.paramForm.controls.alternatives.at(alternativeIndex).setValue(true));

    this.projectInaccuracies = this.decisionData.getInaccuracies();

    if (!this.projectInaccuracies.weights) {
      this.paramForm.controls.objectiveWeights.setValue(false);
      this.paramForm.controls.objectiveWeights.disable();
    }
    if (!this.projectInaccuracies.probabilities) {
      this.paramForm.controls.probabilities.setValue(false);
      this.paramForm.controls.probabilities.disable();
    }
    if (!this.projectInaccuracies.utilityNumericalIndicator && !this.projectInaccuracies.utilityVerbal) {
      this.paramForm.controls.utilities.setValue(false);
      this.paramForm.controls.utilities.disable();
    }
    if (!this.projectInaccuracies.predefinedInfluenceFactors) {
      this.paramForm.controls.predefinedScenarios.setValue(false);
      this.paramForm.controls.predefinedScenarios.disable();
    }

    this.activeParams = this.paramForm.value;

    this.robustnessWorker.onProgressUpdate$.pipe(takeUntil(this.onDestroy$)).subscribe(progress => this.onWorkerProgressUpdate(progress));
    this.robustnessWorker.onFinished$.pipe(takeUntil(this.onDestroy$)).subscribe(result => this.onWorkerResult(result));
    this.start();
  }

  start() {
    if (this.paramForm.invalid) return;

    const v = this.paramForm.value;
    if (
      this.robustnessWorker.startWorker({
        iterationCount: v.calculationSteps,
        // Get the indices of checked alternatives in decisionData (not in rankedIDs)
        selectedAlternatives: v.alternatives.map((checked, index) => (checked ? index : -1)).filter(index => index !== -1),
        parameters: {
          influenceFactorScenarios: {
            predefined: v.predefinedScenarios,
            userdefinedIds: v.userdefinedScenarioIDs.map((checked, index) => (checked ? index : -1)).filter(index => index !== -1),
          },
          probabilities: v.probabilities,
          utilityFunctions: v.utilities,
          objectiveWeights: v.objectiveWeights,
        },
      })
    ) {
      this.state = 'calculating';
      this.progress = null;
    } else {
      this.state = 'no-worker';
    }
  }

  cancelCalculation() {
    if (!this.result || this.state !== 'calculating') return;

    this.robustnessWorker.restartWorker();
    this.state = 'done';
  }

  private onWorkerProgressUpdate(progress: number) {
    if (progress === 1) {
      this.progress = null;
    } else {
      this.progress = progress;
    }
    this.cdRef.detectChanges();
  }

  private onWorkerResult(result: RobustnessWorkerResult) {
    this.activeParams = this.paramForm.value;
    this.paramForm.markAsPristine();
    this.result = result;
    this.state = 'done';
    this.progress = 0;
  }

  updateCalculationSteps(newSteps: number) {
    this.paramForm.patchValue({ calculationSteps: newSteps });
    this.paramForm.controls.calculationSteps.markAsDirty();
  }

  resetParams() {
    this.paramForm.patchValue(this.activeParams);
    this.paramForm.markAsPristine();
  }

  // DEBUG
  dbgStart() {
    this.dbgRunsRemaining = this.dbgRunCount;
    this.dbgStartTime = performance.now();

    this.dbgLastStartTime = performance.now();

    this.paramForm.patchValue({ calculationSteps: 1_000_000 });

    const subscription = this.robustnessWorker.onFinished$.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      console.log(`- Took ${performance.now() - this.dbgLastStartTime}ms for last run`);

      if (--this.dbgRunsRemaining === 0) {
        const totalTime = performance.now() - this.dbgStartTime;
        this.dbgAvgTimePerRun = totalTime / this.dbgRunCount;
        subscription.unsubscribe();
      } else {
        this.dbgLastStartTime = performance.now();
        this.start();
      }
    });

    this.start();
  }
}
