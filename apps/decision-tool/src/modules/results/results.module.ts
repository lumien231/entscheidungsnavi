import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { KatexModule } from '@entscheidungsnavi/widgets/katex';
import { ChartsModule, PolarChartComponent } from '@entscheidungsnavi/widgets/charts';
import { BreadcrumbOptionsModule } from '@entscheidungsnavi/widgets/breadcrumb-options';
import {
  HoverPopOverDirective,
  NoteBtnComponent,
  NoteBtnPresetPipe,
  OverflowDirective,
  RichTextEditorComponent,
  WidthTriggerDirective,
} from '@entscheidungsnavi/widgets';
import { SharedModule } from '../shared/shared.module';
import { NoteHoverComponent } from '../shared/note-hover/note-hover.component';
import { ChartRankingComponent } from './chart-ranking';
import { ResultsExplanationComponent } from './help';
import { ResultsOverviewComponent } from './main';
import {
  DoubleSliderComponent,
  ObjectiveValueCombComponent,
  ObjectiveValueComponent,
  ObjectiveWeightingDetailedComponent,
  ObjectiveWeightingMainComponent,
  ObjectiveWeightingNavigationComponent,
  ObjectiveWeightingOverviewComponent,
  ReferenceObjectiveSelectionComponent,
  ScaleDescriptorComponent,
  ScaleValuesComponent,
  TradeoffComparisonComponent,
} from './objective-weighting';
import { ChartCellComponent, ChartRowComponent, ProContraBarChartComponent, ProContraComponent } from './pro-contra';
import { ResultsRoutingModule } from './results-routing.module';
import {
  ChartNutzenerwartungswertComponent,
  ChartRangpositionPartComponent,
  RobustnessCheckChartComponent,
  RobustnessCheckComponent,
  RobustnessInfluenceFactorDeviationComponent,
} from './robustness-check';
import { SensitivityAnalysisComponent } from './sensitivity-analysis';
import {
  UtilityFunctionDetailedComponent,
  UtilityFunctionDetailedNumericalOrIndicatorComponent,
  UtilityFunctionDetailedVerbalComponent,
  UtilityFunctionElementNumericalOrIndicatorComponent,
  UtilityFunctionElementVerbalComponent,
  UtilityFunctionGraphNumericalOrIndicatorComponent,
  UtilityFunctionGraphVerbalComponent,
  UtilityFunctionMainComponent,
  UtilityFunctionOverviewComponent,
} from './utility-functions';
import { OutcomeSelectionModalComponent } from './sensitivity-analysis/outcome-selection-modal/outcome-selection-modal.component';
import { MatrixPopoverComponent } from './sensitivity-analysis/matrix-popover/matrix-popover.component';
import { Help1Component as Hint1Help1Component } from './help/utility-functions/help1/help1.component';
import { Help2Component as Hint1Help2Component } from './help/utility-functions/help2/help2.component';
import { Help1Component as Hint2Help1Component } from './help/objective-weighting/help1/help1.component';
import { Help2Component as Hint2Help2Component } from './help/objective-weighting/help2/help2.component';
import { HelpBackgroundComponent } from './help/help-background/help-background.component';
import { HelpMainEducationalComponent } from './help/help-main-educational/help-main-educational.component';
import { HelpMainStarterComponent } from './help/help-main-starter/help-main-starter.component';
import { RobustnessCheckHintsComponent } from './help/robustness-check/robustness-check-hints.component';
import { UtilityFunctionNavigationComponent } from './utility-functions/main/utility-function-navigation.component';

@NgModule({
  declarations: [
    // Help
    ResultsExplanationComponent,

    // Utility Function
    UtilityFunctionMainComponent,
    UtilityFunctionDetailedVerbalComponent,
    UtilityFunctionDetailedComponent,
    UtilityFunctionDetailedNumericalOrIndicatorComponent,
    UtilityFunctionGraphNumericalOrIndicatorComponent,
    UtilityFunctionElementNumericalOrIndicatorComponent,
    UtilityFunctionElementVerbalComponent,
    UtilityFunctionGraphVerbalComponent,
    UtilityFunctionOverviewComponent,
    UtilityFunctionNavigationComponent,

    // Objective Weighting
    ObjectiveWeightingMainComponent,
    ObjectiveWeightingNavigationComponent,
    ObjectiveWeightingOverviewComponent,
    ObjectiveValueComponent,
    ObjectiveValueCombComponent,
    ObjectiveWeightingDetailedComponent,
    ReferenceObjectiveSelectionComponent,
    TradeoffComparisonComponent,
    DoubleSliderComponent,
    ScaleDescriptorComponent,
    ScaleValuesComponent,

    // Main results
    ResultsOverviewComponent,
    RobustnessCheckComponent,
    ChartNutzenerwartungswertComponent,
    ChartRangpositionPartComponent,
    SensitivityAnalysisComponent,
    ProContraComponent,
    ProContraBarChartComponent,
    ChartCellComponent,
    ChartRowComponent,
    ChartRankingComponent,
    Hint1Help1Component,
    Hint1Help2Component,
    Hint2Help1Component,
    Hint2Help2Component,
    HelpBackgroundComponent,
    ProContraComponent,
    HelpMainEducationalComponent,
    HelpMainStarterComponent,
    OutcomeSelectionModalComponent,
    MatrixPopoverComponent,
    RobustnessCheckChartComponent,
    RobustnessInfluenceFactorDeviationComponent,
    RobustnessCheckHintsComponent,
  ],
  imports: [
    CommonModule,
    ResultsRoutingModule,
    SharedModule,
    KatexModule,
    ChartsModule,
    BreadcrumbOptionsModule,
    OverflowDirective,
    NoteBtnPresetPipe,
    NoteBtnComponent,
    HoverPopOverDirective,
    PolarChartComponent,
    RichTextEditorComponent,
    NoteHoverComponent,
    WidthTriggerDirective,
  ],
  exports: [
    SensitivityAnalysisComponent,
    ChartRankingComponent,
    ProContraComponent,
    RobustnessCheckComponent,
    UtilityFunctionNavigationComponent,
    UtilityFunctionOverviewComponent,
    UtilityFunctionDetailedComponent,
    ObjectiveWeightingOverviewComponent,
    ObjectiveWeightingDetailedComponent,
    ObjectiveWeightingNavigationComponent,
    ResultsOverviewComponent,
  ],
})
export class ResultsModule {}
