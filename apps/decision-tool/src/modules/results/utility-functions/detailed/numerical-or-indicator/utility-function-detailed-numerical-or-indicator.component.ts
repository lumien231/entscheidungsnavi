import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Objective, ObjectiveType, UtilityFunction } from '@entscheidungsnavi/decision-data/classes';
import '@entscheidungsnavi/tools/number_rounding';
import { clamp, round } from 'lodash';
import { UtilityFunctionNumericalOrIndicatorData } from '../../shared/utility-function-numerical-or-indicator-data';

// TODO: Clean up
@Component({
  selector: 'dt-utility-function-detailed-numerical-or-indicator',
  templateUrl: 'utility-function-detailed-numerical-or-indicator.component.html',
  styleUrls: ['../utility-function-detailed.scss'],
})
export class UtilityFunctionDetailedNumericalOrIndicatorComponent implements OnChanges {
  @Input() objective: Objective;

  data: UtilityFunctionNumericalOrIndicatorData;

  readonly cMax = 25; // abs(cMax)
  readonly pMax = 10;

  private diffMagnitude: number; // the decimal power of |bis - von|
  public sigDigits = 2; // significant digits based on diff_magnitude

  updateC(newC: number) {
    this.utilityFunction.c = clamp(round(newC, 1), -this.cMax, this.cMax);
    this.onChange();
  }

  adjustC(change: number) {
    this.updateC(this.utilityFunction.c + change);
  }

  updatePrecision(newPrecision: number) {
    this.utilityFunction.precision = clamp(round(newPrecision, 1), 0, this.pMax);
    this.onChange();
  }

  adjustPrecision(change: number) {
    this.updatePrecision(this.utilityFunction.precision + change);
  }

  updateSigDigits(newValue: number) {
    this.sigDigits = clamp(newValue, 0, 10);
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('objective' in changes) {
      this.utilityFunction.width = this.utilityFunction.width ?? 0.5;
      this.utilityFunction.level = this.utilityFunction.level ?? 0.5;

      if (this.objective.objectiveType === ObjectiveType.Numerical) {
        this.diffMagnitude = (this.objective.numericalData.to - this.objective.numericalData.from).getMagnitude();
      } else {
        this.diffMagnitude = 0;
      }

      this.data = new UtilityFunctionNumericalOrIndicatorData(this.objective);
      this.onChange();
    }
  }

  onChange() {
    this.data.updateData();
  }

  roundObjectiveValue(value: number): number {
    return value.round(this.sigDigits - this.diffMagnitude);
  }

  getPercentage(x: number) {
    return (x * 100).round(this.sigDigits - 2);
  }

  /**
   * Return the utility function of the current objective depending on its type
   */
  get utilityFunction(): UtilityFunction {
    switch (this.objective.objectiveType) {
      case ObjectiveType.Numerical:
        return this.objective.numericalData.utilityfunction;
      case ObjectiveType.Indicator:
        return this.objective.indicatorData.utilityfunction;
      default:
        throw new Error(`Unsupported objective type: ${this.objective.objectiveType}`);
    }
  }

  get unit(): string {
    switch (this.objective.objectiveType) {
      case ObjectiveType.Numerical:
        return this.objective.numericalData.unit;
      case ObjectiveType.Indicator:
        return this.objective.indicatorData.aggregatedUnit;
      default:
        throw new Error(`Unsupported objective type: ${this.objective.objectiveType}`);
    }
  }
}
