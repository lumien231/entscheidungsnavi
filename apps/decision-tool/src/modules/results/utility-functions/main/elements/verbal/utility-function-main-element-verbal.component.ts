import { Component, Input } from '@angular/core';
import { Objective } from '@entscheidungsnavi/decision-data/classes';
import { clamp, round } from 'lodash';

@Component({
  selector: 'dt-utilityfunction-main-element-verbal',
  templateUrl: './utility-function-main-element-verbal.component.html',
  styleUrls: ['./utility-function-main-element-verbal.component.scss'],
})
export class UtilityFunctionElementVerbalComponent {
  readonly pMax = 50;
  readonly cMax = 25;

  @Input() objective: Objective;
  @Input() hasChanges: boolean;

  updatePrecision(newPrecision: number) {
    if (newPrecision == null) {
      return;
    }

    this.objective.verbalData.precision = clamp(round(newPrecision, 1), 0, this.pMax);
  }

  updateC(newC: number) {
    if (newC == null) {
      return;
    }

    this.objective.verbalData.c = clamp(round(newC, 1), -this.cMax, this.cMax);
    this.objective.verbalData.adjustUtilityToFunction();
  }

  adjustPrecision(change: number) {
    this.updatePrecision(this.objective.verbalData.precision + change);
  }

  adjustC(change: number) {
    this.updateC(this.objective.verbalData.c + change);
  }

  unlockC() {
    this.objective.verbalData.hasCustomUtilityValues = false;
    this.objective.verbalData.adjustUtilityToFunction();
  }
}
