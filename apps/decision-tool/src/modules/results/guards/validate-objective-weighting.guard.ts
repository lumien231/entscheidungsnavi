import { Injectable } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ErrorModalComponent } from '../../shared/error';

@Injectable({
  providedIn: 'root',
})
export class ValidateObjectiveWeightingGuard implements CanActivate {
  constructor(private decisionData: DecisionData, private dialog: MatDialog) {}

  canActivate(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): boolean {
    const result = this.decisionData.validateWeights();
    if (!result[0]) {
      ErrorModalComponent.open(
        this.dialog,
        $localize`Du musst zuerst die Zielgewichtung durchführen, um hierhin navigieren zu können.`,
        result[1]
      );
      return false;
    }
    return true;
  }
}
