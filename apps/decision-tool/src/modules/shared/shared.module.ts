import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  HoverPopOverDirective,
  OverflowDirective,
  RichTextEditorComponent,
  WidgetsModule,
  WidthTriggerDirective,
} from '@entscheidungsnavi/widgets';
import { ApiClientModule } from '@entscheidungsnavi/api-client';
import { RouterLink } from '@angular/router';
import { CreateTeamModalComponent } from '../../app/navigation/projects/create-team-modal/create-team-modal.component';
import { NaviStepPipe } from './navigation/navi-step.pipe';
import { NaviSubStepPipe } from './navigation/navi-sub-step.pipe';
import { ErrorModalComponent, ErrorMsgComponent } from './error';
import { TemplateSelectionComponent } from './template-selection/template-selection.component';
import { ProjectModeDirective } from './project-mode.directive';
import { StepTitleComponent } from './step-title/step-title.component';
import { JsonErrorModalComponent } from './error/json-error-modal/json-error-modal.component';
import { StepDescriptionComponent } from './step-description/step-description.component';
import { DisableInputScrollDirective } from './disable-input-scroll.directive';
import { DecisionQualityChartComponent, StarterExplanationModalComponent } from './decision-quality';
import { SwitchProfessionalEducationalModalComponent } from './mode-transition';
import { ExplainableModule } from './explanation/explainable.module';
import { LeaveStarterModalComponent } from './mode-transition/modals/leave-starter-modal/leave-starter-modal.component';
import { ExtendableHeaderComponent } from './extendable-header/extendable-header.component';
import { ModeTriggerDirective } from './mode-trigger/mode-trigger.directive';
import { DisableInputAutocompletionDirective } from './disable-input-autocompletion.directive';
import { TeamCommentsComponent } from './team';
import { TeamModalComponent } from './team/team-modal/team-modal.component';
import { TeamObjectiveWeightsComponent } from './team/team-objective-weights';
import { TeamProjectProgressComponent } from './team/team-project-progress';
import { NavLineElementComponent, NavlineComponent } from './navline';
import { TeamUnreadCommentsModalComponent } from './team/team-unread-comments-modal';

const DECLARE_AND_EXPORT = [
  DecisionQualityChartComponent,
  DisableInputScrollDirective,
  DisableInputAutocompletionDirective,
  ErrorModalComponent,
  ErrorMsgComponent,
  ModeTriggerDirective,
  NaviStepPipe,
  NaviSubStepPipe,
  ProjectModeDirective,
  StarterExplanationModalComponent,
  StepDescriptionComponent,
  StepTitleComponent,
  SwitchProfessionalEducationalModalComponent,
  TemplateSelectionComponent,
  ExtendableHeaderComponent,
  CreateTeamModalComponent,
  TeamCommentsComponent,
  TeamModalComponent,
  TeamObjectiveWeightsComponent,
  TeamProjectProgressComponent,
  NavlineComponent,
  TeamUnreadCommentsModalComponent,
];

@NgModule({
  imports: [
    ApiClientModule,
    CommonModule,
    ExplainableModule,
    HoverPopOverDirective,
    WidgetsModule,
    OverflowDirective,
    RichTextEditorComponent,
    WidthTriggerDirective,
    RouterLink,
  ],
  declarations: [...DECLARE_AND_EXPORT, LeaveStarterModalComponent, JsonErrorModalComponent, NavLineElementComponent],
  exports: [...DECLARE_AND_EXPORT, ApiClientModule, ExplainableModule, WidgetsModule],
})
export class SharedModule {}
