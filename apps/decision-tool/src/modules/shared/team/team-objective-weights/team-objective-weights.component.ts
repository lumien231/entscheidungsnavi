import { Component, Input, OnInit } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { TeamMember } from '@entscheidungsnavi/api-types';
import { normalizeContinuous } from '@entscheidungsnavi/decision-data/calculation';

@Component({
  selector: 'dt-team-objective-weights',
  templateUrl: './team-objective-weights.component.html',
  styleUrls: ['./team-objective-weights.component.scss'],
})
export class TeamObjectiveWeightsComponent implements OnInit {
  @Input()
  projects: DecisionData[];

  @Input()
  members: readonly TeamMember[];

  objectiveNames: string[];

  weightLines: string[];

  hoveringMemberIndex = -1;

  ngOnInit() {
    this.calcData();
  }

  calcData() {
    this.weightLines = [];

    const mainProject = this.projects[0];

    this.objectiveNames = mainProject.objectives.map(o => o.name);

    if (mainProject.objectives.length < 2) {
      return;
    }

    for (const project of this.projects) {
      if (
        project.objectives.length !== this.objectiveNames.length ||
        project.objectives.some((o1, i) => this.objectiveNames[i] !== o1.name) ||
        !project.validateWeights()[0]
      ) {
        this.weightLines.push(null);
        continue;
      }
      const weights = project.weights.getWeightValues();
      normalizeContinuous(weights);
      const points: [number, number][] = [];

      let x = 0;
      const part = 100 / (project.objectives.length - 1);

      for (let i = 0; i < project.objectives.length; i++) {
        points.push([x, 100 - weights[i] * 100]);
        x += part;
      }

      this.weightLines.push(
        `M ${points[0][0]} ${points[0][1]} ${points
          .slice(1)
          .map(p => `L ${p[0]} ${p[1]}`)
          .join(' ')}`
      );
    }
  }
}
