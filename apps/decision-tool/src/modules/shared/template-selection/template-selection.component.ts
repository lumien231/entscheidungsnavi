import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { PopOverService } from '@entscheidungsnavi/widgets';
import { from, map, mergeMap, toArray } from 'rxjs';
import { TemplatesService, TemplateType } from '../../../app/data/templates.service';

type ValueType = TemplateType | 'blank';

@Component({
  selector: 'dt-template-selection',
  templateUrl: './template-selection.component.html',
  styleUrls: ['./template-selection.component.scss'],
})
export class TemplateSelectionComponent {
  @Input() value: ValueType;
  @Output() valueChange = new EventEmitter<ValueType>();

  @Input() allowBlank = false;

  templates: { name: TemplateType; content: DecisionData }[];

  state: 'loading' | 'offline' | 'ready' = 'loading';

  get activeDecisionData() {
    return this.templates.find(val => val.name === this.value)?.content;
  }

  constructor(private popoverService: PopOverService, templatesService: TemplatesService) {
    templatesService.availableTemplates
      .pipe(
        mergeMap(availableTemplates => from(availableTemplates)),
        mergeMap(availableTemplateName =>
          templatesService
            .getTemplate(availableTemplateName)
            .pipe(map(decisionData => ({ name: availableTemplateName, content: decisionData })))
        ),
        toArray()
      )
      .subscribe({
        next: templates => {
          this.state = 'ready';
          this.templates = templates;
        },
        error: () => {
          this.state = 'offline';
        },
      });
  }

  templateClick(type: ValueType) {
    this.valueChange.emit(type);
  }

  showDetailsPopover(event: Event, template: TemplateRef<any>) {
    this.popoverService.open(template, event.target as HTMLElement, {
      position: [{ originX: 'center', originY: 'top', overlayX: 'center', overlayY: 'bottom' }],
    });

    event.preventDefault();
    event.stopImmediatePropagation();
  }
}
