import { Component } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';

@Component({
  templateUrl: './json-error-modal.component.html',
  styleUrls: ['./json-error-modal.component.scss'],
})
export class JsonErrorModalComponent {
  constructor(public dialogRef: MatDialogRef<JsonErrorModalComponent>) {}
}
