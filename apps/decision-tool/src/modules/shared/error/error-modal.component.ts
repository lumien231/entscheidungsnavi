import { Component, Inject } from '@angular/core';
import {
  MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA,
  MatLegacyDialog as MatDialog,
  MatLegacyDialogRef as MatDialogRef,
} from '@angular/material/legacy-dialog';
import { ErrorMsg } from '@entscheidungsnavi/decision-data/classes';

@Component({
  templateUrl: './error-modal.component.html',
  styleUrls: ['./error-modal.component.scss'],
})
export class ErrorModalComponent {
  static open(dialog: MatDialog, message: string, errors: ErrorMsg[]) {
    return dialog.open(ErrorModalComponent, { data: { message, errors } });
  }

  constructor(
    public dialogRef: MatDialogRef<ErrorModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { message: string; errors: ErrorMsg[] }
  ) {}
}
