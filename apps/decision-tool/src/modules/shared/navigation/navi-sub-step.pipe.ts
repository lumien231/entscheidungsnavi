import { Pipe, PipeTransform } from '@angular/core';
import { NaviSubStep } from '@entscheidungsnavi/decision-data/steps';
import { STEPS } from './navigation-step';

@Pipe({
  name: 'naviSubStep',
})
export class NaviSubStepPipe implements PipeTransform {
  transform(value: NaviSubStep): string {
    const subStep = STEPS[value.step].subSteps[value.subStepIndex];
    return subStep;
  }
}
