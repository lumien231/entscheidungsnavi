import { Pipe, PipeTransform } from '@angular/core';
import { NaviStep } from '@entscheidungsnavi/decision-data/steps';
import { STEPS } from './navigation-step';

@Pipe({
  name: 'naviStep',
})
export class NaviStepPipe implements PipeTransform {
  transform(value: NaviStep): string {
    return STEPS[value].name;
  }
}
