import { NaviStep, NaviSubStep } from '@entscheidungsnavi/decision-data/steps';

export interface NavigationStep {
  name: string;
  subSteps: string[];
  routerLink: string;
  icon: string;
}

export const STEPS: { [key in NaviStep]: NavigationStep } = {
  decisionStatement: {
    name: $localize`Entscheidungsfrage`,
    routerLink: '/decisionstatement',
    subSteps: [$localize`Erste Formulierung`, $localize`Grundlegende Werte`, $localize`Impulsfragen`, $localize`Überprüfung`],
    icon: 'contact_support',
  },
  objectives: {
    name: $localize`Fundamentalziele`,
    routerLink: '/objectives',
    subSteps: [
      $localize`Erstes Brainstorming`,
      $localize`Weitere Überlegungen`,
      $localize`Erste Zielhierarchie`,
      $localize`Beispielziele`,
      $localize`Überprüfung`,
    ],
    icon: 'track_changes',
  },
  alternatives: {
    name: $localize`Alternativen`,
    routerLink: '/alternatives',
    subSteps: [
      $localize`Bekannte Alternativen`,
      $localize`Finden von Schwachpunkten`,
      $localize`Zielfokussierte Suche`,
      $localize`Befragen anderer Leute`,
      $localize`Wichtige Stellhebel`,
      $localize`Sinnvolles Zusammenfassen`,
      $localize`Intuitives Ordnen`,
    ],
    icon: 'alt_route',
  },
  impactModel: {
    name: $localize`Wirkungsmodell`,
    routerLink: '/impactmodel',
    subSteps: [],
    icon: 'view_compact',
  },
  results: {
    name: $localize`Evaluation`,
    routerLink: '/results',
    subSteps: [$localize`Nutzenfunktionen`, $localize`Zielgewichte`],
    icon: 'assessment',
  },
  finishProject: {
    name: $localize`Abschlussbetrachtung`,
    routerLink: '/finishproject',
    subSteps: [],
    icon: 'fact_check',
  },
};

export function urlToNaviSubStep(url: string): NaviSubStep | null {
  const result = Object.entries(STEPS).find(step => url.startsWith(step[1].routerLink)) as [NaviStep, NavigationStep];

  if (result != null) {
    const [stepId, step] = result;
    let subStepIndex: number;
    const restUrl = url.substring(step.routerLink.length + 1);

    const hint = /^steps\/(\d)/.exec(restUrl);
    if (hint) {
      // (the urls are 1-based, the substep indices are 0-based)
      subStepIndex = parseInt(hint[1]) - 1;
    }
    // all other sub-urls are assumed to be equal to the main step

    return { step: stepId, subStepIndex };
  }

  return null;
}

export function naviSubStepToUrl(subStep: NaviSubStep): string {
  let url = Object.entries(STEPS).find(step => step[0] === subStep.step)[1].routerLink;
  if (subStep.subStepIndex != null) {
    url += `/steps/${subStep.subStepIndex + 1}`;
  }
  return url;
}
