import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ButtonStyle, IconPosition } from '../navigation';

@Component({
  selector: 'dt-navline-element',
  styleUrls: ['./navline-element.component.scss'],
  templateUrl: './navline-element.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavLineElementComponent {
  @Input()
  label: string;

  @Input()
  tooltip: string;

  @Input()
  disabled: boolean;

  @Input()
  icon: string;

  @Input()
  iconPosition: IconPosition;

  @Input()
  buttonStyle: ButtonStyle;

  @Input()
  minimized: boolean;

  @Input()
  link: string;

  @Input()
  cypressId: string;

  @Output()
  buttonClick = new EventEmitter();

  get iconOnly() {
    return !this.label || this.minimized;
  }

  get cypressIdWithPrefix() {
    if (!this.cypressId) {
      return undefined;
    }
    return `navline-element-${this.cypressId}`;
  }
}
