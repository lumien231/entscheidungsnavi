import { Injectable } from '@angular/core';
import { ProjectMode } from '@entscheidungsnavi/decision-data/classes';
import { Router } from '@angular/router';
import { firstValueFrom, lastValueFrom } from 'rxjs';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { NAVI_STEP_ORDER, NaviStep, NaviSubStep } from '@entscheidungsnavi/decision-data/steps';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ValidateDecisionDataGuard } from '../../../app/guards';
import { CurrentProgressService } from '../../../app/data/current-progress.service';
import { InterpolationService } from '../../../services/interpolation.service';
import { naviSubStepToUrl } from '../navigation/navigation-step';
import { NavigationComponent } from '../../../app/navigation';
import { LeaveStarterModalComponent } from './modals/leave-starter-modal/leave-starter-modal.component';
import { SwitchProfessionalEducationalModalComponent } from '.';

@Injectable({
  providedIn: 'root',
})
export class ModeTransitionService {
  constructor(
    private dialog: MatDialog,
    private router: Router,
    private decisionData: DecisionData,
    private currentProgressService: CurrentProgressService,
    private stepsInterpolatingService: InterpolationService
  ) {}

  /**
   * @returns A Promise that resolves to true when navigation succeeds, to false when navigation fails, or is rejected on error.
   */
  private async handleTransition(target: string, modeToTransitionTo: ProjectMode, preTransitionHook?: () => void): Promise<boolean> {
    if (target == null) {
      // Transition handler rejected aborts transition.
      console.warn('Transition handler aborted mode transition.');
      return true;
    }

    console.log(`Transitioning to ${target} (${modeToTransitionTo})...`);

    const oldMode = this.decisionData.projectMode;
    this.decisionData.projectMode = modeToTransitionTo;
    preTransitionHook?.();

    const origin = this.router.url;

    if (origin === target || (await this.router.navigateByUrl(target))) {
      // Navigation successful
      return true;
    } else {
      // Navigation aborted. Reset the project mode.
      console.log('Transition aborted.');
      this.decisionData.projectMode = oldMode;

      return false;
    }
  }

  /**
   * Navigates to educational mode.
   *
   * @param toSpecificStep
   * @returns A Promise that resolves to true when navigation succeeds, to false when navigation fails, or is rejected on error.
   */
  async transitionIntoEducational(toSpecificStep?: NaviStep): Promise<boolean> {
    const from = this.decisionData.projectMode;

    if (from === 'educational') {
      if (toSpecificStep === undefined) {
        return true;
      }

      return NavigationComponent.navigateToStep(
        NAVI_STEP_ORDER.indexOf(toSpecificStep),
        this.currentProgressService.currentProgress,
        this.router,
        this.decisionData
      );
    }

    if (from === 'starter') {
      if (toSpecificStep) throw new Error('Cannot select step when switching from starter to educational.');

      const confirmed = await firstValueFrom(
        this.dialog.open(LeaveStarterModalComponent, { data: { target: 'educational' } }).beforeClosed()
      );

      if (confirmed) {
        return await this.handleTransition(this.router.url, 'educational');
      } else {
        return false;
      }
    }

    // We use beforeClosed on the modal to make sure we navigate and modify the history _before_ the modal closes and calls `history.pop()`.
    // Otherwise, the asynchronous nature of that call will revert our navigation.
    const selectedStepToNavigateTo: NaviStep =
      toSpecificStep ??
      (await lastValueFrom(
        this.dialog
          .open<SwitchProfessionalEducationalModalComponent, void, NaviStep>(SwitchProfessionalEducationalModalComponent)
          .beforeClosed()
      ));

    if (selectedStepToNavigateTo == null) return true;

    const subStepToNavigateTo: NaviSubStep = { step: selectedStepToNavigateTo };

    if (!['impactModel', 'finishProject'].includes(selectedStepToNavigateTo)) {
      subStepToNavigateTo.subStepIndex = 0;
    }

    const preTransitionFn = () => {
      let decisionStatement;
      if (this.decisionData.decisionStatement.statement) {
        decisionStatement = this.decisionData.decisionStatement.statement;
      } else {
        decisionStatement = $localize`Wie soll ich mich entscheiden?`;
      }

      // Restore some information from professional (decision statement).
      this.decisionData.decisionStatement.statement =
        this.decisionData.decisionStatement.statement_attempt =
        this.decisionData.decisionStatement.statement_attempt_2 =
          decisionStatement;

      this.currentProgressService.unlock();
    };

    const target = naviSubStepToUrl(subStepToNavigateTo);

    return this.handleTransition(target, 'educational', preTransitionFn);
  }

  /**
   * Navigates to starter mode.
   *
   * @returns A Promise that resolves to true when navigation succeeds, to false when navigation fails, or is rejected on error.
   */
  async transitionIntoProfessional(): Promise<boolean> {
    const from = this.decisionData.projectMode;
    if (from === 'professional') return true;

    if (from === 'starter') {
      const confirmed = await firstValueFrom(
        this.dialog.open(LeaveStarterModalComponent, { data: { target: 'professional' } }).beforeClosed()
      );

      if (!confirmed) {
        return false;
      }
    }

    const firstStepWithError = ValidateDecisionDataGuard.getFirstErrorStepWithErrors(this.decisionData)[0];

    let target: string;

    let preTransitionFn = null;

    if (!firstStepWithError) {
      // Enough data to enter evaluation (second tab). Do it.
      target = '/professional/evaluate-and-decide';
      preTransitionFn = () => this.stepsInterpolatingService.interpolateForSecondProfessionalTab();
    } else {
      // User has to do some work in the first tab until he/she can enter the second one.
      target = '/professional/structure-and-estimate';
      preTransitionFn = () => this.stepsInterpolatingService.interpolateForFirstProfessionalTab();
    }

    return await this.handleTransition(target, 'professional', preTransitionFn);
  }
}
