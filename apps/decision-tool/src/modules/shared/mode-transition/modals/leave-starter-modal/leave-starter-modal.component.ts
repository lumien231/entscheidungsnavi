import { Component, Inject } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';

@Component({
  templateUrl: './leave-starter-modal.component.html',
  styleUrls: ['./leave-starter-modal.component.scss'],
})
export class LeaveStarterModalComponent {
  constructor(
    public modal: MatDialogRef<LeaveStarterModalComponent>,
    @Inject(MAT_DIALOG_DATA) private data: { target: 'educational' | 'professional' }
  ) {}

  get targetModeName() {
    return this.data.target === 'educational' ? 'Educational' : 'Professional';
  }
}
