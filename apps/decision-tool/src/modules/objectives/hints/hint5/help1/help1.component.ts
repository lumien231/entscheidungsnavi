import { Component, ViewChild } from '@angular/core';
import { ModalComponent } from '@entscheidungsnavi/widgets';

@Component({
  templateUrl: './help1.component.html',
  styleUrls: ['../../../../hints.scss'],
})
export class Help1Component {
  @ViewChild('shortcutExplanationModal')
  private shortcutExplanationModal: ModalComponent;

  openShortcuts() {
    this.shortcutExplanationModal.open();
  }
}
