import { Component, ElementRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { ProjectDto, QuickstartProjectDto, QuickstartProjectListDto, QuickstartService } from '@entscheidungsnavi/api-client';
import { Tree } from '@entscheidungsnavi/decision-data/steps';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { importText } from '@entscheidungsnavi/decision-data/export';
import { checkType } from '@entscheidungsnavi/tools';
import { Tag } from '@entscheidungsnavi/widgets';
import { forkJoin, Subject } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { cloneDeep, throttle } from 'lodash';
import { AbstractObjectiveHintComponent } from '../objective-hint.component';
import { DisplayAtMaxWidth } from '../../../../app/interfaces/display-at-max-width';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { HierarchyElement } from '../../aspect-hierarchy/hierarchy-element';
import { Help1Component } from './help1/help1.component';

@Component({
  selector: 'dt-objective-hint-4',
  templateUrl: './hint4.component.html',
  styleUrls: ['./hint4.component.scss'],
})
@DisplayAtMaxWidth
export class ZieleHint4Component extends AbstractObjectiveHintComponent implements OnInit {
  @ViewChild('projectList') projectList: ElementRef;

  @Input()
  showTitle = true;

  inSelectionMode = true;
  smallScreen = false;

  objectiveSystemProjects: QuickstartProjectListDto;
  tags: Tag[];

  /* OS stands for Objective System. */
  treeOS: Tree<HierarchyElement>;
  /* Tree displayed on project hover. */
  treeOSDisplayed: Tree<HierarchyElement>;

  /* Clicked project. */
  currentProjectId = '';
  currentProjectName = '';

  /* Displayed project. */
  displayedProjectId = '';
  displayedProjectName = '';

  mouseIsHovering = false;

  nameInput = '';
  filterTagIds: string[] = [];
  updateProjectsAndTags: () => void;
  filteredProjects: (ProjectDto | QuickstartProjectDto)[] = [];

  displayedProjectSubject = new Subject<{ projectId: string; isHovered: boolean }>();

  get helpMenu() {
    return {
      educational: [
        helpPage()
          .name($localize`So funktioniert's`)
          .component(Help1Component)
          .build(),
        helpPage()
          .name($localize`Hintergrundwissen zum Schritt 2`)
          .component(HelpBackgroundComponent)
          .build(),
      ],
    };
  }

  constructor(injector: Injector, private quickstartService: QuickstartService, private snackBar: MatSnackBar) {
    super(injector);
    this.pageKey = 4;
  }

  override ngOnInit() {
    super.ngOnInit();
    forkJoin({
      projects: this.quickstartService.getProjects(),
      tags: this.quickstartService.getTags(),
    }).subscribe({
      next: result => {
        this.objectiveSystemProjects = result.projects;
        this.tags = result.tags;
        this.updateProjectsAndTags();
      },
      error: () => {
        this.snackBar.open($localize`Die Standardzielsysteme konnten nicht geladen werden.`, 'Ok', { duration: 3000 });
      },
    });

    this.updateProjectsAndTags = throttle(() => {
      let list: (ProjectDto | QuickstartProjectDto)[] = this.nameInput
        ? this.objectiveSystemProjects.search(this.nameInput)
        : this.objectiveSystemProjects.list;
      if (this.filterTagIds) {
        list = list.filter(project =>
          this.filterTagIds.every(tag => project instanceof QuickstartProjectDto && project.tags.includes(tag))
        );
      }
      this.filteredProjects = list.sort((a, b) => a.name.localeCompare(b.name));

      this.tags.forEach(
        tag => (tag.isProductive = this.filteredProjects.some(p => p instanceof QuickstartProjectDto && p.tags.includes(tag.id)))
      );
    }, 50);

    this.displayedProjectSubject
      .pipe(
        switchMap(({ projectId, isHovered }) => {
          return this.quickstartService.getProject(projectId).pipe(
            map(project =>
              checkType<QuickstartProjectDto & { isHovered: boolean }>({
                ...project,
                isHovered,
              })
            )
          );
        })
      )
      .subscribe(project => {
        if (!this.mouseIsHovering) {
          // mouse exited project list, no need to load
          return;
        }
        if (project == null) {
          console.error("Project doesn't exist anymore.");
          return;
        }
        const projectDecisionData = new DecisionData();
        importText(project.data, projectDecisionData);
        const rootName = $localize`Zielhierarchie`;
        this.treeOSDisplayed = projectDecisionData.objectiveAspects.getAspectTree(rootName);
        this.treeOSDisplayed.children.forEach((objective: Tree<HierarchyElement>, objectiveIdx: number) => {
          if (projectDecisionData.objectives[objectiveIdx].isNumerical) {
            objective.value.scale = projectDecisionData.objectives[objectiveIdx].numericalData;
          } else if (projectDecisionData.objectives[objectiveIdx].isVerbal) {
            objective.value.scale = projectDecisionData.objectives[objectiveIdx].verbalData;
          } else if (projectDecisionData.objectives[objectiveIdx].isIndicator) {
            objective.value.scale = projectDecisionData.objectives[objectiveIdx].indicatorData;
          }
          objective.value.collapsed = true;
        });
        if (!project.isHovered) {
          this.treeOS = cloneDeep(this.treeOSDisplayed); // backup displayed Tree, in case user stops hovering
          this.currentProjectId = project.id;
          this.currentProjectName = projectDecisionData.decisionProblem;
        }
        this.displayedProjectId = project.id;
        this.displayedProjectName = projectDecisionData.decisionProblem;
      });
  }

  getRowColor(projectId: string) {
    if (this.currentProjectId === projectId && (this.displayedProjectId === '' || this.displayedProjectId === projectId)) {
      return '#f0f0f0'; // light gray
    }
    if (this.displayedProjectId === projectId) {
      return '#e6e6e6'; // dark gray
    }
    return null;
  }

  loadProject(projectId: string, isHovered = true) {
    if (this.inSelectionMode) {
      this.mouseIsHovering = true;
      if (isHovered && (!this.hoversTheSameProject(projectId) || this.currentProjectId === '')) {
        // clicked and hovered are different OR nothing clicked yet [onHover]
        this.displayedProjectSubject.next({ projectId, isHovered });
      }
      if (!isHovered) {
        if (projectId === this.currentProjectId) {
          // clicked and hovered are equal, clone current from hovered but don't re-render by setting treeOsHovered = null
          this.treeOS = cloneDeep(this.treeOSDisplayed);
        } else {
          // clicked and hovered are different [onClick]
          this.displayedProjectSubject.next({ projectId, isHovered });
        }
      }
    }
  }

  hoversTheSameProject(projectId: string) {
    if (this.treeOSDisplayed == null) {
      return projectId === this.currentProjectId;
    } else {
      return projectId === this.displayedProjectId;
    }
  }

  getProjectTitle() {
    if (this.treeOSDisplayed != null) {
      return this.displayedProjectName;
    } else if (this.treeOS != null) {
      return this.currentProjectName;
    } else {
      return '';
    }
  }

  hideHoveredProject() {
    if (this.inSelectionMode) {
      this.mouseIsHovering = false;
      this.displayedProjectId = '';
      this.displayedProjectName = '';
      this.treeOSDisplayed = null;
    }
  }
}
