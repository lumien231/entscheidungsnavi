import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HoverPopOverDirective, NoteBtnComponent, WidthTriggerDirective } from '@entscheidungsnavi/widgets';
import { SharedModule } from '../shared/shared.module';
import { NoteHoverComponent } from '../shared/note-hover/note-hover.component';
import { AttachedObjectiveDataComponent, ObjectivesComponent } from './main';
import { ZieleHint1Component, ZieleHint2Component, ZieleHint3Component, ZieleHint4Component, ZieleHint5Component } from './hints';
import { ObjectiveAspectHierarchyModule } from './aspect-hierarchy';
import { ObjectiveAspectListComponent } from './aspect-list/objective-aspect-list.component';
import { ObjectivesRoutingModule } from './objectives-routing.module';
import { ZieleExplanationComponent } from './help';
import { HelpBackgroundComponent } from './hints/help-background/help-background.component';
import { Help1Component as Hint1Help1Component } from './hints/hint1/help1/help1.component';
import { Help2Component as Hint1Help2Component } from './hints/hint1/help2/help2.component';
import { Help2Component as Hint3Help2Component } from './hints/hint3/help2/help2.component';
import { Help2Component as Hint5Help2Component } from './hints/hint5/help2/help2.component';
import { Help1Component as Hint3Help1Component } from './hints/hint3/help1/help1.component';
import { Help1Component as Hint5Help1Component } from './hints/hint5/help1/help1.component';
import { ObjectiveBoxComponent } from './objective-box/objective-box.component';
import { DeleteObjectiveModalComponent } from './delete-objective-modal/delete-objective-modal.component';
import { ObjectiveListComponent } from './objective-list/objective-list.component';

const DECLARE_AND_EXPORT = [ZieleHint4Component, ObjectiveAspectListComponent, ObjectiveListComponent, AttachedObjectiveDataComponent];
@NgModule({
  declarations: [
    ...DECLARE_AND_EXPORT,
    DeleteObjectiveModalComponent,
    ObjectiveBoxComponent,
    ObjectivesComponent,
    ZieleExplanationComponent,
    ZieleHint1Component,
    ZieleHint2Component,
    ZieleHint3Component,
    ZieleHint5Component,
    ObjectiveAspectListComponent,
    ZieleExplanationComponent,
    ObjectivesComponent,
    HelpBackgroundComponent,
    Hint1Help1Component,
    Hint1Help2Component,
    Hint3Help2Component,
    Hint5Help2Component,
    Hint3Help1Component,
    Hint5Help1Component,
  ],
  imports: [
    CommonModule,
    ObjectiveAspectHierarchyModule,
    ObjectivesRoutingModule,
    SharedModule,
    NoteBtnComponent,
    HoverPopOverDirective,
    NoteHoverComponent,
    WidthTriggerDirective,
  ],
  exports: [...DECLARE_AND_EXPORT],
})
export class ObjectivesModule {}
