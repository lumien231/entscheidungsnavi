import { ObjectiveElement } from '@entscheidungsnavi/decision-data/steps';
import { IndicatorObjectiveData, NumericalObjectiveData, VerbalObjectiveData } from '@entscheidungsnavi/decision-data/classes';

export class HierarchyElement extends ObjectiveElement {
  collapsed?: boolean;
  scale?: NumericalObjectiveData | VerbalObjectiveData | IndicatorObjectiveData;
}
