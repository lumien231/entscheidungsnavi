export class TreeMirror<T> {
  mirrorValues: Map<string, T>;

  constructor() {
    this.mirrorValues = new Map<string, T>();
  }

  get(locationSequence: number[]) {
    return this.mirrorValues.get(locationSequence.join());
  }

  set(locationSequence: number[], value: T) {
    this.mirrorValues.set(locationSequence.join(), value);
  }

  clear(locationSequence: number[]) {
    this.mirrorValues.delete(locationSequence.join());
  }
}
