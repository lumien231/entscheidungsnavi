import { Input, Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { AttachedObjectiveData, DecisionData } from '@entscheidungsnavi/decision-data';

@Component({
  selector: 'dt-attached-objective-data',
  template: ` <ul class="dt-plain-list">
    <li *ngFor="let id of data">
      <ng-container [ngSwitch]="id">
        <span i18n *ngSwitchCase="'ideas'"> Ideen für Alternativen </span>
        <span i18n *ngSwitchCase="'scale'"> Messskalen </span>
        <span i18n *ngSwitchCase="'outcomes'"> Einträge im Wirkungsmodell </span>
        <span i18n *ngSwitchCase="'weights'"> Zielgewichtung </span>
        <span i18n *ngSwitchCase="'utility'"> Benutzerdefinierte Nutzenfunktionen </span>
        <span i18n *ngSwitchCase="'comment'"> Zielerläuterung </span>
        <span *ngSwitchDefault>
          {{ id }}
        </span>
      </ng-container>
    </li>
  </ul>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AttachedObjectiveDataComponent implements OnInit {
  @Input()
  objectiveIndex: number;

  data: AttachedObjectiveData[];

  constructor(private decisionData: DecisionData) {}

  ngOnInit() {
    this.data = this.decisionData.getAttachedObjectiveData(this.objectiveIndex);
  }
}
