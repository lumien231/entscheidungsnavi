import { Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Objective } from '@entscheidungsnavi/decision-data/classes';
import { PopOverService } from '@entscheidungsnavi/widgets';
import { ObjectiveElement, Tree } from '@entscheidungsnavi/decision-data/steps';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { loadingIndicator } from '@entscheidungsnavi/widgets/loading-snackbar/loading-snackbar.component';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { ProjectService } from '../../../app/data/project';
import { TransferService } from '../../../app/transfer';
import { AppSettingsService } from '../../../app/data/app-settings.service';
import { HierarchyElement } from '../aspect-hierarchy/hierarchy-element';

@Component({
  selector: 'dt-objective-box',
  templateUrl: './objective-box.component.html',
  styleUrls: ['./objective-box.component.scss'],
})
export class ObjectiveBoxComponent implements OnChanges {
  @Input() objective: Objective;

  @Input() expanded: boolean;
  @Output() expandedChange = new EventEmitter<boolean>();

  @Output() deleteClick = new EventEmitter<void>();

  aspectTree: Tree<HierarchyElement>;

  get showSendButton() {
    return this.appSettings.showSendButtons;
  }

  get teamTrait() {
    return this.projectService.getTeamTrait();
  }

  constructor(
    private appSettings: AppSettingsService,
    private transferService: TransferService,
    private popOverService: PopOverService,
    private snackBar: MatSnackBar,
    private projectService: ProjectService,
    private teamsService: TeamsService
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if ('objective' in changes) {
      this.createTree();
    }
  }

  sendObjective(element: ElementRef<HTMLElement>) {
    this.transferService.broadcastObjective(this.objective).then(() => {
      this.popOverService.whistle(element, $localize`Ziel gesendet!`);
    });
  }

  transferObjectiveToMe(buttonElement: ElementRef<HTMLElement>) {
    this.teamsService
      .transferObject(this.teamTrait.info.id, this.teamTrait.activeMember.id, this.objective.uuid)
      .pipe(loadingIndicator(this.snackBar, $localize`Ziel wird übernommen...`))
      .subscribe({
        next: () => this.popOverService.whistle(buttonElement, $localize`Ziel übernommen!`, 'check'),
        error: () => this.popOverService.whistle(buttonElement, $localize`Beim Übernehmen des Ziels ist ein Fehler aufgetreten.`, 'error'),
      });
  }

  private createTree() {
    const aspectTree = this.objective.aspects;

    this.aspectTree = {
      value: new ObjectiveElement('', aspectTree.value.createdInSubStep, '#d3d3d3', '#ffffff'),
      children: aspectTree.children.map(child => ({
        value: {
          // Default values that may be overwritten if specified in the child
          backgroundColor: '#ffffff',
          textColor: '#000000',
          ...child.value,
        },
        children: child.children,
      })),
    };
  }
}
