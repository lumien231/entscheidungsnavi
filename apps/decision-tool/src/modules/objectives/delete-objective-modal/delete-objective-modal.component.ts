import { Component, Inject } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';
import { DecisionData } from '@entscheidungsnavi/decision-data';

@Component({
  templateUrl: './delete-objective-modal.component.html',
  styleUrls: ['./delete-objective-modal.component.scss'],
})
export class DeleteObjectiveModalComponent {
  get name() {
    return this.decisionData.objectives[this.objectiveIndex].name;
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public objectiveIndex: number,
    private dialogRef: MatDialogRef<DeleteObjectiveModalComponent>,
    private decisionData: DecisionData
  ) {}

  close(confirmDelete = false) {
    this.dialogRef.close(confirmDelete);
  }
}
