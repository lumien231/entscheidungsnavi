import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import * as Sentry from '@sentry/angular';
import { DecisionToolModule } from './app/decision-tool.module';
import { ENVIRONMENT } from './environments/environment';

if (ENVIRONMENT.sentryDsn) {
  Sentry.init({
    dsn: ENVIRONMENT.sentryDsn,
    tunnel: '/api/sentry',
    release: ENVIRONMENT.version,
    environment: ENVIRONMENT.type,
    ignoreErrors: ['ResizeObserver loop limit exceeded', 'ResizeObserver loop completed with undelivered notifications.'],
  });
}

if (ENVIRONMENT.production) {
  enableProdMode();
} else {
  Error.stackTraceLimit = Infinity;
}

platformBrowserDynamic()
  .bootstrapModule(DecisionToolModule)
  .catch(err => console.error(err));
