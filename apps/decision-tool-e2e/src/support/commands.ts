// ***********************************************
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Cypress {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars,@typescript-eslint/naming-convention
  interface Chainable<Subject> {
    getBySel<T extends Node = HTMLElement>(
      selector: string,
      options?: Partial<Loggable & Timeoutable & Withinable & Shadow>
    ): Chainable<JQuery<T>>;

    findBySel<T extends Node = HTMLElement>(selector: string, options?: Partial<Loggable & Timeoutable & Shadow>): Chainable<JQuery<T>>;

    findDtModal(): Chainable<JQuery>;

    // Commands for `dt-select`.

    getDtSelect<T extends Node = HTMLElement>(
      optionSelected: string,
      options?: Partial<Loggable & Timeoutable & Shadow>
    ): Chainable<JQuery<T>>;

    getOptionFromDtSelect<T extends Node = HTMLElement>(optionText: string): Chainable<JQuery<T>>;

    selectOptionFromDtSelect(optionSelected: string, optionText: string): void;

    // Commands for `mat-select`.

    openFirstSelect(): Chainable<JQuery>;

    shouldFindSelectWithValue<T extends Node = HTMLElement>(value: string): Chainable<JQuery<T>>;

    shouldFindSelectWithDisabledOption<T extends Node = HTMLElement>(optionName: string): Chainable<JQuery<T>>;

    selectValue<T extends Node = HTMLElement>(value: string): Chainable<JQuery<T>>;

    loadAlex(): void;

    waitForMenuToClose(): Chainable<JQuery>;
  }
}

// adapted from https://docs.cypress.io/guides/references/best-practices#Real-World-Example
Cypress.Commands.add('getBySel', (selector, ...args) => {
  return cy.get(`[data-cy=${selector}]`, ...args);
});

Cypress.Commands.add('findBySel', { prevSubject: true }, (subject, selector, ...args) => {
  return subject.find(`[data-cy=${selector}]`, ...args);
});

Cypress.Commands.add('findDtModal', () => {
  return cy.document().its('body').find('dt-modal-content');
});

// Commands for `dt-select`.

Cypress.Commands.add('getDtSelect', optionSelected => {
  return cy.get(`dt-select[data-cy="${optionSelected}"]`).should('have.length', 1).first();
});

Cypress.Commands.add('getOptionFromDtSelect', { prevSubject: true }, (subject, optionText) => {
  expect(subject.prop('tagName')).to.be.equal('DT-SELECT', 'You can only use this command with `dt-select` as a subject.');
  const currentlySelectedOption = subject.attr('data-cy');
  expect(currentlySelectedOption).not.to.be.empty;

  return cy.get(`mat-selection-list[data-cy="dt-select-${currentlySelectedOption}"]`).find('mat-list-option').contains(optionText);
});

Cypress.Commands.add('selectOptionFromDtSelect', (optionSelected, optionText) => {
  cy.getDtSelect(optionSelected).click();
  cy.getDtSelect(optionSelected).getOptionFromDtSelect(optionText).click();
  cy.waitForMenuToClose();
});

// Commands for `mat-select`.

Cypress.Commands.add('shouldFindSelectWithValue', value => {
  cy.get('mat-select').first().get('.mat-select-value-text').should('have.text', value);
});

Cypress.Commands.add('openFirstSelect', () => {
  return cy.get('mat-select').first().click();
});

Cypress.Commands.add('selectValue', value => {
  cy.document()
    .its('body')
    .within(() => {
      cy.get('.mat-select-panel')
        .should('have.length', 1)
        .first()
        .within(() => {
          cy.get('.mat-option-text').contains(value).click();
        });
    });
  cy.shouldFindSelectWithValue(value);
});

Cypress.Commands.add('shouldFindSelectWithDisabledOption', optionName => {
  cy.document()
    .its('body')
    .within(() => {
      cy.get('.mat-select-panel')
        .should('have.length', 1)
        .first()
        .within(() => {
          cy.get('mat-option.mat-option-disabled').contains(optionName);
        });
    });
});

// Other commands.

Cypress.Commands.add('loadAlex', () => {
  cy.getBySel('project_mgmt').click();

  cy.fixture('alex.json').then(alexJson => {
    cy.getBySel<HTMLInputElement>('project_file_input').then(function (el) {
      const blob = new Blob([JSON.stringify(alexJson)], { type: 'application/json' });
      const file = new File([blob], 'alex.json', { type: 'application/json' });
      const list = new DataTransfer();

      list.items.add(file);
      const myFileList = list.files;

      el[0].files = myFileList;
      el[0].dispatchEvent(new Event('change', { bubbles: true }));
    });
  });
});

Cypress.Commands.add('waitForMenuToClose', () => {
  cy.get('.mat-menu-panel').should('not.exist');
});
