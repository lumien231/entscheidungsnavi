describe('new project: professional', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.getBySel('new-project-button').click();
    cy.getBySel('project-name-input').type('test project');
    cy.getBySel('mode-selection-professional').click();
    cy.getBySel('create-project-button').click();
  });

  it('has a decision statement, two alternatives, and one objective', () => {
    cy.getBySel('statement-input').should('not.to.match', ':empty');
    cy.getBySel('alternative-name-field').should('have.length', 2);
    cy.getBySel('objective-name-field').should('have.length', 1);
  });
});
