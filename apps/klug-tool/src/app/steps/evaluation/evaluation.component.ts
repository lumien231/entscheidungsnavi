import { ChangeDetectorRef, Component } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { CompareResult, PairComparisons, PairComparisonService } from '../../services/pair-comparisons.service';
import { AbstractStepComponent } from '../step-container.component';

@Component({
  selector: 'klug-evaluation',
  templateUrl: './evaluation.component.html',
  styleUrls: ['./evaluation.component.scss'],
})
export class EvaluationComponent extends AbstractStepComponent {
  constructor(private decisionData: DecisionData, private cdRef: ChangeDetectorRef, pairComparisonService: PairComparisonService) {
    super();

    this.turnComparisonsIntoWeights(pairComparisonService.getComparisonTable());
  }

  private turnComparisonsIntoWeights(comparisons: PairComparisons) {
    // Fill comparison
    for (let objectiveIndexRow = 0; objectiveIndexRow < this.decisionData.objectives.length; objectiveIndexRow++) {
      for (
        let objectiveIndexColumn = objectiveIndexRow;
        objectiveIndexColumn < this.decisionData.objectives.length;
        objectiveIndexColumn++
      ) {
        if (objectiveIndexColumn === objectiveIndexRow) {
          comparisons[objectiveIndexColumn][objectiveIndexRow] = 'less';
          continue;
        }

        const setComparison = comparisons[objectiveIndexRow][objectiveIndexColumn];
        comparisons[objectiveIndexColumn][objectiveIndexRow] =
          setComparison === 'less' ? 'more' : setComparison === 'more' ? 'less' : setComparison;
      }
    }

    const points: { [key in Exclude<CompareResult, 'missing'>]: number } = {
      more: 0,
      less: 1,
      equal: 0.5,
    };

    for (let objectiveIndexColumn = 0; objectiveIndexColumn < this.decisionData.objectives.length; objectiveIndexColumn++) {
      const sum = comparisons.reduce((acc, row) => {
        return acc + points[row[objectiveIndexColumn]];
      }, 0);

      this.decisionData.weights.preliminaryWeights[objectiveIndexColumn].value = sum;
    }

    this.decisionData.weights.normalizePreliminaryWeights();
  }
}
