import { Directive, TemplateRef, ViewChild } from '@angular/core';

@Directive()
export abstract class AbstractStepComponent {
  @ViewChild('help', { static: true })
  helpTemplate: TemplateRef<unknown>;
}
