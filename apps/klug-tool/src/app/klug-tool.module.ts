import { NgModule } from '@angular/core';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatLegacyButtonModule as MatButtonModule } from '@angular/material/legacy-button';
import { MatIconModule, MatIconRegistry, MAT_ICON_DEFAULT_OPTIONS } from '@angular/material/icon';
import { MatLegacyFormFieldModule as MatFormFieldModule } from '@angular/material/legacy-form-field';
import { MatLegacyInputModule as MatInputModule } from '@angular/material/legacy-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { MatLegacyRippleModule as MatRippleModule } from '@angular/material/legacy-core';
import {
  MatLegacyTooltipDefaultOptions as MatTooltipDefaultOptions,
  MatLegacyTooltipModule as MatTooltipModule,
  MAT_LEGACY_TOOLTIP_DEFAULT_OPTIONS as MAT_TOOLTIP_DEFAULT_OPTIONS,
} from '@angular/material/legacy-tooltip';
import { HttpClientModule } from '@angular/common/http';
import { MatLegacyProgressSpinnerModule as MatProgressSpinnerModule } from '@angular/material/legacy-progress-spinner';
import { MatLegacyMenuModule as MatMenuModule } from '@angular/material/legacy-menu';
import { MatLegacyProgressBarModule as MatProgressBarModule } from '@angular/material/legacy-progress-bar';
import { MatLegacyDialogModule as MatDialogModule } from '@angular/material/legacy-dialog';
import { MatLegacyCheckboxModule as MatCheckboxModule } from '@angular/material/legacy-checkbox';
import { checkType } from '@entscheidungsnavi/tools';
import { QuillModule } from 'ngx-quill';
import { Platform } from '@angular/cdk/platform';
import { ApiClientModule } from '@entscheidungsnavi/api-client';
import { KlugToolComponent } from './klug-tool.component';
import { NavigationComponent } from './navigation/navigation.component';
import { CodeInputComponent } from './steps/code-input/code-input.component';
import { ObjectivesComponent } from './steps/objectives/objectives.component';
import { DecisionStatementComponent } from './steps/decision-statement/decision-statement.component';
import { HelpComponent } from './help/help.component';
import { ObjectiveListComponent } from './steps/objectives/objective-list/objective-list.component';
import { ObjectiveBoxComponent } from './steps/objectives/objective-list/objective-box/objective-box.component';
import { AlternativesComponent } from './steps/alternatives/alternatives.component';
import { AlternativeListComponent } from './steps/alternatives/alternative-list/alternative-list.component';
import { AlternativeBoxComponent } from './steps/alternatives/alternative-list/alternative-box/alternative-box.component';
import { AssessmentComponent } from './steps/assessment/assessment.component';
import { ComparisonComponent } from './steps/comparison/comparison.component';
import { EvaluationComponent } from './steps/evaluation/evaluation.component';
import { NavlineComponent } from './navigation/navline/navline.component';
import { StepDescriptionComponent } from './shared/step-description/step-description.component';
import { PdfExportComponent } from './shared/pdf-export/pdf-export.component';
import { DecisionStatementBoxComponent } from './steps/decision-statement/decision-statement-box/decision-statement-box.component';
import { EvaluationGraphComponent } from './steps/evaluation/evaluation-graph/evaluation-graph.component';
import { Constants } from './shared/constants';
import { AssessmentGraphComponent } from './steps/assessment/assessment-graph/assessment-graph.component';
import { ComparisonGraphComponent } from './steps/comparison/comparison-graph/comparison-graph.component';
import { KlugRoutingModule } from './klug-tool.routing';

@NgModule({
  declarations: [
    KlugToolComponent,
    NavigationComponent,
    HelpComponent,
    CodeInputComponent,
    DecisionStatementComponent,
    DecisionStatementBoxComponent,
    ObjectivesComponent,
    ObjectiveListComponent,
    ObjectiveBoxComponent,
    AlternativesComponent,
    AlternativeListComponent,
    AlternativeBoxComponent,
    AssessmentComponent,
    AssessmentGraphComponent,
    ComparisonComponent,
    ComparisonGraphComponent,
    EvaluationComponent,
    EvaluationGraphComponent,
    NavlineComponent,
    StepDescriptionComponent,
    PdfExportComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatDialogModule,
    MatCheckboxModule,
    MatProgressBarModule,
    QuillModule.forRoot(),
    KlugRoutingModule,
    BrowserAnimationsModule,
    ApiClientModule,
  ],
  providers: [
    { provide: DecisionData, useValue: new DecisionData() },
    {
      provide: MAT_TOOLTIP_DEFAULT_OPTIONS,
      useValue: checkType<Partial<MatTooltipDefaultOptions>>({ disableTooltipInteractivity: true }),
    },
    {
      provide: Constants,
      useValue: new Constants(),
    },
    {
      provide: MAT_ICON_DEFAULT_OPTIONS,
      useValue: {
        fontSet: 'material-symbols-outlined',
      },
    },
  ],
  bootstrap: [KlugToolComponent],
})
export class KlugToolModule {
  constructor(iconRegistry: MatIconRegistry, domSanitizer: DomSanitizer, platform: Platform) {
    iconRegistry.addSvgIconInNamespace('klug', 'objectives', domSanitizer.bypassSecurityTrustResourceUrl(`assets/icons/objectives.svg`));

    iconRegistry.addSvgIconInNamespace(
      'klug',
      'alternatives',
      domSanitizer.bypassSecurityTrustResourceUrl(`assets/icons/alternatives.svg`)
    );

    iconRegistry.addSvgIconInNamespace('klug', 'comparison', domSanitizer.bypassSecurityTrustResourceUrl(`assets/icons/comparison.svg`));

    if (platform.WEBKIT) {
      document.body.classList.add('dt-webkit');
    }
  }
}
