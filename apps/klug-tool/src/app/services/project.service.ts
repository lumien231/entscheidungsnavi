import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { KlugProjectDto, KlugService } from '@entscheidungsnavi/api-client';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { dataToText, readTextWithVersion, copyProperties } from '@entscheidungsnavi/decision-data/export';
import { map, tap } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { StateService } from './state.service';

export const KLUG_TOKEN_KEY = 'klug-token';

@Injectable({ providedIn: 'root' })
export class KlugProjectService {
  private accessToken$ = new BehaviorSubject('');

  public isOfficialKlugProject = false;
  public isReadonlyProject = false;

  constructor(
    private decisionData: DecisionData,
    private klugService: KlugService,
    private stateService: StateService,
    private router: Router
  ) {}

  hasValidAccessToken() {
    return this.accessToken$.pipe(map(token => token !== ''));
  }

  getAccessToken() {
    return this.accessToken$.value;
  }

  createUnofficialProject() {
    return this.klugService.generateUnofficialProject().pipe(tap(project => this.useProject(project)));
  }

  useToken(token: string) {
    return this.klugService.getKlugProject(token).pipe(tap(project => this.useProject(project)));
  }

  useProject(project: KlugProjectDto) {
    const data = project.data;
    if (data != null) {
      this.importText(data);
    } else {
      this.importDecisionData(new DecisionData());
      this.decisionData.decisionProblem = project.token;
      this.decisionData.lastUrl = '/impactmodel';
      this.klugService.updateKlugProject(project.token, { data: dataToText(this.decisionData, 'klug') }).subscribe();
    }

    this.isOfficialKlugProject = project.isOfficial;
    this.isReadonlyProject = project.readonly;

    this.setToken(project.token);

    this.router.navigateByUrl('decision-statement');
  }

  importText(text: string) {
    try {
      // load the data in a temporary DecisionData object
      const imp = readTextWithVersion(text);
      this.importDecisionData(imp[0]);
    } catch (e) {
      console.log(e);
      throw new Error('Der übergebene Text ist kein gültiges JSON.');
    }
  }

  importDecisionData(data: DecisionData) {
    this.decisionData.reset();
    copyProperties(data, this.decisionData);
  }

  private setToken(newToken: string) {
    localStorage.setItem(KLUG_TOKEN_KEY, newToken);
    this.accessToken$.next(newToken);
  }
}
