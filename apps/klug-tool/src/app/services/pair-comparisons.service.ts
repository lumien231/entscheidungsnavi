import { Injectable } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';

export type CompareResult = 'more' | 'equal' | 'less' | 'missing';
export type PairComparisons = CompareResult[][];

type KlugData = { pairComparisons: PairComparisons };

@Injectable({ providedIn: 'root' })
export class PairComparisonService {
  private readonly dataField = 'klug';
  constructor(private decisionData: DecisionData) {
    decisionData.objectiveAdded$.subscribe(objectiveIndex => {
      const comparisonTable = this.getComparisonTable();
      for (const comparisons of comparisonTable) {
        comparisons.splice(objectiveIndex, 0, 'missing');
      }

      comparisonTable.splice(objectiveIndex, 0, new Array(this.decisionData.objectives.length).fill('missing'));
    });

    decisionData.objectiveRemoved$.subscribe(objectiveIndex => {
      const comparisonTable = this.getComparisonTable();
      for (const comparisons of comparisonTable) {
        comparisons.splice(objectiveIndex, 1);
      }

      this.getComparisonTable().splice(objectiveIndex, 1);
    });
  }

  getComparisonTable() {
    let field = this.decisionData.extraData[this.dataField] as KlugData;

    if (field == null) {
      field = this.decisionData.extraData[this.dataField] = { pairComparisons: [] };
    }

    return field.pairComparisons;
  }
}
