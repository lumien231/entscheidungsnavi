import { ChangeDetectorRef, Component, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatLegacyDialog as MatDialog, MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';
import { NavigationEnd, Router } from '@angular/router';
import { KlugService } from '@entscheidungsnavi/api-client';
import { KlugFinishProject } from '@entscheidungsnavi/api-types';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { filter, finalize, lastValueFrom, Observable, takeUntil } from 'rxjs';
import { AccessService } from '../../routing/access.service';
import { KlugProjectService } from '../../services/project.service';
import { PdfExportComponent } from '../../shared/pdf-export/pdf-export.component';
import { ROUTER_STEPS } from '../navigation.component';

@Component({
  selector: 'klug-navline',
  templateUrl: './navline.component.html',
  styleUrls: ['./navline.component.scss'],
})
export class NavlineComponent {
  @ViewChild('finishTemplate')
  finishTemplate: TemplateRef<unknown>;

  @ViewChild('tokenInfoTemplate')
  tokenInfoTemplate: TemplateRef<unknown>;

  @ViewChild(PdfExportComponent)
  pdfExportComponent: PdfExportComponent;

  @OnDestroyObservable()
  protected onDestroy$: Observable<void>;

  showPdfExport = false;
  exportingProject = false;
  finishErrored = false;

  finishForm: FormGroup<{ email: FormControl<string>; privacyPolicy: FormControl<boolean> }> = new FormGroup(
    {
      email: new FormControl('', [Validators.email]),
      privacyPolicy: new FormControl(false),
    },
    [
      (group: typeof this.finishForm) => {
        if (group.controls.email.value && !this.finishForm.controls.privacyPolicy.value) {
          return { privacyPolicyRequired: true };
        } else {
          return null;
        }
      },
    ]
  );

  private activeLink: string;
  constructor(
    private accessService: AccessService,
    private dialog: MatDialog,
    private klugService: KlugService,
    private projectService: KlugProjectService,
    private cdRef: ChangeDetectorRef,
    router: Router
  ) {
    router.events
      .pipe(
        takeUntil(this.onDestroy$),
        filter(event => event instanceof NavigationEnd)
      )
      .subscribe(() => {
        this.activeLink = router.url.slice(1);
      });
  }

  get isOfficialKlugProject() {
    return this.projectService.isOfficialKlugProject;
  }

  get userIsOnStep() {
    return ROUTER_STEPS.findIndex(step => step === this.activeLink) > -1;
  }

  get userIsOnLastStep() {
    return ROUTER_STEPS.findIndex(step => step === this.activeLink) === ROUTER_STEPS.length - 1;
  }

  get backLink() {
    const step = ROUTER_STEPS.findIndex(step => step === this.activeLink);

    if (step === -1) {
      return 'decision-statement';
    } else if (step > 0) {
      return ROUTER_STEPS[step - 1];
    } else {
      return null;
    }
  }

  get forwardLink() {
    const activeStepIndex = ROUTER_STEPS.findIndex(step => step === this.activeLink);

    if (activeStepIndex === -1) {
      return 'decision-statement';
    }

    if (activeStepIndex === ROUTER_STEPS.length - 1) {
      return null;
    }

    const nextLink = ROUTER_STEPS[activeStepIndex + 1];

    return this.accessService.canAccess(nextLink) ? nextLink : null;
  }

  openFinishDialog() {
    this.dialog.open(this.finishTemplate);
  }

  async openPDFReport() {
    this.showPdfExport = true;
    this.cdRef.detectChanges();

    (await this.pdfExportComponent.createPDFDoc()).open();

    this.showPdfExport = false;
  }

  async continueToQuestionnaire(dialogRef: MatDialogRef<unknown, unknown>) {
    if (!this.finishForm.valid) {
      return;
    }

    if (!this.finishForm.controls.email.value && !this.projectService.isOfficialKlugProject) {
      const confirmed = await lastValueFrom(
        this.dialog
          .open<unknown, { token: string }, 'confirm'>(this.tokenInfoTemplate, {
            data: { token: this.projectService.getAccessToken() },
          })
          .afterClosed()
      );

      if (confirmed !== 'confirm') {
        return;
      }
    }

    this.exportingProject = true;

    const body: KlugFinishProject = {};

    if (this.finishForm.controls.email.value) {
      this.showPdfExport = true;
      this.cdRef.detectChanges();

      body.pdfExportImages = await this.pdfExportComponent.getPdfImages();
      body.email = this.finishForm.controls.email.value;

      this.showPdfExport = false;
    }

    const token = this.projectService.getAccessToken();

    this.klugService
      .finishProject(token, body)
      .pipe(finalize(() => (this.exportingProject = false)))
      .subscribe({
        complete: () => {
          const linkToOpen = this.projectService.isOfficialKlugProject
            ? `https://klug-plattform.org/loginRedirect.php?token=${token}`
            : 'https://entscheidungsnavi.de/';
          window.open(linkToOpen, '_self', 'noopener');

          dialogRef.close();
        },
        error: () => {
          this.finishErrored = true;
        },
      });
  }
}
