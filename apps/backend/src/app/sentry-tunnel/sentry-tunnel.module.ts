import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { SentryTunnelController } from './sentry-tunnel.controller';

@Module({
  imports: [HttpModule],
  controllers: [SentryTunnelController],
  providers: [],
})
export class SentryTunnelModule {}
