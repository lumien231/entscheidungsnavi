import { HttpService } from '@nestjs/axios';
import { BadRequestException, Body, Controller, Logger, Post, Headers, Ip } from '@nestjs/common';

const sentryHost = 'sentry.entscheidungsnavi.de';

const knownProjectIds = ['2'];

const FORWARD_HEADER = 'x-forwarded-for';

@Controller('sentry')
export class SentryTunnelController {
  private readonly logger = new Logger(SentryTunnelController.name);
  constructor(private httpService: HttpService) {}

  @Post()
  async tunnel(@Body() envelope: string, @Headers(FORWARD_HEADER) forwardedFor: string, @Ip() clientIP: string) {
    try {
      const pieces = envelope.split('\n');

      const header = JSON.parse(pieces[0]);

      const dsnURL = new URL(header.dsn);

      const host = dsnURL.host;

      const path = dsnURL.pathname.slice(1);

      if (host !== sentryHost) {
        throw new BadRequestException(`invalid host: ${host}`);
      }

      const projectId = path.endsWith('/') ? path.slice(0, -1) : path;
      if (!knownProjectIds.includes(projectId)) {
        throw new BadRequestException(`invalid project id: ${projectId}`);
      }

      const url = `https://${sentryHost}/api/${projectId}/envelope/`;

      const headers = {};

      // Forward Client IP
      if (forwardedFor) {
        headers[FORWARD_HEADER] = forwardedFor + ', ' + clientIP;
      } else {
        headers[FORWARD_HEADER] = clientIP;
      }

      const response = await this.httpService.post(url, envelope, { headers }).toPromise();

      return response.data;
    } catch (e) {
      if (e instanceof Error) {
        this.logger.error(e.stack);
      }
      throw new BadRequestException();
    }
  }
}
