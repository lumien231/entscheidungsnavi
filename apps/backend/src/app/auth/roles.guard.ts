import { Role } from '@entscheidungsnavi/api-types';
import { CanActivate, ExecutionContext, mixin, Type, UnauthorizedException } from '@nestjs/common';
import { Request } from 'express';

// eslint-disable-next-line @typescript-eslint/naming-convention
export const RolesGuard = (role: Role): Type<CanActivate> => {
  class RolesGuardMixin implements CanActivate {
    async canActivate(context: ExecutionContext) {
      const request = context.switchToHttp().getRequest<Request>();
      if (!request.isAuthenticated()) throw new UnauthorizedException();

      return request.user.roles?.includes(role);
    }
  }

  return mixin(RolesGuardMixin);
};
