import { CSRF_ERROR } from '@entscheidungsnavi/api-types';
import { CsrfSync, CsrfSynchronisedProtection, csrfSync } from 'csrf-sync';
import { NextFunction } from 'express';

// We use only default parameters
const csrf = csrfSync();

/**
 * We wrap the csrf middleware to catch its errors and send well-formed responses
 * to the client.
 * This also silences the error in the server console.
 */
const wrappedCsrfMiddleware: CsrfSynchronisedProtection = (req, res, next) => {
  const wrappedNext: NextFunction = arg => {
    if (arg === CSRF.invalidCsrfTokenError) {
      res.status(403).send(CSRF_ERROR);
    } else {
      next(arg);
    }
  };

  csrf.csrfSynchronisedProtection(req, res, wrappedNext);
};

// Export everything but use our wrapped middleware
export const CSRF: CsrfSync = {
  ...csrf,
  csrfSynchronisedProtection: wrappedCsrfMiddleware,
};
