import { ROLES, Role } from '@entscheidungsnavi/api-types';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Date } from 'mongoose';

export type UserDocument = User & Document;

/**
 * Augmented by `passport-local-mongoose` with fields for password, salt, etc.
 */
@Schema({ timestamps: true })
export class User {
  readonly id: string;

  @Prop({ required: true, unique: true })
  email: string;

  @Prop()
  name?: string;

  @Prop({ default: false })
  emailConfirmed: boolean;

  @Prop({ type: [String], enum: ROLES, default: (): Role[] => [] })
  roles: Role[];

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);
