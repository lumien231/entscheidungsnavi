import {
  OptionsQuestionDisplayType,
  OPTIONS_QUESTION_DISPLAY_TYPES,
  QuestionnaireEntryType,
  QUESTIONNAIRE_ENTRY_TYPES,
  TextBlockType,
  TEXT_BLOCK_TYPES,
} from '@entscheidungsnavi/api-types';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Schema as MongooseSchema } from 'mongoose';

@Schema({ _id: false })
export class TextBlockEntry {
  entryType: 'textBlock';

  @Prop({ required: true })
  text: string;

  @Prop({ required: true, enum: TEXT_BLOCK_TYPES, type: String })
  type: TextBlockType;
}

export const TextBlockEntrySchema = SchemaFactory.createForClass(TextBlockEntry);

@Schema({ _id: false })
export class TextQuestionEntry {
  entryType: 'textQuestion';

  @Prop({ required: true })
  question: string;

  @Prop({ required: true })
  label: string;

  @Prop()
  minLength?: number;

  @Prop()
  maxLength?: number;

  // A regex pattern
  @Prop()
  pattern?: string;
}

export const TextQuestionEntrySchema = SchemaFactory.createForClass(TextQuestionEntry);

@Schema({ _id: false })
export class NumberQuestionEntry {
  entryType: 'numberQuestion';

  @Prop({ required: true })
  question: string;

  @Prop({ required: true })
  label: string;

  @Prop()
  min?: number;

  @Prop()
  max?: number;

  // This is not part of the validation. Entered numbers may have higher resolution.
  @Prop()
  step?: number;
}

export const NumberQuestionEntrySchema = SchemaFactory.createForClass(NumberQuestionEntry);

@Schema({ _id: false })
export class OptionsQuestionEntry {
  entryType: 'optionsQuestion';

  @Prop({ required: true })
  question: string;

  @Prop({ required: true })
  label: string;

  @Prop({ type: [String] })
  options: string[];

  @Prop({ required: true, enum: OPTIONS_QUESTION_DISPLAY_TYPES, type: String })
  displayType: OptionsQuestionDisplayType;
}

export const OptionsQuestionEntrySchema = SchemaFactory.createForClass(OptionsQuestionEntry);

@Schema({ _id: false })
export class TableQuestionEntry {
  entryType: 'tableQuestion';

  @Prop({ required: true, type: [String] })
  options: string[];

  @Prop({ required: true })
  baseQuestion: string;

  @Prop({ required: true, type: [String] })
  subQuestions: string[];
}

export const TableQuestionEntrySchema = SchemaFactory.createForClass(TableQuestionEntry);

@Schema({ _id: false, discriminatorKey: 'entryType' })
export class BaseQuestionnaireEntry {
  @Prop({ required: true, enum: QUESTIONNAIRE_ENTRY_TYPES, type: String })
  entryType: QuestionnaireEntryType;
}

export const BaseQuestionnaireEntrySchema = SchemaFactory.createForClass(BaseQuestionnaireEntry);

export type QuestionnaireEntry = TextBlockEntry | TextQuestionEntry | NumberQuestionEntry | OptionsQuestionEntry | TableQuestionEntry;

@Schema({ _id: false })
export class QuestionnairePage {
  @Prop({ required: true, type: [BaseQuestionnaireEntrySchema] })
  entries: QuestionnaireEntry[];
}

export const QuestionnairePageSchema = SchemaFactory.createForClass(QuestionnairePage);

const questionnaireArray = QuestionnairePageSchema.path<MongooseSchema.Types.Array>('entries');
questionnaireArray.discriminator('textBlock', TextBlockEntrySchema);
questionnaireArray.discriminator('textQuestion', TextQuestionEntrySchema);
questionnaireArray.discriminator('numberQuestion', NumberQuestionEntrySchema);
questionnaireArray.discriminator('optionsQuestion', OptionsQuestionEntrySchema);
questionnaireArray.discriminator('tableQuestion', TableQuestionEntrySchema);
