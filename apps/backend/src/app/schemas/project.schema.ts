import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Types, Document } from 'mongoose';

export type ProjectDocument = Project & Document;

@Schema({ timestamps: true })
export class Project {
  readonly id: string;

  @Prop({ required: true })
  userId: Types.ObjectId;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  data: string;

  @Prop({ required: false })
  shareToken?: string;

  @Prop({ required: false, type: Types.ObjectId, ref: 'Team' })
  team?: Types.ObjectId;

  @Prop({ type: [{ type: Types.ObjectId, ref: 'ProjectHistoryEntry' }], default: [] })
  history: Types.ObjectId[];

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const ProjectSchema = SchemaFactory.createForClass(Project);
