import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { TeamInvite, TeamInviteSchema } from './team-invite.schema';
import { TeamMember, TeamMemberSchema } from './team-member.schema';

export type TeamDocument = Team & Document;

@Schema({ timestamps: true })
export class Team {
  @Prop({ required: true, type: String })
  name: string;

  @Prop({ required: true, type: [TeamMemberSchema], default: [] })
  members: Types.DocumentArray<TeamMember>;

  @Prop({ required: true, type: [TeamInviteSchema], default: [] })
  pendingInvites: Types.DocumentArray<TeamInvite>;

  @Prop({ required: true, type: Types.ObjectId, default: (doc: TeamDocument) => doc.members[0]._id })
  editor: Types.ObjectId;

  @Prop({ required: true, type: Types.ObjectId, default: (doc: TeamDocument) => doc.members[0]._id })
  owner: Types.ObjectId;

  @Prop({ required: true, default: '', type: String })
  whiteboard: string;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const TeamSchema = SchemaFactory.createForClass(Team);
