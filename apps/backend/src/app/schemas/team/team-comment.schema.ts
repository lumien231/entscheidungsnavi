import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

export type TeamCommentDocument = TeamComment & Document;

@Schema({ timestamps: true })
export class TeamComment {
  @Prop({ required: true, type: Types.ObjectId, ref: 'TeamMember' })
  author: Types.ObjectId;

  @Prop({ required: true, type: String })
  objectId: string;

  @Prop({ required: true, type: String })
  content: string;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const TeamCommentSchema = SchemaFactory.createForClass(TeamComment);
