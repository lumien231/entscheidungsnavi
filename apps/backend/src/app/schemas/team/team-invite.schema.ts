import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

export type TeamInviteDocument = TeamInvite & Document;

@Schema({ timestamps: true })
export class TeamInvite {
  @Prop({ required: true, type: String })
  email: string;

  @Prop({ required: true, type: String })
  token: string;

  @Prop({ required: true, type: Types.ObjectId, ref: 'TeamMember' })
  inviter: Types.ObjectId;

  readonly createdAt: Date;
}

export const TeamInviteSchema = SchemaFactory.createForClass(TeamInvite);
