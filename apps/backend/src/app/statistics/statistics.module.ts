import { Module } from '@nestjs/common';
import { ProjectsModule } from '../projects/projects.module';
import { UsersModule } from '../users/users.module';
import { StatisticsController } from './statistics.controller';
import { StatisticsService } from './statistics.service';

@Module({ imports: [UsersModule, ProjectsModule], controllers: [StatisticsController], providers: [StatisticsService] })
export class StatisticsModule {}
