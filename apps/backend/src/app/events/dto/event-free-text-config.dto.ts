import { EventFreeTextConfig } from '@entscheidungsnavi/api-types';
import { Exclude, Expose } from 'class-transformer';
import { IsBoolean, IsNumber, IsString } from 'class-validator';

@Exclude()
export class EventFreeTextConfigDto implements EventFreeTextConfig {
  @Expose()
  @IsBoolean()
  enabled: boolean;

  @Expose()
  @IsString()
  name: string;

  @Expose()
  @IsString()
  placeholder: string;

  @Expose()
  @IsNumber()
  minLength: number;
}
