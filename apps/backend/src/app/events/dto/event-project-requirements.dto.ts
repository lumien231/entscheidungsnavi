import { EventProjectRequirements } from '@entscheidungsnavi/api-types';
import { Exclude, Expose } from 'class-transformer';
import { IsBoolean } from 'class-validator';

@Exclude()
export class EventProjectRequirementsDto implements EventProjectRequirements {
  @Expose()
  @IsBoolean()
  requireDecisionStatement: boolean;

  @Expose()
  @IsBoolean()
  requireObjectives: boolean;

  @Expose()
  @IsBoolean()
  requireAlternatives: boolean;

  @Expose()
  @IsBoolean()
  requireImpactModel: boolean;

  @Expose()
  @IsBoolean()
  requireObjectiveWeights: boolean;

  @Expose()
  @IsBoolean()
  requireDecisionQuality: boolean;
}
