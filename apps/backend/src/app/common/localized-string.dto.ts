import { Exclude, Expose } from 'class-transformer';
import { IsDefined } from 'class-validator';
import { LocalizedString } from '../schemas/common/localized-string.schema';

@Exclude()
export class LocalizedStringDto implements LocalizedString {
  @Expose()
  @IsDefined()
  en: string;

  @Expose()
  @IsDefined()
  de: string;
}
