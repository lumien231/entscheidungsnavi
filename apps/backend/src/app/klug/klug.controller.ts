import { Body, Controller, Get, Param, Patch, Post, Res, UseInterceptors } from '@nestjs/common';
import { Response } from 'express';
import { NotFoundInterceptor } from '../common/not-found.interceptor';
import { KlugFinishProjectDto } from './dto/finish-project.dto';
import { UpdateKlugProjectDto } from './dto/update-klug-project.dto';
import { KlugService } from './klug.service';

@Controller('klug')
export class KlugController {
  constructor(private klugService: KlugService) {}

  @UseInterceptors(NotFoundInterceptor)
  @Patch('projects/:token')
  async updateKlugProject(@Param('token') token: string, @Body() update: UpdateKlugProjectDto) {
    return await this.klugService.updateProjectByToken(token, update);
  }

  @Post('projects')
  async generateUnofficialToken() {
    return await this.klugService.generateUnofficialProject();
  }

  @UseInterceptors(NotFoundInterceptor)
  @Get('projects/:token')
  async findKlugProjectByToken(@Param('token') token: string) {
    return await this.klugService.findProjectByToken(token);
  }

  @Get('projects/:token/pdf')
  async getLatestPDFExportForProject(@Res() response: Response, @Param('token') token: string) {
    const buffer = await this.klugService.getLatestPDFExportForProject(token);

    if (!buffer) {
      response.status(404).send();
      return;
    }

    response.setHeader('Content-Type', 'application/pdf');
    response.setHeader('Content-Disposition', `attachment;filename=Klug Ergebnis.pdf`);

    response.write(buffer);
    response.end();
  }

  @Post('projects/:token/finish')
  async finishProject(@Param('token') token: string, @Body() body: KlugFinishProjectDto) {
    return await this.klugService.finishProject(token, body);
  }
}
