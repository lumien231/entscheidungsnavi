import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EmailModule } from '../email/email.module';
import { KlugProject, KlugProjectSchema } from '../schemas/klug-project.schema';
import { KlugController } from './klug.controller';
import { KlugService } from './klug.service';
import { KlugManagerController } from './klug-manager.controller';

@Module({
  imports: [
    HttpModule,
    EmailModule,
    MongooseModule.forFeature([
      {
        name: KlugProject.name,
        schema: KlugProjectSchema,
      },
    ]),
  ],
  controllers: [KlugController, KlugManagerController],
  providers: [KlugService],
})
export class KlugModule {}
