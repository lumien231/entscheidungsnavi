import { KlugProjectSort, KlugProjectSortBy, KLUG_PROJECT_SORT_BY, SortDirection, SORT_DIRECTIONS } from '@entscheidungsnavi/api-types';
import { IsEnum, IsOptional } from 'class-validator';

export class KlugProjectSortDto implements KlugProjectSort {
  @IsOptional()
  @IsEnum(KLUG_PROJECT_SORT_BY)
  sortBy?: KlugProjectSortBy;

  @IsOptional()
  @IsEnum(SORT_DIRECTIONS)
  sortDirection?: SortDirection;
}
