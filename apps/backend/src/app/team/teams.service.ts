import { ConflictException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Language } from '@entscheidungsnavi/api-types';
import { v4 as uuidv4 } from 'uuid';
import { plainToClass, plainToInstance } from 'class-transformer';
import { Model, Types } from 'mongoose';
import { dataToText, readText, readTextWithVersion } from '@entscheidungsnavi/decision-data/export';
import { pick } from 'lodash';
import { EmailService } from '../email/email.service';
import { ProjectsService } from '../projects/projects.service';
import { Team, TeamDocument } from '../schemas/team/team.schema';
import { UpdateProjectDto } from '../projects/dto/update-project.dto';
import { UsersService } from '../users/users.service';
import { TeamCommentDto } from './dto/team-comment.dto';
import { TeamDto } from './dto/team.dto';
import { TeamInviteDto } from './dto/team-invite.dto';
import { UpdateTeamDto } from './dto/update-team.dto';
import { TeamInviteInfoDto } from './dto/team-invite-info.dto';
import { transferBetweenProjects } from './teams-transfer';

@Injectable()
export class TeamsService {
  constructor(
    @InjectModel(Team.name) private teamModel: Model<TeamDocument>,
    private projectService: ProjectsService,
    private usersService: UsersService,
    private emailService: EmailService
  ) {}

  async create(name: string, creatorUserId: Types.ObjectId, creatorProjectId: Types.ObjectId) {
    const project = await this.projectService.findById(creatorUserId, creatorProjectId);

    if (!project) {
      throw new NotFoundException('The provided project does not exist');
    }

    if (project.team) {
      throw new ConflictException('The provided project is already part of a team');
    }

    return this.toTeamDto(
      await this.teamModel
        .create({
          name,
          members: [
            {
              user: creatorUserId,
              project: creatorProjectId,
            },
          ],
        })
        .then(async teamDocument => {
          await this.projectService.setProjectTeam(creatorUserId, creatorProjectId, teamDocument._id);
          return teamDocument;
        })
    );
  }

  async findById(id: Types.ObjectId, accessorUserId: Types.ObjectId) {
    const { team } = await this.accessTeamUsingMember(id, accessorUserId);

    return this.toTeamDto(team);
  }

  async joinUsingToken(userId: Types.ObjectId, token: string) {
    const team = await this.findTeamUsingToken(token);
    const invite = team.pendingInvites.find(invite => invite.token === token);

    if (team.members.some(teamMember => teamMember.user.equals(userId))) {
      throw new ConflictException('Provided user is already part of the team');
    }

    const project = await this.join(team._id, userId);

    // Consume Token
    team.pendingInvites.remove(invite);
    await team.save();

    return project;
  }

  async join(teamId: Types.ObjectId, userId: Types.ObjectId) {
    const team = await this.teamModel.findById(teamId);

    if (!team) {
      throw new NotFoundException('The provided team does not exist');
    }

    if (team.members.some(teamMember => teamMember.user.equals(userId))) {
      throw new ConflictException('Provided user is already part of the team');
    }

    const ownerMember = team.members.id(team.owner);

    const mainProject = await this.projectService.findById(ownerMember.user, ownerMember.project);

    const clonedMainProject = await this.projectService.create(userId, pick(mainProject, ['name', 'data']));
    await this.projectService.setProjectTeam(userId, new Types.ObjectId(clonedMainProject.id), team._id);

    team.members.push({
      user: userId,
      project: new Types.ObjectId(clonedMainProject.id),
    });

    await team.save();

    return clonedMainProject;
  }

  async comment(
    teamId: Types.ObjectId,
    memberToCommentOnId: Types.ObjectId,
    accessorUserId: Types.ObjectId,
    objectId: string,
    content: string
  ) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, accessorUserId);

    const memberToCommentOn = team.members.find(member => member._id.equals(memberToCommentOnId));

    if (!memberToCommentOn) {
      throw new NotFoundException();
    }

    const commentId = new Types.ObjectId();

    memberToCommentOn.comments.push({
      _id: commentId,
      author: accessorMember._id,
      content,
      objectId,
    });

    for (const teamMember of team.members) {
      if (teamMember === accessorMember) {
        continue;
      }

      teamMember.unreadComments.push(commentId);
    }

    await team.save();

    return memberToCommentOn.comments
      .filter(comment => comment.objectId === objectId)
      .map(comment => plainToClass(TeamCommentDto, comment));
  }

  async getComments(teamId: Types.ObjectId, forMemberId: Types.ObjectId, accessorUserId: Types.ObjectId) {
    const { team } = await this.accessTeamUsingMember(teamId, accessorUserId);

    const member = team.members.find(member => member._id.equals(forMemberId));

    if (!member) {
      throw new NotFoundException();
    }

    return member.comments.map(comment => plainToClass(TeamCommentDto, comment));
  }

  async getCommentsForObject(teamId: Types.ObjectId, forMemberId: Types.ObjectId, objectId: string, accessorUserId: Types.ObjectId) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, accessorUserId);

    const member = team.members.find(member => member._id.equals(forMemberId));

    if (!member) {
      throw new NotFoundException();
    }

    const comments = member.comments.filter(comment => comment.objectId === objectId).map(comment => plainToClass(TeamCommentDto, comment));

    for (const comment of comments) {
      accessorMember.unreadComments.remove(comment.id);
    }

    await team.save();

    return comments;
  }

  async deleteComment(
    teamId: Types.ObjectId,
    memberToDeleteCommentFromId: Types.ObjectId,
    accessorUserId: Types.ObjectId,
    commentId: Types.ObjectId
  ) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, accessorUserId);

    const memberToDeleteCommentFrom = team.members.find(member => member._id.equals(memberToDeleteCommentFromId));

    if (!memberToDeleteCommentFrom) {
      throw new NotFoundException('member');
    }

    const commentIndex = memberToDeleteCommentFrom.comments.findIndex(comment => commentId.equals(comment._id));

    if (commentIndex < 0) {
      throw new NotFoundException('comment');
    }

    const comment = memberToDeleteCommentFrom.comments[commentIndex];

    if (!(comment.author.equals(accessorMember._id) || memberToDeleteCommentFromId.equals(accessorMember.id))) {
      throw new ForbiddenException();
    }

    memberToDeleteCommentFrom.comments.splice(commentIndex, 1);

    await team.save();
  }

  async invite(teamId: Types.ObjectId, accessorUserId: Types.ObjectId, email: string, lang: Language) {
    const { team, accessorMember } = await this.accessTeamUsingOwner(teamId, accessorUserId);

    const accessorUser = await this.usersService.findOne(accessorMember.user);

    const inviteToken = uuidv4();
    team.pendingInvites.push({
      email,
      token: inviteToken,
      inviter: accessorMember._id,
    });

    await team.save();
    this.emailService.sendTeamInviteMail(email, accessorUser.name ?? accessorUser.email, inviteToken, team.name, lang);

    return team.pendingInvites.map(invite => plainToInstance(TeamInviteDto, invite));
  }

  async getTeamProject(teamId: Types.ObjectId, forMemberId: Types.ObjectId, accessorUserId: Types.ObjectId) {
    const { team } = await this.accessTeamUsingMember(teamId, accessorUserId);

    const memberToAccess = team.members.find(member => member._id.equals(forMemberId));

    if (!memberToAccess) {
      throw new NotFoundException();
    }

    return await this.projectService.findById(memberToAccess.user, memberToAccess.project);
  }

  async getInviteInfoFromToken(token: string) {
    const team = await this.findTeamUsingToken(token);

    const invite = team.pendingInvites.find(invite => invite.token === token);

    if (!invite) {
      return null;
    }

    const inviterMember = team.members.find(member => member._id.equals(invite.inviter));
    if (!inviterMember) {
      return null;
    }

    const inviterUser = await this.usersService.findOne(inviterMember.user);

    return plainToClass(TeamInviteInfoDto, { teamName: team.name, inviterName: inviterUser.name ?? inviterUser.email });
  }

  async transferObject(teamId: Types.ObjectId, sourceMemberId: Types.ObjectId, accessorUserId: Types.ObjectId, objectId: string) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, accessorUserId);

    const sourceMember = team.members.find(member => member._id.equals(sourceMemberId));

    if (!sourceMember) {
      throw new NotFoundException();
    }

    const sourceProject = await this.projectService.findById(sourceMember.user, sourceMember.project);
    const destinationProject = await this.projectService.findById(accessorMember.user, accessorMember.project);

    const decisionDataSource = readText(sourceProject.data);
    const [decisionDataDestination, dstVersion] = readTextWithVersion(destinationProject.data);

    transferBetweenProjects(decisionDataSource, decisionDataDestination, objectId);

    await this.projectService.update(accessorMember.user, accessorMember.project, {
      data: dataToText(decisionDataDestination, dstVersion),
    });
  }

  async updateTeam(teamId: Types.ObjectId, update: UpdateTeamDto, accessorUserId: Types.ObjectId) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, accessorUserId);

    const isAccessorOwner = accessorMember._id.equals(team.owner);
    const isAccessorEditor = accessorMember._id.equals(team.editor);

    if (update.name != null && !isAccessorOwner) {
      throw new ForbiddenException();
    }

    if (update.whiteboard != null && !isAccessorEditor) {
      throw new ForbiddenException(team.editor);
    }

    if (update.editor != null && !isAccessorOwner && !(isAccessorEditor && team.owner.equals(update.editor))) {
      throw new ForbiddenException();
    }

    if (update.editor && !team.members.some(member => member._id.equals(update.editor))) {
      throw new ConflictException(this.toTeamDto(team));
    }

    const updatedTeam = await this.teamModel.findByIdAndUpdate(team, { $set: update }, { new: true }).exec();

    return this.toTeamDto(updatedTeam);
  }

  async updateMainProject(teamId: Types.ObjectId, update: UpdateProjectDto, accessorUserId: Types.ObjectId) {
    const { team } = await this.accessTeamUsingEditor(teamId, accessorUserId);

    const ownerMember = team.members.id(team.owner);

    return await this.projectService.update(ownerMember.user, ownerMember.project, update);
  }

  private async accessTeamUsingMember(teamId: Types.ObjectId, memberUserId: Types.ObjectId) {
    const team = await this.teamModel.findById(teamId);

    if (!team) {
      throw new NotFoundException();
    }

    const accessorMember = team.members.find(member => member.user.equals(memberUserId));
    if (!accessorMember) {
      throw new ForbiddenException();
    }

    return { team, accessorMember };
  }

  private async accessTeamUsingEditor(teamId: Types.ObjectId, editorUserId: Types.ObjectId) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, editorUserId);

    if (!accessorMember._id.equals(team.editor)) {
      throw new ForbiddenException(team.editor.toString());
    }

    return { team, accessorMember };
  }

  private async accessTeamUsingOwner(teamId: Types.ObjectId, ownerUserId: Types.ObjectId) {
    const { team, accessorMember } = await this.accessTeamUsingMember(teamId, ownerUserId);

    if (!accessorMember._id.equals(team.owner)) {
      throw new ForbiddenException();
    }

    return { team, accessorMember };
  }

  private async findTeamUsingToken(token: string) {
    const team = await this.teamModel.findOne({
      pendingInvites: { $elemMatch: { token } },
    });

    if (!team) {
      throw new NotFoundException();
    }

    return team;
  }

  private async toTeamDto(doc: TeamDocument) {
    return plainToClass(TeamDto, await doc.populate({ path: 'members.user', select: ['id', 'name', 'email'] }));
  }
}
