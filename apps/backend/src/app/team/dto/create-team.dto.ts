import { Type } from 'class-transformer';
import { IsNotEmpty, IsMongoId, IsString } from 'class-validator';
import { Types } from 'mongoose';

export class CreateTeamDto {
  @IsNotEmpty()
  @IsMongoId()
  @Type(() => Types.ObjectId)
  projectId: Types.ObjectId;

  @IsNotEmpty()
  @IsString()
  teamName: string;
}
