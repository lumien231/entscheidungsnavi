import { IsUUID } from 'class-validator';

export class TeamTransferDto {
  @IsUUID()
  objectId: string;
}
