import { Exclude, Expose, Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';
import { TeamMember as ApiTeamMember } from '@entscheidungsnavi/api-types';
import { TeamCommentDto } from './team-comment.dto';
import { PartialUserInfoDto } from './partial-user-info.dto';

@Exclude()
export class TeamMemberDto implements ApiTeamMember {
  @Expose()
  @Type(() => String)
  id: string;

  @IsNotEmpty()
  @Expose()
  @Type(() => PartialUserInfoDto)
  @ValidateNested()
  user: PartialUserInfoDto;

  @IsNotEmpty()
  @Expose()
  @Type(() => String)
  project: string;

  @Expose()
  @Type(() => TeamCommentDto)
  @ValidateNested({ each: true })
  comments: TeamCommentDto[];

  @Expose()
  @Type(() => String)
  unreadComments: string[];
}
