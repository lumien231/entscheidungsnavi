import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  Param,
  ParseBoolPipe,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Types } from 'mongoose';
import { isMongoId } from 'class-validator';
import { Throttle } from '@nestjs/throttler';
import { Request } from 'express';
import { RolesGuard } from '../auth/roles.guard';
import { NotFoundInterceptor } from '../common/not-found.interceptor';
import { ParseMongoIdPipe } from '../common/parse-mogo-id.pipe';
import { UniqueConstraintViolationInterceptor } from '../common/unique-constraint-violation.interceptor';
import { CreateQuickstartProjectDto } from './dto/create-quickstart-project.dto';
import { CreateQuickstartTagDto } from './dto/create-quickstart-tag.dto';
import { UpdateQuickstartProjectDto } from './dto/update-quickstart-project.dto';
import { UpdateQuickstartTagDto } from './dto/update-quickstart-tag.dto';
import { QuickstartProjectsService } from './quickstart-projects.service';
import { QuickstartTagsService } from './quickstart-tags.service';
import { QuickstartValuesService } from './quickstart-values.service';
import { CreateQuickstartValueDto, UpdateQuickstartValueDto } from './dto/quickstart-value.dto';
import { QuickstartValueTrackingDto } from './dto/quickstart-value-tracking.dto';

@Controller('quickstart')
export class QuickstartController {
  constructor(
    private quickstartProjectsService: QuickstartProjectsService,
    private quickstartTagsService: QuickstartTagsService,
    private quickstartValuesService: QuickstartValuesService
  ) {}

  @Get('projects')
  async listProjects(@Query('showInvisible', ParseBoolPipe) showInvisible: boolean) {
    return showInvisible ? await this.quickstartProjectsService.listAll() : await this.quickstartProjectsService.listVisible();
  }

  @UseGuards(RolesGuard('admin'))
  @UseInterceptors(UniqueConstraintViolationInterceptor('A project with this link token already exists'))
  @Post('projects')
  async createProject(@Body() project: CreateQuickstartProjectDto) {
    return await this.quickstartProjectsService.create(project);
  }

  @UseInterceptors(NotFoundInterceptor)
  @Get('projects/:handle')
  async findProject(@Param('handle') handle: string) {
    // We allow unauthorized users to find insivible quickstart projects on purpose. Visibility
    // is not a permission, just defines whether the project is shown in the list.
    if (isMongoId(handle)) {
      const id = new Types.ObjectId(handle);
      return await this.quickstartProjectsService.findById(id);
    } else {
      return await this.quickstartProjectsService.findByShareToken(handle);
    }
  }

  @UseGuards(RolesGuard('admin'))
  @UseInterceptors(NotFoundInterceptor)
  @UseInterceptors(UniqueConstraintViolationInterceptor('A project with this link token already exists'))
  @Patch('projects/:id')
  async updateProject(@Param('id', ParseMongoIdPipe) id: Types.ObjectId, @Body() project: UpdateQuickstartProjectDto) {
    return await this.quickstartProjectsService.update(id, project);
  }

  @UseGuards(RolesGuard('admin'))
  @Delete('projects/:id')
  async deleteProject(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    await this.quickstartProjectsService.remove(id);
  }

  @Get('tags')
  async listTags() {
    return await this.quickstartTagsService.list();
  }

  @UseGuards(RolesGuard('admin'))
  @Post('tags')
  async createTag(@Body() tag: CreateQuickstartTagDto) {
    return await this.quickstartTagsService.create(tag);
  }

  @UseInterceptors(NotFoundInterceptor)
  @Get('tags/:id')
  async findTagById(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    return await this.quickstartTagsService.findById(id);
  }

  @UseInterceptors(NotFoundInterceptor)
  @UseGuards(RolesGuard('admin'))
  @Patch('tags/:id')
  async updateTag(@Param('id', ParseMongoIdPipe) id: Types.ObjectId, @Body() tag: UpdateQuickstartTagDto) {
    return await this.quickstartTagsService.update(id, tag);
  }

  @UseGuards(RolesGuard('admin'))
  @Delete('tags/:id')
  async deleteTag(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    await this.quickstartTagsService.remove(id);
  }

  @Get('values')
  async listValues(@Req() req: Request, @Query('includeStats', ParseBoolPipe) includeStats: boolean) {
    if (includeStats && (!req.isAuthenticated() || !req.user.roles.includes('admin'))) {
      throw new ForbiddenException('admin role required to read stats');
    }

    return await this.quickstartValuesService.list(includeStats);
  }

  @Throttle(1, 60) // Allow at most one request per minute
  @Post('values/metrics')
  async trackValueUsage(@Body() metrics: QuickstartValueTrackingDto) {
    await this.quickstartValuesService.collectMetrics(metrics);
  }

  @UseInterceptors(NotFoundInterceptor)
  @Get('values/:id')
  async findValueById(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    return await this.quickstartValuesService.findById(id);
  }

  @UseGuards(RolesGuard('admin'))
  @Post('values')
  async createValue(@Body() value: CreateQuickstartValueDto) {
    return await this.quickstartValuesService.create(value);
  }

  @UseInterceptors(NotFoundInterceptor)
  @UseGuards(RolesGuard('admin'))
  @Patch('values/:id')
  async updateValue(@Param('id', ParseMongoIdPipe) id: Types.ObjectId, @Body() value: UpdateQuickstartValueDto) {
    return await this.quickstartValuesService.update(id, value);
  }

  @UseGuards(RolesGuard('admin'))
  @Delete('values/:id')
  async deleteValue(@Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    await this.quickstartValuesService.remove(id);
  }
}
