import { QuickstartValue } from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsDefined, ValidateNested } from 'class-validator';
import { PartialType, PickType } from '@nestjs/mapped-types';
import { LocalizedStringDto } from '../../common/localized-string.dto';

@Exclude()
export class QuickstartValueDto implements QuickstartValue {
  @Expose()
  @Type(() => String)
  id: string;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  @Expose()
  name: LocalizedStringDto;

  @Expose()
  createdAt: Date;

  @Expose()
  updatedAt: Date;
}

@Exclude()
export class QuickstartValueWithStatsDto extends QuickstartValueDto implements QuickstartValueWithStatsDto {
  @Expose()
  countInTop5: number;

  @Expose()
  countAssigned: number;

  @Expose()
  countTracked: number;

  @Expose()
  accumulatedScore: number;
}

export class CreateQuickstartValueDto extends PickType(QuickstartValueDto, ['name']) {}

export class UpdateQuickstartValueDto extends PartialType(CreateQuickstartValueDto) {}
