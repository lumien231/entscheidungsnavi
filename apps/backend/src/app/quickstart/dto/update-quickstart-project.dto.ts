import { PartialType } from '@nestjs/mapped-types';
import { CreateQuickstartProjectDto } from './create-quickstart-project.dto';

export class UpdateQuickstartProjectDto extends PartialType(CreateQuickstartProjectDto) {}
