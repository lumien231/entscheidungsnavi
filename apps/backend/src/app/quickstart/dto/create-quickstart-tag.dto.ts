import { PickType } from '@nestjs/mapped-types';
import { QuickstartTagDto } from './quickstart-tag.dto';

export class CreateQuickstartTagDto extends PickType(QuickstartTagDto, ['name', 'weight']) {}
