import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Project, ProjectSchema } from '../schemas/project.schema';
import { TeamsModule } from '../team/teams.module';
import { ProjectHistoryEntry, ProjectHistoryEntrySchema } from '../schemas/project-history.schema';
import { ProjectsController } from './projects.controller';
import { ProjectsService } from './projects.service';
import { SharedController } from './shared.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Project.name, schema: ProjectSchema },
      { name: ProjectHistoryEntry.name, schema: ProjectHistoryEntrySchema },
    ]),
    forwardRef(() => TeamsModule),
  ],
  controllers: [ProjectsController, SharedController],
  providers: [ProjectsService],
  exports: [ProjectsService],
})
export class ProjectsModule {}
