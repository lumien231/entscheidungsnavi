import { ProjectTeamInfo } from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';

@Exclude()
export class ProjectTeamInfoDto implements ProjectTeamInfo {
  @Expose()
  @Type(() => String)
  id: string;

  @Expose()
  @IsString()
  @IsNotEmpty()
  name: string;
}
