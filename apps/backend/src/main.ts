import helmet from 'helmet';
import { Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { json, text } from 'body-parser';
import * as Sentry from '@sentry/node';
import { pick } from 'lodash';
import { AppModule, EnvironmentType } from './app/app.module';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { version } = require('../../../package.json');

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app
    // Set base path of the api
    .setGlobalPrefix('/api')
    // IPs are extracted from the X-Forwarded-For header. Needed for rate limiting.
    .enable('trust proxy')
    // Increase max payload limit
    .use(json({ limit: '10mb' }), text({ limit: '10mb' }))
    // Add security related HTTP Headers
    .use(helmet());

  const configService = app.get(ConfigService);

  const sentryDsn = configService.get<string>('SENTRY_DSN');
  const environmentType = configService.get<EnvironmentType>('ENVIRONMENT_TYPE');
  Sentry.init({
    enabled: environmentType === 'production' || environmentType === 'nightly',
    dsn: sentryDsn,
    release: version,
    environment: environmentType,
    beforeSend: event => {
      // Sanitize the event before sending it to Sentry. nest-raven dumps the whole express req
      // object into event.extra.req by default. We only want to keep parts of it.
      const req = event.extra?.['req'];

      if (typeof req === 'object') {
        // Make sure we never send passwords to sentry (req.data is the unparsed request string)
        if (typeof req['data'] === 'string' && req['data'].toLowerCase().includes('password')) {
          delete req['data'];
        }

        // Whitelisted parameters that we actually want to log
        event.extra['req'] = pick(req, ['method', 'data', 'query_string', 'url']);
      }

      return event;
    },
  });

  const port = configService.get<number>('PORT');
  await app.listen(port, () => {
    Logger.log('Listening at http://localhost:' + port + '/api');
  });
}

bootstrap();
