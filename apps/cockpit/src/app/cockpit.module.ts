import { InjectionToken, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ApiClientModule } from '@entscheidungsnavi/api-client';
import { HoverPopOverDirective, WidgetsModule, WidthTriggerDirective } from '@entscheidungsnavi/widgets';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MAT_LEGACY_FORM_FIELD_DEFAULT_OPTIONS as MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/legacy-form-field';
import { MAT_LEGACY_RADIO_DEFAULT_OPTIONS as MAT_RADIO_DEFAULT_OPTIONS } from '@angular/material/legacy-radio';
import { MAT_LEGACY_CHECKBOX_DEFAULT_OPTIONS as MAT_CHECKBOX_DEFAULT_OPTIONS } from '@angular/material/legacy-checkbox';
import { MAT_LEGACY_SLIDE_TOGGLE_DEFAULT_OPTIONS as MAT_SLIDE_TOGGLE_DEFAULT_OPTIONS } from '@angular/material/legacy-slide-toggle';
import { MatLegacyNativeDateModule } from '@angular/material/legacy-core';
import { OverlayProgressBarDirective } from '@entscheidungsnavi/widgets/overlay-progress-bar';
import { ENVIRONMENT } from '../environments/environment';
import { CockpitComponent } from './cockpit.component';
import { CockpitRoutingModule } from './cockpit.routing';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavigationLayoutComponent } from './navigation-layout/navigation-layout.component';
import { QuickstartProjectsComponent } from './quickstart-projects/quickstart-projects.component';
import { QuickstartTagsComponent } from './quickstart-tags/quickstart-tags.component';
import { QuickstartTagDetailsModalComponent } from './quickstart-tags/details-modal/quickstart-tag-details-modal.component';
import { QuickstartProjectDetailsModalComponent } from './quickstart-projects/details-modal/quickstart-project-details-modal.component';
import { EventOverviewComponent } from './event-overview/event-overview.component';
import { EventExportComponent } from './event-export/event-export.component';
import { EventExportContentComponent } from './event-export/event-export-content/event-export-content.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { EventQuestionnaireEditorComponent } from './event-details/event-questionnaire-editor/event-questionnaire-editor.component';
import { EventStatusComponent } from './event-details/event-status/event-status.component';
import { EventConfigurationComponent } from './event-details/event-configuration/event-configuration.component';
import { CreateEventComponent } from './event-details/create-event/create-event.component';
import { EventQuestionnairePreviewComponent } from './event-details/event-questionnaire-preview/event-questionnaire-preview.component';
import { EventQuestionnaireReorderComponent } from './event-details/event-questionnaire-reorder/event-questionnaire-reorder.component';
import { EventQuestionnaireEntryComponent } from './event-details/event-questionnaire-editor/entry/event-questionnaire-entry.component';
import { QuestionnaireEntryTypePipe } from './event-details/questionnaire-entry-type.pipe';
import { EventDeleteModalComponent } from './event-details/event-status/delete-modal/event-delete-modal.component';
import { EventTransferOwnerModalComponent } from './event-details/event-status/transfer-owner-modal/event-transfer-owner-modal.component';
import { KlugComponent } from './klug/klug.component';
import { KlugProjectModalComponent } from './klug/klug-project-modal/klug-project-modal.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { UserDetailsModalComponent } from './user-management/user-details-modal/user-details-modal.component';
import { UserProjectsComponent } from './user-management/user-projects/user-projects.component';
import { UserPropertiesComponent } from './user-management/user-properties/user-properties.component';
import { TableLayoutComponent } from './table-layout/table-layout.component';
import { TableFooterComponent } from './table-layout/table-footer/table-footer.component';
import { DataLoadDirective } from './data-load/data-load.directive';
import { DataLoadErrorComponent } from './data-load/data-load-error.component';
import { QuickstartValuesComponent } from './quickstart-values/quickstart-values.component';
import { QuickstartValueDetailsModalComponent } from './quickstart-values/details-modal/quickstart-value-details-modal.component';

export const DECISION_TOOL_ORIGIN = new InjectionToken<string>('Origin of the decision tool.');

@NgModule({
  declarations: [
    CockpitComponent,
    LoginComponent,
    NavbarComponent,
    DashboardComponent,
    UserManagementComponent,
    NavigationLayoutComponent,
    UserDetailsModalComponent,
    UserPropertiesComponent,
    UserProjectsComponent,
    QuickstartProjectsComponent,
    QuickstartTagsComponent,
    QuickstartTagDetailsModalComponent,
    QuickstartProjectDetailsModalComponent,
    EventOverviewComponent,
    EventExportComponent,
    EventExportContentComponent,
    EventDetailsComponent,
    EventQuestionnaireEditorComponent,
    EventStatusComponent,
    EventConfigurationComponent,
    CreateEventComponent,
    EventQuestionnairePreviewComponent,
    EventQuestionnaireReorderComponent,
    EventQuestionnaireEntryComponent,
    QuestionnaireEntryTypePipe,
    EventDeleteModalComponent,
    EventTransferOwnerModalComponent,
    KlugComponent,
    KlugProjectModalComponent,
    TableLayoutComponent,
    TableFooterComponent,
    DataLoadDirective,
    DataLoadErrorComponent,
    QuickstartValuesComponent,
    QuickstartValueDetailsModalComponent,
  ],
  imports: [
    BrowserModule,
    CockpitRoutingModule,
    WidgetsModule,
    ApiClientModule,
    FormsModule,
    BrowserAnimationsModule,
    HoverPopOverDirective,
    WidthTriggerDirective,
    MatLegacyNativeDateModule,
    OverlayProgressBarDirective,
  ],
  providers: [
    {
      provide: DECISION_TOOL_ORIGIN,
      useValue: ENVIRONMENT.decisionToolOrigin,
    },
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: 'fill' },
    },
    {
      provide: MAT_RADIO_DEFAULT_OPTIONS,
      useValue: { color: 'primary' },
    },
    {
      provide: MAT_CHECKBOX_DEFAULT_OPTIONS,
      useValue: { color: 'primary' },
    },
    {
      provide: MAT_SLIDE_TOGGLE_DEFAULT_OPTIONS,
      useValue: { color: 'primary' },
    },
  ],
  bootstrap: [CockpitComponent],
})
export class CockpitModule {}
