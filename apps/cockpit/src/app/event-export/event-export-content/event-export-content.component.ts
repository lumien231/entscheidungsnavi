import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { EventManagementService } from '@entscheidungsnavi/api-client';
import {
  EventDefaultExportAttribute,
  EVENT_DEFAULT_EXPORT_ATTRIBUTES,
  EVENT_ALT_AND_OBJ_EXPORT_ALTERNATIVES_ATTRIBUTES,
  EVENT_ALT_AND_OBJ_EXPORT_OBJECTIVES_ATTRIBUTES,
} from '@entscheidungsnavi/api-types';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { createControlDependency } from '@entscheidungsnavi/widgets';
import { Observable, finalize } from 'rxjs';

@Component({
  selector: 'dt-event-export-excel',
  templateUrl: './event-export-content.component.html',
  styleUrls: ['./event-export-content.component.scss'],
})
export class EventExportContentComponent {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @Input() selectedEvents: string[];

  exportType: 'standard' | 'alternatives-and-objectives' | 'project-files' = 'standard';

  private attributeFormControls: Record<EventDefaultExportAttribute, FormControl<boolean>> = {
    projectInformation: new FormControl(true, { nonNullable: true }),
    modelParameterCounts: new FormControl(true, { nonNullable: true }),
    processStatus: new FormControl(false, { nonNullable: true }),

    stepExplanations: new FormControl(false, { nonNullable: true }),
    objectiveExplanations: new FormControl(false, { nonNullable: true }),
    alternativeExplanations: new FormControl(false, { nonNullable: true }),
    influenceFactorExplanations: new FormControl(false, { nonNullable: true }),

    decisionQuality: new FormControl(false, { nonNullable: true }),
    timeRecording: new FormControl(false, { nonNullable: true }),

    submissionQuestionnaire: new FormControl(false, { nonNullable: true }),
    submissionFreeText: new FormControl(false, { nonNullable: true }),
  };

  protected parametersFormStandard = new FormGroup({
    includeObjectives: new FormControl(true, { nonNullable: true }),
    limitObjectiveCount: new FormControl(false, { nonNullable: true }),
    objectiveCountLimit: new FormControl<number>(10, { validators: [Validators.required, Validators.min(1)], nonNullable: true }),

    includeAlternatives: new FormControl(true, { nonNullable: true }),
    limitAlternativeCount: new FormControl(false, { nonNullable: true }),
    alternativeCountLimit: new FormControl<number>(10, { validators: [Validators.required, Validators.min(1)], nonNullable: true }),

    includeInfluenceFactors: new FormControl(false, { nonNullable: true }),
    limitInfluenceFactorCount: new FormControl(false, { nonNullable: true }),
    influenceFactorCountLimit: new FormControl<number>(10, { validators: [Validators.required, Validators.min(1)], nonNullable: true }),

    ...this.attributeFormControls,
  });

  protected parametersFormAltAndObj = this.fb.group({
    alternativesSheet: [true],
    defaultAnalysis: [true],
    linearNumericalAnalysis: [true],
    linearVerbalAnalysis: [true],
    linearBothAnalysis: [true],
    linearMostImportantAnalyses: [true],
    linearMostImportantAccumulatedAnalyses: [true],
    linearLeastImportantAnalyses: [true],
    linearLeastImportantAccumulatedAnalyses: [true],

    objectivesSheet: [true],
    scaleInfo: [true],
    weightInfo: [true],
    utilityInfo: [true],
  });

  protected formGroupStandard = new FormGroup({
    parameters: this.parametersFormStandard,
  });

  protected formGroupAltAndObj = new FormGroup({
    parameters: this.parametersFormAltAndObj,
  });

  protected isLoading = false;

  constructor(private eventService: EventManagementService, private snackBar: MatSnackBar, protected fb: NonNullableFormBuilder) {
    // Disable sub formcontrols when objectives, alternatives, influence factors are disabled
    const ps = this.parametersFormStandard.controls;
    createControlDependency(ps.includeObjectives, ps.objectiveExplanations, this.onDestroy$);
    createControlDependency(ps.includeObjectives, ps.limitObjectiveCount, this.onDestroy$);
    createControlDependency(ps.limitObjectiveCount, ps.objectiveCountLimit, this.onDestroy$);

    createControlDependency(ps.includeAlternatives, ps.alternativeExplanations, this.onDestroy$);
    createControlDependency(ps.includeAlternatives, ps.limitAlternativeCount, this.onDestroy$);
    createControlDependency(ps.limitAlternativeCount, ps.alternativeCountLimit, this.onDestroy$);

    createControlDependency(ps.includeInfluenceFactors, ps.influenceFactorExplanations, this.onDestroy$);
    createControlDependency(ps.includeInfluenceFactors, ps.limitInfluenceFactorCount, this.onDestroy$);
    createControlDependency(ps.limitInfluenceFactorCount, ps.influenceFactorCountLimit, this.onDestroy$);

    const pi = this.parametersFormAltAndObj.controls;
    createControlDependency(pi.alternativesSheet, pi.defaultAnalysis, this.onDestroy$);
    createControlDependency(pi.alternativesSheet, pi.linearNumericalAnalysis, this.onDestroy$);
    createControlDependency(pi.alternativesSheet, pi.linearVerbalAnalysis, this.onDestroy$);
    createControlDependency(pi.alternativesSheet, pi.linearBothAnalysis, this.onDestroy$);
    createControlDependency(pi.alternativesSheet, pi.linearMostImportantAnalyses, this.onDestroy$);
    createControlDependency(pi.alternativesSheet, pi.linearMostImportantAccumulatedAnalyses, this.onDestroy$);
    createControlDependency(pi.alternativesSheet, pi.linearLeastImportantAnalyses, this.onDestroy$);
    createControlDependency(pi.alternativesSheet, pi.linearLeastImportantAccumulatedAnalyses, this.onDestroy$);

    createControlDependency(pi.objectivesSheet, pi.scaleInfo, this.onDestroy$);
    createControlDependency(pi.objectivesSheet, pi.weightInfo, this.onDestroy$);
    createControlDependency(pi.objectivesSheet, pi.utilityInfo, this.onDestroy$);
  }

  createExportStandard() {
    if (this.formGroupStandard.invalid || this.isLoading) return;

    const val = this.parametersFormStandard.value;
    const enabledAttributes = EVENT_DEFAULT_EXPORT_ATTRIBUTES.filter(attribute => val[attribute]);
    const objectiveLimit = !val.includeObjectives ? 0 : !val.limitObjectiveCount ? undefined : val.objectiveCountLimit;
    const alternativeLimit = !val.includeAlternatives ? 0 : !val.limitAlternativeCount ? undefined : val.alternativeCountLimit;
    const influenceFactorLimit = !val.includeInfluenceFactors
      ? 0
      : !val.limitInfluenceFactorCount
      ? undefined
      : val.influenceFactorCountLimit;
    this.isLoading = true;
    this.eventService
      .generateDefaultExport({
        eventIds: this.selectedEvents,
        attributes: enabledAttributes,
        objectiveLimit,
        alternativeLimit,
        influenceFactorLimit,
      })
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe({
        next: response => {
          const dataType = response.type;
          const downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(new Blob([response], { type: dataType }));
          downloadLink.setAttribute('download', `event-export-${Date.now()}.xlsx`);
          document.body.appendChild(downloadLink);
          downloadLink.click();
        },
        error: () => this.snackBar.open($localize`Fehler beim Export`, 'Ok'),
      });
  }

  createExportAltAndObj() {
    if (!this.areAltAndObjParametersValid()) return;
    const val = this.parametersFormAltAndObj.value;
    const alternativeAttributes = EVENT_ALT_AND_OBJ_EXPORT_ALTERNATIVES_ATTRIBUTES.filter(attribute => val[attribute]);
    const objectiveAttributes = EVENT_ALT_AND_OBJ_EXPORT_OBJECTIVES_ATTRIBUTES.filter(attribute => val[attribute]);
    this.isLoading = true;
    this.eventService
      .generateAltAndObjExport({ eventIds: this.selectedEvents, alternativeAttributes, objectiveAttributes })
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe({
        next: response => {
          const dataType = response.type;
          const downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(new Blob([response], { type: dataType }));
          downloadLink.setAttribute('download', `event-export-${Date.now()}.xlsx`);
          document.body.appendChild(downloadLink);
          downloadLink.click();
        },
        error: () => this.snackBar.open($localize`Fehler beim Export`, 'Ok'),
      });
  }

  createExportProjectFiles() {
    if (this.isLoading) return;

    this.isLoading = true;
    this.eventService
      .generateProjectsExport({ eventIds: this.selectedEvents })
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe({
        next: response => {
          const dataType = response.type;
          const downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(new Blob([response], { type: dataType }));
          downloadLink.setAttribute('download', `event-export-${Date.now()}.zip`);
          document.body.appendChild(downloadLink);
          downloadLink.click();
        },
        error: () => this.snackBar.open($localize`Fehler beim Export`, 'Ok'),
      });
  }

  selectFullExport() {
    if (this.exportType === 'standard') {
      const attributeValues = Object.fromEntries(EVENT_DEFAULT_EXPORT_ATTRIBUTES.map(attribute => [attribute, true])) as Record<
        EventDefaultExportAttribute,
        boolean
      >;

      this.parametersFormStandard.patchValue({
        includeObjectives: true,
        limitObjectiveCount: false,
        includeAlternatives: true,
        limitAlternativeCount: false,
        includeInfluenceFactors: true,
        limitInfluenceFactorCount: false,
        ...attributeValues,
      });
    } else if (this.exportType === 'alternatives-and-objectives') {
      this.parametersFormAltAndObj.patchValue({
        alternativesSheet: true,
        defaultAnalysis: true,
        linearNumericalAnalysis: true,
        linearVerbalAnalysis: true,
        linearBothAnalysis: true,
        linearMostImportantAnalyses: true,
        linearMostImportantAccumulatedAnalyses: true,
        linearLeastImportantAnalyses: true,
        linearLeastImportantAccumulatedAnalyses: true,
        objectivesSheet: true,
        scaleInfo: true,
        weightInfo: true,
        utilityInfo: true,
      });
    }
  }

  selectDefaultExport(parametersForm: FormGroup) {
    parametersForm.reset();
  }

  areAltAndObjParametersValid() {
    return this.formGroupAltAndObj.value.parameters.alternativesSheet || this.formGroupAltAndObj.value.parameters.objectivesSheet;
  }

  selectMinimalExport() {
    if (this.exportType === 'standard') {
      const attributeValues = Object.fromEntries(EVENT_DEFAULT_EXPORT_ATTRIBUTES.map(attribute => [attribute, false])) as Record<
        EventDefaultExportAttribute,
        boolean
      >;

      this.parametersFormStandard.patchValue({
        includeObjectives: false,
        includeAlternatives: false,
        includeInfluenceFactors: false,
        ...attributeValues,
      });
    } else if (this.exportType === 'alternatives-and-objectives') {
      this.parametersFormAltAndObj.patchValue({
        alternativesSheet: true,
        defaultAnalysis: false,
        linearNumericalAnalysis: false,
        linearVerbalAnalysis: false,
        linearBothAnalysis: false,
        linearMostImportantAnalyses: false,
        linearMostImportantAccumulatedAnalyses: false,
        linearLeastImportantAnalyses: false,
        linearLeastImportantAccumulatedAnalyses: false,
        objectivesSheet: true,
        scaleInfo: false,
        weightInfo: false,
        utilityInfo: false,
      });
    }
  }
}
