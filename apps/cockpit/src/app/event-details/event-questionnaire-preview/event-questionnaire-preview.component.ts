import { Component, Inject } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';
import { QuestionnairePage } from '@entscheidungsnavi/api-types';

@Component({
  templateUrl: './event-questionnaire-preview.component.html',
  styleUrls: ['./event-questionnaire-preview.component.scss'],
})
export class EventQuestionnairePreviewComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) protected questionnaire: QuestionnairePage[],
    protected dialogRef: MatDialogRef<EventQuestionnairePreviewComponent>
  ) {}
}
