import { Component, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatLegacyDialogRef, MAT_LEGACY_DIALOG_DATA } from '@angular/material/legacy-dialog';
import { MatLegacySnackBar } from '@angular/material/legacy-snack-bar';
import { Router } from '@angular/router';
import { EventManagementService } from '@entscheidungsnavi/api-client';
import { EventWithStats } from '@entscheidungsnavi/api-types';
import { finalize } from 'rxjs';

@Component({
  templateUrl: './event-delete-modal.component.html',
  styleUrls: ['./event-delete-modal.component.scss'],
})
export class EventDeleteModalComponent {
  eventNameForm = new FormControl('', { validators: control => (control.value === this.event.name ? null : { invalid: true }) });

  constructor(
    @Inject(MAT_LEGACY_DIALOG_DATA) protected event: EventWithStats,
    private dialogRef: MatLegacyDialogRef<EventDeleteModalComponent>,
    private snackBar: MatLegacySnackBar,
    private eventManagementService: EventManagementService,
    private router: Router
  ) {}

  delete() {
    if (this.eventNameForm.invalid || this.eventNameForm.disabled) return;

    this.eventNameForm.disable({ emitEvent: false });
    this.eventManagementService
      .deleteEvent(this.event.id)
      .pipe(finalize(() => this.eventNameForm.enable()))
      .subscribe({
        next: () => {
          this.snackBar.open($localize`Veranstaltung erfolgreich gelöscht`, 'Ok', { duration: 7000 });
          this.eventManagementService.refreshEvents();
          this.router.navigate(['/events']);
          this.dialogRef.close();
        },
        error: () => this.snackBar.open($localize`Fehler beim Löschen der Veranstaltung`, 'Ok'),
      });
  }

  cancel() {
    if (this.eventNameForm.enabled) this.dialogRef.close();
  }
}
