import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatLegacySnackBar } from '@angular/material/legacy-snack-bar';
import { ActivatedRoute } from '@angular/router';
import { EventDto, EventManagementService } from '@entscheidungsnavi/api-client';
import { Observable, map, switchMap, takeUntil } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { EventConfigurationFormService } from './event-configuration/event-configuration-form.service';

@Component({
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss'],
  providers: [EventConfigurationFormService],
})
export class EventDetailsComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  event: EventDto;

  error: 'network' | 'permissions';
  isDeleting = false;
  questionnaireDirty = false;

  get isDirty() {
    return (this.eventConfiguration.form.dirty || this.questionnaireDirty) && !this.isDeleting;
  }

  constructor(
    private route: ActivatedRoute,
    private eventsService: EventManagementService,
    protected eventConfiguration: EventConfigurationFormService,
    private snackBar: MatLegacySnackBar
  ) {}

  ngOnInit() {
    this.route.params
      .pipe(
        switchMap(params => this.eventsService.events$.pipe(map(events => events.list.find(event => event.id === params['eventId'])))),
        takeUntil(this.onDestroy$)
      )
      .subscribe({
        next: event => {
          if (event == null) {
            this.error = 'permissions';
            return;
          }

          this.event = event;
          this.resetConfiguration();
        },
        error: () => {
          this.error = 'network';
        },
      });
  }

  saveConfiguration() {
    this.eventConfiguration.form.disable({ emitEvent: false });
    this.eventsService.updateEvent(this.event.id, this.eventConfiguration.getEventData()).subscribe({
      next: event => {
        this.eventConfiguration.form.enable();
        Object.assign(this.event, event);
        this.resetConfiguration();
      },
      error: error => {
        this.eventConfiguration.form.enable();
        if (error instanceof HttpErrorResponse && error.status === 409) {
          this.eventConfiguration.form.controls.code.setErrors({ codeConflict: true });
          this.snackBar.open($localize`Die Eingabe enthält Fehler`, 'Ok');
        } else {
          this.snackBar.open($localize`Fehler beim Speichern der Veranstaltung`, 'Ok');
        }

        console.error(error);
      },
    });
  }

  resetConfiguration() {
    this.eventConfiguration.loadFromEvent(this.event);
  }
}
