version: '3'

services:
  decision-tool:
    image: $CI_REGISTRY_IMAGE/decision-tool:prod
    restart: unless-stopped
    environment:
      - SENTRY_DSN=$SENTRY_DT_DSN
      - COCKPIT_ORIGIN=https://cockpit.enavi.app
    networks:
      - proxy
    labels:
      traefik.enable: true
      traefik.http.routers.prod-decision-tool.rule: Host(`enavi.app`)
      # Special router that redirects alternative domains
      traefik.http.routers.prod-domain-redirect.rule: Host(`tool.entscheidungsnavi.de`, `tool.entscheidungsnavi.com`, `www.enavi.app`, `www.entscheidungsnavi.app`, `entscheidungsnavi.app`)
      traefik.http.middlewares.prod-redirect-domains.redirectregex.regex: ^https://[\w\.]+/(.*)$$
      traefik.http.middlewares.prod-redirect-domains.redirectregex.replacement: https://enavi.app/$${1}
      traefik.http.routers.prod-domain-redirect.middlewares: prod-redirect-domains
    volumes:
      - /srv/entscheidungsnavi/matomo.js:/usr/share/nginx/html/mounts/matomo.js

  cockpit:
    image: $CI_REGISTRY_IMAGE/cockpit:prod
    restart: unless-stopped
    environment:
      - DECISION_TOOL_ORIGIN=https://enavi.app
    networks:
      - proxy
    labels:
      traefik.enable: true
      traefik.http.routers.prod-cockpit.rule: Host(`cockpit.enavi.app`)

  klug-tool:
    image: $CI_REGISTRY_IMAGE/klug-tool:prod
    restart: unless-stopped
    networks:
      - proxy
    labels:
      traefik.enable: true
      traefik.http.routers.prod-klug-tool.rule: Host(`klugnavi.app`)
    volumes:
      - /srv/entscheidungsnavi/matomo-klug.js:/usr/share/nginx/html/mounts/matomo.js

  backend:
    image: $CI_REGISTRY_IMAGE/backend:prod
    restart: unless-stopped
    environment:
      - NODE_ENV=production
      - MONGODB_URI=mongodb://mongo:27017/navi
      - BASE_URL=https://enavi.app
      - SESSION_SECRET=$PROD_SESSION_SECRET
      - ENVIRONMENT_TYPE=production
      - SENTRY_DSN=$SENTRY_BE_DSN
      - MAIL_HOST=$PROD_MAIL_SERVER
      - MAIL_USER=$PROD_MAIL_USER
      - MAIL_PASSWORD=$PROD_MAIL_PASS
      - MAIL_FROM=$PROD_MAIL_FROM
    networks:
      - default
      - proxy
    depends_on:
      mongo:
        condition: service_healthy
    labels:
      traefik.enable: true
      # set correct network for traefik to use. required when the service has multiple networks.
      # https://github.com/traefik/traefik/issues/1156
      traefik.docker.network: proxy
      traefik.http.routers.prod-backend.rule: Host(`enavi.app`, `cockpit.enavi.app`, `klugnavi.app`) && PathPrefix(`/api`)

  mongo:
    image: mongo:5
    command: --replSet ra0 --bind_ip_all
    hostname: mongo
    healthcheck:
      # Initialize the replication set if that was not already done
      test: '[ $$(mongosh --eval "rs.status().ok" --quiet) -eq 1 ] || [ $$(mongosh --eval "rs.initiate().ok" --quiet) -eq 1 ]'
      interval: 10s
      start_period: 30s
    restart: unless-stopped
    volumes:
      - /var/lib/entscheidungsnavi-prod:/data/db

  mongo-express:
    image: mongo-express
    restart: unless-stopped
    depends_on:
      mongo:
        condition: service_healthy
    ports:
      - 127.0.0.1:28910:8081

networks:
  proxy:
    external: true
